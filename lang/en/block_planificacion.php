<?php

/**
 * Strings for component 'block_message', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_message
 * @copyright  2018 Great Wall Studio <brian@greatwallstudio.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['message:addinstance'] = 'Add a new message block';
$string['message:myaddinstance'] = 'Add a new message block to Dashboard';
$string['pluginname'] = 'planning';
$string['privacy:metadata'] = 'The Message block only shows static data and does not store data itself.';
$string['message:whateveryouwant'] = 'Custom message!';
$string['iraprobaciones'] = 'Ver mas';
$string['titulo'] = 'Planning';
$string['tit'] = 'Name';
$string['agregar'] = 'Add';
$string['editar'] = 'Edit';
$string['eliminar'] = 'Delete';
$string['tiempolibre'] = 'Free Time';
$string['cancelar'] = 'Cancel';
$string['copy'] = 'Copy';
$string['new'] = 'New';
$string['abreviatura'] = 'Abbreviation';
$string['color'] = 'Colour';
$string['grados'] = 'Grade';
$string['test'] = 'Test';
$string['cursos'] = 'Courses';
$string['grupos'] = 'Groups';
$string['centros'] = 'Center´s';
$string['aulas'] = 'Classroom';
$string['profesores'] = 'Teachers';
$string['nombre'] = 'Name';
$string['nomina'] = 'Payroll';
$string['apellidopat'] = 'First Name';
$string['apellidomat'] = 'Second Name';
$string['email'] = 'Email';
$string['telefono'] = 'Phone Number';
$string['tutorclase'] = 'Class Tutor';
$string['cambiar'] = 'Change';
$string['fechaIn'] = 'Start Date';
$string['fechaFin'] = 'Final Date';
$string['grado'] = 'Grade';
$string['choose'] = 'Select Opcion';
$string['horas'] = 'Hours';
$string['consistencia'] = 'Consistency';
$string['datosgenerales'] = 'General Date';
$string['capacidad'] = 'Capacity';
$string['info'] = 'Information';
$string['msg_modal'] = 'Option do available';
$string['close'] = 'Close';
$string['Restricciones'] = 'Restrictions';
$string['limitehuecos'] = 'Limit number of hollows in the Schedule ';
$string['maximahuecos'] = 'Maximum amount of hollows in';
$string['limitardias'] = 'Limit the number of days ';
$string['diasenque'] = 'Number of days in which the  ';
$string['notadiasenque'] = 'Note. Whit this limits the  number of days the teacher should teaching';
$string['leccionesxdia'] = 'Add mín./máx. of lessons for day ';
$string['numeroleccionesxdia'] = 'Number of lessons for day must be given in this interval';
$string['noctrlfinde'] = 'Do not control the minimum or maximum on weekends';
$string['leccionesconsecutivas'] = 'Limit the number of consecutive lessons';
$string['maxleccionesconsecutivas'] = 'Maximum number of lessons';
$string['notamaxleccionesconsecutivas'] = 'Note. Whit this limit the number of lessons the teacher can give without rest';
$string['agotamiento'] = 'Not controlling exhaustion on weekends';
$string['treshuecos'] = 'The teacher cannot have 3 hollows a day';
$string['doshuecos'] = 'The teacher cannot have 2 hollows a day';
$string['capacidadgrupos'] = 'Groups Capacity';
$string['lecciones'] = 'Lessons';
$string['leccion'] = 'Lesson';
$string['nombre_asignatura '] = 'Lessons Names';
$string['cantidad'] = 'Lessons Amount';
$string['duracion'] = 'Lessons Quantity';
$string['aceptar'] = 'Acept';
$string['curso'] = 'Course';
$string['grupo'] = 'Group';
$string['profesor'] = 'Teacher';
$string['cantidad'] = 'Count';
$string['duracion'] = 'Length';
$string['aula'] = 'Class room';
$string['semana'] = 'Week';
$string['periodo'] = 'Term';
$string['relaciones'] = 'Relationship';
$string['incluye'] = 'Applay to';
$string['descripcion'] = 'Description';
$string['recesos'] = 'Recesses';
$string['periodoName'] = 'Period';
$string['NombrePeriodo'] = 'Name of Period';
$string['IdentDRecreo'] = ' Recreation Identifier';
$string['UbicRecreo'] = 'Recreation Location';
$string['inicio'] = 'Start';
$string['fin'] = 'End';
$string['recreo'] = 'Recreation';
$string['TextPimp'] = 'Print Text';
$string['impresiones'] = 'Impressions';
$string['impPrh'] = 'Print this period in a summary of schedules';
$string['impPhidClases'] = 'Print this period for individual  class schedules';
$string['impPhiProfesores'] = ' Print this period in individual teacher schedules';
$string['impPhiD'] = 'Print this period in an individual schedule of';
$string['IMPTT'] = 'Print on all stamps';
$string['Timbre'] = 'Bell';
$string['asignaturas'] = 'Subjects';
$string['EAsig'] = 'Choose Subjects';
$string['Clas'] = ' Class';
$string['Todas'] = 'Every';
$string['Eleccion'] = 'Choice';
$string['CClases'] = ' Change Classes';
$string['ImpERelacion'] = 'Importance of this relationship';
$string['Desac'] = 'Desactivate';
$string['Nota'] = 'Note';
$string['Condicion'] = ' 3.Condition';
$string['Condicion1'] = ' They cannot occur the same day';
$string['Condicion2'] = ' They cannot be consecutive ';
$string['Condicion3'] = ' Distribution of tokens throughout the week';
$string['config'] = 'Configuration';
$string['Condicion4'] = ' Two subjects must be given in one day';
$string['Condicion5'] = ' Two subjects must be consecutive';
$string['OrEsp'] = 'By Specified Order';
$string['OrArb'] = 'By Arbitrary Order';
$string['CambOrden'] = ' Change Order';
$string['SFichasDivididas'] = 'Split Chips Only';
$string['Condicion6'] = 'Recess cannot be a group of lessons';
$string['Condicion7'] = 'Group of tokens of different classes must be given the same day';
$string['Condicion8'] = 'The divided tokens of the same subject must take the same day';
$string['Condicion9'] = 'The Subjects of the groups of the listed classes must start at the same time.';
$string['Condicion10'] = 'The selected subjects must be at the same time in all the selected classes';
$string['Condicion11'] = 'This subject must be in the same period each day';
$string['Condicion12'] = 'Reserve space for the selected subjects';
$string['Condicion13'] = 'The subject must be at the beginning or at the end';
$string['Condicion14'] = 'The selected subjects may be outside the teaching block';
$string['NCentro'] = 'Name Of Center';
$string['CEscolar'] = 'School cycle';
$string['CLD'] = 'Number of lessons for day';
$string['T/RPeriodo'] = 'Bell/Rename Periods';
$string['LC'] = 'Work with o Lessons';
$string['PCP0'] = 'Allows generator to place lessons in period 0';
$string['ND'] = 'Numbers of Days';
$string['R/Dias'] = 'Rename Days';
$string['FSemana'] = 'Weekend';
$string['MNDIA'] = 'Display the number instead of the Days name (eg Day 1 in)';
$string['MS/P'] = 'I want to create multi-period or multi-week schedules that may be different in each week or period.';
$string['escuela'] = 'School';
$string['renombrar'] = 'Rename';
$string['siguienteProfesor'] = 'Next teacher';
$string['asignatura'] = 'Subject';
$string['grupoUnido'] = 'United group';
$string['sesionSemana'] = 'Sessions / week ';
$string['masSemanas'] = 'More weeks';
$string['aulaFija'] = 'Fixed classroom';
$string['aulaProfesor'] = 'Teacher classrooms';
$string['aulaCompartida'] = 'Shared classrooms :';
$string['AulaAsignatura'] = 'Classrooms of the subject';
$string['otrasAulasPermitidas'] = 'Other permitted classrooms';
$string['masAulas'] = 'More classrooms';
$string['ayuda'] = 'Help';
$string['aulaAgregar'] = 'Add classroom';
$string['copy_to'] = 'Copy to';
$string['fecha_actual'] = 'Current date';
/********** DATOS NUEVOS DE DEFINIR PERIODOS */
$string['DPeriodos'] = 'Define Periods ';
$string['NotaPeriodo1'] = 'Select the number of periods, the number greater than 1 is useful
        for schools that have different hours throughout the year. So you can define
        each lesson, in which period  or periods can be';
$string['NumPeriodos'] = 'Periods :';
$string['NotaPeriodo2'] = 'Here you can rename Individual Periods using the Edit button or create new periods for the school year, if you have for example lessons that have to be in Per-1 and Per-3 or lessons that you have than being in Per-1 or Per-3 or Per-4:';
/********** DATOS NUEVOS DE DEFINIR SEMANAS */
$string['DfSemana'] = 'Define weeks';
$string['NotaSemana1'] = 'Select the number of weeks. This is useful for schools that have different times in odd or even weeks. The software can also choose which week of the lesson it can be placed in case you allow it to be in any week.';
$string['NumSemana'] = 'Weeks :';
$string['NotaSemana2'] = 'Here you can rename individual weeks.';
/********** DATOS NUEVOS DE RENOMBRAR */
$string['RDias'] = 'Rename Days :';
$string['NotaDias'] = 'Select the number of days if you have lesson  Monday through Friday, specify 5. If you also have classes on select Saturdays 6. If you have two different days as Day A / Day B then leave 5 days, but you must define new "days" using the continue button.';
$string['DiasRenombrados'] = ' Days : ';
$string['Nota2Dias'] = 'Here you can Rename days individually using the Edit button or create new days if you have, for example, lessons that must be in the same period on Monday, Wednesday  And Friday';
$string['Combinar'] = 'Combine';
$string['TituloRDias'] = 'Title';
/*****************DATOS NUEVOS DE TIMBRES */
$string['Tlddclas'] = 'We have different bell times in different classes';
$string['EHDTAD'] = 'This schedule has different timbres on some days';
$string['VaPara'] = 'Valid for ';
$string['EditTimRec'] = 'Edit Bell/Recess';
/********************OBJETO */
$string['Objeto'] = 'Object';
/****************PERIODO****************/
$string['P1'] = 'Period:0 - 7:10-7:55';
$string['NotaPeriodoP'] = 'Note : it is only necessary to change the time periods that are different from the default ones. If you leave the field empty then the default value shown under the field is used for this period';
$string['Lunes'] = 'Monday';
$string['Martes'] = 'Tuesday';
$string['Miercoles'] = 'Wednesday';
$string['Jueves'] = 'Thursday';
$string['Viernes'] = 'Friday';
$string['Sabado'] = 'Saturday';
$string['Domingo'] = 'Sunday';
/************Lecciones***********/
$string['IN/Fin'] = 'Start/End';
/****Eleccion********************/
$string['ClTselect'] = 'Click to Select';
$string['Eleccion'] = 'Choice';
$string['Todas'] = 'All';
$string['SelRpida'] = 'Quick Selection';
/****Combinacion */
$string['Combinacion'] = 'Combination';
$string['LabelCombinacion'] = 'Please select a type of combination';
$string['OP1CombinacionSemanas'] = 'The lesson can be placed in the same period and on the same day of all the selected weeks';
$string['OP2CombinacionSemanas'] = 'The lesson can be placed in one of the selected weeks.';
$string['OP1CombinacionPeriodos'] = 'The lesson must be placed in the same period and on the same day of all the selected periods';
$string['OP2CombinacionPeriodos'] = 'The lesson can be placed in one of the selected periods.';
$string['OP1CombinacionDias'] = 'Lesson can be placed in the same period on all selected days';
$string['OP2CombinacionDias'] = 'The lesson can be placed on one of the selected days.';
/*****TiempoLibre */
$string['CambiarTodas'] = 'Chage in alls';
$string['IgualPTPeriodos'] = 'Distinct for all periods';
$string['IgualPTSemanas'] = 'Distinct for every week';

$string['agregarTimbre'] = 'Add Bell';
$string['usar'] = 'Used';

$string['vistaprevia'] = 'Preview';
$string['nombreHorario'] = 'Schedule name';
$string['development'] = 'Development';
/***********Generar Horario Nuevo */
$string['gnuevohorario'] = 'Generar New Schedule';
$string['ghorario'] = 'Generate the Schedule ';
$string['cdn'] = 'Terms';
$string['Prueba'] = 'Test';
$string['PRC'] = 'Permit reduction of conditions';
$string['Estricto'] = 'Stric';
$string['CHorario'] = 'Schedule complexity';
$string['Normal'] = 'Normal';
$string['Grande'] = 'Big';
$string['MuyGrande'] = 'Biggest';
$string['Psdg'] = 'Please, Select what you want to generate';
/*****tabla principal */
$string['tablap'] = 'Schedule';
$string['tprincipal'] = 'Schedule view';
$string['selectBy'] = 'Select by';
$string['todo'] = 'All';
/*****Generacion Terminada */
$string['EGT'] = 'Exit - Generation Completed';
$string['Tiempo'] = 'Time';
$string['HorComp'] = 'Checked Schedules';
$string['EPCM'] = 'The most complicated Professor';
$string['LCMCP'] = 'The most complicated Class';
$string['NCV'] = 'Number of Conditions Violated';
$string['NFNS'] = 'Number of files not placed';
/***** Condiciones Relajadas*/
$string['CR'] = 'Relaxed Conditions';
$string['LTCR'] = 'The generation was carried out successfully, however it was necessary to relax certain conditions.';
$string['MCR'] = 'Show me which ones were relaxed';
/************** Distribucion */
$string['DCALSTD'] = 'Distribution of each subject on the list separately';
$string['LFCDOT'] = 'The Files can  be placed on two or three consecutive days.';
$string['LFCNDOT'] = 'The Files can´t be placed on two or three consecutive days.';
$string['SDC'] = 'If there are more lessons on the same day, they must be consecutive';
$string['ENDYNLPDF'] = 'Specify the number of days and the number of lessons per day for the files distribution';
$string['DFHND'] = 'Distribute Files up to this number of days';
$string['Desde'] = 'Since :';
$string['Hasta'] = 'Until :';
$string['NLDI'] = 'Number of lessons per day should be given that interval :';
/****** Asesor */
$string['Asesor'] = 'Consultant';
$string['PC'] = 'Critical Problems';
$string['RVPC'] = 'The Advisor does not see any critical issues in their schedule at this time';
$string['RVPCV'] = 'To generate your schedule, these points need to be checked';
$string['SPB'] = 'These points are probably ok, but please consult the administrator';
$string['Sugerecia'] = 'Suggestion';
/*****  Comprobacion del Horario*/
$string['CH'] = 'Checking the Schedule';
$string['Test'] = 'Schedule Test';
$string['General'] = 'General';
$string['Estudiantes'] = 'Students';
$string['Aviso'] = 'Before checking the schedule, the program deletes all the non-closed tabs. Do you wish to continue?';
$string['AvisoTitle'] = 'Note';
/********Modificaciones */
$string['Primero'] = 'First';
$string['Ultimo'] = 'Last';
$string['PYU'] = 'First or Last';
$string['nleccion'] = 'Num of Less';

/*******Eliminar Horario  */
$string['BH'] = 'Delete Schedule';
$string['BH2'] = 'Delete - Move to the no-suit :';
$string['FA'] = 'Files Adjust';
$string['FAYNA'] = 'Files Adjust and Not-Ajusted' ;



