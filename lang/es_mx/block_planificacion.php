<?php

/**
 * Strings for component 'block_message', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_message
 * @copyright  2018 Great Wall Studio <brian@greatwallstudio.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['message:addinstance'] = 'Add a new message block';
$string['message:myaddinstance'] = 'Add a new message block to Dashboard';
$string['pluginname'] = 'Planificacion';
$string['privacy:metadata'] = 'The Message block only shows static data and does not store data itself.';
$string['message:whateveryouwant'] = 'Custom message!';
$string['iraprobaciones'] = 'Ver mas';
$string['titulo'] = 'Planificación';
$string['tit'] = 'Nombre';
$string['agregar'] = 'Agregar';
$string['editar'] = 'Editar';
$string['eliminar'] = 'Eliminar';
$string['tiempolibre'] = 'Tiempo Libre';
$string['cancelar'] = 'Cancelar';
$string['copy'] = 'Copiar';
$string['new'] = 'Nueva';
$string['abreviatura'] = 'Abreviatura';
$string['color'] = 'Color';
$string['test'] = 'Prueba';
$string['cursos'] = 'Cursos';
$string['grupos'] = 'Grupos';
$string['centros'] = 'Centros';
$string['aulas'] = 'Aulas';
$string['profesores'] = 'Profesores';
$string['grados'] = 'Grados';
$string['nomina'] = 'Nomina';
$string['nombre'] = 'Nombre';
$string['apellidopat'] = 'Apellido Paterno';
$string['apellidomat'] = 'Apellido Materno';
$string['email'] = 'Correo';
$string['telefono'] = 'Teléfono';
$string['tutorclase'] = 'Tutor Clase';
$string['cambiar'] = 'Cambiar';
$string['fechaIn'] = 'Fecha Inicial';
$string['fechaFin'] = 'Fecha Final';
$string['grado'] = 'Grado';
$string['choose'] = 'Seleccionar opción';
$string['horas'] = 'Horas';
$string['consistencia'] = 'Consistencia';
$string['datosgenerales'] = 'Datos Generales';
$string['capacidad'] = 'Capacidad';
$string['info'] = 'Información';
$string['msg_modal'] = 'Opción no disponible';
$string['close'] = 'Cerrar';
$string['Restricciones'] = 'Restricciones';
$string['limitehuecos'] = 'Limitar numero de huecos en el horario';
$string['maximahuecos'] = 'Cantidad máxima de los huecos en ';
$string['limitardias'] = 'Limitar el numero de los días ';
$string['diasenque'] = 'Numero de días en los que el  ';
$string['notadiasenque'] = 'Nota. Con esto limita ud. la cantidad de días en los que debería enseñar el profesor';
$string['leccionesxdia'] = 'Poner mín./máx. de lecciones por día ';
$string['numeroleccionesxdia'] = 'Número de lecciones por día debe darse en este intervalo';
$string['noctrlfinde'] = 'No controle el mínimo ni el máximo los fines de semana';
$string['leccionesconsecutivas'] = 'Limitar el número de lecciones consecutivas';
$string['maxleccionesconsecutivas'] = 'Número máximo de las lecciones';
$string['notamaxleccionesconsecutivas'] = 'Nota. Con este parametro limita Ud. el numero de las lecciones que puede dar el profesor sin descanso';
$string['agotamiento'] = 'No controlar el agotamiento los fines de semana';
$string['treshuecos'] = 'El profesor no puede tener 3 huecos al día';
$string['doshuecos'] = 'El profesor no puede tener 2 huecos al día';
$string['capacidadgrupos'] = 'Capacidad de Grupos';
$string['lecciones'] = 'Lecciones';
$string['leccion'] = 'Lección';
$string['nombre_asignatura'] = 'Nombre  Asignatura';
$string['cantidad'] = 'Cantidad de Lecciones';
$string['duracion'] = 'Duracion de Lección';
$string['aceptar'] = 'Aceptar';
$string['curso'] = 'Curso';
$string['grupo'] = 'Grupo';
$string['profesor'] = 'Profesor';
$string['cantidad'] = 'Cantidad';
$string['duracion'] = 'Duración';
$string['aula'] = 'Aula';
$string['semana'] = 'Semana';
$string['periodo'] = 'Periodo';
$string['relaciones'] = 'Relaciones';
$string['incluye'] = 'Vale para';
$string['descripcion'] = 'Descripción';
$string['recesos'] = 'Recesos';
$string['periodoName'] = 'Periodo';
$string['NombrePeriodo'] = 'Nombre del Periodo';
$string['IdentDRecreo'] = 'Identificador del Recreo';
$string['UbicRecreo'] = 'Ubicacion del Recreo';
$string['inicio'] = 'Inicio';
$string['fin'] = 'Fin';
$string['recreo'] = 'Recreo';
$string['TextPimp'] = 'Texto Para Impresión';
$string['impresiones'] = 'Impresiones';
$string['impPrh'] = 'Imprimir este periodo en un resumen de horarios';
$string['impPhidClases'] = 'Imprimir este periodo para horarios individuales de clases';
$string['impPhiProfesores'] = ' Imprimir este periodo en horarios individuales de profesores';
$string['impPhiD'] = 'Imprimir este periodo en un horario individual de';
$string['IMPTT'] = 'Imprimir en todos los timbres';
$string['Timbre'] = 'Timbre';
$string['asignaturas'] = 'Asignaturas';
$string['EAsig'] = 'Elegir Asignaturas';
$string['Clas'] = 'Clases';
$string['Todas'] = 'Todas';
$string['Eleccion'] = 'Elección';
$string['CClases'] = 'Cambiar clases';
$string['ImpERelacion'] = 'Importancia de esta relación';
$string['Desac'] = 'Desactivarlo';
$string['Nota'] = 'Nota';
$string['Condicion'] = 'Condición';
$string['Condicion1'] = 'No pueden darse el mismo día';
$string['Condicion2'] = 'No pueden ser consecutivas';
$string['Condicion3'] = 'Distribución de fichas a lo largo de la semana';
$string['config'] = 'Configuración';
$string['Condicion4'] = 'Dos asignaturas deben de darse en un día';
$string['Condicion5'] = 'Dos asignaturas deben de ser consecutivas';
$string['OrEsp'] = 'Por orden especificado';
$string['OrArb'] = 'Por orden arbitrario';
$string['CambOrden'] = 'Cambiar el orden';
$string['SFichasDivididas'] = 'Sólo fichas divididas';
$string['Condicion6'] = 'El recreo no puede estar un grupo de lecciones';
$string['Condicion7'] = 'Grupo de fichas de diferentes clases deben darse el mismo dia';
$string['Condicion8'] = 'Las fichas divididas de la misma asignatura deben realizarse el mismo día';
$string['Condicion9'] = 'Las asignaturas de los grupos de las clases listadas deben empezar al mismo tiempo.';
$string['Condicion10'] = 'Las asignaturas seleccionadas deben de estar a la misma hora en todas las clases seleccionadas';
$string['Condicion11'] = 'Esta asignatura debe de estar en el mismo periodo cada dia';
$string['Condicion12'] = 'Reservar espacio para las asignaturas seleccionadas';
$string['Condicion13'] = 'La asignatura deber estar al inicio o al final';
$string['Condicion14'] = 'Las asignaturas seleccionadas pueden estar fuera del bloque didactico';
$string['NCentro'] = 'Nombre del Centro';
$string['CEscolar'] = 'Ciclo Escolar';
$string['CLD'] = 'Cantidad de lecciones por día';
$string['T/RPeriodo'] = 'Timbre/Renombrar Periodos';
$string['LC'] = 'Trabajo con lecciones cero';
$string['PCP0'] = 'Permite que el generador coloque lecciones en el periodo 0';
$string['ND'] = 'Numero de Días';
$string['R/Dias'] = 'Renombrar Días';
$string['FSemana'] = 'Fin de Semana';
$string['MNDIA'] = 'Mostrar el numero en vez del nombre del Dia (p.ej. Dia 1 en)';
$string['MS/P'] = 'Yo quiero crear horarios multi periodos o multi semanas que puedan ser diferentes en cada semana o periodo.';
$string['escuela'] = 'Escuela';
$string['renombrar'] = 'Renombrar';
$string['siguienteProfesor'] = 'Siguiente profesor';
$string['asignatura'] = 'Asignatura';
$string['grupoUnido'] = 'Grupo unido';
$string['sesionSemana'] = 'Sesiones/semana';
$string['masSemanas'] = 'Mas semanas';
$string['aulaFija'] = 'Aula Fija  ';
$string['aulaProfesor'] = 'Aulas del Profesor';
$string['aulaCompartida'] = 'Aulas compartidas :';
$string['AulaAsignatura'] = 'Aulas de la asignatura';
$string['otrasAulasPermitidas'] = 'Otras Aulas Permitidas';
$string['masAulas'] = 'Más Aulas';
$string['ayuda'] = 'Ayuda';
$string['aulaAgregar'] = 'Agregar aula';
$string['copy_to'] = 'Copiar a';
$string['fecha_actual'] = 'Fecha actual';
/********** DATOS NUEVOS DE DEFINIR PERIODOS */
$string['DPeriodos'] = 'Definir Periodos';
$string['NotaPeriodo1'] = 'Selecciona el número de períodos.El número mayor que 1 es útil para escuelas que tienen diferentes horarios durante el año. Entoces Usted puede definir cada lección , en cual periodo o períodos o periodos puede estar';
$string['NumPeriodos'] = 'Períodos :';
$string['NotaPeriodo2'] = 'Aquí puede renombrar Periodos Individuales utilizando el botón Editar o crear nuevos períodos para el año escolar, si usted tieme por ejemplo lecciones que tiene que estar en Per-1 y Per-3 o lecciones que tiene que estar en Per-1 o Per-3 o Per-4 : ';
/********** DATOS NUEVOS DE DEFINIR SEMANAS */
$string['DfSemana'] = 'Definir Semanas';
$string['NotaSemana1'] = 'Selecciona el número de semanas.Esto es útil para escuelas que tienen diferentes horarios en semanas pares o nones. El software puede también elegir en cual semana de la lección puede colocarse en caso de que Usted permita que pueda estar en cualquier semana.';
$string['NumSemana'] = 'Semanas :';
$string['NotaSemana2'] = 'Aqui puede Usted renombrar semanas individuales.';
/********** DATOS NUEVOS DE RENOMBRAR */
$string['RDias'] = ' Renombrar Dias';
$string['NotaDias'] = 'Seleccione el número de dias . Si Usted tiene lecciones de lunes a viernes , especifique 5 . Si usted también tiene clases los sábados seleccione 6 . Si usted tiene dos días diferentes como Dia A/Día B entonces deje 5 días , pero debe definir nuevos "días" utilizando el botón Combinar.';
$string['DiasRenombrados'] = ' Dias : ';
$string['Nota2Dias'] = 'Aquí Usted puede Renombrar días individualmente utilizando el botón Editar o crear nuevos "días" si Usted tiene por ejemplo lecciones que tienen que estar en el mismo periodo en Lunes , Miércoles   Y Viernes ';
$string['Combinar'] = 'Combinar';
$string['TituloRDias'] = 'Titulo';
/*****************DATOS NUEVOS DE TIMBRES */
$string['Tlddclas'] = 'Tenemos tiempos de timbres diferentes en diferentes clases';
$string['EHDTAD'] = 'Este horario tiene diferentes timbres en algunos días';
$string['VaPara'] = 'Valido para ';
$string['EditTimRec'] = 'Editar Timbre/Receso';
/********************OBJETO */
$string['Objeto'] = 'Objeto';
/****************PERIODO****************/
$string['P1'] = 'Período:0 - 7:10-7:55';
$string['NotaPeriodoP'] = 'Nota : sólo es necesario cambiar los períodos de tiempo que son diferentes de los predeterminados . Si deja el campo vacío , entonces el valor predeterminado que se muestra bajo el campo se utiliza para este período';
$string['Lunes'] = 'Lunes';
$string['Martes'] = 'Martes';
$string['Miercoles'] = 'Miércoles';
$string['Jueves'] = 'Jueves';
$string['Viernes'] = 'Viernes';
$string['Sabado'] = 'Sabado';
$string['Domingo'] = 'Domingo';
/************Lecciones***********/
$string['IN/Fin'] = 'Inicio/Fin';
/****Eleccion********************/
$string['ClTselect'] = 'Click para seleccionar';
$string['Eleccion'] = 'Elección';
$string['Todas'] = 'Todas';
$string['SelRpida'] = 'Selección Rápida';
/****Combinacion */
$string['Combinacion'] = 'Combinacion';
$string['LabelCombinacion'] = 'Por favor seleccione un tipo de combinación';
$string['OP1CombinacionSemanas'] = 'La lección se puede colocar en el mismo período y en el mismo día de todas las semanas seleccionadas';
$string['OP2CombinacionSemanas'] = 'La lección puede colocarse en una de las semanas seleccionadas.';
$string['OP1CombinacionPeriodos'] = 'La lección debe ser colocada en el mismo período y en el mismo día de todos los periodos seleccionados';
$string['OP2CombinacionPeriodos'] = 'La lección se puede colocar en una de los períodos seleccionados.';
$string['OP1CombinacionDias'] = 'La lección puede colocarse en el mismo período en todos los días seleccionados';
$string['OP2CombinacionDias'] = 'La lección puede colocarse en uno de los días seleccionados.';
/*****TiempoLibre */
$string['CambiarTodas'] = 'Cambiar en todas';
$string['IgualPTPeriodos'] = 'Diferente para todos los periodos';
$string['IgualPTSemanas'] = 'Diferente para todas las semanas ';
/******* */
$string['agregarTimbre'] = 'Agregar Timbres';
$string['usar'] = 'Usar';

$string['vistaprevia'] = 'Vista Previa';
$string['nombreHorario'] = 'Nombre del horario';
$string['development'] = 'Desarrollo';
/*********Generar Nuevo Horario */
$string['gnuevohorario'] = 'Generar Nuevo Horario';
$string['ghorario'] = 'Generar el Horario';
$string['cdn'] = 'Condiciones';
$string['Prueba'] = 'Prueba';
$string['PRC'] = 'Permitir reducción de condiciones';
$string['Estricto'] = 'Estricto';
$string['CHorario'] = 'Complejidad del horario';
$string['Normal'] = 'Normal';
$string['Grande'] = 'Grande';
$string['MuyGrande'] = 'Muy Grande';
$string['Psdg'] = 'Por favor, Seleccione que es lo que desea generar';
/*****tabla principal */
$string['tablap'] = 'Schedule';
$string['tprincipal'] = 'Visualizar horario';
$string['selectBy'] = 'Seleccionar por';
$string['todo'] = 'Todo';
/*****Generacion Terminada */
$string['EGT'] = 'Exito - Generación Terminada';
/***** Condiciones Relajadas listo */
$string['CR'] = 'Condiciones Relajadas';
$string['LTCR'] = 'La generación fue realizada con éxito, sin embargo fue necesario relajar ciertas condiciones.';
$string['MCR'] = 'Muéstrame cuáles fueron relajadas';
/************** listo*/
$string['DCALSTD'] = 'Distribución de cada asignatura de la lista separadamente';
$string['LFCDOT'] = 'Las Fichas se pueden colocar en dos o tres días consecutivos.';
$string['LFCNDOT'] = 'Las Fichas no se pueden colocar en dos o tres días consecutivos.';
$string['SDC'] = 'Si hay más lecciones el mismo día, éstas deben ser consecutivas';
$string['ENDYNLPDF'] = 'Especique el número de días y el número de lecciones por día de la distribución de fichas';
$string['DFHND'] = 'Distribuir Fichas hasta este número de dias';
$string['Desde'] = 'Desde :';
$string['Hasta'] = 'Hasta :';
$string['NLDI'] = 'Número de lecciones por día debe darse ese intervalo :';
/****** Asesor */
$string['Asesor'] = 'Asesor';
$string['PC'] = 'Problemas Criticos';
$string['RVPC'] = 'El Asesor no ve ningún problema critíco en su horario en este momento';
$string['RVPCV'] = 'Para generar su horario , estos puntos necesitan ser revisados';
$string['SPB'] = 'Estos puntos están probablemente bien, pero por favor consulte al administrador';
$string['Sugerecia'] = 'Sugerencia';
/*****  Comprobacion del Horario*/
$string['CH'] = 'Comprobacion del Horario';
$string['Test'] = 'Test del Horario';
$string['General'] = 'General';
$string['Estudiantes'] = 'Estudiantes';
/***AVIS */
$string['Aviso'] = 'Antes de la comprobación del horario el programa borra todas las fichas no cerradas. ¿Desea Continuar?';
$string['AvisoTitle'] = 'Aviso';
/*********Modificaciones Nuevas* */

$string['Primero'] = 'Primero ';
$string['Ultimo'] = 'Ultimo';
$string['PYU'] = 'Primero Y Ultimo';
$string['nleccion'] = 'N° Lección';


/*******Eliminar Horario  */
$string['BH'] = 'Borrar Horario';
$string['BH2'] = 'Borrar - Trasladar a las fichas no bloqueadas :';
$string['FA'] = 'Fichas No Bloqueadas';
$string['FAYNA'] = 'Fichas Bloqueadas  Y No Bloqueadas' ;














