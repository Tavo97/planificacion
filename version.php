<?php

/**
 * Version details
 *
 * @package    block_message
 * @copyright  2018 Great Wall Studio <brian@greatwallstudio.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2021071304;         // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2018050800;         // Requires this Moodle version
$plugin->component = 'block_planificacion'; // Full name of the plugin (used for diagnostics)
//$plugin->dependencies = array('mod_forum' => 2018050800);
