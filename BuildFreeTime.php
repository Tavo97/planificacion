<?php
class BuildFreeTime
{
    public function __construct()
    {
    }

    public function getFreeTimeList($main, $levels, $levelsId, $sqlArray)
    {
        $response = $this->mainFreeTimeList($main, $sqlArray);
        if ($levels != false && $levelsId != false) {
            if (count($levelsId) > 0) {
                // numero de periodos existentes
                foreach ($levelsId as $keyP => $lapse) {
                    $levelsFreeTime = [];
                    $status = (object) array('Green' => 0, 'Red' => 0, 'Blue' => 0);
                    // recorrer todos los tiempos libres verificando los que deben cambiar
                    foreach ($main as $keyFT => $freeTime) {
                        if (array_key_exists($freeTime->id, $levels)) {
                            $new = array();
                            $new = clone $freeTime;
                            if (array_key_exists('multiple', $levels[$freeTime->id])) {
                                foreach ($levels[$freeTime->id]->multiple as $key => $individual) {
                                    if ($individual->{$sqlArray->levelsField} == $lapse->{$sqlArray->levelsField}) {
                                        $new->id_estado_tl = $individual->id_estado_tl;
                                    }
                                }
                            } else {
                                if ($levels[$freeTime->id]->{$sqlArray->levelsField} == $lapse->{$sqlArray->levelsField}) {
                                    $new->id_estado_tl = $levels[$freeTime->id]->id_estado_tl;
                                }
                            }
                            array_push($levelsFreeTime, $new);
                        } else {
                            array_push($levelsFreeTime, $freeTime);
                        }
                        $endAux = end($levelsFreeTime);
                        $status = $this->evaluateStatus($status, $endAux);
                    }
                    $response[$lapse->{$sqlArray->levelsField}]->freTime = $levelsFreeTime;
                    $response[$lapse->{$sqlArray->levelsField}]->Status = $status;
                    $response[$lapse->{$sqlArray->levelsField}]->dataSettings = $sqlArray;
                }
            } else {
                array_push($response, ['Error' => 'msg_info_db_no_data']);
            }
        }
        return $response;
    }

    public function mainFreeTimeList($main, $sqlArray)
    {
        $response = [];
        $mainFreeTime = [];
        if (count($main) > 0) {
            $status = (object) array('Green' => 0, 'Red' => 0, 'Blue' => 0);
            foreach ($main as $keyFT => $freeTime) {
                array_push($mainFreeTime, $freeTime);
                $endAux = end($mainFreeTime);
                $status = $this->evaluateStatus($status, $endAux);
            }
            $response['base']->freTime = $mainFreeTime;
            $response['base']->Status = $status;
            $response['base']->dataSettings = $sqlArray;
        } else {
            $response = ['Error' => 'msg_info_db_no_data'];
        }
        return $response;
    }

    public function evaluateStatus($statusArray, $status)
    {
        if ($status->id_estado_tl == 1) {
            $statusArray->Green++;
        }
        if ($status->id_estado_tl == 2) {
            $statusArray->Red++;
        }
        if ($status->id_estado_tl == 3) {
            $statusArray->Blue++;
        }
        return $statusArray;
    }

    public function makeSqlLine($element, $horario, $item, $multiple, $periodo, $semana)
    {
        $arraySql = (object) [];
        $arrayData = (object)[];
        switch ($element) {
            case 'profesores':
                $arrayData->field = 'id_profesor';
                $arrayData = $this->valuateTableNamesLevel($multiple, $periodo, $semana, $arrayData);
                break;

            case 'cursos':
                $arrayData->field = 'id_curso';
                $arrayData = $this->valuateTableNamesLevel($multiple, $periodo, $semana, $arrayData);
                break;

            case 'clases':
                $arrayData->field = 'id_grupo';
                $arrayData = $this->valuateTableNamesLevel($multiple, $periodo, $semana, $arrayData);
                break;

            case 'aulas':
                $arrayData->field = 'id_aula';
                $arrayData = $this->valuateTableNamesLevel($multiple, $periodo, $semana, $arrayData);
                break;

            default:
                return false;
                break;
        }
        $element = $element == 'clases' ? 'grupos' : $element;
        $arraySql->main = "SELECT * FROM {blm_tiempo_libre_" . $element . "} WHERE id_horario = $horario AND " . $arrayData->field . " = $item;";
        if ($arrayData->levels != false) {
            $arraySql->levels = "SELECT * FROM {" . $arrayData->levels . "} WHERE id_horario = $horario AND " . $arrayData->field . " = $item;";
            $arraySql->levelsId = "SELECT " . $arrayData->levelsField . " FROM {" . $arrayData->levels . "} WHERE id_horario = $horario AND " . $arrayData->field . " = $item;";
        }
        return [$arraySql, $arrayData];
    }

    public function valuateTableNamesLevel($multiple, $periodo, $semana, $arrayData)
    {
        if ($multiple == 1) {
            $item = explode('_', $arrayData->field)[1];
            $item = $item == 'grupo' ? 'clase' : $item;
            if ($periodo == 1) {
                $arrayData->levels = 'blm_vista_tl_' . $item . '_periodo';
                $arrayData->levelsField = 'id_periodo';
            }
            if ($semana == 1) {
                $arrayData->levels = 'blm_vista_tl_' . $item . '_semana';
                $arrayData->levelsField = 'id_semana';
            }
            if ($periodo == 1 && $semana == 1) {
                $arrayData->levels = 'blm_vista_tl_' . $item . '_periodo_semana';
                $arrayData->levelsField = 'id_periodo_semana';
            }
        } else {
            $arrayData->levels = false;
        }
        return $arrayData;
    }
}
