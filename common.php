<?php
class Common
{
    public function __construct()
    {
    }

    public function elementHtml($number, $kind, $subfix = "")
    {
        $options = "";
        for ($i = 1; $i <= $number; $i++) {
            $options .= '<' . $kind . ' value="' . $i . '">' . $i . $subfix . '</' . $kind . '>';
        }
        return $options;
    }

    public function menuStart($data)
    {
        $groupButtons = '';
        foreach ($data->buttons as $key => $value) {
            $groupButtons .= '<i id="btn' . $data->prefix . $key . '" name="btn' . $key . '" title="' . $value[2] . '" class="btn-' . $value[0] . ' icon-custome-button ' . $value[1] . '"></i>';
        }

        $groupMainMenu = '';
        foreach ($data->mainMenu as $key => $value) {
            $groupMainMenu .= '<i id="mainMenu' . $key . '" data-title="' . $key . '" class="icon-custome-menu ' . $value[0] . '" title="' . $value[1] . '"></i>';
        }

        $tableStart = $this->buildTable($data);
        $cardStart = '<div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                            <div class="card-header d-flex flex-row justify-content-between" > <span id="tabla' . $data->prefix . '">' . $data->title . ' </span><span id="titleModule' . $data->prefix . '"></span></div>
                                <div class="card-body p-1 d-flex flex-row justify-content-between">
                                    <div id="mainMenu' . $data->prefix . '" class="col-1 flex-fill bd-highlight d-flex flex-column justify-content-between align-items-center">
                                    ' . $groupMainMenu . '
                                    </div>
                                    <div class="col-10 flex-fill bd-highlight content-table">' . $tableStart . '</div>
                                    <div class="col-1 flex-fill bd-highlight d-flex flex-column justify-content-start align-items-center">' . $groupButtons . '</div>
                                </div>
                            </div>
                        </div>
                    </div>';
        return $cardStart;
    }

    public function modal($data)
    {
        $groupButtonsLabel = '';
        foreach ($data->buttons as $key => $value) {
            $groupButtonsLabel .= '<div class="text-nowrap col-auto flex-fill"><label title="' . $key . '" for="btnModal' . $data->prefix . $key . '" id="labelBtnModal' . $data->prefix . $key . '" name="labelBtnModal' . $data->prefix . $key . '" class="m-1 text-truncate btn btn-' . $value[0] . '"' . $value[2] . '>' . $value[1] . '</label></div>';
        }
        $body = $this->modalBodyContent($data);
        $modal = '<div class="modal fade bd-example-modal-lg" id="Modal' . $data->prefix . '" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel' . $data->prefix . '" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Modal-title-' . $data->prefix . '" name="Modal-title-' . $data->prefix . '" data-title="' . $data->title . '">' . $data->title . '</h5>
                                    <span id="data' . $data->prefix . '" name="data' . $data->prefix . '"></span>
                                </div>
                                <div class="modal-body" id="modal-body-' . $data->prefix . '" >
                                    ' . $body . '
                                </div>
                                <div class="modal-footer" id="Modal-fotter-' . $data->prefix . '" name="Modal-fotter-' . $data->prefix . '">
                                    <div class="d-flex flex-row justify-content-between flex-wrap col-12 contenedorBoton">
                                    ' . $groupButtonsLabel . '
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
        return $modal;
    }

    public function buildTable($data, $data2 = '')
    {
        $headTable = '';
        foreach ($data->table as $key => $value) {
            $headTable .= '<th id="table' . $data->prefix . $value[0] . '" class="nowrap" scope="col">' . $value[1] . '</th>';
        }
        $table = '<table id="table' . $data->prefix . $data2 . '" class="table table-sm">
                        <thead class="thead-dark">
                            <tr id="tableTitle' . $data->prefix . $data2 . '">' . $headTable . '</tr>
                        </thead>
                        <tbody id="tableBody' . $data->prefix . $data2 . '">
                        </tbody>
                    </table>';
        return $table;
    }

    public function loaderAnimation($data)
    {
        $loader = '<div class="containerLoader" style="display: none;">
                        <div class="working">
                            <svg class="loader" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 340 340">
                                <circle cx="170" cy="170" r="160" stroke="' . $data->color1 . '"/>
                                <circle cx="170" cy="170" r="85" stroke="' . $data->color2 . '"/>
                            </svg>
                        </div>
                        <div class="labelLoader">
                            ' . $data->label . '
                        </div>
                    </div>';
        return $loader;
    }

    public function modalBodyContent($data)
    {
        switch ($data->body) {
            case 'mainForm':
                $groupButtons = '';
                foreach ($data->buttons as $key => $value) {
                    $groupButtons .= '<button id="btnModal' . $data->prefix . $key . '" name="btnModal' . $data->prefix . $key . '" type="button" class="m-1 btn btn-' . $value . '" title="' . $key . '">' . $key . '</button>';
                }
                $content = '
                        <form id="gestionFrm' . $data->prefix . '" name="gestionFrm' . $data->prefix . '" action="#" method="post" enctype="text/plain">
                            <div id="generalSegFrm' . $data->prefix . '" class="d-flex flex-row flex-wrap">
                                <h5 class="col-12 mb-4">' . $data->labelElements['grpDatosGenerales'] . '</h5>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="idChooseFrm' . $data->prefix . '">ID</label>
                                    <select class="form-control flex-fill" id="idChooseFrm' . $data->prefix . '" name="idChooseFrm' . $data->prefix . '">
                                        <option selected>Choose...</option>
                                    </select>
                                </div>
                                <div id="selectFrmProfesores" class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="selectProfesores">Nomina</label>
                                    <select class="js-example-basic-single form-control flex-fill" id="selectProfesores" name="selectProfesores">
                                    </select>
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="nombreFrm' . $data->prefix . '">' . $data->labelElements['nombre'] . '</label>
                                    <input  type="text" class="form-control flex-fill" id="nombreFrm' . $data->prefix . '" name="nombreFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="apellidoPatFrm' . $data->prefix . '">' . $data->labelElements['apellidoPat'] . '</label>
                                    <input   type="text" class="form-control flex-fill" id="apellidoPatFrm' . $data->prefix . '" name="apellidoPatFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="apellidoMatFrm' . $data->prefix . '">' . $data->labelElements['apellidoMat'] . '</label>
                                    <input  type="text" class="form-control flex-fill" id="apellidoMatFrm' . $data->prefix . '" name="apellidoMatFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="abreviaturaFrm' . $data->prefix . '">' . $data->labelElements['abreviatura'] . '</label>
                                    <input  type="text" class="form-control flex-fill" id="abreviaturaFrm' . $data->prefix . '" name="abreviaturaFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="emailFrm' . $data->prefix . '">' . $data->labelElements['email'] . '</label>
                                    <input  type="text" id="emailFrm' . $data->prefix . '" name="emailFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="telefonoFrm' . $data->prefix . '">' . $data->labelElements['telefono'] . '</label>
                                    <input type="text" id="telefonoFrm' . $data->prefix . '" name="telefonoFrm' . $data->prefix . '">
                                </div>
                            </div>
                            <div id="colorSegFrm' . $data->prefix . '" class="d-flex flex-row flex-wrap">
                                <h5 class="col-12 mb-4">' . $data->labelElements['grpColor'] . '</h5>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                    <label class="flex-fill" for="colorFrm' . $data->prefix . '">' . $data->labelElements['color'] . '</label>
                                    <input class="flex-fill" type="color" id="colorFrm' . $data->prefix . '" name="colorFrm' . $data->prefix . '">
                                </div>
                            </div>
                            <div id="claseSegFrm' . $data->prefix . '" class="d-flex flex-row flex-wrap">
                                <h5 class="col-12 mb-4">' . $data->labelElements['grpClase'] . '</h5>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="tutorFrm' . $data->prefix . '">' . $data->labelElements['tutor'] . '</label>
                                    <input class="flex-fill" type="submit" value="' . $data->labelElements['tutorBtn'] . '" id="tutorFrm' . $data->prefix . '" name="tutorFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="fechaInFrm' . $data->prefix . '">' . $data->labelElements['fechaIn'] . '</label>
                                    <input class="flex-fill" type="date" id="fechaInFrm' . $data->prefix . '" name="fechaInFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="fechaFinFrm' . $data->prefix . '">' . $data->labelElements['fechaFin'] . '</label>
                                    <input class="flex-fill" type="date" id="fechaFinFrm' . $data->prefix . '" name="fechaFinFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="gradoFrm' . $data->prefix . '">' . $data->labelElements['grado'] . '</label>
                                    <select class="custom-select flex-fill" id="gradoFrm' . $data->prefix . '" name="gradoFrm' . $data->prefix . '">
                                        <option selected>' . $data->labelElements['choose'] . '</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </div>
                            <div id="aulaSegFrm' . $data->prefix . '" class="d-flex flex-row flex-wrap">
                                <h5 class="col-12 mb-4">' . $data->labelElements['grpAulas'] . '</h5>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="capacidadFrm' . $data->prefix . '">' . $data->labelElements['capacidad'] . '</label>
                                    <input class="flex-fill" type="number" id="capacidadFrm' . $data->prefix . '" name="capacidadFrm' . $data->prefix . '">
                                </div>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <input class="flex-fill btn bg-dark" type="button" id="addClassroom' . $data->prefix . '" value="' . $data->labelElements['addClassroom'] . '">
                                </div>
                            </div>
                            <div id="centroSegFrm' . $data->prefix . '" class="d-flex flex-row flex-wrap">
                                <h5 class="col-12 mb-4">' . $data->labelElements['grpCentros'] . '</h5>
                                <div class="form-group d-flex flex-row justify-content-between col-sm-6 flex-wrap">
                                    <label class="flex-fill" for="horasFrm' . $data->prefix . '">' . $data->labelElements['horas'] . '</label>
                                    <input class="flex-fill" type="text" id="horasFrm' . $data->prefix . '" name="horasFrm' . $data->prefix . '">
                                </div>
                            </div>
                            <div style="display: none;">
                                <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                <input class="flex-fill" type="number" id="peticionFrm' . $data->prefix . '" name="peticion">
                                <input class="flex-fill" type="number" id="idProfesorFrm' . $data->prefix . '" name="idProfesorFrm' . $data->prefix . '">
                                ' . $groupButtons . '
                            </div>
                        </form>
                        ';
                break;

            case 'VPrevia':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-4">
                                    <select id="ValidoVPrevia' . $data->prefix . '" name="ValidoParaid' . $data->prefix . '" class="form-control flex-fill">
                                        ' . $this->elementHtml(50, 'option') . '
                                    </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                    <select id="ValidoVPreviaPeriodos' . $data->prefix . '" name="ValidoParaidPeriodos' . $data->prefix . '" class="form-control flex-fill">
                                        ' . $this->elementHtml(50, 'option') . '
                                    </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                    <select id="ValidoVPreviaSemanas' . $data->prefix . '" name="ValidoParaidSemanas' . $data->prefix . '" class="form-control flex-fill">
                                    ' . $this->elementHtml(50, 'option') . '
                                    </select>
                                    </div>
                                    <div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>
                        ';
                break;

            case 'tiempoLibre':
                $content = '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                $content .= '<div id="notify' . $data->prefix . '" class="alert msg-alert" role="alert"></div>';
                $content .= '<div id="grpPeriodoSemana' . $data->prefix . '" class="col-12 mt-3" style="display: none;">
                                <form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                    <div class="d-flex flex-row flex-wrap col-12">
                                        <div id="grpPeriodos' . $data->prefix . '" class="col-lg-6 d-flex flex-column justify-content-between">
                                            <div class="form-group d-flex flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="igualptperiodos' . $data->prefix . '">' . $data->labelElements['igualptperiodos'] . '</label>
                                                <label class="switch">
                                                    <input id="periodosCheck' . $data->prefix . '" name="periodosCheck' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="0">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <select id="periodoTL' . $data->prefix . '" name="periodoTL' . $data->prefix . '" class="form-control flex-fill col-12" disabled>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="grpSemanas' . $data->prefix . '" class="col-lg-6 d-flex flex-column justify-content-between">
                                            <div class="form-group d-flex flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="igualptsemanas' . $data->prefix . '">' . $data->labelElements['igualptsemanas'] . '</label>
                                                <label class="switch">
                                                    <input id="semanasCheck' . $data->prefix . '" name="semanasCheck' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="0">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <select id="semanaTL' . $data->prefix . '" name="semanaTL' . $data->prefix . '" class="form-control flex-fill col-12" disabled>
                                                </select>
                                            </div>
                                        </div>
                                        <div style="display: none;">
                                            <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                            <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                            <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                            <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                            <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                        </div>
                                    </div>
                                </form>
                            </div>';
                break;

            case 'restricciones':
                $groupButtons = '';
                foreach ($data->buttons as $key => $value) {
                    $groupButtons .= '<button id="btnModal' . $data->prefix . $key . '" name="btnModal' . $data->prefix . $key . '" type="button" class="m-1 btn btn-' . $value . '" title="' . $key . '">' . $key . '</button>';
                }
                $content = '
                            <form id="gestionFrm' . $data->prefix . '" name="gestionFrm' . $data->prefix . '" action="#" method="post" enctype="text/plain">
                                <div id="generalSegFrm' . $data->prefix . '" class="d-flex flex-row flex-wrap">
                                    <div class="col-md-6 flex-fill p-2 border-custome">
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="limitehuecosFrm' . $data->prefix . '">' . $data->labelElements['limitehuecos'] . '</label>
                                            <label class="switch">
                                                <input data-target-to="maximahuecosFrm' . $data->prefix . '" class="form-check-input primary" type="checkbox" value="1" id="limitehuecosFrm' . $data->prefix . '" name="limitehuecosFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                            <label class="flex-fill" for="maximahuecosFrm' . $data->prefix . '">' . $data->labelElements['maximahuecos'] . ' </label>
                                            <input class="flex-fill" type="number" id="maximahuecosFrm' . $data->prefix . '" name="maximahuecosFrm' . $data->prefix . '" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 flex-fill p-2 border-custome">
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="leccionesxdiaFrm' . $data->prefix . '">' . $data->labelElements['leccionesxdia'] . '</label>
                                            <label class="switch">
                                                <input data-target-to="numeroleccionesxdiaFrm' . $data->prefix . '&numero2leccionesxdiaFrm' . $data->prefix . '" class="form-check-input primary" type="checkbox" value="1" id="leccionesxdiaFrm' . $data->prefix . '" name="leccionesxdiaFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                            <label class="flex-fill" for="">' . $data->labelElements['numeroleccionesxdia'] . ' </label>
                                            <div class="col-md-12 d-flex flex-row justify-content-between flex-wrap">
                                                <input class="col-md-4" type="number" id="numeroleccionesxdiaFrm' . $data->prefix . '" name="numeroleccionesxdiaFrm' . $data->prefix . '" disabled>
                                                <label class="col-md-2" style="text-align: center;"> - </label>
                                                <input class="col-md-4" type="number" id="numero2leccionesxdiaFrm' . $data->prefix . '" name="numero2leccionesxdiaFrm' . $data->prefix . '" disabled>
                                            </div>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="">' . $data->labelElements['noctrlfinde'] . '</label>
                                            <label class="switch">
                                                <input class="form-check-input primary" type="checkbox" value="1" id="noctrlfindeFrm' . $data->prefix . '" name="noctrlfindeFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 flex-fill p-2 border-custome">
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="limitardiasFrm">' . $data->labelElements['limitardias'] . '</label>
                                            <label class="switch">
                                                <input data-target-to="diasenqueFrm' . $data->prefix . '" class="form-check-input primary" type="checkbox" value="1" id="limitardiasFrm' . $data->prefix . '" name="limitardiasFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                            <label class="flex-fill" for="diasenqueFrm' . $data->prefix . '">' . $data->labelElements['diasenque'] . '</label>
                                            <input class="flex-fill" type="number" id="diasenqueFrm' . $data->prefix . '" name="diasenqueFrm' . $data->prefix . '" disabled>
                                            <label class="flex-fill" for="diasenqueFrm' . $data->prefix . '">' . $data->labelElements['notadiasenque'] . '</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 flex-fill p-2 border-custome">
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="">' . $data->labelElements['leccionesconsecutivas'] . '</label>
                                            <label class="switch">
                                                <input data-target-to="maxleccionesconsecutivasFrm' . $data->prefix . '" class="form-check-input primary" type="checkbox" value="1" id="leccionesconsecutivasFrm' . $data->prefix . '" name="leccionesconsecutivasFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                            <label class="flex-fill" for="">' . $data->labelElements['maxleccionesconsecutivas'] . '</label>
                                            <input class="flex-fill" type="number" id="maxleccionesconsecutivasFrm' . $data->prefix . '" name="maxleccionesconsecutivasFrm' . $data->prefix . '" disabled>
                                            <label class="flex-fill" for="">' . $data->labelElements['notamaxleccionesconsecutivas'] . '</label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class=" form-check-label" for="">' . $data->labelElements['agotamiento'] . '</label>
                                            <label class="switch">
                                                <input class="form-check-input primary" type="checkbox" value="1" id="agotamientoFrm' . $data->prefix . '" name="agotamientoFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 flex-fill p-2 border-custome">
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="">' . $data->labelElements['treshuecos'] . '</label>
                                            <label class="switch">
                                                <input class="form-check-input primary" type="checkbox" value="1" id="tresHuecosFrm' . $data->prefix . '" name="tresHuecosFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="">' . $data->labelElements['doshuecos'] . '</label>
                                            <label class="switch">
                                                <input class="form-check-input primary" type="checkbox" value="1" id="dosHuecosFrm' . $data->prefix . '" name="dosHuecosFrm' . $data->prefix . '">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="idItemHorario' . $data->prefix . '" name="idItemHorario' . $data->prefix . '">
                                    ' . $groupButtons . '
                                </div>
                            </form>
                        ';
                break;

            case 'lecciones':
                $content = '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                break;

            case 'relaciones':
                $content = '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                break;

            case 'timbres':
                $content = '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                $content .= '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="tiempDif' . $data->prefix . '">' . $data->labelElements['tiempDif'] . '</label>
                                        <label class="switch">
                                            <input id="Tlddclasid' . $data->prefix . '" name="Tlddclasid' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="form-group d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <select id="ValidoParaid' . $data->prefix . '" name="ValidoParaid' . $data->prefix . '" class="form-control flex-fill">
                                            ' . $this->elementHtml(50, 'option') . '
                                        </select>
                                        <div class="col-2 col-md-2 d-flex justify-content-center align-items-center">
                                            <span id="addItem' . $data->prefix . '" class="bg-dark btn text-center d-flex align-items-center" style="color: white; font-size: 1.5em; width: 2em; height: 2em;"><i class="far fa-plus-square"></i></span>
                                        </div>
                                    </div>
                                    <div id="resultadoGrupos' . $data->prefix . '"></div>
                                    <div class="form-group col-sm-12">
                                        <input type="button" id="VaPara' . $data->prefix . '" name="VaPara' . $data->prefix . '" value="' . $data->labelElements['ValidoPara'] . '" class="flex-fill form-control btn btn-dark" disabled>
                                    </div>
                                    <div class="form-group d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="EHDTAD' . $data->prefix . '">' . $data->labelElements['ehdf'] . '</label>
                                        <label class="switch">
                                            <input id="EHDTADid' . $data->prefix . '" name="EHDTADid' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div id="resultadoDias' . $data->prefix . '"></div>
                                    <div class="form-group col-sm-12">
                                        <input type="button" id="editar' . $data->prefix . '" name="editar' . $data->prefix . '" value="' . $data->labelElements['editar'] . '" class="flex-fill form-control btn btn-dark" disabled>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'addTimbre':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                <div class="d-flex flex-row flex-wrap col-12">
                    <div class="form-group col-sm-12">
                        <label class="form-check-label" for="nombreTimbre' . $data->prefix . '">' . $data->labelElements['nombre'] . '</label>
                        <input type="text" id="nombreTimbre' . $data->prefix . '" name="nombreTimbre' . $data->prefix . '"  class="flex-fill form-control" >
                    </div>
                    <div style="display: none;">
                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                    </div>
                </div>
            </form>';
                break;


            case 'clases':
                $groupButtons = '';
                foreach ($data->buttons as $key => $value) {
                    $groupButtons .= '<button id="btnModal' . $data->prefix . $key . '" name="btnModal' . $data->prefix . $key . '" type="button" class="m-1 btn btn-' . $value . '" title="' . $key . '">' . $key . '</button>';
                }
                $content = '<form id="frm' . $data->prefix . '" name="gestionFrm' . $data->prefix . '" class="col-12" action="#" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap">
                                    <div class="d-flex flex-column col-sm-6 p-0 m-0">
                                        <div class="p-2 border-custome">
                                            <h5>' . $data->labelElements['asignaturas'] . '</h5>
                                            <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                                <label class="flex-fill" for="asignaturas' . $data->prefix . '">' . $data->labelElements['asignaturas'] . '</label>
                                                <div class="d-flex flex-row col-12">
                                                    <select id="asignatura' . $data->prefix . '" name="asignatura' . $data->prefix . '"  class="col-10">
                                                    </select>
                                                    <input id="asignaturas' . $data->prefix . '" name="asignaturasBtn' . $data->prefix . '" type="button" class="btn btn-info col-2" value="+"></input>
                                                </div>
                                            </div>
                                            <div class="col-md-12 d-flex flex-row justify-content-center p-0">
                                                <ul id="coursesContainer' . $data->prefix . '" class="list-group col-10 p-0 ul-container scroll-theme">
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="p-2 border-custome">
                                            <h5>' . $data->labelElements['Clas'] . '</h5>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="todas' . $data->prefix . '">' . $data->labelElements['Todas'] . '</label>
                                                <label class="switch">
                                                    <input data-group="clases' . $data->prefix . '" id="todas' . $data->prefix . '" name="clases' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1" checked>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="' . $data->prefix . '">' . $data->labelElements['Eleccion'] . '</label>
                                                <label class="switch">
                                                    <input data-group="clases' . $data->prefix . '" id="eleccion' . $data->prefix . '" name="clases' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="2">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="form-group d-flex flex-row justify-content-between col-sm-12 flex-wrap">
                                                <label for="cambiarClase' . $data->prefix . '" class="flex-fill">' . $data->labelElements['CClases'] . '</label>
                                                <div class="d-flex flex-row col-12">
                                                    <select id="clase' . $data->prefix . '" name="clase' . $data->prefix . '"  class="col-10">
                                                    </select>
                                                    <input disabled id="cambiarClase' . $data->prefix . '" name="cambiarClase' . $data->prefix . '" type="button" class="btn btn-info col-2" value="+"></input>
                                                </div>
                                                <div class="col-md-12 d-flex flex-row justify-content-center p-0">
                                                    <ul id="groupsContainer' . $data->prefix . '" class="list-group col-10 p-0 ul-container scroll-theme">
                                                    </ul>
                                                </div>
                                            </div>
                                           
                                            <div class="form-group d-flex flex-column justify-content-between col-sm-12">
                                                <label for="nota' . $data->prefix . '">' . $data->labelElements['Nota'] . '</label>
                                                <textarea id="nota' . $data->prefix . '" name="nota' . $data->prefix . '" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2 border-custome col-sm-6">
                                        <h5>' . $data->labelElements['Condicion'] . '</h5>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="noMismoDia' . $data->prefix . '">' . $data->labelElements['Condicion1'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="noMismoDia' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="noConsecutiva' . $data->prefix . '">' . $data->labelElements['Condicion2'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="noConsecutiva' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="2">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="fichasSemana' . $data->prefix . '">' . $data->labelElements['Condicion3'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="fichasSemana' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="3">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex flex-column justify-content-between col-sm-12 flex-wrap">
                                            <input id="configuracion' . $data->prefix . '" name="configuracion' . $data->prefix . '" type="button" class="btn btn-info flex-fill" value="' . $data->labelElements['config'] . '"></input>
                                        </div>
                                        <hr>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="dosDia' . $data->prefix . '">' . $data->labelElements['Condicion4'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="dosDia' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="4">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="dosConsecutivas' . $data->prefix . '">' . $data->labelElements['Condicion5'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="dosConsecutivas' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="5">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="asignaturasSeleccionadas' . $data->prefix . '">' . $data->labelElements['Condicion10'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="asignaturasSeleccionadas' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="10">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="asignaturaMismoPeriodo' . $data->prefix . '">' . $data->labelElements['Condicion11'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="asignaturaMismoPeriodo' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="11">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="inicioFinal' . $data->prefix . '">' . $data->labelElements['Condicion13'] . '</label>
                                            <label class="switch">
                                                <input data-group="condicion' . $data->prefix . '" id="inicioFinal' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                    type="checkbox" value="13">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItemCurso' . $data->prefix . '" name="idItemCurso' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="idItemGrupo' . $data->prefix . '" name="idItemGrupo' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="idItemHorario' . $data->prefix . '" name="idItemHorario' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="FichasDivididasFrm' . $data->prefix . '" name="FichasDivididas' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="SHMDCFrm' . $data->prefix . '" name="SHMDC' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="DLecciones' . $data->prefix . '" name="DLecciones' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="dfndd1Frm' . $data->prefix . '" name="dfndd1' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="dfndh1Frm' . $data->prefix . '" name="dfndh1' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="nldid2Frm' . $data->prefix . '" name="nldid2' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="nldih2Frm' . $data->prefix . '" name="nldih2' . $data->prefix . '">
                                    ' . $groupButtons . '
                                </div>
                            </form>';
                break;

            case 'timbre':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-4">
                                        <label for="identificadorRecreo' . $data->prefix . '">' . $data->labelElements['identificadorRecreo'] . '</label>
                                        <input type="text" id="identificadorRecreo' . $data->prefix . '" name="identificadorRecreo' . $data->prefix . '" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="abreviatura' . $data->prefix . '">' . $data->labelElements['abreviatura'] . '</label>
                                        <input type="text" id="abreviatura' . $data->prefix . '" name="abreviatura' . $data->prefix . '" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ubicacionRecreo' . $data->prefix . '">' . $data->labelElements['ubicacionRecreo'] . '</label>
                                        <select id="ubicacionRecreo' . $data->prefix . '" name="ubicacionRecreo' . $data->prefix . '" class="form-control">
                                            <option value="ARecreo1">Antes del : Recreo1</option>
                                            <option value="ARecreo2">Antes del : Recreo2</option>
                                        </select>
                                    </div>
                                    <div class="d-flex flex-row flex-wrap col-12">
                                        <div class="form-group col-sm-6">
                                            <label for="inicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="inicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="fin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="fin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <h6>' . $data->labelElements['Recreo'] . '</h6>
                                        <label for="textoImpresion' . $data->prefix . '" class="flex-fill">' . $data->labelElements['textoImpresion'] . '</label>
                                        <input type="text" id="textoImpresion' . $data->prefix . '" name="textoImpresion' . $data->prefix . '" class="form-control flex-fill">
                                    </div>
                                    <div id="imprimirGrp' . $data->prefix . '">
                                        <h6>' . $data->labelElements['Impresiones'] . '</h6>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="ImprimirPeriodoIndividual' . $data->prefix . '">' . $data->labelElements['ImprimirPeriodoIndividual'] . '</label>
                                            <label class="switch">
                                                <input id="ImprimirPeriodoIndividual' . $data->prefix . '" name="ImprimirPeriodoIndividual' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="imprimirPeriodoIndividualDe' . $data->prefix . '">' . $data->labelElements['imprimirPeriodoIndividualDe'] . '</label>
                                            <label class="switch">
                                                <input id="imprimirPeriodoIndividualDe' . $data->prefix . '" name="imprimirPeriodoIndividualDe' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="imprimirTodos' . $data->prefix . '">' . $data->labelElements['imprimirTodos'] . '</label>
                                            <select id="imprimirTodos' . $data->prefix . '" name="imprimirTodos' . $data->prefix . '" class="form-control flex-fill">
                                                <option value="1">Todas las clases </option>
                                                <option value="2">Solo para : Timbre 1</option>
                                                <option value="3">Solo para : Timbre 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="number" id="tipo' . $data->prefix . '" name="tipo' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                </div>
                            </form>';
                break;

            case 'escuela':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-4 col-sm-12 col-xs-12 col-lg-4 col-xl-4 d-flex flex-column">
                                        <label for="nombreHorario' . $data->prefix . '">' . $data->labelElements['nombreHorario'] . '</label>
                                        <input type="text" id="nombreHorario' . $data->prefix . '" name="nombreHorario' . $data->prefix . '" class="form-control flex-fill">
                                    </div>
                                    <div class="form-group col-md-4 col-sm-12 col-xs-12 col-lg-4 col-xl-4 d-flex flex-column">
                                        <label for="cicloEscolar' . $data->prefix . '">' . $data->labelElements['cicloEscolar'] . '</label>
                                        <input type="text" id="cicloEscolar' . $data->prefix . '" name="cicloEscolar' . $data->prefix . '" class="form-control flex-fill">
                                    </div>
                                    <div class="form-group col-md-4 d-flex flex-column">
                                        <label for="centro' . $data->prefix . '">' . $data->labelElements['centro'] . '</label>
                                        <select id="centro' . $data->prefix . '" name="centro' . $data->prefix . '" class="form-control flex-fill col-12">
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 d-flex flex-column">
                                        <label for="leccionesDia' . $data->prefix . '">' . $data->labelElements['leccionesDia'] . '</label>
                                        <select id="leccionesDia' . $data->prefix . '" name="leccionesDia' . $data->prefix . '" class="form-control flex-fill col-12">
                                            <option value="">Example</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 d-flex flex-column">
                                        <label for="renombrarPeriodo' . $data->prefix . '">' . $data->labelElements['renombrarPeriodo'] . '</label>
                                        <input type="button" id="renombrarPeriodo' . $data->prefix . '" name="renombrarPeriodo' . $data->prefix . '" value="' . $data->labelElements['renombrar'] . '" class="form-control flex-fill col-12">
                                    </div>
                                    <div class="form-group col-md-6 d-flex flex-column">
                                        <label for="numeroDias' . $data->prefix . '">' . $data->labelElements['numeroDias'] . '</label>
                                        <select id="numeroDias' . $data->prefix . '" name="numeroDias' . $data->prefix . '" class="form-control flex-fill col-12">
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 d-flex flex-column">
                                        <label for="renombrarDia' . $data->prefix . '">' . $data->labelElements['renombrarDia'] . '</label>
                                        <input type="button" id="renombrarDia' . $data->prefix . '" name="renombrarDia' . $data->prefix . '" class="form-control flex-fill col-12" value="' . $data->labelElements['renombrar'] . '">
                                    </div>
                                    <div class="form-group col-md-6 d-flex flex-column">
                                        <label for="finSemana' . $data->prefix . '">' . $data->labelElements['finSemana'] . '</label>
                                        <select id="finSemana' . $data->prefix . '" name="finSemana' . $data->prefix . '" class="form-control flex-fill col-12">
                                            <option value="1">Sábado - Domingo </option>
                                            <option value="2">Viernes - Sábado </option>
                                            <option value="3">Jueves - Viernes</option>
                                        </select>
                                    </div>
                                    <div id="makeOwnTimes' . $data->prefix . '" class="col-md-12 d-flex flex-row flex-wrap p-0">
                                        <div class="form-group col-md-12">
                                            <label class="form-check-label" for="personalHorarios' . $data->prefix . '">' . $data->labelElements['personalHorarios'] . '</label>
                                            <label class="switch">
                                                <input id="personalHorarios' . $data->prefix . '" name="personalHorarios' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="DPeriodos' . $data->prefix . '">' . $data->labelElements['DPeriodos'] . '</label>
                                            <input type="button" id="DPeriodos' . $data->prefix . '" name="DPeriodos' . $data->prefix . '" value="' . $data->labelElements['DPeriodos'] . '" class="form-control flex-fill col-12" disabled>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="DSemanas' . $data->prefix . '">' . $data->labelElements['DSemanas'] . '</label>
                                            <input type="button" id="DSemanas' . $data->prefix . '" name="DSemanas' . $data->prefix . '" value="' . $data->labelElements['DSemanas'] . '" class="form-control flex-fill col-12" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                </div>
                            </form>';
                break;

            case 'leccion':
                $content = ' <form id="frm' . $data->prefix . '" method="post" enctype="text/plain" class="col-12">
                                <div class="claseGrpFrm d-flex flex-row flex-wrap justify-content-between col-md-12">
                                    <div class="d-flex flex-row col-md-2"><i class="icon-custome-menu fas fa-book"></i></div>
                                    <div class="d-flex flex-row col-md-10">
                                        <div class="form-group col-md-12">
                                            <label for="asignatura' . $data->prefix . '">' . $data->labelElements['asignatura'] . '</label>
                                            <select id="asignatura' . $data->prefix . '" name="asignatura' . $data->prefix . '" class="form-control flex-fill">
                                                <option value="#"></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="profesorGrpFrm d-flex flex-row flex-wrap justify-content-between col-md-12">
                                    <div class="d-flex flex-row col-md-2"><i class="icon-custome-menu fas fa-graduation-cap"></i></div>
                                    <div class="d-flex flex-row col-md-8">
                                        <div class="form-group col-md-12">
                                            <label for="profesor' . $data->prefix . '">' . $data->labelElements['profesor'] . '</label>
                                            <select id="profesor' . $data->prefix . '" name="profesor' . $data->prefix . '" class="form-control flex-fill">
                                                <option value="#">Profesor #</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column col-md-2 justify-content-center">
                                        <span id="masProfesor' . $data->prefix . '" name="masProfesor' . $data->prefix . '" class="btn btn-dark d-flex flex-row justify-content-center hand"><i class="fas fa-plus-circle"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-12 d-flex flex-row justify-content-center p-0">
                                    <ul id="profesoresContainer' . $data->prefix . '" class="list-group col-10 p-0 ul-container scroll-theme">
                                    </ul>
                                </div>
                                <div class="GrupoGrpFrm d-flex flex-row flex-wrap justify-content-between col-md-12">
                                    <div class="d-flex flex-row col-md-2"><i class="icon-custome-menu fas fa-user-friends"></i></div>
                                    <div class="d-flex flex-row col-md-8">
                                        <div class="form-group col-md-12">
                                        <label for="grupo' . $data->prefix . '">' . $data->labelElements['grupo'] . '</label>
                                        <select id="grupo' . $data->prefix . '" name="grupo' . $data->prefix . '" class="form-control">
                                            <option value="#">GRUPO:NOMBRE</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column col-md-2 justify-content-center">
                                        <span id="claseUnida' . $data->prefix . '" name="claseUnida' . $data->prefix . '" class="btn btn-dark d-flex flex-row justify-content-center hand"><i class="fas fa-plus-circle"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-12 d-flex flex-row justify-content-center p-0">
                                    <ul id="grupoContainer' . $data->prefix . '" class="list-group col-10 p-0 ul-container scroll-theme">
                                    </ul>
                                </div>
                                <div class="sessionGrpFrm d-flex flex-row flex-wrap justify-content-between col-md-12">
                                    <div class="d-flex flex-row col-md-2"><i class="icon-custome-menu fas fa-calendar-alt"></i></div>
                                    <div class="d-flex flex-row col-md-10 flex-wrap">
                                        <div class="form-group col-md-6">
                                            <label for="SesionSemana' . $data->prefix . '">' . $data->labelElements['sesionSemana'] . '</label>
                                            <select id="SesionSemana' . $data->prefix . '" name="SesionSemana' . $data->prefix . '" class="form-control flex-fill">
                                                ' . $this->elementHtml(20, 'option') . '
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="seleccionarLeccion' . $data->prefix . '">' . $data->labelElements['leccion'] . '</label>
                                            <select id="seleccionarLeccion' . $data->prefix . '" name="seleccionarLeccion' . $data->prefix . '" class="form-control">
                                                ' . $this->elementHtml(12, 'option', $data->labelElements['leccion']) . '
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="periodo' . $data->prefix . '">' . $data->labelElements['periodo'] . '</label>
                                            <select id="periodo' . $data->prefix . '" name="periodo' . $data->prefix . '" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="semana' . $data->prefix . '">' . $data->labelElements['semana'] . '</label>
                                            <select id="semana' . $data->prefix . '" name="semana' . $data->prefix . '" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row col-md-2" style="display:none !important">
                                        <input type="button" id="moreWeek' . $data->prefix . '" name="moreWeek' . $data->prefix . '" value="' . $data->labelElements['masSemanas'] . '" class="form-control btn btn-dark">
                                    </div>
                                </div>
                                <div class="aulaGrpFrm d-flex flex-row flex-wrap justify-content-between col-md-12">
                                    <div class="d-flex flex-row col-md-2"><i class="icon-custome-menu fas fa-door-open"></i></div>
                                    <div class="d-flex flex-row col-md-10 flex-wrap">
                                        <div id="aulasDisplay' . $data->prefix . '" class="col-md-6">
                                        </div>
                                        <div class="col-md-6 form-group" style="display:none !important">
                                            <input disabled type="button" class="btn btn-dark form-control" id="openAddClassroom' . $data->prefix . '" value="' . $data->labelElements['addAula'] . '">
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row col-md-8 flex-wrap" style="display:none !important">
                                        <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="aulaFija' . $data->prefix . '">' . $data->labelElements['aulaFija'] . '</label>
                                            <label class="switch">
                                                <input id="aulaFija' . $data->prefix . '" name="aulaFija' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="aulaProfesor' . $data->prefix . '">' . $data->labelElements['aulaProfesor'] . '</label>
                                            <label class="switch">
                                                <input id="aulaProfesor' . $data->prefix . '" name="aulaProfesor' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="aulaCompartida' . $data->prefix . '">' . $data->labelElements['aulaCompartida'] . '</label>
                                            <label class="switch">
                                                <input id="aulaCompartida' . $data->prefix . '" name="aulaCompartida' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                            <label class="form-check-label" for="aulaAsignatura' . $data->prefix . '">' . $data->labelElements['AulaAsignatura'] . '</label>
                                            <label class="switch">
                                                <input id="aulaAsignatura' . $data->prefix . '" name="aulaAsignatura' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <input type="button" id="otrosPermitidos' . $data->prefix . '" name="otrosPermitidos' . $data->prefix . '" class="form-control" value="' . $data->labelElements['otrasAulasPermitidas'] . '">
                                        </div>
                                        <div class="form-group">
                                            <input type="button" id="masAulas' . $data->prefix . '" name="masAulas' . $data->prefix . '" class="form-control" value="' . $data->labelElements['masAulas'] . '">
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row col-md-2">
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                </div>
                            </form>';
                break;

            case 'Rdias':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="NotaDias' . $data->prefix . '">' . $data->labelElements['notaDias'] . '</label>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="diasRenombrados' . $data->prefix . '">' . $data->labelElements['diasRenombrados'] . '</label>
                                        <select id="diasRenombrado' . $data->prefix . '" name="diasRenombrado' . $data->prefix . '" class="form-control flex-fill">
                                            ' . $this->elementHtml(10, 'option') . '
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="Nota2Dias' . $data->prefix . '">' . $data->labelElements['Nota2Dias'] . '</label>
                                    </div>
                                    <div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'VPrevia':
                $content = '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'Objeto':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="nombre' . $data->prefix . '">' . $data->labelElements['nombre'] . '</label>
                                        <input type="text" id="nombreObjeto' . $data->prefix . '" name="nombreObjeto' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="abreviatura' . $data->prefix . '">' . $data->labelElements['abreviatura'] . '</label>
                                        <input type="text" id="abreviaturaObjeto' . $data->prefix . '" name="abreviaturaObjeto' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'CombinacionPeriodo':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="nombre' . $data->prefix . '">' . $data->labelElements['nombre'] . '</label>
                                        <input type="text" id="nombreObjetoPeriodo' . $data->prefix . '" name="nombreObjetoPeriodo' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="abreviatura' . $data->prefix . '">' . $data->labelElements['abreviatura'] . '</label>
                                        <input type="text" id="abreviatura' . $data->prefix . '" name="abreviatura' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="lcombinacionperiodos' . $data->prefix . '">' . $data->labelElements['lcombinacionperiodos'] . '</label>
                                    </div>
                                    <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="op1periodo' . $data->prefix . '">' . $data->labelElements['op1periodo'] . '</label>
                                        <label class="switch">
                                            <input id="op1periodo' . $data->prefix . '" name="op1periodo' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="op2periodo' . $data->prefix . '">' . $data->labelElements['op2periodo'] . '</label>
                                        <label class="switch">
                                            <input id="op2periodoid' . $data->prefix . '" name="op2periodoid' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'CombinacionDias':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="nombre' . $data->prefix . '">' . $data->labelElements['nombre'] . '</label>
                                        <input type="text" id="nombreObjetoDias' . $data->prefix . '" name="nombreObjetoPeriodo' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="abreviatura' . $data->prefix . '">' . $data->labelElements['abreviatura'] . '</label>
                                        <input type="text" id="abreviaturaObjetoDias' . $data->prefix . '" name="abreviaturaObjetoPeriodo' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="lcombinaciondias' . $data->prefix . '">' . $data->labelElements['lcombinaciondias'] . '</label>
                                    </div>
                                    <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="op1dia' . $data->prefix . '">' . $data->labelElements['op1dia'] . '</label>
                                        <label class="switch">
                                            <input id="op1diaid' . $data->prefix . '" name="op1diaid' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="op2dia' . $data->prefix . '">' . $data->labelElements['op2dia'] . '</label>
                                        <label class="switch">
                                            <input id="op2diaid' . $data->prefix . '" name="op2diaid' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'CombinacionSemanas':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="nombre' . $data->prefix . '">' . $data->labelElements['nombre'] . '</label>
                                        <input type="text" id="nombreObjetoSemanas' . $data->prefix . '" name="nombreObjetoSemanas' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="abreviatura' . $data->prefix . '">' . $data->labelElements['abreviatura'] . '</label>
                                        <input type="text" id="abreviatura' . $data->prefix . '" name="abreviatura' . $data->prefix . '" class="flex-fill form-control">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="lcombinacionsemana' . $data->prefix . '">' . $data->labelElements['lcombinacionsemana'] . '</label>
                                    </div>
                                    <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="op1semana' . $data->prefix . '">' . $data->labelElements['op1semana'] . '</label>
                                        <label class="switch">
                                            <input id="op1semana' . $data->prefix . '" name="op1semana' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="form-group d-flex col-md-6 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="op2semana' . $data->prefix . '">' . $data->labelElements['op2semana'] . '</label>
                                        <label class="switch">
                                            <input id="op2semana' . $data->prefix . '" name="op2semana' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'Periodo':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-sm-12 d-flex flex-column">
                                        <label id="P1' . $data->prefix . '">' . $data->labelElements['p1'] . '</label>
                                        <label for="notaPeriodoP' . $data->prefix . '">' . $data->labelElements['notaPeriodoP'] . '</label>
                                    </div>
                                    <div class= "col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="lunes' . $data->prefix . '" class="col-12">' . $data->labelElements['lunes'] . '</label> 
                                        <div class="form-group col-md-6">
                                            <label for="lunesinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="lunesinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="lunesfin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="lunesfin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="martes' . $data->prefix . '" class="col-12">' . $data->labelElements['martes'] . '</label>
                                        <div class="form-group col-md-6">
                                            <label for="martesinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="martesinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="martesfin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="martesfin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="miercoles' . $data->prefix . '" class="col-12">' . $data->labelElements['miercoles'] . '</label>
                                        <div class="form-group col-md-6">
                                            <label for="miercolesinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="miercolesinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="miercolesfin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="miercolesfin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="jueves' . $data->prefix . '" class="col-12">' . $data->labelElements['jueves'] . '</label>
                                        <div class="form-group col-md-6">
                                            <label for="juevesinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="juevesinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="juevesfin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="juevesfin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="viernes' . $data->prefix . '" class="col-12">' . $data->labelElements['viernes'] . '</label>
                                        <div class="form-group col-md-6">
                                            <label for="viernesinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="viernesinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="viernesfin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="viernesfin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="sabado' . $data->prefix . '" class="col-12">' . $data->labelElements['sabado'] . '</label>
                                        <div class="form-group col-md-6">
                                            <label for="sabadoinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="sabadoinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="sabadofin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="sabadofin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-row flex-wrap oculto diasMultiples' . $data->prefix . '">
                                        <label for="domingo' . $data->prefix . '" class="col-12">' . $data->labelElements['domingo'] . '</label>
                                        <div class="form-group col-md-6">
                                            <label for="domingoinicio' . $data->prefix . '" class="flex-fill">' . $data->labelElements['inicio'] . '</label>
                                            <input id="domingoinicio' . $data->prefix . '" name="inicio' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="domingofin' . $data->prefix . '" class="flex-fill">' . $data->labelElements['fin'] . '</label>
                                            <input id="domingofin' . $data->prefix . '" name="fin' . $data->prefix . '" type="time" class="form-control flex-fill">
                                        </div>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="horaInicio' . $data->prefix . '" name="horaInicio' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="horaFin' . $data->prefix . '" name="horaFin' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="timbre' . $data->prefix . '" name="timbre' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'Eleccion':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="cltselect' . $data->prefix . '">' . $data->labelElements['cltselect'] . '</label>
                                    </div>
                                    <div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>
                                    <div class="form-group col-md-12">
                                        <label for="eleccion' . $data->prefix . '">' . $data->labelElements['eleccion'] . '</label>
                                    </div>
                                    <div class="flex-fill bd-highlight content-table">' . $this->buildTable($data, 'eleccion2') . '</div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'DefPeriodos':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="notaPeriodo1' . $data->prefix . '">' . $data->labelElements['notaPeriodo1'] . '</label>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="numPeriodos' . $data->prefix . '">' . $data->labelElements['numPeriodos'] . '</label>
                                        <select id="numPeriodos' . $data->prefix . '" name="numPeriodos' . $data->prefix . '" class="form-control flex-fill">
                                            ' . $this->elementHtml(12, 'option') . '
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="notaPeriodo2' . $data->prefix . '">' . $data->labelElements['notaPeriodo2'] . '</label>
                                    </div>
                                    <div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'DefSemanas':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group col-md-12">
                                        <label for="notaSemana' . $data->prefix . '">' . $data->labelElements['notaSemana'] . '</label>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="numSemana' . $data->prefix . '">' . $data->labelElements['numSemana'] . '</label>
                                        <select id="numSemana' . $data->prefix . '" name="numSemana' . $data->prefix . '" class="form-control flex-fill">
                                            ' . $this->elementHtml(20, 'option') . '
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="notaSemana2' . $data->prefix . '">' . $data->labelElements['notaSemana2'] . '</label>
                                    </div>
                                    <div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'GNuevHorario':
                $content = '<form id="frm' . $data->prefix . '"  class="col-12" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="form-group d-flex flex-column justify-content-between col-sm-12 flex-wrap">
                                        <input id="gnhorario' . $data->prefix . '" name="gnhorario' . $data->prefix . '" type="button" class="btn btn-info flex-fill" value="' . $data->labelElements['ghorario'] . '"></input>
                                    </div>
                                    <div class="d-flex flex-column col-sm-6 p-0 m-0">
                                        <div class="p-2 border-custome">
                                            <h5>' . $data->labelElements['chorario'] . '</h5>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="normal' . $data->prefix . '">' . $data->labelElements['normal'] . '</label>
                                                <label class="switch">
                                                    <input data-group="complejo' . $data->prefix . '" id="normal' . $data->prefix . '" name="complejo' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="grande' . $data->prefix . '">' . $data->labelElements['grande'] . '</label>
                                                <label class="switch">
                                                    <input data-group="complejo' . $data->prefix . '" id="grande' . $data->prefix . '" name="complejo' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="2">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="muyGrande' . $data->prefix . '">' . $data->labelElements['muygrande'] . '</label>
                                                <label class="switch">
                                                    <input data-group="complejo' . $data->prefix . '" id="muyGrande' . $data->prefix . '" name="complejo' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="3">                                               
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div id="contenedorVueltas' . $data->prefix . '" class="d-flex col-md-12 flex-row justify-content-between flex-wrap oculto">
                                                <input id="vueltasGNuevHorario" name="vueltasGNuevHorario" type="number" value="5000" class="form-control flex-fill">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column col-sm-6 p-0 m-0">
                                        <div class="p-2 border-custome">
                                            <h5>' . $data->labelElements['cdn'] . '</h5>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="prueba' . $data->prefix . '">' . $data->labelElements['prueba'] . '</label>
                                                <label class="switch">
                                                    <input data-group="condicion' . $data->prefix . '" id="prueba' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="prc' . $data->prefix . '">' . $data->labelElements['prc'] . '</label>
                                                <label class="switch">
                                                    <input data-group="condicion' . $data->prefix . '" id="prc' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="2">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                            <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                <label class="form-check-label" for="estricto' . $data->prefix . '">' . $data->labelElements['estricto'] . '</label>
                                                <label class="switch">
                                                    <input data-group="condicion' . $data->prefix . '" id="estricto' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="2">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'VCRelajadas':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div>
                                    <div class="d-flex flex-row flex-wrap col-12">
                                        <div class="form-group col-md-12">
                                            <label for="ltcr' . $data->prefix . '">' . $data->labelElements['ltcr'] . '</label>
                                        </div>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'Dcls':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="d-flex flex-row flex-wrap col-12">
                                    <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="LFCDOT' . $data->prefix . '">' . $data->labelElements['lfcdot'] . '</label>
                                        <label class="switch">
                                            <input data-group="diasConsecutivos' . $data->prefix . '" id="LFCDOTCHECK' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                type="checkbox" value="0">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="LFCNDOT' . $data->prefix . '">' . $data->labelElements['lfcndot'] . '</label>
                                        <label class="switch">
                                            <input data-group="diasConsecutivos' . $data->prefix . '" id="LFCNDOTCHECK' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary" 
                                            type="checkbox" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="SDC' . $data->prefix . '">' . $data->labelElements['sdc'] . '</label>
                                        <label class="switch">
                                            <input id="SDCCHECK' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                            <span class="slider round"></span>
                                            </label>
                                    </div>
                                    <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label class="form-check-label" for="ENDYNLPDF' . $data->prefix . '">' . $data->labelElements['endynlpdf'] . '</label>
                                        <label class="switch">
                                            <input id="ENDYNLPDFCHECK' . $data->prefix . '" name="condicion' . $data->prefix . '" data-target-to="" class="form-check-input primary"
                                                type="checkbox" value="1">
                                            <span class="slider round"></span>
                                            </label>
                                    </div>
                                    <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                        <label for="DFHND' . $data->prefix . '">' . $data->labelElements['dfhnd'] . '</label>
                                    </div>
                                    <div class="d-flex flex-row flex-wrap col-12">
                                        <div class="d-flex col-md-6">
                                                <label for="Desde1' . $data->prefix . '">' . $data->labelElements['desde'] . '</label>
                                                <select id="DesdeH1' . $data->prefix . '" name="DesdeH1' . $data->prefix . '" class="form-control flex-fill">
                                                    <option value="0">Automáticamente</option>    
                                                    ' . $this->elementHtml(10, 'option') . '
                                                </select>
                                        </div>
                                        <div class="d-flex col-md-6">
                                                <label for="Hasta1' . $data->prefix . '">' . $data->labelElements['hasta'] . '</label>
                                            <select id="HastaH1' . $data->prefix . '" name="HastaH1' . $data->prefix . '" class="form-control flex-fill">
                                                <option value="0">Automáticamente</option>     
                                                ' . $this->elementHtml(10, 'option') . '
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row flex-wrap col-12">
                                        <label for="NLDI' . $data->prefix . '">' . $data->labelElements['nldi'] . '</label>
                                        <div class="d-flex col-md-6">
                                            <label for="Desde2' . $data->prefix . '">' . $data->labelElements['desde'] . '</label>
                                            <select id="DesdeH2' . $data->prefix . '" name="DesdeH2' . $data->prefix . '" class="form-control flex-fill">
                                                <option value="0">Automáticamente</option> 
                                                ' . $this->elementHtml(10, 'option') . '
                                            </select>
                                        </div>
                                        <div class="d-flex col-md-6">
                                            <label for="Hasta2' . $data->prefix . '">' . $data->labelElements['hasta'] . '</label>
                                            <select id="HastaH2' . $data->prefix . '" name="HastaH2' . $data->prefix . '" class="form-control flex-fill">
                                                <option value="0">Automáticamente</option> 
                                                ' . $this->elementHtml(10, 'option') . '
                                            </select>
                                        </div>
                                    </div>
                                    <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    </div>
                                </div>
                            </form>';
                break;

            case 'Aviso':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="form-group col-md-12">
                                    <label for="Aviso' . $data->prefix . '">' . $data->labelElements['Aviso'] . '</label>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                </div>
                            </form>';
                break;

            case 'Asesor':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="form-group col-md-12">
                                    <label for="pc' . $data->prefix . '">' . $data->labelElements['pc'] . '</label>
                                    <div id="container' . $data->prefix . '" class="border border-danger non-selectable scroll-theme">
                                        <ul id="criticos' . $data->prefix . '" class="list-group p-0">
                                        </ul>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                </div>
                            </form>';
                break;

            case 'GTerminada':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div>
                                    <div class="d-flex flex-column flex-wrap col-12" id="boxNotify' . $data->prefix . '">
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                    <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                </div>
                            </form>';
                break;

            case 'EscuelaTabla':
                $content = '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                break;

            case 'TPrincipal':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                             <div  id= "imprHorario">
                                <div class="row">
                                <label title="PDF" id="btnPDF" name="btnPDF" class="m-1 btn btn-success">PDF</label>
                                    <div class="form-group col-md-4">
                                        <label for="selectBy' . $data->prefix . '">' . $data->labelElements['selectBy'] . '</label>
                                        <select id="selectBy' . $data->prefix . '" name="selectBy' . $data->prefix . '" class="form-control flex-fill input-s">
                                            <option value ="1">' . $data->labelElements['todo'] . '</option>
                                            <option value ="2">' . $data->labelElements['profesores'] . '</option>
                                            <option value ="3">' . $data->labelElements['aulas'] . '</option>
                                            <option value ="4">' . $data->labelElements['cursos'] . '</option>
                                        </select>
                                    </div>
                                    <!--
                                    <div class="form-group col-md-4">
                                        <label for="periodo' . $data->prefix . '">' . $data->labelElements['periodoName'] . '</label>
                                        <select id="periodo' . $data->prefix . '" name="periodo' . $data->prefix . '" class="form-control flex-fill">
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="semana' . $data->prefix . '">' . $data->labelElements['semana'] . '</label>
                                        <select id="semana' . $data->prefix . '" name="semana' . $data->prefix . '" class="form-control flex-fill">
                                        </select>
                                    </div>-->
                                </div>
                                <br>
                                <div id="menu' . $data->prefix . '" class="dropdown-menu row" role="menu" aria-labelledby="dropdownMenu1">
                                    <div class="col-12">
                                            <div class="row text-center bold">
                                                <label class="col-12">Mover a:</label>
                                            </div>
                                            <hr class="m-1">
                                            <div class="row" id="quitSwitch">
                                                <div class="form-group col-5 m-b-0">
                                                    <label for="bloquearF' . $data->prefix . '">Bloquear </label>
                                                </div>
                                                <div class="form-group col-7 m-b-0">
                                                    <label class="switch">
                                                        <input id="bloquearF' . $data->prefix . '" name="bloquearF' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div  id="Block">
                                            <div class="row">
                                                <div class="form-group col-5 m-b-0">
                                                    <label for="selectDia' . $data->prefix . '">Dia: </label>
                                                </div>
                                                <div class="form-group col-7 m-b-0">
                                                <select id="selectDia' . $data->prefix . '" name="selectDia' . $data->prefix . '" >
                                                </select>          
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-5 m-b-0">
                                                    <label for="selectHora' . $data->prefix . '">Hora: </label>
                                                </div>
                                                <div class="form-group col-7 m-b-0">
                                                    <select id="selectHora' . $data->prefix . '" name="selectHora' . $data->prefix . '">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row" id="quitClassroom">
                                                <div class="form-group col-5 m-b-0">
                                                    <label for="cambiarAula' . $data->prefix . '">Aula :</label>
                                                </div>
                                                <div class="form-group col-7 m-b-0">
                                                    <select name="cambiarAula' . $data->prefix . '" id="cambiarAula' . $data->prefix . '">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-6 text-center m-0 m-t-1 p-0 " id="deleteCard">
                                                    <label class="btn btn-danger m-0" style="font-size: 12px" id="labelBtn' . $data->prefix . 'BorrarFicha" name="labelBtn' . $data->prefix . 'BorrarFicha">Borrar</label>
                                                </div>
                                                <div class="form-group col-6 text-center m-0 m-t-1 p-0" id="moveCard">
                                                <label data-type_card="" class="btn btn-success m-0" style="font-size: 12px" id="labelBtn' . $data->prefix . 'MoverFicha" name="labelBtn' . $data->prefix . 'MoverFicha">Aceptar</label>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                </div>

                                <table class="table table-hover table-bordered columnas">
                                    <thead id="diaHora' . $data->prefix . '">                                     
                                    </thead>
                                    <tbody id="bodyTodo' . $data->prefix . '">
                                    </tbody>
                                </table>
                                
                                <div style="display:none">
                                    Id_H<input class="flex-fill" type="text" id="cmpIdHorario' . $data->prefix . '" name="cmpIdHorario' . $data->prefix . '">
                                    Id_F<input class="flex-fill" type="text" id="cmpId' . $data->prefix . '" name="cmpId' . $data->prefix . '">
                                    Fich<input class="flex-fill" type="text" id="cmpFichas' . $data->prefix . '" name="cmpFichas' . $data->prefix . '">
                                    Lecc<input class="flex-fill" type="text" id="cmpIdLeccion' . $data->prefix . '" name="cmpIdLeccion' . $data->prefix . '">
                                    Curs<input class="flex-fill" type="text" id="cmpIdCurso' . $data->prefix . '" name="cmpIdCurso' . $data->prefix . '">
                                    Grup<input class="flex-fill" type="text" id="cmpIdGrupo' . $data->prefix . '" name="cmpIdGrupo' . $data->prefix . '">
                                    Dura<input class="flex-fill" type="text" id="cmpDuracion' . $data->prefix . '" name="cmpDuracion' . $data->prefix . '">
                                    Bloq<input class="flex-fill" type="text" id="cmpBloqueado' . $data->prefix . '" name="cmpBloqueado' . $data->prefix . '">
                                    Peri<input class="flex-fill" type="text" id="cmpPeriodo' . $data->prefix . '" name="cmpPeriodo' . $data->prefix . '">
                                    Sema<input class="flex-fill" type="text" id="cmpSemana' . $data->prefix . '" name="cmpSemana' . $data->prefix . '">
                                    Dias<input class="flex-fill" type="text" id="cmpDias' . $data->prefix . '" name="cmpDias' . $data->prefix . '">
                                    Hora<input class="flex-fill" type="text" id="cmpHoras' . $data->prefix . '" name="cmpHoras' . $data->prefix . '">
                                    Aula<input class="flex-fill" type="text" id="cmpAula' . $data->prefix . '" name="cmpAula' . $data->prefix . '">
                                </div>

                                </div>

                            </form>
                            ';
                            
                break;

            case 'EliminarHorario':
                $content = '<form id="frm' . $data->prefix . '"  class="col-12" method="post" enctype="text/plain">
                                    <div class="d-flex flex-row flex-wrap col-12">
                                        <div class="d-flex flex-column col-sm-12 p-0 m-0">
                                            <div class="p-2 border-custome">
                                                <h5>' . $data->labelElements['BH2'] . '</h5>
                                                <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                    <label class="form-check-label" for="FA' . $data->prefix . '">' . $data->labelElements['FA'] . '</label>
                                                    <label class="switch">
                                                        <input data-group="FAcomodadas' . $data->prefix . '" id="FAcomodadas' . $data->prefix . '" name="FAcomodadas' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                                <div class="d-flex col-md-12 flex-row justify-content-between flex-wrap">
                                                    <label class="form-check-label" for="FAYNA' . $data->prefix . '">' . $data->labelElements['FAYNA'] . '</label>
                                                    <label class="switch">
                                                        <input data-group="FNAcomodadas' . $data->prefix . '" id="FNAcomodadas' . $data->prefix . '" name="FNAcomodadas' . $data->prefix . '" data-target-to="" class="form-check-input primary" type="checkbox" value="1">
                                                        <span class="slider round"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none;">
                                        <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                        <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                        <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                        <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                        <input class="flex-fill" type="text" id="idItem' . $data->prefix . '" name="idItem' . $data->prefix . '">
                                        </div>
                                    </div>
                                </form>';
                break;

            case 'add':
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <div class="col-12 col-md-12 d-flex flex-row align-items-center">
                                    <div class="form-group col-9 col-md-9">
                                        <label for="chooseElement' . $data->prefix . '">' . $data->labelElements['choose'] . '</label>
                                        <select id="chooseElement' . $data->prefix . '" name="chooseElement' . $data->prefix . '" class="form-control flex-fill">
                                            <option value="#">' . $data->labelElements['choose'] . '</option>
                                        </select>
                                    </div>
                                    <div class="col-2 col-md-2 d-flex justify-content-center align-items-center">
                                        <span id="addItem' . $data->prefix . '" class="bg-dark btn text-center d-flex align-items-center hand" style="color: white; font-size: 1.5em; width: 2em; height: 2em;"><i class="far fa-plus-square"></i></span>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                </div>
                            </form>';
                $content .= '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                break;

            case 'copy':
                $groupMenuCopy = '';
                foreach ($data->menuCopy as $key => $value) {
                    $groupMenuCopy .= '<i id="menuCopy' . $key . '" data-title="' . $key . '" class="icon-custome-menu ' . $value[0] . '" title="' . $value[1] . '"></i>';
                }
                $content = '<form id="frm' . $data->prefix . '" method="post" enctype="text/plain">
                                <h5>' . $data->labelElements['copyTo'] . '</h5>
                                <div class="col-12 col-md-12 d-flex flex-row align-items-center">
                                    <div id="menuCopy' . $data->prefix . '" class="col-12 col-md-12 flex-fill bd-highlight d-flex flex-row justify-content-between align-items-center">
                                        ' . $groupMenuCopy . '
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 d-flex flex-row align-items-center">
                                    <div class="form-group col-12 col-md-12">
                                        <label for="chooseElement' . $data->prefix . '">' . $data->labelElements['choose'] . '</label>
                                        <div class="d-flex flex-row flex-nowrap justify-content-between">
                                            <select id="chooseElement' . $data->prefix . '" name="chooseElement' . $data->prefix . '" class="form-control flex-fill" disabled>
                                                <option value="#">' . $data->labelElements['choose'] . '</option>
                                            </select>
                                            <div class="d-flex flex-column col-md-2 justify-content-center">
                                                <span id="masItem' . $data->prefix . '" name="masItem' . $data->prefix . '" class="btn btn-dark d-flex flex-row justify-content-center hand"><i class="fas fa-plus-circle"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 d-flex flex-row justify-content-center p-0">
                                    <ul id="itemsContainer' . $data->prefix . '" class="list-group col-10 p-0 ul-container scroll-theme"></ul>
                                </div>
                                <div style="display: none;">
                                    <input class="flex-fill" type="text" id="tablaFrm' . $data->prefix . '" name="tabla">
                                    <input class="flex-fill" type="text" id="idFrm' . $data->prefix . '" name="idFrm' . $data->prefix . '">
                                    <input class="flex-fill" type="text" id="camposFrm' . $data->prefix . '" name="campos">
                                    <input class="flex-fill" type="text" id="peticionFrm' . $data->prefix . '" name="peticion">
                                </div>
                            </form>';
                $content .= '<div class="flex-fill bd-highlight content-table">' . $this->buildTable($data) . '</div>';
                break;
        }
        return $content;
    }

    public function modalMsg($data)
    {
        $groupButtonsLabel = '';
        foreach ($data->buttons as $key => $value) {
            $groupButtonsLabel .= '<div class="col-sm-5 flex-fill">
                                        <label title="' . $key . '" for="btnModal' . $data->prefix . $key . '" id="labelBtnModal' . $data->prefix . $key . '" name="labelBtnModal' . $data->prefix . $key . '" class="m-1 btn btn-' . $value[0] . '" ' . $value[2] . '>' . $value[1] . '</label>
                                    </div>';
        }
        $modal = '<div id="Modal' . $data->prefix . '" class="modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <i id="modalIco' . $data->prefix . '" class="' . $data->ico_class . '"></i>
                                <h5 id="modalTitle' . $data->prefix . '" class="modal-title">' . $data->title . '</h5>
                            </div>
                            <div id="modalBody' . $data->prefix . '" class="modal-body">
                                <p>' . $data->msg . '</p>
                            </div>
                            <div id="modalFooter' . $data->prefix . '" class="modal-footer">
                                ' . $groupButtonsLabel . '
                            </div>
                        </div>
                    </div>
                </div>';
        return $modal;
    }
}
