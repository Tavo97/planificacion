<?php
$url = 'https://webservicedemo.cfbocar.com/api/user';
$data = json_decode($_POST['data']);
$ch = curl_init($url);
$jsonData = array($data);

//SE ENVIA POR POST
curl_setopt($ch, CURLOPT_POST, 1);

//ADD json
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));

//cabeceras
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

//ejecucion
$result = curl_exec($ch);
echo $result;
if (curl_errno($ch)) {
    echo 'Request Error:' . curl_error($ch);
}
