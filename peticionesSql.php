<?php

use PhpParser\Node\Expr\Cast\Array_;

require_once('../../config.php');
global $DB;
$peticion = $_POST['peticion'];
$table = $_POST['tabla'];

switch ($peticion) {
    case 1: //select elementos de la base
        function createCondition($flag, $conditions, $field, $operador, $condition)
        {
            if ($flag) {
                $conditions .= " WHERE " . $field . " " . $operador . $condition;
                $flag = false;
            } else {
                $conditions .= " AND " . $field . " " . $operador . $condition;
            }
            return [$flag, $conditions];
        }
        $complements = '';
        $campos = '';
        if (isset($_POST['select']) && !empty($_POST['select'])) {
            $campos = implode(",", json_decode($_POST['select']));
        } else {
            $campos = '*';
        }
        $flag = true;
        if (isset($_POST['conditions']) && !empty($_POST['conditions'])) {
            $condiciones = json_decode($_POST['conditions']);
            $conditions = '';
            $condition = '';
            // echo print_r($condiciones);
            foreach ($condiciones as $key => $value) {
                if (strtoupper($value[0]) == 'IN') {
                    $condition = '';
                    $values = explode(',', $value[1]);
                    foreach ($values as $k => $v) {
                        $condition .= "'$v' ,";
                    }
                    $condition = "(" . substr($condition, 0, -1) . ")";
                } else {
                    $condition = " '$value[1]'";
                }

                if (strtoupper($value[0]) == 'MULTIPLE') {
                    for ($i = 1; $i < count($value); $i++) {
                        $createCond = createCondition($flag, $conditions, $key, $value[$i][0], "'" . $value[$i][1] . "'");
                        $flag = $createCond[0];
                        $conditions = $createCond[1];
                    }
                } else {
                    $createCond = createCondition($flag, $conditions, $key, $value[0], $condition);
                    $flag = $createCond[0];
                    $conditions = $createCond[1];
                }
            }
            $complements .= $conditions;
        }
        $flag = true;
        if (isset($_POST['group']) && !empty($_POST['group'])) {
            $group = json_decode($_POST['group']);
            $groupBy = '';
            foreach ($group as $key => $value) {
                if ($flag) {
                    $groupBy .= " GROUP by " . $key . " " . $value;
                    $flag = false;
                } else {
                    $groupBy .= ", " . $key . " " . $value;
                }
            }
            $complements .= $groupBy;
        }
        $flag = true;
        if (isset($_POST['order']) && !empty($_POST['order'])) {
            $order = json_decode($_POST['order']);
            $orderBy = '';
            foreach ($order as $key => $value) {
                if ($flag) {
                    $orderBy .= " ORDER BY " . $key . " " . $value;
                    $flag = false;
                } else {
                    $orderBy .= ", " . $key . " " . $value;
                }
            }
            $complements .= $orderBy;
        }
        $flag = true;
        if (isset($_POST['limit']) && !empty($_POST['limit'])) {
            $limit = json_decode($_POST['limit']);
            $limits = ' LIMIT';
            if (count($limit) == 2) {
                $limits .= " $limit[0], $limit[1]";
            } else {
                $limits .= " $limit[0]";
            }
            $complements .= $limits;
        }
        if ($registros != 'all') {
            $sql .= " LIMIT $offset $registros";
        }
        $sql = "SELECT $campos FROM {" . $_POST['tabla'] . "} $complements;";
        if (isset($_POST['rs']) && !empty($_POST['rs'])) {
            if ($_POST['rs']) {
                $result = $DB->get_recordset_sql($sql);
                $obj = array();
                foreach ($variable as $key => $value) {
                    # code...
                }
                foreach ($result as $value) {
                    array_push($obj, $value);
                }
                echo json_encode($obj);
            }
        } else {
            $result = $DB->get_records_sql($sql);
            echo json_encode($result);
        }
        break;

    case 2: //guardar en la bese de datos
        $campos = json_decode($_POST['campos']);
        $obj = array();
        foreach ($campos as $key => $value) {
            $obj[$key] = $_POST[$value];
        }
        $insert = '';
        try {
            $insert = $DB->insert_record($table, (object) $obj, $returnid = true, $bulk = true);
            echo json_encode(array($insert, $campos));
        } catch (\Throwable $th) {
            $cadenaError = $th->error;
            $errores = ['Duplicate entry' => 1, 'Data too long' => 2];
            $numError = '';
            foreach ($errores as $key => $value) {
                $res = strpos($cadenaError, $key);
                if ($res !== false) {
                    $numError = $value;
                    break;
                }
            }
            $cadenaError = explode(" ", $cadenaError);
            $dataColum = array();
            foreach ($cadenaError as $key => $value) {
                $res = strpos($value, "'");
                if ($res !== false) {
                    $data = substr($value, 1, -1);
                    array_push($dataColum, $data);
                }
            }
            $field = '';
            foreach ($campos as $key => $value) {
                foreach ($dataColum as $clave => $valor) {
                    if ($key == $valor) {
                        $field = $valor;
                        break;
                    }
                }
            }
            echo json_encode(array(false, $campos, array($th->errorcode,  $numError, $field, $th->error, $errores)));
        }
        break;

    case 3: //modificar elemento de la base de datos
        $campos = json_decode($_POST['campos']);
        $obj = array();
        foreach ($campos as $key => $value) {
            $obj[$key] = $_POST[$value];
        }
        $edit = $DB->update_record($table, (object) $obj, $bulk = false);
        echo json_encode(array($edit, $campos, $obj));
        break;

    case 4: //eliminar un elemento de la base de datos
        $conditions = array('id' => $_POST['id']);
        $delete = $DB->delete_records($table, $conditions);
        echo json_encode($delete);
        break;

    case 5: //insert multiple rows
        $camposObj = json_decode($_POST['camposObj']);
        $res = $DB->insert_records($table, $camposObj);
        echo json_encode([$res]);
        break;

    case 6: //count records
        $condiciones = '';
        $condition = array();
        if (isset($_POST['conditions']) && !empty($_POST['conditions'])) {
            $condiciones = json_decode($_POST['conditions']);
            foreach ($condiciones as $key => $value) {
                $condition[$key] = $value;
            }
        }
        $count = $DB->count_records($table, $condition);
        echo $count;
        break;

    case 7: //Buscar tiempo libre
        require_once "BuildFreeTime.php";
        $BFT = new BuildFreeTime();
        $sqlArray = $BFT->makeSqlLine($_POST['element'], $_POST['horario'], $_POST['item'], $_POST['multiple'], $_POST['periodo'], $_POST['semana']);
        $main = $DB->get_records_sql($sqlArray[0]->main);
        if (array_key_exists('levels', $sqlArray[0]) && array_key_exists('levelsId', $sqlArray[0])) {
            $levels = $DB->get_recordset_sql($sqlArray[0]->levels);
            $auxLevels = array();
            foreach ($levels as $key => $value) {
                if (array_key_exists($key, $auxLevels)) {
                    if (array_key_exists('multiple', $auxLevels[$key])) {
                        $aux = clone $auxLevels[$key]->multiple;
                        array_push($aux, $value);
                        $auxLevels[$key]->multiple = $aux;
                    } else {
                        $auxLevels[$key] = (object) array('multiple' => (object) array($auxLevels[$key], $value));
                    }
                } else {
                    $auxLevels[$key] = $value;
                }
            }
            $levels = $auxLevels;
            $levelsId = $DB->get_records_sql($sqlArray[0]->levelsId);
        } else {
            $levelsId = false;
            $levels = false;
        }

        $res = $BFT->getFreeTimeList($main, $levels, $levelsId, $sqlArray[1]);
        // echo json_encode([$main, $levels, $levelsId, $sqlArray[1]]);
        echo json_encode($res);
        break;
}
