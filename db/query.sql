CREATE TABLE mdl_blm_centros (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  abreviatura VARCHAR(20) NOT NULL,
  color VARCHAR(7) NOT NULL,
  horas INT(20) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE mdl_blm_calendario_centro (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  dia VARCHAR(100) NOT NULL,
  fecha DATE NOT NULL,
  laborable CHAR(1) NOT NULL,
  id_centro BIGINT(10) NOT NULL,
  PRIMARY KEY (id),
  INDEX (id_centro),
  FOREIGN KEY (id_centro) REFERENCES mdl_blm_centros (id) ON DELETE CASCADE
);
CREATE TABLE mdl_blm_equipos (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  descripcion TEXT NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE mdl_blm_aulas (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  abreviatura VARCHAR(20) NOT NULL,
  color VARCHAR(7) NOT NULL,
  capacidad INT(20) NOT NULL,
  id_centro BIGINT(10) NOT NULL,
  PRIMARY KEY (id),
  INDEX (id_centro),
  FOREIGN KEY (id_centro) REFERENCES mdl_blm_centros (id) ON DELETE CASCADE
);
CREATE TABLE mdl_blm_aula_equipo (
  id_aula BIGINT(10) NOT NULL,
  id_equipo BIGINT(10) NOT NULL,
  INDEX (id_equipo),
  FOREIGN KEY (id_equipo) REFERENCES mdl_blm_equipos (id) ON DELETE CASCADE,
  INDEX (id_aula),
  FOREIGN KEY (id_aula) REFERENCES mdl_blm_aulas (id) ON DELETE CASCADE
);
/* Santi */
CREATE TABLE mdl_blm_grados (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  abreviatura VARCHAR(20) NOT NULL,
  color VARCHAR(7) NOT NULL,
  nivel INT(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
CREATE TABLE mdl_blm_grupos (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  abreviatura VARCHAR(20) NOT NULL,
  color VARCHAR(7) NOT NULL,
  fecha_inicio DATE NOT NULL,
  fecha_fin DATE NOT NULL,
  id_grado BIGINT(10) NOT NULL,
  PRIMARY KEY (id),
  INDEX (id_grado),
  FOREIGN KEY (id_grado) REFERENCES mdl_blm_grados (id) ON DELETE CASCADE
);
/* Manjarrez */
CREATE TABLE mdl_blm_cursos (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL UNIQUE,
  abreviatura VARCHAR(20) NOT NULL,
  color VARCHAR(7) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE mdl_blm_profesores (
  id bigint(10) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(250) NOT NULL,
  apellido_paterno VARCHAR(250) NOT NULL,
  apellido_materno VARCHAR(250) NOT NULL,
  abreviatura VARCHAR(20) NOT NULL,
  color VARCHAR(7) NOT NULL,
  telefono VARCHAR(15) NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  nomina VARCHAR(20) NOT NULL,
  id_user bigint(10),
  PRIMARY KEY (id)
);
CREATE TABLE mdl_blm_dias_tl (
  id BIGINT(10) NOT NULL AUTO_INCREMENT,
  dia VARCHAR(20) NOT NULL,
  abreviatura VARCHAR(20) DEFAULT '0',
  estado CHAR(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (id)
);
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('lunes');
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('martes');
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('miercoles');
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('jueves');
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('viernes');
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('sabado');
INSERT INTO
  mdl_blm_dias_tl (dia)
VALUES
  ('domingo');
CREATE TABLE mdl_blm_horas_tl (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    hora VARCHAR(12) NOT NULL,
    estado CHAR(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (id)
  );
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('1');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('2');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('3');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('4');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('5');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('6');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('7');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('8');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('9');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('10');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('11');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('12');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('13');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('14');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('15');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('16');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('17');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('18');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('19');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('20');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('21');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('22');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('23');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('24');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('25');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('26');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('27');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('28');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('29');
INSERT INTO
  mdl_blm_horas_tl (hora)
VALUES
  ('30');
CREATE TABLE mdl_blm_horarios (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    ciclo_escolar VARCHAR(200) NOT NULL,
    dias CHAR(2) NOT NULL,
    lecciones CHAR(2) NOT NULL,
    fin_semana CHAR(1) NOT NULL,
    crear_horarios CHAR(1) DEFAULT 0,
    periodo_bool CHAR(1) DEFAULT 0,
    semana_bool CHAR(1) DEFAULT 0,
    tiempo_timbre_clases CHAR(1) DEFAULT 0,
    id_centro BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_centro),
    FOREIGN KEY (id_centro) REFERENCES mdl_blm_centros (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_restricciones_profesores (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    check_lim_huecos_horario CHAR(1) DEFAULT 0,
    max_huecos INT(100) DEFAULT NULL,
    limite_num_dias CHAR(1) DEFAULT 0,
    input_num_dias INT(100) DEFAULT NULL,
    min_max_lecciones_dia CHAR(1) DEFAULT 0,
    lecciones_por_dia_1 INT(100) DEFAULT NULL,
    lecciones_por_dia_2 INT(100) DEFAULT NULL,
    fin_semana_min_max CHAR(1) DEFAULT 0,
    lecciones_consecutivas CHAR(1) DEFAULT 0,
    input_lecciones_consecutivas INT(100) DEFAULT NULL,
    agotamiento_fines CHAR(1) DEFAULT 0,
    tres_huecos CHAR(1) DEFAULT 0,
    dos_huecos CHAR(1) DEFAULT 0,
    id_profesor BIGINT(10) NOT NULL,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY (id),
    INDEX (id_profesor),
    FOREIGN KEY (id_profesor) REFERENCES mdl_blm_profesores (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_estado_tl (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    tiempo_libre_estado VARCHAR(12) NOT NULL,
    estado CHAR(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (id)
  );
INSERT INTO
  mdl_blm_estado_tl (tiempo_libre_estado)
VALUES
  ('Adecuado');
INSERT INTO
  mdl_blm_estado_tl (tiempo_libre_estado)
VALUES
  ('Inadecuado');
INSERT INTO
  mdl_blm_estado_tl (tiempo_libre_estado)
VALUES
  ('Condicional');
CREATE TABLE mdl_blm_tiempo_libre_profesores (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_profesor BIGINT(10) NOT NULL,
    id_dia_tl BIGINT(10) NOT NULL,
    id_hora_tl BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY (id),
    INDEX (id_profesor),
    FOREIGN KEY (id_profesor) REFERENCES mdl_blm_profesores (id) ON DELETE CASCADE,
    INDEX (id_dia_tl),
    FOREIGN KEY (id_dia_tl) REFERENCES mdl_blm_dias_tl (id) ON DELETE CASCADE,
    INDEX (id_hora_tl),
    FOREIGN KEY (id_hora_tl) REFERENCES mdl_blm_horas_tl (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tiempo_libre_aulas (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_aula BIGINT(10) NOT NULL,
    id_dia_tl BIGINT(10) NOT NULL,
    id_hora_tl BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY (id),
    INDEX (id_aula),
    FOREIGN KEY (id_aula) REFERENCES mdl_blm_aulas (id) ON DELETE CASCADE,
    INDEX (id_dia_tl),
    FOREIGN KEY (id_dia_tl) REFERENCES mdl_blm_dias_tl (id) ON DELETE CASCADE,
    INDEX (id_hora_tl),
    FOREIGN KEY (id_hora_tl) REFERENCES mdl_blm_horas_tl (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tiempo_libre_grupos (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_grupo BIGINT(10) NOT NULL,
    id_dia_tl BIGINT(10) NOT NULL,
    id_hora_tl BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY (id),
    INDEX (id_grupo),
    FOREIGN KEY (id_grupo) REFERENCES mdl_blm_grupos (id) ON DELETE CASCADE,
    INDEX (id_dia_tl),
    FOREIGN KEY (id_dia_tl) REFERENCES mdl_blm_dias_tl (id) ON DELETE CASCADE,
    INDEX (id_hora_tl),
    FOREIGN KEY (id_hora_tl) REFERENCES mdl_blm_horas_tl (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tiempo_libre_cursos (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_curso BIGINT(10) NOT NULL,
    id_dia_tl BIGINT(10) NOT NULL,
    id_hora_tl BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY (id),
    INDEX (id_curso),
    FOREIGN KEY (id_curso) REFERENCES mdl_blm_cursos (id) ON DELETE CASCADE,
    INDEX (id_dia_tl),
    FOREIGN KEY (id_dia_tl) REFERENCES mdl_blm_dias_tl (id) ON DELETE CASCADE,
    INDEX (id_hora_tl),
    FOREIGN KEY (id_hora_tl) REFERENCES mdl_blm_horas_tl (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_leccion (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    sesiones_semana INT(10) NOT NULL,
    leccion INT(10) NOT NULL,
    id_curso BIGINT(10) NOT NULL,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY (id),
    INDEX (id_curso),
    FOREIGN KEY (id_curso) REFERENCES mdl_blm_cursos (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_relaciones (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    estado_grupo CHAR(1) DEFAULT 0,
    nota TEXT NOT NULL,
    condicion CHAR(2) NOT NULL DEFAULT 0,
    orden CHAR(1) DEFAULT 0,
    posicion CHAR(1) DEFAULT 0,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_distribucion_asignatura (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    dias_consecutivos CHAR(1) DEFAULT 0,
    lecciones_consecutivos CHAR(1) DEFAULT 0,
    distribucion_dias_lecciones CHAR(1) DEFAULT 0,
    ficha_desde INT(10) DEFAULT 0,
    ficha_hasta INT(10) DEFAULT 0,
    leccion_desde INT(10) DEFAULT 0,
    leccion_hasta INT(10) DEFAULT 0,
    id_relacion BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_relacion),
    FOREIGN KEY (id_relacion) REFERENCES mdl_blm_relaciones (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_curso_relacion (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_curso BIGINT(10) NOT NULL,
    id_relacion BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_curso),
    FOREIGN KEY (id_curso) REFERENCES mdl_blm_cursos (id) ON DELETE CASCADE,
    INDEX (id_relacion),
    FOREIGN KEY (id_relacion) REFERENCES mdl_blm_relaciones (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_grupo_relacion (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_grupo BIGINT(10) NOT NULL,
    id_relacion BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_grupo),
    FOREIGN KEY (id_grupo) REFERENCES mdl_blm_grupos (id) ON DELETE CASCADE,
    INDEX (id_relacion),
    FOREIGN KEY (id_relacion) REFERENCES mdl_blm_relaciones (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_aula_curso (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_aula BIGINT(10) NOT NULL,
    id_curso BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_aula),
    FOREIGN KEY (id_aula) REFERENCES mdl_blm_aulas (id) ON DELETE CASCADE,
    INDEX (id_curso),
    FOREIGN KEY (id_curso) REFERENCES mdl_blm_cursos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_leccion_grupo (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_leccion BIGINT(10) NOT NULL,
    id_grupo BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE,
    INDEX (id_grupo),
    FOREIGN KEY (id_grupo) REFERENCES mdl_blm_grupos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_leccion_profesor (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_leccion BIGINT(10) NOT NULL,
    id_profesor BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE,
    INDEX (id_profesor),
    FOREIGN KEY (id_profesor) REFERENCES mdl_blm_profesores (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_timbres (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    tipo CHAR(1) NOT NULL DEFAULT 0,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_horas_recesos (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    abreviatura VARCHAR(20) NOT NULL,
    inicio TIME NOT NULL,
    fin TIME DEFAULT 0,
    impresion VARCHAR(255) NULL,
    impresion_condicion_dos CHAR(1) DEFAULT 1,
    impresion_condicion_tres CHAR(1) DEFAULT 1,
    tipo CHAR(1) NOT NULL DEFAULT 0,
    tiempo_timbre_dias CHAR(1) DEFAULT 0,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_grupos_timbres (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_timbre BIGINT(10) NOT NULL,
    id_grupo BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_timbre),
    FOREIGN KEY (id_timbre) REFERENCES mdl_blm_timbres (id) ON DELETE CASCADE,
    INDEX (id_grupo),
    FOREIGN KEY (id_grupo) REFERENCES mdl_blm_grupos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_nueva_hora (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    hora_inicio TIME NOT NULL,
    hora_fin TIME NOT NULL,
    id_timbre BIGINT(10) NOT NULL,
    id_hora BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_timbre),
    FOREIGN KEY (id_timbre) REFERENCES mdl_blm_timbres (id) ON DELETE CASCADE,
    INDEX (id_hora),
    FOREIGN KEY (id_hora) REFERENCES mdl_blm_horas_recesos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_multi_dia_timbre(
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    hora_inicio TIME NOT NULL,
    hora_fin TIME NOT NULL,
    id_dia BIGINT(10) NOT NULL,
    id_timbre BIGINT(10) NOT NULL,
    id_hora BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_dia),
    FOREIGN KEY (id_dia) REFERENCES mdl_blm_dias_tl (id) ON DELETE CASCADE,
    INDEX (id_timbre),
    FOREIGN KEY (id_timbre) REFERENCES mdl_blm_timbres (id) ON DELETE CASCADE,
    INDEX (id_hora),
    FOREIGN KEY (id_hora) REFERENCES mdl_blm_horas_recesos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_multi_dia(
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    hora_inicio TIME NOT NULL,
    hora_fin TIME NOT NULL,
    id_dia BIGINT(10) NOT NULL,
    id_hora BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_dia),
    FOREIGN KEY (id_dia) REFERENCES mdl_blm_dias_tl (id) ON DELETE CASCADE,
    INDEX (id_hora),
    FOREIGN KEY (id_hora) REFERENCES mdl_blm_horas_recesos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_semanas (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    abreviatura VARCHAR(100) NOT NULL,
    tipo CHAR(1) NOT NULL DEFAULT 0,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_periodos (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    abreviatura VARCHAR(100) NOT NULL,
    tipo CHAR(1) NOT NULL DEFAULT 0,
    id_horario BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_leccion_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_leccion BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_timbres_hr (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_timbre BIGINT(10) NOT NULL,
    id_horas_recesos BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_timbre),
    FOREIGN KEY (id_timbre) REFERENCES mdl_blm_timbres (id) ON DELETE CASCADE,
    INDEX (id_horas_recesos),
    FOREIGN KEY (id_horas_recesos) REFERENCES mdl_blm_horas_recesos (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_aulas_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_aula BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_aula),
    FOREIGN KEY (id_tl_aula) REFERENCES mdl_blm_tiempo_libre_aulas (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_aulas_periodo (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_aula BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_aula),
    FOREIGN KEY (id_tl_aula) REFERENCES mdl_blm_tiempo_libre_aulas (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_aulas_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_aula BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_aula),
    FOREIGN KEY (id_tl_aula) REFERENCES mdl_blm_tiempo_libre_aulas (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_clases_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_clase BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_clase),
    FOREIGN KEY (id_tl_clase) REFERENCES mdl_blm_tiempo_libre_grupos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_clases_periodo (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_clase BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_clase),
    FOREIGN KEY (id_tl_clase) REFERENCES mdl_blm_tiempo_libre_grupos (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_clases_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_clase BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_clase),
    FOREIGN KEY (id_tl_clase) REFERENCES mdl_blm_tiempo_libre_grupos (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_profesores_periodo (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_profesor BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_profesor),
    FOREIGN KEY (id_tl_profesor) REFERENCES mdl_blm_tiempo_libre_profesores (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_profesores_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_profesor BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_profesor),
    FOREIGN KEY (id_tl_profesor) REFERENCES mdl_blm_tiempo_libre_profesores (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_profesores_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_profesor BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_profesor),
    FOREIGN KEY (id_tl_profesor) REFERENCES mdl_blm_tiempo_libre_profesores (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_cursos_periodo (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_curso BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_curso),
    FOREIGN KEY (id_tl_curso) REFERENCES mdl_blm_tiempo_libre_cursos (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_cursos_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_curso BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_curso),
    FOREIGN KEY (id_tl_curso) REFERENCES mdl_blm_tiempo_libre_cursos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tl_cursos_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_tl_curso BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    id_estado_tl BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_tl_curso),
    FOREIGN KEY (id_tl_curso) REFERENCES mdl_blm_tiempo_libre_cursos (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE,
    INDEX (id_estado_tl),
    FOREIGN KEY (id_estado_tl) REFERENCES mdl_blm_estado_tl (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tablero_principal (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_horario BIGINT(10) NOT NULL,
    id_hora BIGINT(10) NOT NULL,
    id_leccion BIGINT(10) NOT NULL,
    id_dia BIGINT(10) NOT NULL,
    ficha BIGINT(10) NOT NULL,
    duracion BIGINT(10) NOT NULL,
    activo BIGINT(10) NOT NULL,
    id_aula BIGINT(10) NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE,
    INDEX (id_aula),
    FOREIGN KEY (id_aula) REFERENCES mdl_blm_aulas (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tablero_principal_na (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_horario BIGINT(10) NOT NULL,
    id_leccion BIGINT(10) NOT NULL,
    ficha BIGINT(10) NOT NULL,
    duracion BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tablero_principal_na_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_horario BIGINT(10) NOT NULL,
    id_leccion BIGINT(10) NOT NULL,
    ficha BIGINT(10) NOT NULL,
    duracion BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_tablero_principal_periodo_semana (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    id_horario BIGINT(10) NOT NULL,
    id_hora BIGINT(10) NOT NULL,
    id_leccion BIGINT(10) NOT NULL,
    id_dia BIGINT(10) NOT NULL,
    ficha BIGINT(10) NOT NULL,
    duracion BIGINT(10) NOT NULL,
    activo BIGINT(10) NOT NULL,
    id_periodo BIGINT(10) NOT NULL,
    id_semana BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_leccion),
    FOREIGN KEY (id_leccion) REFERENCES mdl_blm_leccion (id) ON DELETE CASCADE,
    INDEX (id_horario),
    FOREIGN KEY (id_horario) REFERENCES mdl_blm_horarios (id) ON DELETE CASCADE,
    INDEX (id_periodo),
    FOREIGN KEY (id_periodo) REFERENCES mdl_blm_periodos (id) ON DELETE CASCADE,
    INDEX (id_semana),
    FOREIGN KEY (id_semana) REFERENCES mdl_blm_semanas (id) ON DELETE CASCADE
  );
CREATE TABLE mdl_blm_active_tiempo_timbre_dias (
    id BIGINT(10) NOT NULL AUTO_INCREMENT,
    tiempo_timbre_dias CHAR(1) DEFAULT 1,
    id_horas_recesos BIGINT(10) NOT NULL,
    id_timbre BIGINT(10) NOT NULL,
    PRIMARY KEY(id),
    INDEX (id_timbre),
    FOREIGN KEY (id_timbre) REFERENCES mdl_blm_timbres (id) ON DELETE CASCADE,
    INDEX (id_horas_recesos),
    FOREIGN KEY (id_horas_recesos) REFERENCES mdl_blm_horas_recesos (id) ON DELETE CASCADE
  );
-- Vistas
  CREATE VIEW mdl_blm_vista_leccion AS
SELECT
  l.id,
  sesiones_semana,
  leccion,
  id_curso,
  l.id_horario,
  c.nombre As curso,
  p.id As id_periodo,
  p.nombre As periodo,
  s.id As id_semana,
  s.nombre As semana
FROM
  mdl_blm_leccion l
  inner JOIN mdl_blm_cursos c on l.id_curso = c.id
  left JOIN mdl_blm_leccion_periodo_semana lps on l.id = lps.id_leccion
  left JOIN mdl_blm_periodos p on lps.id_periodo = p.id
  left JOIN mdl_blm_semanas s on lps.id_semana = s.id;
CREATE VIEW mdl_blm_vista_relacion AS
SELECT
  r.id,
  estado_grupo,
  nota,
  condicion,
  orden,
  posicion,
  id_curso,
  c.nombre as curso,
  id_grupo,
  g.nombre as grupo,
  gr.id as id_grupo_relacion,
  cr.id as id_curso_relacion
FROM
  mdl_blm_relaciones r
  left JOIN mdl_blm_grupo_relacion gr on r.id = gr.id_relacion
  left JOIN mdl_blm_grupos g on gr.id_grupo = g.id
  left JOIN mdl_blm_curso_relacion cr on r.id = cr.id_relacion
  left JOIN mdl_blm_cursos c on cr.id_curso = c.id;
CREATE VIEW mdl_blm_vista_grupo_relacion AS
SELECT
  gr.id,
  id_grupo,
  g.nombre as grupo,
  r.id as id_relacion
FROM
  mdl_blm_grupo_relacion gr
  left JOIN mdl_blm_relaciones r on gr.id_relacion = r.id
  left JOIN mdl_blm_grupos g on gr.id_grupo = g.id;
CREATE VIEW mdl_blm_vista_curso_relacion AS
SELECT
  cr.id,
  id_curso,
  c.nombre as curso,
  r.id as id_relacion
FROM
  mdl_blm_curso_relacion cr
  left JOIN mdl_blm_relaciones r on cr.id_relacion = r.id
  left JOIN mdl_blm_cursos c on cr.id_curso = c.id;
CREATE VIEW mdl_blm_vista_aula_curso AS
SELECT
  ac.id,
  id_aula,
  a.nombre as aula,
  id_curso,
  c.nombre as curso
FROM
  mdl_blm_aula_curso ac
  left JOIN mdl_blm_aulas a on ac.id_aula = a.id
  left JOIN mdl_blm_cursos c on ac.id_curso = c.id;
CREATE VIEW mdl_blm_vista_leccion_grupo AS
SELECT
  lg.id,
  l.id as id_leccion,
  g.id as id_grupo,
  nombre,
  id_horario
FROM
  mdl_blm_leccion_grupo lg
  left JOIN mdl_blm_leccion l on lg.id_leccion = l.id
  left JOIN mdl_blm_grupos g on lg.id_grupo = g.id;
CREATE VIEW mdl_blm_vista_leccion_profesor AS
SELECT
  lp.id,
  l.id as id_leccion,
  p.id as id_profesor,
  nombre,
  apellido_paterno,
  apellido_materno,
  id_horario
FROM
  mdl_blm_leccion_profesor lp
  left JOIN mdl_blm_leccion l on lp.id_leccion = l.id
  left JOIN mdl_blm_profesores p on lp.id_profesor = p.id;
CREATE VIEW mdl_blm_vista_horario_centro AS
SELECT
  h.id,
  h.nombre AS nombre,
  ciclo_escolar,
  dias,
  lecciones,
  (dias * lecciones) as horas_maximas,
  fin_semana,
  crear_horarios,
  periodo_bool,
  semana_bool,
  id_centro,
  c.nombre as centro,
  tiempo_timbre_clases
FROM
  mdl_blm_horarios h
  left JOIN mdl_blm_centros c on h.id_centro = c.id;
CREATE VIEW mdl_blm_vista_timbres AS
SELECT
  nh.id,
  hora_inicio,
  hora_fin,
  t.id as id_timbre,
  t.nombre as nombre_timbre,
  t.id_horario,
  hr.id as id_hora_receso,
  hr.nombre as nombre_hora,
  inicio,
  fin
FROM
  mdl_blm_nueva_hora nh
  JOIN mdl_blm_timbres t on nh.id_timbre = t.id
  JOIN mdl_blm_horas_recesos hr on nh.id_hora = hr.id;
CREATE VIEW mdl_blm_vista_timbres_grupos AS
SELECT
  gt.id,
  t.id as id_timbre,
  t.nombre as nombre_timbre,
  g.id as id_grupo,
  g.abreviatura as abreviatura_grupo,
  g.nombre as nombre_grupo
FROM
  mdl_blm_grupos_timbres gt
  JOIN mdl_blm_grupos g on gt.id_grupo = g.id
  JOIN mdl_blm_timbres t on gt.id_timbre = t.id;
CREATE VIEW mdl_blm_vista_multi_dia_timbre AS
SELECT
  mdt.id,
  hora_inicio,
  hora_fin,
  id_timbre,
  id_hora,
  d.id as id_dia,
  dia
FROM
  mdl_blm_multi_dia_timbre mdt
  JOIN mdl_blm_dias_tl d on mdt.id_dia = d.id;
CREATE VIEW mdl_blm_vista_multi_dia AS
SELECT
  md.id,
  hora_inicio,
  hora_fin,
  id_hora,
  d.id as id_dia,
  dia
FROM
  mdl_blm_multi_dia md
  JOIN mdl_blm_dias_tl d on md.id_dia = d.id;
CREATE VIEW mdl_blm_vista_horas_recesos AS
SELECT
  hr.id,
  hr.nombre,
  hr.abreviatura,
  hr.inicio,
  hr.fin,
  hr.impresion,
  hr.impresion_condicion_dos,
  hr.impresion_condicion_tres,
  hr.tipo,
  hr.tiempo_timbre_dias,
  hr.id_horario,
  t.nombre as nombre_timbre,
  thr.id_timbre
FROM
  mdl_blm_horas_recesos hr
  left JOIN mdl_blm_timbres_hr thr on hr.id = thr.id_horas_recesos
  left JOIN mdl_blm_timbres t on thr.id_timbre = t.id;
CREATE VIEW mdl_blm_vista_tl_aula_periodo AS
SELECT
  tla.id,
  id_aula,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlap.id AS id_tlm,
  tlap.id_estado_tl AS id_estado_tl,
  id_periodo
FROM
  mdl_blm_tiempo_libre_aulas tla
  inner JOIN mdl_blm_tl_aulas_periodo tlap on tla.id = tlap.id_tl_aula;
CREATE VIEW mdl_blm_vista_tl_aula_semana AS
SELECT
  tla.id,
  id_aula,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlas.id AS id_tlm,
  tlas.id_estado_tl AS id_estado_tl,
  id_semana
FROM
  mdl_blm_tiempo_libre_aulas tla
  inner JOIN mdl_blm_tl_aulas_semana tlas on tla.id = tlas.id_tl_aula;
CREATE VIEW mdl_blm_vista_tl_aula_periodo_semana AS
SELECT
  tla.id,
  id_aula,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlaps.id AS id_tlm,
  tlaps.id_estado_tl AS id_estado_tl,
  id_periodo,
  id_semana,
  CONCAT(id_periodo, '->', id_semana) AS id_periodo_semana
FROM
  mdl_blm_tiempo_libre_aulas tla
  inner JOIN mdl_blm_tl_aulas_periodo_semana tlaps on tla.id = tlaps.id_tl_aula;
CREATE VIEW mdl_blm_vista_tl_clase_periodo AS
SELECT
  tlg.id,
  id_grupo,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlgp.id AS id_tlm,
  tlgp.id_estado_tl AS id_estado_tl,
  id_periodo
FROM
  mdl_blm_tiempo_libre_grupos tlg
  inner JOIN mdl_blm_tl_clases_periodo tlgp on tlg.id = tlgp.id_tl_clase;
CREATE VIEW mdl_blm_vista_tl_clase_semana AS
SELECT
  tlg.id,
  id_grupo,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlgs.id AS id_tlm,
  tlgs.id_estado_tl AS id_estado_tl,
  id_semana
FROM
  mdl_blm_tiempo_libre_grupos tlg
  inner JOIN mdl_blm_tl_clases_semana tlgs on tlg.id = tlgs.id_tl_clase;
CREATE VIEW mdl_blm_vista_tl_clase_periodo_semana AS
SELECT
  tlg.id,
  id_grupo,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlgps.id AS id_tlm,
  tlgps.id_estado_tl AS id_estado_tl,
  id_periodo,
  id_semana,
  CONCAT(id_periodo, '->', id_semana) AS id_periodo_semana
FROM
  mdl_blm_tiempo_libre_grupos tlg
  inner JOIN mdl_blm_tl_clases_periodo_semana tlgps on tlg.id = tlgps.id_tl_clase;
CREATE VIEW mdl_blm_vista_tl_profesor_periodo AS
SELECT
  tlp.id,
  id_profesor,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlpp.id AS id_tlm,
  tlpp.id_estado_tl AS id_estado_tl,
  id_periodo
FROM
  mdl_blm_tiempo_libre_profesores tlp
  inner JOIN mdl_blm_tl_profesores_periodo tlpp on tlp.id = tlpp.id_tl_profesor;
CREATE VIEW mdl_blm_vista_tl_profesor_semana AS
SELECT
  tlp.id,
  id_profesor,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlps.id AS id_tlm,
  tlps.id_estado_tl AS id_estado_tl,
  id_semana
FROM
  mdl_blm_tiempo_libre_profesores tlp
  inner JOIN mdl_blm_tl_profesores_semana tlps on tlp.id = tlps.id_tl_profesor;
CREATE VIEW mdl_blm_vista_tl_profesor_periodo_semana AS
SELECT
  tlp.id,
  id_profesor,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlpps.id AS id_tlm,
  tlpps.id_estado_tl AS id_estado_tl,
  id_periodo,
  id_semana,
  CONCAT(id_periodo, '->', id_semana) AS id_periodo_semana
FROM
  mdl_blm_tiempo_libre_profesores tlp
  inner JOIN mdl_blm_tl_profesores_periodo_semana tlpps on tlp.id = tlpps.id_tl_profesor;
CREATE VIEW mdl_blm_vista_tl_curso_periodo AS
SELECT
  tlc.id,
  id_curso,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlcp.id AS id_tlm,
  tlcp.id_estado_tl AS id_estado_tl,
  id_periodo
FROM
  mdl_blm_tiempo_libre_cursos tlc
  inner JOIN mdl_blm_tl_cursos_periodo tlcp on tlc.id = tlcp.id_tl_curso;
CREATE VIEW mdl_blm_vista_tl_curso_semana AS
SELECT
  tlc.id,
  id_curso,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlcs.id AS id_tlm,
  tlcs.id_estado_tl AS id_estado_tl,
  id_semana
FROM
  mdl_blm_tiempo_libre_cursos tlc
  inner JOIN mdl_blm_tl_cursos_semana tlcs on tlc.id = tlcs.id_tl_curso;
CREATE VIEW mdl_blm_vista_tl_curso_periodo_semana AS
SELECT
  tlc.id,
  id_curso,
  id_dia_tl,
  id_hora_tl,
  id_horario,
  tlcps.id AS id_tlm,
  tlcps.id_estado_tl AS id_estado_tl,
  id_periodo,
  id_semana,
  CONCAT(id_periodo, '->', id_semana) AS id_periodo_semana
FROM
  mdl_blm_tiempo_libre_cursos tlc
  inner JOIN mdl_blm_tl_cursos_periodo_semana tlcps on tlc.id = tlcps.id_tl_curso;
CREATE VIEW mdl_blm_vista_prueba_lecciones AS
SELECT
  l.id as id_leccion,
  l.id_horario,
  l.sesiones_semana,
  l.leccion,
  (l.sesiones_semana * l.leccion) as lecciones_max,
  lp.id As id_leccion_profesor,
  lp.id_profesor,
  concat (
    p.nombre,
    ' ',
    p.apellido_paterno,
    ' ',
    p.apellido_materno
  ) as nombre,
  c.id as id_curso,
  c.nombre as nombre_curso,
  lg.id_grupo,
  g.nombre as nombre_grupo,
  ac.id_aula,
  a.nombre as nombre_aula,
  id_periodo,
  per.nombre AS periodo,
  per.tipo AS tipo_periodo,
  id_semana,
  s.nombre AS semana,
  s.tipo AS tipo_semana
FROM
  mdl_blm_leccion l
  INNER JOIN mdl_blm_leccion_profesor lp on lp.id_leccion = l.id
  INNER JOIN mdl_blm_profesores p on p.id = lp.id_profesor
  INNER JOIN mdl_blm_leccion_grupo lg on lg.id_leccion = l.id
  INNER JOIN mdl_blm_aula_curso ac on ac.id_curso = l.id_curso
  INNER JOIN mdl_blm_aulas a on ac.id_aula = a.id
  INNER JOIN mdl_blm_cursos c on l.id_curso = c.id
  INNER JOIN mdl_blm_grupos g on lg.id_grupo = g.id
  LEFT JOIN mdl_blm_leccion_periodo_semana lps on l.id = lps.id_leccion
  LEFT JOIN mdl_blm_periodos per on lps.id_periodo = per.id
  LEFT JOIN mdl_blm_semanas s on lps.id_semana = s.id;
CREATE VIEW mdl_blm_vista_tablero AS
SELECT
  tp.id,
  lec.id_horario,
  tp.ficha,
  tp.activo,
  tp.id_leccion,
  tp.id_dia as dia,
  tp.id_hora as hora,
  tp.duracion,
  tc.id as id_curso,
  tc.nombre as nombre_curso,
  tc.abreviatura as abreviatura_curso,
  tpr.id as id_profesor,
  tpr.nombre as nombre_profesor,
  tpr.abreviatura as abreviatura_profesor,
  tpr.color,
  tg.id as id_grupo,
  tg.nombre as nombre_grupo,
  tg.abreviatura as abreviatura_grupo,
  ta.id as id_aula,
  ta.nombre as nombre_aula,
  ta.abreviatura as abreviatura_aula
FROM
  mdl_blm_tablero_principal tp
  INNER JOIN mdl_blm_leccion lec on lec.id = tp.id_leccion
  INNER JOIN mdl_blm_cursos tc on tc.id = lec.id_curso
  INNER JOIN mdl_blm_leccion_profesor plec on plec.id_leccion = lec.id
  INNER JOIN mdl_blm_profesores tpr on tpr.id = plec.id_profesor
  INNER JOIN mdl_blm_leccion_grupo glec on glec.id_leccion = lec.id
  INNER JOIN mdl_blm_grupos tg on tg.id = glec.id_grupo
  /* LEFT JOIN mdl_blm_aula_curso tac on tac.id_curso = tc.id */
  LEFT JOIN mdl_blm_aulas ta on ta.id = tp.id_aula;
CREATE VIEW mdl_blm_vista_tablero_na AS
SELECT
  tp.id,
  lec.id_horario,
  tp.ficha,
  tp.id_leccion,
  tp.duracion,
  tc.id as id_curso,
  tc.nombre as nombre_curso,
  tc.abreviatura as abreviatura_curso,
  tpr.id as id_profesor,
  tpr.nombre as nombre_profesor,
  tpr.abreviatura as abreviatura_profesor,
  tpr.color,
  tg.id as id_grupo,
  tg.nombre as nombre_grupo,
  tg.abreviatura as abreviatura_grupo,
  ta.id as id_aula,
  ta.nombre as nombre_aula,
  ta.abreviatura as abreviatura_aula
FROM
  mdl_blm_tablero_principal_na tp
  INNER JOIN mdl_blm_leccion lec on lec.id = tp.id_leccion
  INNER JOIN mdl_blm_cursos tc on tc.id = lec.id_curso
  INNER JOIN mdl_blm_leccion_profesor plec on plec.id_leccion = lec.id
  INNER JOIN mdl_blm_profesores tpr on tpr.id = plec.id_profesor
  INNER JOIN mdl_blm_leccion_grupo glec on glec.id_leccion = lec.id
  INNER JOIN mdl_blm_grupos tg on tg.id = glec.id_grupo
  LEFT JOIN mdl_blm_aula_curso tac on tac.id_curso = tc.id
  LEFT JOIN mdl_blm_aulas ta on ta.id = tac.id_aula;