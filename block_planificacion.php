<?php

/**
 * This file contains the message block class, based upon block_base.
 *
 * @package    block_message
 * @copyright  2018 Great Wall Studio <brian@greatwallstudio.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Class block_message
 *
 * @package    block_message
 * @copyright  2018 Great Wall Studio <brian@greatwallstudio.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
?>
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">
<!-- Boostrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--Fontawsemo-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<?php
class block_planificacion extends block_base
{
    function init()
    {
        $this->title = get_string('pluginname', "block_planificacion");
    }

    function get_content()
    {
        require_once "common.php";
        $items = new Common();
        global $CFG, $USER, $PAGE;
        require_once("{$CFG->libdir}/completionlib.php");
        // CSS FILES
        // $PAGE->requires->css(new moodle_url('../blocks/planificacion/bower_components/select2/dist/css/select2.min.css'));
        $PAGE->requires->css(new moodle_url('../blocks/planificacion/css/common.css'));
        $PAGE->requires->css(new moodle_url('../blocks/planificacion/css/planificacion.css'));
        // JS FILES
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/select2/dist/js/select2.min.js'), true);
        $PAGE->requires->css(new moodle_url('../blocks/planificacion/js/select2/dist/css/select2.min.css'), true);
        // $PAGE->requires->js(new moodle_url('../blocks/planificacion/bower_components/select2/dist/js/select2.full.min.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/lang/lang.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/common.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/commonPlanificacion.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/planificacion.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/tiempoLibre.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/agregarElementos.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/horarios.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/restricciones.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/relaciones.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/lecciones.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/tablaPrincipal.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/generarhorario.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/prueba.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/DIscadAsigSep.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/javascript.js'), true);
        $PAGE->requires->js(new moodle_url('../blocks/planificacion/js/html2pdf.bundle.min.js'), true);

        if ($this->content !== NULL) {
            return $this->content;
        }
        $this->content = new stdClass;
        $this->content->text = '';
        if (empty($this->instance)) {
            return $this->content;
        }

        //only show content if a user is logged in
        if ($USER->firstname) {
            $cardStart = $items->menuStart((object) array(
                'title' => get_string('titulo', "block_planificacion"),
                'prefix' => 'Planeacion',
                'table' => array(['#', '#'], ['Titulo', get_string('tit', "block_planificacion")], ['Abreviatura', get_string('abreviatura', "block_planificacion")], ['Color', get_string('color', "block_planificacion")]),
                'buttons' => array(
                    'Agregar' => ['agregar', 'fas fa-plus-circle', get_string('agregar', "block_planificacion")],
                    'Editar' => ['editar', 'fas fa-edit', get_string('editar', "block_planificacion")],
                    'Eliminar' => ['eliminar', 'fas fa-trash', get_string('eliminar', "block_planificacion")],
                    'TiempoLibre' => ['tiempo', 'fas fa-clock', get_string('tiempolibre', "block_planificacion")],
                    'Restricciones' => ['restricciones', 'fas fa-hashtag', get_string('Restricciones', "block_planificacion")],
                    'Lecciones' => ['lecciones', 'far fa-window-restore', get_string('lecciones', "block_planificacion")],
                    'Escuela' => ['escuela', 'fas fa-sliders-h', get_string('escuela', "block_planificacion")],
                    'Relaciones' => ['relaciones', 'fas fa-project-diagram', get_string('relaciones', "block_planificacion")],
                    'Prueba' => ['Prueba', 'fas fa-check-double', get_string('Prueba', "block_planificacion")],
                    'GHorario' => ['ghorario', 'fas fa-bell', get_string('ghorario', "block_planificacion")],
                    'TablaP' => ['tablap', 'fas fa fa-table', get_string('tablap', "block_planificacion")],
                    // 'Development' => ['development', 'fas fa-code', get_string('development', "block_planificacion")]
                ),
                'mainMenu' => array(
                    // 'Test' => ['fas fa-laptop-code', get_string('test', "block_planificacion")],
                    'Cursos' => ['fas fa-book', get_string('cursos', "block_planificacion")],
                    'Clases' => ['fas fa-user-friends', get_string('grupos', "block_planificacion")],
                    'Centros' => ['fas fa-school', get_string('centros', "block_planificacion")],
                    'Aulas' => ['fas fa-door-open', get_string('aulas', "block_planificacion")],
                    'Profesores' => ['fas fa-graduation-cap', get_string('profesores', "block_planificacion")],
                    'Grados' => ['fab fa-hubspot', get_string('grados', "block_planificacion")]
                )
            ));

            $modal = $items->modal((object) array(
                'title' => get_string('titulo', "block_planificacion"),
                'prefix' => 'Planeacion',
                'buttons' => array('Agregar' => ['primary', get_string('agregar', "block_planificacion")], 'Cancelar' => ['danger', get_string('cancelar', "block_planificacion")]),
                'body' => 'mainForm',
                'labelElements' => array(
                    'nombre' => get_string('nombre', "block_planificacion"),
                    'apellidoPat' => get_string('apellidopat', "block_planificacion"),
                    'apellidoMat' => get_string('apellidomat', "block_planificacion"),
                    'abreviatura' => get_string('abreviatura', "block_planificacion"),
                    'email' => get_string('email', "block_planificacion"),
                    'telefono' => get_string('telefono', "block_planificacion"),
                    'color' => get_string('color', "block_planificacion"),
                    'tutor' => get_string('tutorclase', "block_planificacion"),
                    'tutorBtn' => get_string('cambiar', "block_planificacion"),
                    'fechaIn' => get_string('fechaIn', "block_planificacion"),
                    'fechaFin' => get_string('fechaFin', "block_planificacion"),
                    'grado' => get_string('grado', "block_planificacion"),
                    'choose' => get_string('choose', "block_planificacion"),
                    'aulas' => get_string('aulas', "block_planificacion"),
                    'capacidad' => get_string('capacidad', "block_planificacion"),
                    'aulasBtn' => get_string('cambiar', "block_planificacion"),
                    'horas' => get_string('horas', "block_planificacion"),
                    'grpDatosGenerales' => get_string('datosgenerales', "block_planificacion"),
                    'grpColor' => get_string('color', "block_planificacion"),
                    'grpClase' => get_string('grupos', "block_planificacion"),
                    'grpAulas' => get_string('aulas', "block_planificacion"),
                    'grpCentros' => get_string('centros', "block_planificacion"),
                    'addClassroom' => get_string('aulaAgregar', "block_planificacion")
                )
            ));

            $modalTiempo = $items->modal((object) array(
                'title' => get_string('tiempolibre', "block_planificacion"),
                'prefix' => 'Tiempo',
                // 'CrearTodas' => ['success', get_string('CambiarTodas', "block_planificacion"), 'data-dismiss="modal"'],
                'buttons' => array('Close' => ['danger', get_string('close', "block_planificacion"), 'data-dismiss="modal"']),
                'body' => 'tiempoLibre',
                'table' => array(['#', '#']),
                'labelElements' => array(
                    // 'lunes' => get_string('nombre', "block_planificacion"),
                    // 'martes' => get_string('apellidopat', "block_planificacion"),
                    // 'miercoles' => get_string('apellidomat', "block_planificacion"),
                    // 'jueves' => get_string('abreviatura', "block_planificacion"),
                    // 'viernes' => get_string('email', "block_planificacion"),
                    // 'crearentodas' => get_string('CambiarTodas', "block_planificacion")
                    'igualptperiodos' => get_string('IgualPTPeriodos', "block_planificacion"),
                    'igualptsemanas' => get_string('IgualPTSemanas', "block_planificacion")
                )
            ));

            $modalRestricciones = $items->modal((object) array(
                'title' => get_string('Restricciones', "block_planificacion"),
                'prefix' => 'Restricciones',
                'buttons' => array('Agregar' => ['primary', get_string('agregar', "block_planificacion")], 'Cancelar' => ['danger', get_string('cancelar', "block_planificacion")]),
                'body' => 'restricciones',
                'labelElements' => array(
                    'limitehuecos' => get_string('limitehuecos', "block_planificacion"),
                    'maximahuecos' => get_string('maximahuecos', "block_planificacion"),
                    'limitardias' => get_string('limitardias', "block_planificacion"),
                    'diasenque' => get_string('diasenque', "block_planificacion"),
                    'notadiasenque' => get_string('notadiasenque', "block_planificacion"),
                    'leccionesxdia' => get_string('leccionesxdia', "block_planificacion"),
                    'numeroleccionesxdia' => get_string('numeroleccionesxdia', "block_planificacion"),
                    'noctrlfinde' => get_string('noctrlfinde', "block_planificacion"),
                    'leccionesconsecutivas' => get_string('leccionesconsecutivas', "block_planificacion"),
                    'maxleccionesconsecutivas' => get_string('maxleccionesconsecutivas', "block_planificacion"),
                    'notamaxleccionesconsecutivas' => get_string('notamaxleccionesconsecutivas', "block_planificacion"),
                    'agotamiento' => get_string('agotamiento', "block_planificacion"),
                    'doshuecos' => get_string('doshuecos', "block_planificacion"),
                    'treshuecos' => get_string('treshuecos', "block_planificacion")
                )
            ));
            $modalLecciones = $items->modal((object) array(
                'title' => get_string('lecciones', "block_planificacion"),
                'prefix' => 'Lecciones',
                'buttons' => array(
                    'Nueva' => ['success', get_string('new', "block_planificacion")],
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Eliminar' => ['danger', get_string('eliminar', "block_planificacion")],
                    'Copiar' => ['warning', get_string('copy', "block_planificacion")],
                    // 'Añadir' => ['info', get_string('agregar', "block_planificacion")],
                    'Cerrar' => ['dark', get_string('close', "block_planificacion")]
                ),
                'body' => 'lecciones',
                'table' => array(
                    ['#', '#'],
                    ['NLeccion', get_String('nleccion', 'block_planificacion')],
                    ['Profesor', get_String('profesor', 'block_planificacion')],
                    ['Clase', get_String('grupo', 'block_planificacion')],
                    ['Asignatura', get_String('curso', 'block_planificacion')],
                    ['SessionSemana', get_String('sesionSemana', 'block_planificacion')],
                    ['Leccion', get_String('leccion', 'block_planificacion')],
                    ['AulaAsignatura', get_String('AulaAsignatura', 'block_planificacion')]
                    // ['Semana', get_String('semana', 'block_planificacion')],
                    // ['Periodo', get_String('periodo', 'block_planificacion')]
                )
            ));

            $modalTimbres = $items->modal((object) array(
                'title' => get_string('Timbre', "block_planificacion"),
                'prefix' => 'Timbres',
                'buttons' => array(
                    'Nueva' => ['success', get_string('recesos', "block_planificacion")],
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Eliminar' => ['danger', get_string('eliminar', "block_planificacion")],
                    'Cerrar' => ['dark', get_string('close', "block_planificacion")]
                ),
                'body' => 'timbres',
                'labelElements' => array(
                    'tiempDif' => get_string('Tlddclas', "block_planificacion"),
                    'ehdf' => get_string('EHDTAD', "block_planificacion"),
                    'ValidoPara' => get_string('VaPara', "block_planificacion"),
                    'editar' => get_string('editar', "block_planificacion")
                ),
                'table' => array(
                    ['#', '#'],
                    ['Nombre', get_String('nombre', 'block_planificacion')],
                    ['Abreviatura', get_String('abreviatura', 'block_planificacion')],
                    ['Inicio', get_String('inicio', 'block_planificacion')],
                    ['Fin', get_String('fin', 'block_planificacion')],
                    ['Imprimir', get_String('impresiones', 'block_planificacion')],
                    ['Timbre', get_String('Timbre', 'block_planificacion')]
                )
            ));

            $modalRelaciones = $items->modal((object) array(
                'title' => get_string('relaciones', "block_planificacion"),
                'prefix' => 'Relaciones',
                'buttons' => array(
                    'Añadir' => ['info', get_string('agregar', "block_planificacion")],
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Eliminar' => ['danger', get_string('eliminar', "block_planificacion")],
                    'Cerrar' => ['dark', get_string('close', "block_planificacion")]
                ),
                'body' => 'relaciones',
                'table' => array(
                    ['#', '#'],
                    ['Asignatura', get_String('curso', 'block_planificacion')],
                    ['Incluye', get_String('incluye', 'block_planificacion')],
                    ['descripcion', get_String('descripcion', 'block_planificacion')]
                )
            ));

            $modalRestriccionesCurso = $items->modal((object) array(
                'title' => get_string('relaciones', "block_planificacion"),
                'prefix' => 'RestriccionesClases',
                'buttons' => array(
                    'Agregar' => ['primary', get_string('agregar', "block_planificacion")],
                    'Cancelar' => ['danger', get_string('cancelar', "block_planificacion")]
                ),
                'body' => 'clases',
                'labelElements' => array(
                    'asignaturas' => get_string('asignaturas', "block_planificacion"),
                    'elejirAsignaturas' => get_string('EAsig', "block_planificacion"),
                    'Clas' => get_string('Clas', "block_planificacion"),
                    'Todas' => get_string('Todas', "block_planificacion"),
                    'Eleccion' => get_string('Eleccion', "block_planificacion"),
                    'CClases' => get_string('CClases', "block_planificacion"),
                    // 'ImpERelacion' => get_string('ImpERelacion', "block_planificacion"),
                    // 'Desac' => get_string('Desac', "block_planificacion"),
                    'Nota' => get_string('Nota', "block_planificacion"),
                    'Condicion' => get_string('Condicion', "block_planificacion"),
                    'Condicion1' => get_string('Condicion1', "block_planificacion"),
                    'Condicion2' => get_string('Condicion2', "block_planificacion"),
                    'Condicion3' => get_string('Condicion3', "block_planificacion"),
                    'config' => get_string('config', "block_planificacion"),
                    'Condicion4' => get_string('Condicion4', "block_planificacion"),
                    'Condicion5' => get_string('Condicion5', "block_planificacion"),
                    'OrEsp' => get_string('OrEsp', "block_planificacion"),
                    'OrArb' => get_string('OrArb', "block_planificacion"),
                    'CambOrden' => get_string('CambOrden', "block_planificacion"),
                    'Condicion6' => get_string('Condicion6', "block_planificacion"),
                    'Condicion10' => get_string('Condicion10', "block_planificacion"),
                    'Condicion11' => get_string('Condicion11', "block_planificacion"),
                    'Condicion13' => get_string('Condicion13', "block_planificacion"),
                    'cambiar' => get_string('cambiar', "block_planificacion")
                )
            ));
            $modalTimbre = $items->modal((object) array(
                'title' => get_string('Timbre', "block_planificacion"),
                'prefix' => 'Timbre',
                'buttons' => array('Agregar' => ['primary', get_string('agregar', "block_planificacion")], 'Close' => ['info', get_string('close', "block_planificacion")]),
                'body' => 'timbre',
                // 'table' => array(['#', '#']),
                'labelElements' => array(
                    'identificadorRecreo' => get_string('IdentDRecreo', "block_planificacion"),
                    'abreviatura' => get_string('abreviatura', "block_planificacion"),
                    'ubicacionRecreo' => get_string('UbicRecreo', "block_planificacion"),
                    'inicio' => get_string('inicio', "block_planificacion"),
                    'fin' => get_string('fin', "block_planificacion"),
                    'Recreo' => get_string('recreo', "block_planificacion"),
                    'textoImpresion' => get_string('TextPimp', "block_planificacion"),
                    'Impresiones' => get_string('impresiones', "block_planificacion"),
                    // 'imprimirPeriodoResumen' => get_string('impPrh', "block_planificacion"),
                    'ImprimirPeriodoIndividual' => get_string('impPhidClases', "block_planificacion"),
                    'imprimirPeriodoIndividualDe' => get_string('impPhiProfesores', "block_planificacion"),
                    // 'imprimirPeriodoActual' => get_string('impPhiD', "block_planificacion"),
                    'imprimirTodos' => get_string('IMPTT', "block_planificacion")
                )
            ));

            $modalEscuela = $items->modal((object) array(
                'title' => get_string('escuela', "block_planificacion"),
                'prefix' => 'Escuela',
                'buttons' => array(
                    'Agregar' => ['success', get_string('agregar', "block_planificacion")],
                    'Close' => ['danger', get_string('close', "block_planificacion")]
                ),
                'body' => 'escuela',
                // 'table' => array(['#', '#']),
                'labelElements' => array(
                    'centro' => get_string('NCentro', "block_planificacion"),
                    'cicloEscolar' => get_string('CEscolar', "block_planificacion"),
                    'nombreHorario' => get_string('nombreHorario', "block_planificacion"),
                    'leccionesDia' => get_string('CLD', "block_planificacion"),
                    'renombrarPeriodo' => get_string('T/RPeriodo', "block_planificacion"),
                    'leccionesCero' => get_string('LC', "block_planificacion"),
                    'generadorCero' => get_string('PCP0', "block_planificacion"),
                    'numeroDias' => get_string('ND', "block_planificacion"),
                    'renombrarDia' => get_string('R/Dias', "block_planificacion"),
                    'finSemana' => get_string('FSemana', "block_planificacion"),
                    'personalHorarios' => get_string('MS/P', "block_planificacion"),
                    'renombrar' => get_string('renombrar', "block_planificacion"),
                    'DPeriodos' => get_string('DPeriodos', "block_planificacion"),
                    'DSemanas' => get_string('DfSemana', "block_planificacion")
                )
            ));

            $modalRenomDias = $items->modal((object) array(
                'title' => get_string('RDias', "block_planificacion"),
                'prefix' => 'Rdias',
                'buttons' => array(
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'Rdias',
                'labelElements' => array(
                    'notaDias' => get_string('NotaDias', "block_planificacion"),
                    'diasRenombrados' => get_string('DiasRenombrados', "block_planificacion"),
                    'Nota2Dias' => get_string('Nota2Dias', "block_planificacion")
                ),
                'table' => array(
                    ['#', '#'],
                    ['Titulo', get_string('TituloRDias', 'block_planificacion')],
                    ['abreviatura', get_string('abreviatura', 'block_planificacion')],
                    ['Descripcion', get_string('descripcion', 'block_planificacion')]
                )
            ));

            $modalDefSemanas = $items->modal((object) array(
                'title' => get_string('DfSemana', "block_planificacion"),
                'prefix' => 'DefSemanas',
                'buttons' => array(
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'DefSemanas',
                'labelElements' => array(
                    'notaSemana' => get_string('NotaSemana1', "block_planificacion"),
                    'numSemana' => get_string('NumSemana', "block_planificacion"),
                    'notaSemana2' => get_string('NotaSemana2', "block_planificacion")
                ),
                'table' => array(
                    ['#', '#'],
                    ['Titulo', get_string('TituloRDias', 'block_planificacion')],
                    ['abreviatura', get_string('abreviatura', 'block_planificacion')],
                    ['Descripcion', get_string('descripcion', 'block_planificacion')]
                )
            ));

            $modalDefPeriodos = $items->modal((object) array(
                'title' => get_string('DPeriodos', "block_planificacion"),
                'prefix' => 'DefPeriodos',
                'buttons' => array(
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'DefPeriodos',
                'labelElements' => array(
                    'notaPeriodo1' => get_string('NotaPeriodo1', "block_planificacion"),
                    'numPeriodos' => get_string('NumPeriodos', "block_planificacion"),
                    'notaPeriodo2' => get_string('NotaPeriodo2', "block_planificacion")
                ),
                'table' => array(
                    ['#', '#'],
                    ['Titulo', get_string('TituloRDias', 'block_planificacion')],
                    ['abreviatura', get_string('abreviatura', 'block_planificacion')],
                    ['Descripcion', get_string('descripcion', 'block_planificacion')]
                )
            ));

            $modalObjeto = $items->modal((object) array(
                'title' => get_string('Objeto', "block_planificacion"),
                'prefix' => 'Objeto',
                'buttons' => array(
                    'Aceptar' => ['success', get_string('aceptar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'Objeto',
                'labelElements' => array(
                    'nombre' => get_string('nombre', "block_planificacion"),
                    'abreviatura' => get_string('abreviatura', "block_planificacion")
                )
            ));

            $modalPeriodo = $items->modal((object) array(
                'title' => get_string('periodoName', "block_planificacion"),
                'prefix' => 'Periodo',
                'buttons' => array(
                    'Aceptar' => ['success', get_string('aceptar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'Periodo',
                'labelElements' => array(
                    'p1' => get_string('P1', "block_planificacion"),
                    'notaPeriodoP' => get_string('NotaPeriodoP', "block_planificacion"),
                    'lunes' => get_string('Lunes', "block_planificacion"),
                    'martes' => get_string('Martes', "block_planificacion"),
                    'miercoles' => get_string('Miercoles', "block_planificacion"),
                    'jueves' => get_string('Jueves', "block_planificacion"),
                    'viernes' => get_string('Viernes', "block_planificacion"),
                    'sabado' => get_string('Sabado', "block_planificacion"),
                    'domingo' => get_string('Domingo', "block_planificacion"),
                    'inicio' => get_string('inicio', "block_planificacion"),
                    'fin' => get_string('fin', "block_planificacion")
                )
            ));

            $modalEleccion = $items->modal((object) array(
                'title' => get_string('Eleccion', "block_planificacion"),
                'prefix' => 'Eleccion',
                'buttons' => array(
                    'Close' => ['danger', get_string('close', "block_planificacion")]
                ),
                'body' => 'Eleccion',
                'labelElements' => array(
                    'eleccion' => get_string('Eleccion', "block_planificacion"),
                    'cltselect' => get_string('ClTselect', "block_planificacion"),
                    'todas' => get_string('Todas', "block_planificacion"),
                    'selrpida' => get_string('SelRpida', "block_planificacion"),
                    'Borrar' =>  get_string('eliminar', "block_planificacion")
                ),
                'table' => array(
                    ['#', '#'],
                    ['Titulo', get_string('TituloRDias', 'block_planificacion')],
                    ['abreviatura', get_string('abreviatura', 'block_planificacion')]
                )
            ));

            $modalCombinacionSemanas = $items->modal((object) array(
                'title' => get_string('Combinacion', "block_planificacion"),
                'prefix' => 'CombinacionSemanas',
                'buttons' => array(
                    'Aceptar' => ['success', get_string('aceptar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion"), 'data-dismiss="modal"']
                ),
                'body' => 'CombinacionSemanas',
                'labelElements' => array(
                    'nombre' => get_string('nombre', "block_planificacion"),
                    'abreviatura' => get_string('abreviatura', "block_planificacion"),
                    'lcombinacionsemana' => get_string('LabelCombinacion', "block_planificacion"),
                    'op1semana' => get_string('OP1CombinacionSemanas', "block_planificacion"),
                    'op2semana' => get_string('OP2CombinacionSemanas', "block_planificacion")
                )
            ));

            $modalCombinacionPeriodos = $items->modal((object) array(
                'title' => get_string('Combinacion', "block_planificacion"),
                'prefix' => 'CombinacionPeriodo',
                'buttons' => array(
                    'Aceptar' => ['success', get_string('aceptar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion"), 'data-dismiss="modal"']
                ),
                'body' => 'CombinacionPeriodo',
                'labelElements' => array(
                    'nombre' => get_string('nombre', "block_planificacion"),
                    'abreviatura' => get_string('abreviatura', "block_planificacion"),
                    'lcombinacionperiodos' => get_string('LabelCombinacion', "block_planificacion"),
                    'op1periodo' => get_string('OP1CombinacionPeriodos', "block_planificacion"),
                    'op2periodo' => get_string('OP2CombinacionPeriodos', "block_planificacion")
                )
            ));

            $modalCombinacionDias = $items->modal((object) array(
                'title' => get_string('Combinacion', "block_planificacion"),
                'prefix' => 'CombinacionDias',
                'buttons' => array(
                    'Aceptar' => ['success', get_string('aceptar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion"), 'data-dismiss="modal"']
                ),
                'body' => 'CombinacionDias',
                'labelElements' => array(
                    'nombre' => get_string('nombre', "block_planificacion"),
                    'abreviatura' => get_string('abreviatura', "block_planificacion"),
                    'lcombinaciondias' => get_string('LabelCombinacion', "block_planificacion"),
                    'op1dia' => get_string('OP1CombinacionDias', "block_planificacion"),
                    'op2dia' => get_string('OP2CombinacionDias', "block_planificacion")
                )
            ));


            $modalEscuelaTabla = $items->modal((object) array(
                'title' => get_string('escuela', "block_planificacion"),
                'prefix' => 'EscuelaTabla',
                'buttons' => array(
                    'Agregar' => ['success', get_string('agregar', "block_planificacion")],
                    'Editar' => ['primary', get_string('editar', "block_planificacion")],
                    'Borrar' => ['danger', get_string('eliminar', "block_planificacion")],
                    'VPrevia' => ['dark', get_string('vistaprevia', "block_planificacion")],
                    'Usar' => ['dark', get_string('usar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'EscuelaTabla',
                'table' => array(
                    ['#', '#'],
                    ['nombreHorario', get_string('nombreHorario', 'block_planificacion')],
                    ['NDcentro', get_string('NCentro', 'block_planificacion')],
                    ['CEscolar', get_string('CEscolar', 'block_planificacion')],
                    ['CLD', get_string('CLD', 'block_planificacion')],
                    ['ND', get_string('ND', 'block_planificacion')]
                )
            ));

            $modalVPrevia = $items->modal((object) array(
                'title' => get_string('vistaprevia', "block_planificacion"),
                'prefix' => 'VPrevia',
                'buttons' => array(
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'VPrevia',
                'table' => array(
                    ['#', '#']
                )
            ));

            $modalLeccion = $items->modal((object) array(
                'title' => get_string('leccion', "block_planificacion"),
                'prefix' => 'Leccion',
                'buttons' => array(
                    'Agregar' => ['primary', get_string('agregar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'leccion',
                // 'table' => array(['#', '#']),
                'labelElements' => array(
                    'profesor' => get_string('profesor', "block_planificacion"),
                    'siguienteProfesor' => get_string('siguienteProfesor', "block_planificacion"),
                    'asignatura' => get_string('asignatura', "block_planificacion"),
                    'grupo' => get_string('grupo', "block_planificacion"),
                    'grupoUnido' => get_string('grupoUnido', "block_planificacion"),
                    'sesionSemana' => get_string('sesionSemana', "block_planificacion"),
                    'leccion' => get_string('leccion', "block_planificacion"),
                    'masSemanas' => get_string('masSemanas', "block_planificacion"),
                    'aulaFija' => get_string('aulaFija', "block_planificacion"),
                    'aulaProfesor' => get_string('aulaProfesor', "block_planificacion"),
                    'aulaCompartida' => get_string('aulaCompartida', "block_planificacion"),
                    'AulaAsignatura' => get_string('AulaAsignatura', "block_planificacion"),
                    'otrasAulasPermitidas' => get_string('otrasAulasPermitidas', "block_planificacion"),
                    'masAulas' => get_string('masAulas', "block_planificacion"),
                    'addAula' => get_string('aulaAgregar', "block_planificacion"),
                    'semana' => get_string('semana', "block_planificacion"),
                    'periodo' => get_string('periodo', "block_planificacion")
                )
            ));

            $modalGNuevHorario = $items->modal((object) array(
                'title' => get_string('gnuevohorario', "block_planificacion"),
                'prefix' => 'GNuevHorario',
                'buttons' => array(
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'GNuevHorario',
                'labelElements' => array(
                    'ghorario' => get_string('ghorario', "block_planificacion"),
                    'prueba' => get_string('Prueba', "block_planificacion"),
                    'prc' => get_string('PRC', "block_planificacion"),
                    'cdn' => get_string('cdn', "block_planificacion"),
                    'estricto' => get_string('Estricto', "block_planificacion"),
                    'chorario' => get_string('CHorario', "block_planificacion"),
                    'normal' => get_string('Normal', "block_planificacion"),
                    'grande' => get_string('Grande', "block_planificacion"),
                    'muygrande' => get_string('MuyGrande', "block_planificacion"),
                    'psdg' => get_string('Psdg', "block_planificacion")
                )
            ));

            $modalVCRelajas = $items->modal((object) array(
                'title' => get_string('CR', "block_planificacion"),
                'prefix' => 'VCRelajadas',
                'buttons' => array(
                    'mcr' => ['primary', get_string('MCR', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'VCRelajadas',
                'labelElements' => array(
                    'ltcr' => get_string('LTCR', "block_planificacion")
                )
            ));

            $modalDcls = $items->modal((object) array(
                'title' => get_string('DCALSTD', "block_planificacion"),
                'prefix' => 'Dcls',
                'buttons' => array(
                    'Agregar' => ['primary', get_string('aceptar', "block_planificacion")],
                    'Cancelar' => ['danger', get_string('cancelar', "block_planificacion")]
                ),
                'body' => 'Dcls',
                'labelElements' => array(
                    'lfcdot' => get_string('LFCDOT', "block_planificacion"),
                    'lfcndot' => get_string('LFCNDOT', "block_planificacion"),
                    'sdc' => get_string('SDC', "block_planificacion"),
                    'endynlpdf' => get_string('ENDYNLPDF', "block_planificacion"),
                    'dfhnd' => get_string('DFHND', "block_planificacion"),
                    'desde' => get_string('Desde', "block_planificacion"),
                    'hasta' => get_string('Hasta', "block_planificacion"),
                    'nldi' => get_string('NLDI', "block_planificacion")
                )
            ));

            $modalAviso = $items->modal((object) array(
                'title' => get_string('AvisoTitle', "block_planificacion"),
                'prefix' => 'Aviso',
                'buttons' => array(
                    'Agregar' => ['primary', get_string('aceptar', "block_planificacion")],
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'Aviso',
                'labelElements' => array(
                    'Aviso' => get_string('Aviso', "block_planificacion")
                )
            ));

            $modalAsesor = $items->modal((object) array(
                'title' => get_string('Asesor', "block_planificacion"),
                'prefix' => 'Asesor',
                'buttons' => array(
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'Asesor',
                'labelElements' => array(
                    'pc' => get_string('PC', "block_planificacion"),
                    'rvpc' => get_string('RVPC', "block_planificacion"),
                    'rvpcv' => get_string('RVPCV', "block_planificacion"),
                    'spb' => get_string('SPB', "block_planificacion"),
                    'sugerencia' => get_string('Sugerecia', "block_planificacion")
                )
            ));

            $modalGTerminada = $items->modal((object) array(
                'title' => get_string('EGT', "block_planificacion"),
                'prefix' => 'GTerminada',
                'buttons' => array(
                    'Close' => ['info', get_string('close', "block_planificacion")]
                ),
                'body' => 'GTerminada',
                'labelElements' => array(
                    'tiempo' => get_string('Tiempo', "block_planificacion"),
                    'horcomp' => get_string('HorComp', "block_planificacion"),
                    'epcm' => get_string('EPCM', "block_planificacion"),
                    'lcmcp' => get_string('LCMCP', "block_planificacion"),
                    'ncv' => get_string('NCV', "block_planificacion"),
                    'nfns' => get_string('NFNS', "block_planificacion")
                )
            ));

            $modalAddElements = $items->modal((object) array(
                'title' => get_string('agregar', "block_planificacion"),
                'prefix' => 'AddElement',
                'buttons' => array('Close' => ['danger', get_string('close', "block_planificacion"), 'data-dismiss="modal"']),
                'body' => 'add',
                'table' => array(['#', '#']),
                'labelElements' => array(
                    'choose' => get_string('choose', "block_planificacion")
                )
            ));

            $modalAddTimbre = $items->modal((object) array(
                'title' => get_string('agregarTimbre', "block_planificacion"),
                'prefix' => 'AddTimbre',
                'buttons' => array('Agregar' => ['primary', get_string('agregar', "block_planificacion")], 'Close' => ['danger', get_string('close', "block_planificacion")]),
                'body' => 'addTimbre',
                'labelElements' => array(
                    'nombre' => get_string('nombre', "block_planificacion")
                )
            ));

            $modalCopy = $items->modal((object) array(
                'title' => get_string('copy', "block_planificacion"),
                'prefix' => 'Copy',
                'buttons' => array('Agregar' => ['primary', get_string('agregar', "block_planificacion")], 'Close' => ['danger', get_string('close', "block_planificacion")]),
                'body' => 'copy',
                'labelElements' => array(
                    'choose' => get_string('choose', "block_planificacion"),
                    'copyTo' => get_string('copy_to', "block_planificacion")
                ),
                'menuCopy' => array(
                    'Clases' => ['fas fa-user-friends', get_string('grupos', "block_planificacion")],
                    'Profesores' => ['fas fa-graduation-cap', get_string('profesores', "block_planificacion")]
                )
            ));

            $modalTPrincipal = $items->modal((object) array(
                'title' => get_string('tprincipal', "block_planificacion"),
                'prefix' => 'TPrincipal',
                'body' => 'TPrincipal',
                'labelElements' => array(
                    'periodoName' => get_string('periodo', "block_planificacion"),
                    'semana' => get_string('semana', "block_planificacion"),
                    'selectBy' => get_string('selectBy', "block_planificacion"),
                    'profesores' => get_string('profesores', "block_planificacion"),
                    'aulas' => get_string('aulas', "block_planificacion"),
                    'cursos' => get_string('cursos', "block_planificacion"),
                    'todo' => get_string('todo', "block_planificacion"),
                ),
                'buttons' => array(
                    'Close' => ['danger', get_string('close', "block_planificacion")],
                    'BH' => ['danger', get_string('BH', "block_planificacion")]   
                )
            ));


            $modalEliminarHorario = $items->modal((object) array(
                'title' => get_string('BH', "block_planificacion"),
                'prefix' => 'EliminarHorario',
                'body' => 'EliminarHorario',
                'labelElements' => array(
                    'BH' => get_string('BH', "block_planificacion"),
                    'BH2' => get_string('BH2', "block_planificacion"),
                    'FA' => get_string('FA', "block_planificacion"),
                    'FAYNA' => get_string('FAYNA', "block_planificacion"),
                    'aceptar' => get_string('aceptar', "block_planificacion"),
                    'eliminar' => get_string('close', "block_planificacion")
                ),
                'buttons' => array(
                    'Agregar' => ['primary', get_string('aceptar', "block_planificacion")],
                    'Close' => ['danger', get_string('close', "block_planificacion")]
                ) 
                ));


            $loader = $items->loaderAnimation((object) array(
                'color1' => '#1177d1',
                'color2' => '#373a3c',
                'label' => 'Working'
            ));

            $modalMsg = $items->modalMsg((object) array(
                'title' => get_string('info', "block_planificacion"),
                'msg' => get_string('msg_modal', "block_planificacion"),
                'buttons' => array('Aceptar' => ['success', get_string('aceptar', "block_planificacion")], 'Cancelar' => ['danger', get_string('close', "block_planificacion")]),
                'ico_class' => 'fas fa-info-circle info-custome',
                'prefix' => 'Msg'
            ));

            // Is necessary use " for get the chance to concat the variable on php
            $text = "<div id='container' class='col-12'>
                        $cardStart
                        $modal
                        $loader
                        $modalTiempo
                        $modalRestricciones
                        $modalLecciones
                        $modalLeccion
                        $modalRelaciones
                        $modalRestriccionesCurso
                        $modalEscuelaTabla
                        $modalEscuela
                        $modalDefSemanas
                        $modalDefPeriodos
                        $modalTimbres
                        $modalTimbre
                        $modalAddElements
                        $modalCopy
                        $modalRenomDias
                        $modalObjeto
                        $modalEleccion
                        $modalPeriodo
                        $modalCombinacionDias
                        $modalCombinacionPeriodos
                        $modalCombinacionSemanas
                        $modalAddTimbre
                        $modalVPrevia
                        $modalGNuevHorario
                        $modalVCRelajas
                        $modalTPrincipal
                        $modalEliminarHorario
                        $modalMsg
                        $modalAviso
                        $modalAsesor
                        $modalDcls
                        $modalGTerminada
                    </div>";
            //assign $text variable to the content object
            $this->content->text = $text;
            //function returns the content object
            return $this->content;
        }
    }
}
