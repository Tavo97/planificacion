window.addEventListener('load', (e) => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;

    btnPlaneacionEscuela.addEventListener('click', (e) => {
        let formData = new FormData();
        formData.append('peticion', 1);
        formData.append('select', JSON.stringify(['*']));
        formData.append('tabla', 'blm_vista_horario_centro');
        let data = new Object();
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [false, formData];
        ajaxData(data).then((res) => {
            msGObj.loader.style.display = 'block';
            deleteAllChilds(tableBodyEscuelaTabla);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = {
                        'id': res[item].id,
                        'nombre': res[item].nombre,
                        'ciclo_escolar': res[item].ciclo_escolar,
                        'dias': res[item].dias,
                        'lecciones': res[item].lecciones,
                        'fin_semana': res[item].fin_semana,
                        'crear_horarios': res[item].crear_horarios,
                        'periodo_bool': res[item].periodo_bool,
                        'semana_bool': res[item].semana_bool,
                        'id_centro': res[item].id_centro,
                        'horas_maximas': res[item].horas_maximas,
                        'centro': res[item].centro,
                        'tiempo_timbre_clases': res[item].tiempo_timbre_clases
                    };
                    items = [res[item].id, res[item].nombre, res[item].centro, res[item].ciclo_escolar, res[item].lecciones, res[item].dias];
                    tr = addRowTable(data, items, res[item].id, 'Escuela', tableBodyEscuelaTabla);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_data;
            }
            $('#ModalEscuelaTabla').modal({ backdrop: 'static', keyboard: false });
            closeWorkLand(msGObj);
        });
    });

    labelBtnModalEscuelaTablaAgregar.addEventListener('click', (e) => {
        frmEscuela.reset();
        let hide = [renombrarPeriodoEscuela.parentNode, renombrarDiaEscuela.parentNode, makeOwnTimesEscuela];
        hideElement(hide, 0);
        let selectItems = [{
                select: {
                    item: centroEscuela,
                    prefix: `${centroEscuela.id}Op`,
                },
                table: {
                    name: 'blm_centros',
                    fields: ['id', 'nombre'],
                    exeption: ['nombre'],
                    conditions: ''
                }
            },
            {
                select: {
                    item: leccionesDiaEscuela,
                    prefix: `${leccionesDiaEscuela.id}Op`,
                },
                table: {
                    name: 'blm_horas_tl',
                    fields: ['id'],
                    exeption: ['id'],
                    conditions: ''
                },
                type: 'number'
            },
            {
                select: {
                    item: numeroDiasEscuela,
                    prefix: `${centroEscuela.id}Op`,
                },
                table: {
                    name: 'blm_dias_tl',
                    fields: ['id'],
                    exeption: ['id'],
                    conditions: ''
                },
                type: 'number'
            }
        ];
        selectItems.forEach(element => {
            fillSelectItem(element.select, element.table, { firstItem: true }, element.type);
        });
        tablaFrmEscuela.value = 'blm_horarios';
        peticionFrmEscuela.value = 2;
        camposFrmEscuela.value = JSON.stringify({
            'nombre': 'nombreHorarioEscuela',
            'id_centro': 'centroEscuela',
            'ciclo_escolar': 'cicloEscolarEscuela',
            'dias': 'numeroDiasEscuela',
            'lecciones': 'leccionesDiaEscuela',
            'fin_semana': 'finSemanaEscuela',
            'crear_horarios': 'personalHorariosEscuela'
        });
        $('#ModalEscuela').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalEscuelaTablaEditar.addEventListener('click', (e) => {
        let selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectItem != "none") {
            let dataItem = selectItem.dataset;
            let selectItems = [{
                    select: {
                        item: centroEscuela,
                        prefix: `${centroEscuela.id}Op`,
                        currentOption: dataItem.id_centro
                    },
                    table: {
                        name: 'blm_centros',
                        fields: ['id', 'nombre'],
                        exeption: ['nombre'],
                        conditions: ''
                    }
                },
                {
                    select: {
                        item: leccionesDiaEscuela,
                        prefix: `${leccionesDiaEscuela.id}Op`,
                        currentOption: dataItem.lecciones
                    },
                    table: {
                        name: 'blm_horas_tl',
                        fields: ['id'],
                        exeption: ['id'],
                        conditions: ''
                    },
                    type: 'number'
                },
                {
                    select: {
                        item: numeroDiasEscuela,
                        prefix: `${centroEscuela.id}Op`,
                        currentOption: dataItem.dias
                    },
                    table: {
                        name: 'blm_dias_tl',
                        fields: ['id'],
                        exeption: ['id'],
                        conditions: ''
                    },
                    type: 'number'
                }
            ];
            let hide = [renombrarPeriodoEscuela.parentNode, renombrarDiaEscuela.parentNode, makeOwnTimesEscuela];
            hideElement(hide, 1);
            selectItems.forEach(element => {
                fillSelectItem(element.select, element.table, { firstItem: false }, element.type);
            });
            nombreHorarioEscuela.value = dataItem.nombre;
            cicloEscolarEscuela.value = dataItem.ciclo_escolar;
            finSemanaEscuela.value = dataItem.fin_semana;
            if (parseInt(dataItem.crear_horarios) == 1) {
                DPeriodosEscuela.disabled = false;
                DSemanasEscuela.disabled = false;
            } else {
                DPeriodosEscuela.disabled = true;
                DSemanasEscuela.disabled = true;
            }
            console.log(dataItem);
            personalHorariosEscuela.checked = parseInt(dataItem.crear_horarios);
            idFrmEscuela.value = dataItem.id;
            tablaFrmEscuela.value = 'blm_horarios';
            peticionFrmEscuela.value = 3;
            camposFrmEscuela.value = JSON.stringify({
                'id': 'idFrmEscuela',
                'nombre': 'nombreHorarioEscuela',
                'id_centro': 'centroEscuela',
                'ciclo_escolar': 'cicloEscolarEscuela',
                'dias': 'numeroDiasEscuela',
                'lecciones': 'leccionesDiaEscuela',
                'fin_semana': 'finSemanaEscuela',
                'crear_horarios': 'personalHorariosEscuela'
            });
            $('#ModalEscuela').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalEscuelaAgregar.addEventListener('click', (e) => {
        e.preventDefault();
        msGObj.loader.style.display = 'block';
        let data = new Object();
        let send;
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        send = [false, getCheckboxData({ name: 'frmEscuela', item: frmEscuela })];
        data.form = send;
        printFormData(send[1]);
        ajaxData(data).then((res) => {
            if (res[0] != false) {
                msGObj.labelLoader.style.color = '#28a745';
                let op = parseInt(peticionFrmEscuela.value);
                switch (op) {
                    case 2:
                        msGObj.labelLoader.innerHTML = langText.msg_added;
                        let formData = new FormData();
                        let timbres = new Array();
                        let firstHour = '5:00';
                        let longClass = '1:00';
                        let longRest = '0:15';
                        for (let index = 1; index <= send[1].get('leccionesDiaEscuela'); index++) {
                            classStart = addHours(firstHour, longClass);
                            classEnds = subHours(addHours(classStart, longClass), longRest);
                            timbres.push({
                                nombre: ` ${langText.hour} ${index}`,
                                abreviatura: `${langText.hour.substring(0, 1)} - ${index}`,
                                inicio: classStart,
                                fin: classEnds,
                                id_horario: res[0]
                            });
                            firstHour = classStart;
                        }
                        formData.append('camposObj', JSON.stringify(timbres));
                        saveItemsDB(langText, 5, formData, false, { name: 'blm_horas_recesos' }).then(horas => console.log(horas)).catch(e => {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = e;
                        });
                        break;

                    case 3:
                        table = {
                            name: 'blm_horas_recesos',
                            fields: ['*'],
                            conditions: { 'id_horario': ['=', idFrmEscuela.value] },
                            order: { 'inicio': 'ASC' }
                        };
                        searchItemsDB(table).then((horas) => {
                            msGObj.loader.style.display = 'block';
                            let resAux = [];
                            for (i in horas) {
                                if (horas[i].tipo != 1)
                                    resAux.push(horas[i]);
                            }
                            deleteAllChilds(tableBodyTimbres);
                            if (resAux.length != send[1].get('leccionesDiaEscuela')) {
                                let formDataLecciones = new FormData();
                                formDataLecciones.append('peticion', 4);
                                let dataLecciones = new Object();
                                dataLecciones.target = '../blocks/planificacion/peticionesSql.php';
                                dataLecciones.method = 'POST';
                                dataLecciones.send = true;
                                msGObj.loader.style.display = 'block';
                                formDataLecciones.append('tabla', 'blm_horas_recesos');
                                let deleteid = resAux.length - 1;
                                for (variable in resAux) {
                                    let eliminar = true;
                                    if (resAux.length < leccionesDiaEscuela.value) {
                                        eliminar = false;
                                        if (!eliminar) {
                                            let var2 = resAux.length - 1;
                                            let var1 = new Number(resAux[variable].id);
                                            let var3 = var2 + var1;
                                            console.log('no eliminaré registros y crearé los restantes  ' + var2 + '   ' + var3, resAux[var3])
                                            eliminar = true
                                            setTimeout(() => {
                                                let formData = new FormData();
                                                let timbres = new Array();
                                                let firstHour = addHours(resAux[var2].inicio, '1:00');
                                                let longClass = '1:00';
                                                let longRest = '0:15';
                                                console.log('first hour ' + firstHour);
                                                for (let index = resAux.length + 1; index <= send[1].get('leccionesDiaEscuela'); index++) {
                                                    classStart = addHours(firstHour, longClass);
                                                    classEnds = subHours(addHours(classStart, longClass), longRest);
                                                    timbres.push({
                                                        nombre: ` ${langText.hour} ${index}`,
                                                        abreviatura: `${langText.hour.substring(0, 1)} - ${index}`,
                                                        inicio: classStart,
                                                        fin: classEnds,
                                                        id_timbre: null,
                                                        id_horario: idFrmEscuela.value
                                                    });
                                                    firstHour = classStart;
                                                }
                                                formData.append('camposObj', JSON.stringify(timbres));
                                                saveItemsDB(langText, 5, formData, false, { name: 'blm_horas_recesos' }).then(hours => console.log(hours));
                                            }, 3000);
                                            break
                                        }
                                    } else if (resAux.length >= leccionesDiaEscuela.value && eliminar) {
                                        if (deleteid < leccionesDiaEscuela.value) {} else {
                                            for (let i = deleteid; i >= leccionesDiaEscuela.value; i--) {
                                                console.log('eliminar timbres sobrantes', i)
                                                let var1 = new Number(resAux[variable].id);
                                                console.log('delete id && var   ', var1 + i + '    ' + deleteid, resAux)
                                                formDataLecciones.append('id', resAux[i].id);
                                                dataLecciones.form = [false, formDataLecciones];
                                                ajaxData(dataLecciones).then((lecciones) => {
                                                    if (lecciones) {
                                                        msGObj.labelLoader.style.color = 'tomato';
                                                        msGObj.labelLoader.innerHTML = langText.msg_deleted;
                                                    } else {
                                                        msGObj.labelLoader.innerHTML = langText.msg_no_deleted;
                                                    }
                                                    setTimeout(() => {
                                                        msGObj.loader.style.display = 'none';
                                                        msGObj.labelLoader.style.color = 'black';
                                                        msGObj.labelLoader.innerHTML = langText.msg_working;
                                                    }, 3000);
                                                });
                                            }
                                        }
                                    } else {
                                        console.log('no hice nada')
                                    }
                                }
                            } else {
                                console.log('no hice nada x2')
                            }
                            closeWorkLand(msGObj);
                        });
                        if (isset(titleModulePlaneacion.dataset.id) && idFrmEscuela.value == titleModulePlaneacion.dataset.id) {
                            setDatato(res[2], titleModulePlaneacion);
                        }
                        msGObj.labelLoader.innerHTML = langText.msg_saved;
                        break;
                }
                closeModal(JSON.stringify({ status: 0, close: ['ModalEscuela', 'ModalEscuelaTabla'] }));
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_save;
            }
            closeWorkLand(msGObj);
        });
    });

    labelBtnModalEscuelaTablaUsar.addEventListener('click', (e) => {
        let selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectItem != 'none') {
            let data = selectItem.dataset;
            titleModulePlaneacion.innerHTML = `${data.id}- ${data.nombre} - ${data.centro}`;
            setDatato(data, titleModulePlaneacion);
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg', 'ModalEscuelaTabla'] })
                },
                'msg': langText.msg_info_set_horario,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalEscuelaTablaBorrar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectItem != 'none') {
            let msg = msgDeleteItem(langText.msg_alert_delete, selectItem.dataset.centro, langText.horario);
            data = {
                'dataSets': {
                    'action': 'eliminar',
                    'data': JSON.stringify(selectItem.dataset),
                    'table': 'blm_horarios',
                    'item': selectItem.id,
                    'modal': JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': msg,
                'ico': 'info',
                'closeDataSet': {
                    visible: 1,
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
        }
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalEscuelaTablaClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 0, close: ['ModalMsg', 'ModalEscuelaTabla'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalEscuelaClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalEscuela'] })
            },
            'msg': langText.msg_alert_Cancel,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    personalHorariosEscuela.addEventListener('change', (e) => {
        let selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        let formData = new FormData();
        let crearhorarios = e.target.checked == true ? 1 : 0;
        let periodoBool = 0;
        let semanaBool = 0;
        let table = {
            name: 'blm_horarios',
            campos: {
                'id': 'idE',
                'crear_horarios': 'crearhorarios',
                'periodo_bool': 'periodoBool',
                'semana_bool': 'semanaBool'
            }
        };
        formData = objectToFormData({ 'idE': idFrmEscuela.value, 'crearhorarios': crearhorarios, 'periodoBool': periodoBool, 'semanaBool': semanaBool });
        saveItemsDB(langText, 3, formData, false, table).then(response => {
            if (response[0] != false) {
                msGObj.labelLoader.style.color = '#20c997';
                setDatato(response[2], titleModulePlaneacion);
                setDatato(response[2], selectItem);
                msGObj.labelLoader.innerHTML = langText.msg_saved;
                if (personalHorariosEscuela.checked == true) {
                    DPeriodosEscuela.disabled = false;
                    DSemanasEscuela.disabled = false;
                    let table = {
                        name: 'blm_periodos',
                        campos: {
                            'nombre': 'name',
                            'abreviatura': 'abbreviation',
                            'tipo': 'kind',
                            'id_horario': 'schedule_id'
                        }
                    };
                    formData = objectToFormData({ 'name': 'Periodo 01', 'abbreviation': 'P 01', 'kind': 1, 'schedule_id': idFrmEscuela.value });
                    saveItemsDB(langText, 2, formData, false, table).then(response => {
                        if (response[0] != false) {
                            let periodos = [{
                                nombre: `Todo el año`,
                                abreviatura: `Año`,
                                id_horario: idFrmEscuela.value,
                                tipo: 2
                            }, {
                                nombre: `Cualquier periodo`,
                                abreviatura: `Cualquier`,
                                id_horario: idFrmEscuela.value,
                                tipo: 2
                            }];
                            let formData = objectToFormData({ 'camposObj': JSON.stringify(periodos) });
                            saveItemsDB(langText, 5, formData, false, { name: 'blm_periodos' }).then(res => console.log(res));
                            return response[0];
                        }
                    }).then(idPeriodo => {
                        table.name = 'blm_semanas';
                        formData = objectToFormData({ 'name': 'Semana 01', 'abbreviation': 'S 01', 'kind': 1, 'schedule_id': idFrmEscuela.value });
                        return saveItemsDB(langText, 2, formData, false, table).then(response => {
                            if (response[0] != false) {
                                let periodos = [{
                                    nombre: `Todas las semanas`,
                                    abreviatura: `	Todas`,
                                    id_horario: idFrmEscuela.value,
                                    tipo: 2
                                }, {
                                    nombre: `Cualquier semana`,
                                    abreviatura: `Cualquier`,
                                    id_horario: idFrmEscuela.value,
                                    tipo: 2
                                }];
                                let formData = objectToFormData({ 'camposObj': JSON.stringify(periodos) });
                                saveItemsDB(langText, 5, formData, false, { name: 'blm_semanas' }).then(res => console.log(res));
                                return [{ 'idPeriodo': idPeriodo, 'idSemana': response[0] }];
                            }
                        });
                    }).then(ids => {
                        ids = ids[0];
                        let table = {
                            name: 'blm_leccion',
                            fields: ['*'],
                            conditions: { 'id_horario': ['=', idFrmEscuela.value] }
                        }
                        searchItemsDB(table).then((response) => {
                            if (Object.keys(response).length > 0) {
                                let formData = new FormData();
                                let table = { name: 'blm_leccion_periodo_semana', campos: { 'id_leccion': 'leccion', 'id_periodo': 'periodo', 'id_semana': 'semana' } };
                                for (const item in response) {
                                    formData = objectToFormData({ 'leccion': response[item].id, 'periodo': ids.idPeriodo, 'semana': ids.idSemana });
                                    printFormData(formData)
                                    saveItemsDB(langText, 2, formData, false, table).then(res => console.log(res));
                                }
                            }
                        });
                    });
                } else {
                    DPeriodosEscuela.disabled = true;
                    DSemanasEscuela.disabled = true;
                    let table = {
                        name: 'blm_periodos',
                        fields: ['*'],
                        conditions: { 'id_horario': ['=', idFrmEscuela.value] }
                    };
                    searchItemsDB(table).then((response) => {
                        if (Object.keys(response).length > 0) {
                            let table = { name: 'blm_periodos' };
                            for (const item in response) {
                                table.id = response[item].id;
                                deleteItemDB(table).then(deleted => console.log(deleted));
                            }
                        }
                    });
                    table.name = 'blm_semanas';
                    searchItemsDB(table).then((response) => {
                        if (Object.keys(response).length > 0) {
                            let table = { name: 'blm_semanas' };
                            for (const item in response) {
                                table.id = response[item].id;
                                deleteItemDB(table).then(deleted => console.log(deleted));
                            }
                        }
                    });
                }
            }
        }).catch(e => {
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = e;
        });
    });

    DPeriodosEscuela.addEventListener('click', (e) => {
        table = {
            name: 'blm_periodos',
            fields: ['*'],
            conditions: { 'id_horario': ['=', idFrmEscuela.value] }
        };
        idFrmDefPeriodos.value = idFrmEscuela.value;
        deleteAllChilds(tableBodyDefPeriodos);
        searchItemsDB(table).then((res) => {
            if (Object.keys(res).length > 0) {
                return orderDBresponse(res, 'nombre');
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_data;
            }
        }).then(sortData => {
            sortData.forEach((element, i) => {
                data = {
                    'id': element.id,
                    'nombre': element.nombre,
                    'abreviatura': element.abreviatura
                };
                items = [i + 1, element.nombre, element.abreviatura, ""];
                tr = addRowTable(data, items, element.id, 'multiperiodo', tableBodyDefPeriodos);
                tr.child.addEventListener('click', (e) => {
                    selectRow(e);
                });
            });
        }).catch(e => {
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = e;
        });;
        $('#ModalDefPeriodos').modal({ backdrop: 'static', keyboard: false });
        console.log("Prueba2");
    });

    DSemanasEscuela.addEventListener('click', (e) => {
        table = {
            name: 'blm_semanas',
            fields: ['*'],
            conditions: { 'id_horario': ['=', idFrmEscuela.value] }
        };
        idFrmDefSemanas.value = idFrmEscuela.value;
        deleteAllChilds(tableBodyDefSemanas);
        searchItemsDB(table).then((res) => {
            if (Object.keys(res).length > 0) {
                return orderDBresponse(res, 'nombre');
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_data;
            }
        }).then(sortData => {
            sortData.forEach((element, i) => {
                data = {
                    'id': element.id,
                    'nombre': element.nombre,
                    'abreviatura': element.abreviatura
                };
                items = [i + 1, element.nombre, element.abreviatura, ""];
                tr = addRowTable(data, items, element.id, 'multiperiodo', tableBodyDefSemanas);
                tr.child.addEventListener('click', (e) => {
                    selectRow(e);
                });
            });
        }).catch(e => {
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = e;
        });
        $('#ModalDefSemanas').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalDefPeriodosEditar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyDefPeriodos, 'choosed')[1];
        if (selectItem != "none") {
            nombreObjetoObjeto.value = selectItem.dataset.nombre;
            abreviaturaObjetoObjeto.value = selectItem.dataset.abreviatura;
            let campos = JSON.stringify({ "nombre": "nombreObjetoObjeto", "abreviatura": "abreviaturaObjetoObjeto", "id": "idFrmObjeto" });
            tablaFrmObjeto.value = "blm_periodos";
            idFrmObjeto.value = selectItem.dataset.id;
            camposFrmObjeto.value = campos;
            peticionFrmObjeto.value = 3;
            $('#ModalObjeto').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalObjetoAceptar.addEventListener('click', (e) => {
        saveItemsDB(langText, 3, frmObjeto, true).then(res => {
            msGObj.loader.style.display = 'block';
            if (res[0]) {
                msGObj.labelLoader.style.color = '#28a745';
                msGObj.labelLoader.innerHTML = langText.msg_saved;
            } else {
                msGObj.labelLoader.innerHTML = langText.msg_no_save;
                msGObj.labelLoader.style.color = 'tomato';
            }
            closeModal(JSON.stringify({ status: 1, close: ['ModalRdias', 'ModalObjeto', 'ModalDefSemanas', 'ModalDefPeriodos'] }));
            closeWorkLand(msGObj);
        });
    });

    renombrarPeriodoEscuela.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        let select = {
            item: ValidoParaidTimbres,
            prefix: `${ValidoParaidTimbres.id}Op`
        };
        let table = {
            name: 'blm_horarios',
            fields: ['*'],
            conditions: { 'id': ['=', selectItem.dataset.id] }
        };
        searchItemsDB(table).then((respuesta) => {
            let key = respuesta[Object.keys(respuesta)];
            TlddclasidTimbres.checked = parseInt(key.tiempo_timbre_clases);
            if (parseInt(key.tiempo_timbre_clases) == 1) {
                VaParaTimbres.disabled = false;
                ValidoParaidTimbres.disabled = false;
            } else {
                VaParaTimbres.disabled = true;
                ValidoParaidTimbres.disabled = true;
            }
        }).then(() => {
            let table = {
                name: 'blm_timbres',
                fields: ['id', 'nombre', 'tipo'],
                exeption: ['nombre'],
                conditions: { 'id_horario': ['=', selectItem.dataset.id] }
            };
            let settings = new Object();
            if (TlddclasidTimbres.checked) {
                settings.firstItem = false;
            } else {
                settings.firstItem = true;
            }
            fillSelectItem(select, table, settings);
        }).catch((e) => {
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = langText.msg_no_data;
            console.error(e);
        });
        table = {
            name: 'blm_vista_horas_recesos',
            fields: ['*'],
            conditions: { 'id_horario': ['=', selectItem.dataset.id] },
            order: { 'inicio': 'ASC' }
        };
        searchItemsDB(table).then((res) => {
            msGObj.loader.style.display = 'block';
            deleteAllChilds(tableBodyTimbres);
            if (Object.keys(res).length > 0) {
                return orderDBresponse(res, 'inicio');
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_data;
            }
        }).then(arrayItems => {
            arrayItems.forEach((element, i) => {
                console.log(element)
                let cualquiera = element.nombre_timbre == null ? langText.msg_info_for_all : element.nombre_timbre;
                data = {
                    'id': element.id,
                    'nombre': element.nombre,
                    'abreviatura': element.abreviatura,
                    'id_horario': element.id_horario,
                    'inicio': element.inicio,
                    'fin': element.fin,
                    'impresion': element.impresion,
                    'impresion_condicion_dos': element.impresion_condicion_dos,
                    'impresion_condicion_tres': element.impresion_condicion_tres,
                    'id_timbre': element.id_timbre,
                    'nombre_timbre': element.nombre_timbre,
                    'tipo': element.tipo,
                    'tiempo_timbre_dias': element.tiempo_timbre_dias,
                };
                items = [i + 1, element.nombre, element.abreviatura, element.inicio, element.fin, element.impresion, cualquiera];
                console.log(items);
                tr = addRowTable(data, items, element.id, 'Timbres', tableBodyTimbres);
                tr.child.addEventListener('click', (e) => {
                    selectRow(e);
                });
            });
            closeWorkLand(msGObj);
        }).catch(e => console.log(e));
        $('#ModalTimbres').modal({ backdrop: 'static', keyboard: false });
    });

    renombrarDiaEscuela.addEventListener('click', (e) => {
        let selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        select = {
            item: diasRenombradoRdias,
            prefix: `${diasRenombradoRdias.id}Op`,
            currentOption: selectItem.dataset.dias
        };
        table = {
            name: 'blm_dias_tl',
            fields: ['id'],
            exeption: ['id'],
            conditions: ''
        };
        fillSelectItem(select, table, { firstItem: false });
        selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        table = {
            name: 'blm_dias_tl',
            fields: ['*'],
            limit: [selectItem.dataset.dias]
        }
        searchItemsDB(table).then((res) => {
            msGObj.loader.style.display = 'block';
            deleteAllChilds(tableBodyRdias);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = {
                        'id': res[item].id,
                        'dia': res[item].dia,
                        'abreviatura': res[item].abreviatura
                    };
                    items = [res[item].id, res[item].dia, res[item].abreviatura, ""];
                    tr = addRowTable(data, items, res[item].id, 'Dias', tableBodyRdias);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_data;
            }
            closeWorkLand(msGObj);
            $('#ModalRdias').modal({ backdrop: 'static', keyboard: false });
        });
    });

    labelBtnModalTimbresEliminar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyTimbres, 'choosed')[1];
        if (selectItem != 'none') {
            if (selectItem.dataset.tipo == 1) {
                let msg = msgDeleteItem(langText.msg_alert_delete, selectItem.dataset.nombre, langText.timbre);
                data = {
                    'dataSets': {
                        'action': 'eliminar',
                        'data': JSON.stringify(selectItem.dataset),
                        'table': 'blm_horas_recesos',
                        'item': selectItem.id,
                        'modal': JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    },
                    'msg': msg,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 1,
                        modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            } else {
                data = {
                    'dataSets': {
                        action: 'Info',
                        modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                    },
                    'msg': langText.msg_alert_no_delete_allowed,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 0
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalTimbresEditar.addEventListener('click', (e) => {
        let hide = [ubicacionRecreoTimbre.parentNode, ubicacionRecreoTimbre.parentNode, identificadorRecreoTimbre.parentNode, abreviaturaTimbre.parentNode, imprimirGrpTimbre, textoImpresionTimbre.parentNode];
        hideElement(hide, 1);
        selectItem = getChooseItem('tr', tableBodyTimbres, 'choosed')[1];
        selectItemEscuela = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (TlddclasidTimbres.checked == true) {
            let div = imprimirTodosTimbre.parentNode;
            div.classList.remove('oculto');
            let id_timbre = selectItem.dataset.id_timbre == 'null' ? 0 : selectItem.dataset.id_timbre;
            let select1 = {
                item: imprimirTodosTimbre,
                prefix: `${imprimirTodosTimbre.id}Op`,
                currentOption: id_timbre
            };
            let table1 = {
                name: 'blm_timbres',
                fields: ['id', 'nombre', 'tipo'],
                exeption: ['nombre'],
                conditions: { 'id_horario': ['=', selectItemEscuela.dataset.id] }
            };
            fillSelectItem(select1, table1, { firstItem: true, textFirstItem: langText.msg_info_for_all, previous: langText.msg_info_only_for });
        } else {
            hide = [imprimirTodosTimbre.parentNode];
            hideElement(hide, 0);
        }
        if (ValidoParaidTimbres.value == 0 && TlddclasidTimbres.checked == true) {
            idItemTimbre.value = ValidoParaidTimbres.options[0].value;
        } else {
            idItemTimbre.value = ValidoParaidTimbres.value;
        }
        select = {
            item: ubicacionRecreoTimbre,
            prefix: `${ubicacionRecreoTimbre.id}Op`,
        };
        table = {
            name: 'blm_horas_recesos',
            fields: ['id', 'nombre', 'inicio', 'fin'],
            exeption: ['nombre'],
            conditions: { 'id_horario': ['=', selectItemEscuela.dataset.id], 'tipo': ['=', 0] }
        };
        fillSelectItem(select, table, { firstItem: true, previous: langText.msg_info_previous_of });
        if (selectItem != 'none') {
            let dataItem = selectItem.dataset;
            identificadorRecreoTimbre.value = dataItem.nombre;
            abreviaturaTimbre.value = dataItem.abreviatura;
            if (dataItem.hasOwnProperty('new_inicio') && dataItem.hasOwnProperty('new_fin')) {
                inicioTimbre.value = dataItem.new_inicio;
                finTimbre.value = dataItem.new_fin;
            } else {
                inicioTimbre.value = dataItem.inicio;
                finTimbre.value = dataItem.fin;
            }
            textoImpresionTimbre.value = dataItem.impresion;
            ImprimirPeriodoIndividualTimbre.checked = parseInt(dataItem.impresion_condicion_dos);
            imprimirPeriodoIndividualDeTimbre.checked = parseInt(dataItem.impresion_condicion_tres);
            idFrmTimbre.value = dataItem.id;
            imprimirTodosTimbre.value = dataItem.id_timbre;
            if (selectItemEscuela.dataset.tiempo_timbre_clases == 0) {
                if (dataItem.tipo == 0) {
                    let hide = [ubicacionRecreoTimbre.parentNode];
                    hideElement(hide, 0);
                }
                camposFrmTimbre.value = JSON.stringify({
                    'id': 'idFrmTimbre',
                    'nombre': 'identificadorRecreoTimbre',
                    'abreviatura': 'abreviaturaTimbre',
                    'id_hora': 'ubicacionRecreoTimbre',
                    'inicio': 'inicioTimbre',
                    'fin': 'finTimbre',
                    'impresion': 'textoImpresionTimbre',
                    'impresion_condicion_dos': 'ImprimirPeriodoIndividualTimbre',
                    'impresion_condicion_tres': 'imprimirPeriodoIndividualDeTimbre',
                    'id_timbre': 'imprimirTodosTimbre'
                });
                tablaFrmTimbre.value = 'blm_horas_recesos';
                peticionFrmTimbre.value = 3;
                $('#ModalTimbre').modal({ backdrop: 'static', keyboard: false });
            }
            if (selectItemEscuela.dataset.tiempo_timbre_clases == 1) {
                if (ValidoParaidTimbres.value != 0) {
                    let timbre = selectedOption(ValidoParaidTimbres);
                    if (timbre.dataset.tipo == 1) {
                        let dataItem = selectItem.dataset;
                        if (dataItem.tipo == 0) {
                            let hide = [ubicacionRecreoTimbre.parentNode];
                            hideElement(hide, 0);
                        }
                        camposFrmTimbre.value = JSON.stringify({
                            'id': 'idFrmTimbre',
                            'nombre': 'identificadorRecreoTimbre',
                            'abreviatura': 'abreviaturaTimbre',
                            'id_hora': 'ubicacionRecreoTimbre',
                            'inicio': 'inicioTimbre',
                            'fin': 'finTimbre',
                            'impresion': 'textoImpresionTimbre',
                            'impresion_condicion_dos': 'ImprimirPeriodoIndividualTimbre',
                            'impresion_condicion_tres': 'imprimirPeriodoIndividualDeTimbre',
                            'id_timbre': 'imprimirTodosTimbre'
                        });
                        tablaFrmTimbre.value = 'blm_horas_recesos';
                        peticionFrmTimbre.value = 3;
                    } else {
                        let dataItem = selectItem.dataset;
                        if (dataItem.tipo == 0) {
                            let hide = [ubicacionRecreoTimbre.parentNode];
                            hideElement(hide, 0);
                        }
                        peticionFrmTimbre.value = 2;
                        camposFrmTimbre.value = JSON.stringify({
                            'id_hora': 'idFrmTimbre',
                            'hora_inicio': 'inicioTimbre',
                            'hora_fin': 'finTimbre',
                            'id_timbre': 'idItemTimbre'
                        });
                        tablaFrmTimbre.value = 'blm_nueva_hora';
                    }
                    $('#ModalTimbre').modal({ backdrop: 'static', keyboard: false });
                } else {
                    data = {
                        'dataSets': {
                            action: 'Info',
                            modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                        },
                        'msg': langText.msg_alert_select_timbre,
                        'ico': 'info',
                        'closeDataSet': {
                            visible: 0
                        }
                    };
                    changeMsgModal(data);
                    $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                }
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalTimbresNueva.addEventListener('click', (e) => {
        let hide = [ubicacionRecreoTimbre.parentNode, ubicacionRecreoTimbre.parentNode, identificadorRecreoTimbre.parentNode, abreviaturaTimbre.parentNode, imprimirGrpTimbre, textoImpresionTimbre.parentNode];
        hideElement(hide, 1);
        hide = [imprimirTodosTimbre.parentNode];
        hideElement(hide, 0);
        frmTimbre.reset();
        deleteAllChilds(ubicacionRecreoTimbre);
        selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        select = {
            item: ubicacionRecreoTimbre,
            prefix: `${ubicacionRecreoTimbre.id}Op`,
        };
        table = {
            name: 'blm_horas_recesos',
            fields: ['id', 'nombre', 'inicio', 'fin'],
            exeption: ['nombre'],
            conditions: { 'id_horario': ['=', selectItem.dataset.id], 'tipo': ['=', 0] }
        };
        fillSelectItem(select, table, { firstItem: true, previous: langText.msg_info_previous_of });
        let select1 = {
            item: imprimirTodosTimbre,
            prefix: `${imprimirTodosTimbre.id}Op`
        };
        let table1 = {
            name: 'blm_timbres',
            fields: ['id', 'nombre', 'tipo'],
            exeption: ['nombre'],
            conditions: { 'id_horario': ['=', selectItem.dataset.id] }
        };
        fillSelectItem(select1, table1, { firstItem: true, textFirstItem: langText.msg_info_for_all, previous: langText.msg_info_only_for });
        tipoTimbre.value = 1;
        tablaFrmTimbre.value = 'blm_horas_recesos';
        peticionFrmTimbre.value = 2;
        idItemTimbre.value = selectItem.dataset.id;
        ImprimirPeriodoIndividualTimbre.checked = 1;
        imprimirPeriodoIndividualDeTimbre.checked = 1;
        camposFrmTimbre.value = JSON.stringify({
            'nombre': 'identificadorRecreoTimbre',
            'abreviatura': 'abreviaturaTimbre',
            'id_hora': 'ubicacionRecreoTimbre',
            'inicio': 'inicioTimbre',
            'fin': 'finTimbre',
            'impresion': 'textoImpresionTimbre',
            'impresion_condicion_dos': 'ImprimirPeriodoIndividualTimbre',
            'impresion_condicion_tres': 'imprimirPeriodoIndividualDeTimbre',
            'id_timbre': 'imprimirTodosTimbre',
            'id_horario': 'idItemTimbre',
            'tipo': 'tipoTimbre'
        });
        $('#ModalTimbre').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalDefSemanasEditar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyDefSemanas, 'choosed')[1];
        if (selectItem != "none") {
            nombreObjetoObjeto.value = selectItem.dataset.nombre;
            abreviaturaObjetoObjeto.value = selectItem.dataset.abreviatura;
            let campos = JSON.stringify({ "nombre": "nombreObjetoObjeto", "abreviatura": "abreviaturaObjetoObjeto", "id": "idFrmObjeto" });
            tablaFrmObjeto.value = "blm_semanas";
            idFrmObjeto.value = selectItem.dataset.id;
            camposFrmObjeto.value = campos;
            peticionFrmObjeto.value = 3;
            $('#ModalObjeto').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalTimbreAgregar.addEventListener('click', (e) => {
        e.preventDefault();
        msGObj.loader.style.display = 'block';
        let data = new Object();
        let send;
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        send = [false, getCheckboxData({ name: 'frmTimbre', item: frmTimbre })];
        data.form = send;
        if (send[1].get("tabla") == 'blm_nueva_hora') {
            let c2 = ImprimirPeriodoIndividualTimbre.checked ? 1 : 0;
            let c3 = imprimirPeriodoIndividualDeTimbre.checked ? 1 : 0;
            let formData = new FormData();
            let idHr = send[1].get("idFrmTimbre");
            formData.append('idHr', idHr);
            formData.append('nombre', identificadorRecreoTimbre.value);
            formData.append('abreviatura', abreviaturaTimbre.value);
            formData.append('impresion', textoImpresionTimbre.value);
            formData.append('impresion_condicion_dos', c2);
            formData.append('impresion_condicion_tres', c3);
            saveItemsDB(langText, 3, formData, false, { name: 'blm_horas_recesos', campos: { 'id': 'idHr', 'nombre': 'nombre', 'abreviatura': 'abreviatura', 'impresion': 'impresion', 'impresion_condicion_dos': 'impresion_condicion_dos', 'impresion_condicion_tres': 'impresion_condicion_tres' } }).then(res => {});
            selectItem = getChooseItem('tr', tableBodyTimbres, 'choosed')[1];
            console.log(selectItem.dataset.id)
            table = {
                name: 'blm_nueva_hora',
                fields: ['*'],
                conditions: { 'id_timbre': ['=', send[1].get("idItemTimbre")], 'id_hora': ['=', send[1].get("idFrmTimbre")] }
            };
            searchItemsDB(table).then((res) => {
                console.log(res);
                if (Object.keys(res).length > 0) {
                    let formDataEN = new FormData();
                    formDataEN.append('peticion', 4);
                    let dataLecciones = new Object();
                    dataLecciones.target = '../blocks/planificacion/peticionesSql.php';
                    dataLecciones.method = 'POST';
                    dataLecciones.send = true;
                    msGObj.loader.style.display = 'block';
                    formDataEN.append('tabla', 'blm_nueva_hora');
                    for (const item in res) {
                        formDataEN.append('id', res[item].id);
                        dataLecciones.form = [false, formDataEN];
                        ajaxData(dataLecciones).then((resEliminado) => {
                            if (resEliminado) {
                                console.log(resEliminado);
                                ajaxData(data).then((res) => {
                                    msGObj.labelLoader.style.color = '#28a745';
                                    if (res[0] != false) {
                                        let op = parseInt(peticionFrmTimbre.value);
                                        switch (op) {
                                            case 2:
                                                table = {
                                                    name: 'blm_timbres_hr',
                                                    fields: ['*'],
                                                    conditions: { 'id_horas_recesos': ['=', idFrmTimbre.value] }
                                                };
                                                searchItemsDB(table).then((res_nh) => {
                                                    console.log(Object.values(res_nh)[0].id)
                                                    let formData = new FormData();
                                                    let idThr = Object.values(res_nh)[0].id;
                                                    formData.append('idTimbre', imprimirTodosTimbre.value);
                                                    formData.append('idThr', idThr);
                                                    if (imprimirTodosTimbre.value == 0) {
                                                        let table = {
                                                            name: 'blm_timbres_hr',
                                                            id: idThr
                                                        };
                                                        deleteItemDB(table);
                                                    } else {
                                                        saveItemsDB(langText, 3, formData, false, { name: 'blm_timbres_hr', campos: { 'id': 'idThr', 'id_timbre': 'idTimbre' } }).then(res => {});
                                                    }
                                                }).catch((e) => { //??????????
                                                    console.log('Hola ----->')
                                                    console.log(e);
                                                    if (e == "No se encontraron datos en la fuente..." || e == "No data on source...") {
                                                        let formDatahR = new FormData();
                                                        formDatahR.append('peticion', 2);
                                                        formDatahR.append('tabla', 'blm_timbres_hr');
                                                        let id_timbre = selectedOption(imprimirTodosTimbre).dataset.id;
                                                        let id_hr = idFrmTimbre.value;
                                                        formDatahR.append('id_timbre', id_timbre);
                                                        formDatahR.append('id_hr', id_hr);
                                                        let campos = {
                                                            'id_timbre': 'id_timbre',
                                                            'id_horas_recesos': 'id_hr'
                                                        };
                                                        formDatahR.append('campos', JSON.stringify(campos));
                                                        printFormData(formDatahR);
                                                        let dataHR = new Object();
                                                        dataHR.target = '../blocks/planificacion/peticionesSql.php';
                                                        dataHR.method = 'POST';
                                                        dataHR.send = true;
                                                        dataHR.form = [false, formDatahR];
                                                        if (imprimirTodosTimbre.value == 0) {
                                                            let table = {
                                                                name: 'blm_timbres_hr',
                                                                id: idThr
                                                            };
                                                            deleteItemDB(table)
                                                        } else {
                                                            ajaxData(dataHR).then((res1) => {
                                                                console.log(res1);
                                                            });
                                                        }
                                                    }
                                                    console.log(e)
                                                });
                                                msGObj.labelLoader.innerHTML = langText.msg_added;
                                                break;
                                            case 3:
                                                msGObj.labelLoader.innerHTML = langText.msg_saved;
                                                break;
                                        }
                                        closeModal(JSON.stringify({ status: 1, close: ['ModalTimbre', 'ModalTimbres'] }));
                                        closeWorkLand(msGObj);
                                    }
                                });
                            } else {
                                console.log(langText.msg_no_deleted);
                            }
                        });
                    }
                }
            }).catch(r => {
                ajaxData(data).then((res) => {
                    msGObj.labelLoader.style.color = '#28a745';
                    if (res[0] != false) {
                        let op = parseInt(peticionFrmTimbre.value);
                        switch (op) {
                            case 2:
                                table = {
                                    name: 'blm_timbres_hr',
                                    fields: ['*'],
                                    conditions: { 'id_horas_recesos': ['=', idFrmTimbre.value] }
                                };
                                searchItemsDB(table).then((res_nh) => {
                                    console.log(Object.values(res_nh)[0].id)
                                    let formData = new FormData();
                                    let idThr = Object.values(res_nh)[0].id;
                                    formData.append('idTimbre', imprimirTodosTimbre.value);
                                    formData.append('idThr', idThr);
                                    if (imprimirTodosTimbre.value == 0) {
                                        let table = {
                                            name: 'blm_timbres_hr',
                                            id: idThr
                                        };
                                        deleteItemDB(table)
                                    } else {
                                        saveItemsDB(langText, 3, formData, false, { name: 'blm_timbres_hr', campos: { 'id': 'idThr', 'id_timbre': 'idTimbre' } }).then(res => {
                                            console.log(res);
                                        }).catch(e => console.log(e));
                                    }
                                }).catch((e) => {
                                    if (e == "No se encontraron datos en la fuente..." || e == "No data on source...") {
                                        let formDatahR = new FormData();
                                        formDatahR.append('peticion', 2);
                                        formDatahR.append('tabla', 'blm_timbres_hr');
                                        let id_timbre = selectedOption(imprimirTodosTimbre).dataset.id;
                                        let id_hr = idFrmTimbre.value;
                                        formDatahR.append('id_timbre', id_timbre);
                                        formDatahR.append('id_hr', id_hr);
                                        let campos = {
                                            'id_timbre': 'id_timbre',
                                            'id_horas_recesos': 'id_hr'
                                        };
                                        formDatahR.append('campos', JSON.stringify(campos));
                                        printFormData(formDatahR);
                                        let dataHR = new Object();
                                        dataHR.target = '../blocks/planificacion/peticionesSql.php';
                                        dataHR.method = 'POST';
                                        dataHR.send = true;
                                        dataHR.form = [false, formDatahR];
                                        ajaxData(dataHR).then((res1) => {});
                                    }
                                    console.log(e)
                                });
                                msGObj.labelLoader.innerHTML = langText.msg_added;
                                break;
                            case 3:
                                msGObj.labelLoader.innerHTML = langText.msg_saved;
                                break;
                        }
                        closeModal(JSON.stringify({ status: 1, close: ['ModalTimbre', 'ModalTimbres'] }));
                        closeWorkLand(msGObj);
                    }
                });
            });
        } else {
            ajaxData(data).then((res) => {
                msGObj.labelLoader.style.color = '#28a745';
                if (res[0] != false) {
                    let op = parseInt(peticionFrmTimbre.value);
                    switch (op) {
                        case 2:
                            msGObj.labelLoader.innerHTML = langText.msg_added;
                            break;
                        case 3:
                            msGObj.labelLoader.innerHTML = langText.msg_saved;
                            break;
                    }
                    closeModal(JSON.stringify({ status: 1, close: ['ModalTimbre', 'ModalTimbres'] }));
                    closeWorkLand(msGObj);
                }
                console.log(res)
                return res[0];
            }).then((id) => {
                /**pruebas */
                table = {
                    name: 'blm_timbres_hr',
                    fields: ['*'],
                    conditions: { 'id_horas_recesos': ['=', idFrmTimbre.value] }
                };
                searchItemsDB(table).then((res_nh) => {
                    console.log(Object.values(res_nh)[0])
                    let formData = new FormData();
                    let idThr = Object.values(res_nh)[0].id;
                    formData.append('idTimbre', imprimirTodosTimbre.value);
                    formData.append('idThr', idThr);
                    if (imprimirTodosTimbre.value == 0) {
                        let table = {
                            name: 'blm_timbres_hr',
                            id: idThr
                        };
                        deleteItemDB(table)
                    } else {
                        saveItemsDB(langText, 3, formData, false, { name: 'blm_timbres_hr', campos: { 'id': 'idThr', 'id_timbre': 'idTimbre' } }).then(res => {});
                    }
                }).catch((e) => {
                    if (e == "No se encontraron datos en la fuente..." || e == "No data on source...") {
                        console.log(selectedOption(imprimirTodosTimbre).dataset.id)
                        let formDatahR = new FormData();
                        formDatahR.append('peticion', 2);
                        formDatahR.append('tabla', 'blm_timbres_hr');
                        let id_timbre = selectedOption(imprimirTodosTimbre).dataset.id;
                        let id_hr = idFrmTimbre.value;
                        formDatahR.append('id_timbre', id_timbre);
                        formDatahR.append('id_hr', id_hr);
                        let campos = {
                            'id_timbre': 'id_timbre',
                            'id_horas_recesos': 'id_hr'
                        };
                        formDatahR.append('campos', JSON.stringify(campos));
                        printFormData(formDatahR);
                        let dataHR = new Object();
                        dataHR.target = '../blocks/planificacion/peticionesSql.php';
                        dataHR.method = 'POST';
                        dataHR.send = true;
                        dataHR.form = [false, formDatahR];
                        console.log('aaaaaaaaaaaaaaaaaaaaa', id_hr, id_timbre)
                        ajaxData(dataHR).then((res1) => { console.log(res1) });
                    }
                });
                /** fin pruebas*/
            });
        }
    });

    labelBtnModalRdiasEditar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyRdias, 'choosed')[1];
        if (selectItem != "none") {
            nombreObjetoObjeto.value = selectItem.dataset.dia;
            abreviaturaObjetoObjeto.value = selectItem.dataset.abreviatura;
            let campos = JSON.stringify({ "dia": "nombreObjetoObjeto", "abreviatura": "abreviaturaObjetoObjeto", "id": "idFrmObjeto" });
            tablaFrmObjeto.value = "blm_dias_tl";
            idFrmObjeto.value = selectItem.dataset.id;
            camposFrmObjeto.value = campos;
            peticionFrmObjeto.value = 3;
            $('#ModalObjeto').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    VaParaTimbres.addEventListener('click', (e) => {
        if (ValidoParaidTimbres.options[ValidoParaidTimbres.selectedIndex].dataset.tipo == 1) {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_timbre,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        } else {
            if (ValidoParaidTimbres.value != 'none') {
                idFrmEleccion.value = ValidoParaidTimbres.value;
                tablaFrmEleccion.value = 'blm_grupos_timbres';
                peticionFrmEleccion.value = 2;
                camposFrmEleccion.value = JSON.stringify({ 'id_timbre': 'idFrmEleccion', 'id_grupo': 'idItemEleccion' });
                table = {
                    name: 'blm_grupos',
                    fields: ['id', 'nombre', 'abreviatura'],
                    exeption: '',
                    conditions: ''
                };
                searchItemsDB(table).then((res) => {
                    msGObj.loader.style.display = 'block';
                    deleteAllChilds(tableBodyEleccion);
                    deleteAllChilds(tableBodyEleccioneleccion2);
                    if (Object.keys(res).length > 0) {
                        console.log(res)
                        for (const item in res) {
                            data = {
                                'id': res[item].id,
                                'nombre': res[item].nombre,
                                'abreviatura': res[item].abreviatura
                            };
                            items = [res[item].id, res[item].nombre, res[item].abreviatura];
                            tr = addRowTable(data, items, res[item].id, 'Eleccion', tableBodyEleccion);
                            tr.child.addEventListener('click', (e) => {
                                selectRow(e);
                            });
                        }
                    } else {
                        msGObj.labelLoader.style.color = 'tomato';
                        msGObj.labelLoader.innerHTML = langText.msg_no_data;
                    }
                    closeWorkLand(msGObj);
                    $('#ModalEleccion').modal({ backdrop: 'static', keyboard: false });
                });
                table = {
                    name: 'blm_vista_timbres_grupos',
                    fields: ['id', 'id_grupo', 'nombre_grupo', 'abreviatura_grupo'],
                    exeption: '',
                    conditions: { 'id_timbre': ['=', ValidoParaidTimbres.value] }
                };
                setTimeout(() => {
                    searchItemsDB(table).then((res) => {
                        console.log('grupos timbres', res)
                        msGObj.loader.style.display = 'block';
                        deleteAllChilds(tableBodyEleccioneleccion2);
                        if (Object.keys(res).length > 0) {
                            console.log(res)
                            for (const item in res) {
                                data = {
                                    'id': res[item].id_grupo,
                                    'nombre': res[item].nombre_grupo,
                                    'abreviatura': res[item].abreviatura_grupo,
                                    'id_grupo_timbre': res[item].id
                                };
                                items = [res[item].id_grupo, res[item].nombre_grupo, res[item].abreviatura_grupo];
                                tr = addRowTable(data, items, res[item].id_grupo, 'Eleccion2', tableBodyEleccioneleccion2);
                                tr.child.addEventListener('click', (e) => {
                                    selectRow(e);
                                });
                            }
                        } else {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = langText.msg_no_data;
                        }
                        closeWorkLand(msGObj);
                        $('#ModalEleccion').modal({ backdrop: 'static', keyboard: false });
                    });
                }, 1000);
            } else {
                data = {
                    'dataSets': {
                        action: 'Info',
                        modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    },
                    'msg': langText.msg_alert_select_timbre,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 0
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        }
    });

    TlddclasidTimbres.addEventListener('change', (e) => {
        let formData = new FormData();
        let selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        formData.append('peticion', 3);
        formData.append('tabla', 'blm_horarios');
        let idE = selectItem.dataset.id;
        let tiempo_timbre_clases = e.target.checked == true ? 1 : 0;
        formData.append('idE', idE);
        formData.append('tiempo_timbre_clases', tiempo_timbre_clases);
        let campos = {
            'id': 'idE',
            'tiempo_timbre_clases': 'tiempo_timbre_clases'
        };
        formData.append('campos', JSON.stringify(campos));
        printFormData(formData);
        let data = new Object();
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [false, formData];
        ajaxData(data).then((res) => {
            msGObj.loader.style.display = 'block';
            console.log(res);
            if (res[0] == true) {
                selectItem.dataset.tiempo_timbre_clases = res[2].tiempo_timbre_clases;
                if (res[2].tiempo_timbre_clases == 1) {
                    let idHorario = res[2].id;
                    let tipoTimbre = 1;
                    let nombreTimbre = 'Timbre 1';
                    let campos = { 'id_horario': 'idHorario', 'tipo': 'tipoTimbre', 'nombre': 'nombreTimbre' };
                    let formDataTimbreBase = new FormData();
                    formDataTimbreBase.append('idHorario', idHorario);
                    formDataTimbreBase.append('tipoTimbre', tipoTimbre);
                    formDataTimbreBase.append('nombreTimbre', nombreTimbre);
                    table = {
                        name: 'blm_timbres',
                        fields: ['*'],
                        conditions: { 'id_horario': ['=', idHorario] }
                    };
                    searchItemsDB(table).then((res) => {}).catch(error => {
                        table = {
                            'name': 'blm_timbres',
                            'campos': campos
                        }
                        console.log('fallo ', error)
                        saveItemsDB(langText, 2, formDataTimbreBase, false, table).then(res => {
                            console.log(res);
                            msGObj.loader.style.display = 'block';
                            if (res[0]) {
                                table = {
                                    name: 'blm_grupos',
                                    fields: ['id']
                                };
                                searchItemsDB(table).then((grupos) => {
                                    if (Object.keys(grupos).length > 0) {
                                        for (const item in grupos) {
                                            let idGrupo = grupos[item].id;
                                            let idTimbre = res[0];
                                            let campos = { 'id_grupo': 'idGrupo', 'id_timbre': 'idTimbre' };
                                            let formDataGT = new FormData();
                                            formDataGT.append('idGrupo', idGrupo);
                                            formDataGT.append('idTimbre', idTimbre);
                                            table = {
                                                'name': 'blm_grupos_timbres',
                                                'campos': campos
                                            }
                                            saveItemsDB(langText, 2, formDataGT, false, table).then(gt => {
                                                console.log(gt);
                                                if (gt[0]) {}
                                            });
                                        }
                                    } else {
                                        msGObj.labelLoader.style.color = 'tomato';
                                        msGObj.labelLoader.innerHTML = langText.msg_no_data;
                                    }
                                    closeWorkLand(msGObj);
                                });
                                msGObj.labelLoader.style.color = '#28a745';
                                msGObj.labelLoader.innerHTML = langText.msg_saved;
                            } else {
                                msGObj.labelLoader.innerHTML = langText.msg_no_save;
                                msGObj.labelLoader.style.color = 'tomato';
                            }
                            closeWorkLand(msGObj);
                        });
                    });
                }
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_no_data;
            }
            closeWorkLand(msGObj);
        });
    });

    TlddclasidTimbres.addEventListener('click', (e) => {
        if (TlddclasidTimbres.checked == true) {
            VaParaTimbres.disabled = false;
            ValidoParaidTimbres.disabled = false;
        } else {
            VaParaTimbres.disabled = true;
            ValidoParaidTimbres.disabled = true;
        }
    });

    EHDTADidTimbres.addEventListener('change', (e) => {
        selectItem = getChooseItem('tr', tableBodyTimbres, 'choosed')[1];
        if (selectItem != 'none') {
            let formData = new FormData();
            let data = new Object();
            let timbreDia = e.target.checked == true ? 1 : 0;
            if (selectedOption(ValidoParaidTimbres).dataset.tipo == 0) {
                table = {
                    name: 'blm_active_tiempo_timbre_dias',
                    fields: ['*'],
                    conditions: { 'id_horas_recesos': ['=', selectItem.dataset.id], 'id_timbre': ['=', ValidoParaidTimbres.value] }
                };
                searchItemsDB(table).then((res) => {
                    let id;
                    for (c in res) {
                        id = res[c].id
                    }
                    let formDataE = new FormData();
                    formDataE.append('idAttd', id);
                    formDataE.append('timbreDia', timbreDia);
                    saveItemsDB(langText, 3, formDataE, false, { name: 'blm_active_tiempo_timbre_dias', campos: { 'id': 'idAttd', 'tiempo_timbre_dias': 'timbreDia' } }).then(res => {
                        if (res[2].tiempo_timbre_dias == 1) {
                            EHDTADidTimbres.checked = 1;
                            selectItem.dataset.tiempo_timbre_dias = 1;
                            editarTimbres.disabled = false;
                        }
                        if (res[2].tiempo_timbre_dias == 0) {
                            EHDTADidTimbres.checked = 0;
                            selectItem.dataset.tiempo_timbre_dias = 0;
                            editarTimbres.disabled = true;
                        }
                    });
                }).catch((e) => {
                    let formDataI = new FormData();
                    formDataI.append('idHr', selectItem.dataset.id);
                    formDataI.append('idTimbre', ValidoParaidTimbres.value);
                    saveItemsDB(langText, 2, formDataI, false, { name: 'blm_active_tiempo_timbre_dias', campos: { 'id_horas_recesos': 'idHr', 'id_timbre': 'idTimbre' } }).then(res => {
                        EHDTADidTimbres.checked = 1;
                        selectItem.dataset.tiempo_timbre_dias = 1;
                        editarTimbres.disabled = false;
                        EHDTADidTimbres.checked = 0;
                        selectItem.dataset.tiempo_timbre_dias = 0;
                        editarTimbres.disabled = true;
                    });
                });
            } else {
                formData.append('peticion', 3);
                formData.append('tabla', 'blm_horas_recesos');
                let idE = selectItem.dataset.id;
                formData.append('idE', idE);
                formData.append('timbreDia', timbreDia);
                let campos = {
                    'id': 'idE',
                    'tiempo_timbre_dias': 'timbreDia'
                };
                formData.append('campos', JSON.stringify(campos));
                printFormData(formData);
                data.target = '../blocks/planificacion/peticionesSql.php';
                data.method = 'POST';
                data.send = true;
                data.form = [false, formData];
                ajaxData(data).then((res) => {
                    msGObj.loader.style.display = 'block';
                    console.log(res);
                    if (res[0] == true) {
                        console.log(res[2].tiempo_timbre_dias);
                        if (res[2].tiempo_timbre_dias == 1) {
                            EHDTADidTimbres.checked = 1;
                            selectItem.dataset.tiempo_timbre_dias = 1;
                            editarTimbres.disabled = false;
                        }
                        if (res[2].tiempo_timbre_dias == 0) {
                            EHDTADidTimbres.checked = 0;
                            selectItem.dataset.tiempo_timbre_dias = 0;
                            editarTimbres.disabled = true;
                        }
                    } else {
                        msGObj.labelLoader.style.color = 'tomato';
                        msGObj.labelLoader.innerHTML = langText.msg_no_data;
                    }
                    closeWorkLand(msGObj);
                });
            }
        } else {
            EHDTADidTimbres.checked = 0;
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    tableBodyTimbres.addEventListener('click', (e) => {
        resultadoDiasTimbres.innerHTML = '';
        selectItem = getChooseItem('tr', tableBodyTimbres, 'choosed')[1];
        selectItemEscuela = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectedOption(ValidoParaidTimbres).dataset.tipo == 0) {
            console.log('no se que hacer')
            table = {
                name: 'blm_active_tiempo_timbre_dias',
                fields: ['*'],
                conditions: { 'id_horas_recesos': ['=', selectItem.dataset.id], 'id_timbre': ['=', ValidoParaidTimbres.value] }
            };
            searchItemsDB(table).then((res) => {
                console.log(res);
                let tiempo;
                for (c in res) {
                    tiempo = res[c].tiempo_timbre_dias;
                }
                if (tiempo == 1) {
                    EHDTADidTimbres.checked = 1;
                    editarTimbres.disabled = false;
                }
                if (tiempo == 0) {
                    EHDTADidTimbres.checked = 0;
                    editarTimbres.disabled = true;
                }
                infoDiffDaySchechule();
            }).catch((error) => {
                EHDTADidTimbres.checked = 0;
                editarTimbres.disabled = true;
            });
        } else {
            if (selectItem != 'none') {
                if (selectItem.dataset.tiempo_timbre_dias == 1) {
                    EHDTADidTimbres.checked = 1;
                    editarTimbres.disabled = false;
                }
                if (selectItem.dataset.tiempo_timbre_dias == 0) {
                    EHDTADidTimbres.checked = 0;
                    editarTimbres.disabled = true;
                }
                infoDiffDaySchechule();
            }
        }
    });

    function infoDiffDaySchechule() {
        let tableName, cond;
        if (selectItemEscuela.dataset.tiempo_timbre_clases == 0) {
            tableName = 'blm_vista_multi_dia';
            cond = { 'id_hora': ['=', selectItem.dataset.id] };
        }
        if (selectItemEscuela.dataset.tiempo_timbre_clases == 1) {
            if (selectedOption(ValidoParaidTimbres).dataset.tipo == 1) {
                tableName = 'blm_vista_multi_dia';
                cond = { 'id_hora': ['=', selectItem.dataset.id] };
            } else {
                tableName = 'blm_vista_multi_dia_timbre';
                cond = { 'id_timbre': ['=', ValidoParaidTimbres.value], 'id_hora': ['=', selectItem.dataset.id] };
            }
        }
        table = {
            name: tableName,
            fields: ['*'],
            conditions: cond
        };
        console.log(selectItem, table)
        searchItemsDB(table).then((dias) => {
            console.log(dias)
            return orderDBresponse(dias, 'id_dia');
        }).then((sortData) => {
            if (Object.keys(sortData).length > 0) {
                let stringDia = new Array();
                sortData.forEach(element => {
                    stringDia.push(`${element.dia}: ${element.hora_inicio} - ${element.hora_fin}`);
                });
                resultadoDiasTimbres.innerHTML = stringDia.join(', ');
            }
        }).catch(error => {
            resultadoDiasTimbres.innerHTML = '';
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = error;
        });
    }

    editarTimbres.addEventListener('click', (e) => {
        frmPeriodo.reset();
        let flag = false;
        selectItem = getChooseItem('tr', tableBodyTimbres, 'choosed')[1];
        selectItemEscuela = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectItem != "none") {
            let itemData = selectItem.dataset;
            if (itemData.hasOwnProperty('new_inicio') && itemData.hasOwnProperty('new_fin')) {
                P1Periodo.innerHTML = `${itemData.nombre} : ${itemData.new_inicio} - ${itemData.new_fin}`;
            } else {
                P1Periodo.innerHTML = `${itemData.nombre} : ${itemData.inicio} - ${itemData.fin}`;
            }
            let diasml = document.querySelectorAll(".diasMultiplesPeriodo");
            let inputdias;
            console.log((selectedOption(ValidoParaidTimbres).dataset.tipo));
            if (selectedOption(ValidoParaidTimbres).dataset.tipo == 1) {
                console.log((selectedOption(ValidoParaidTimbres).dataset.tipo));
                table = {
                    name: 'blm_multi_dia',
                    fields: ['*'],
                    conditions: { 'id_hora': ['=', itemData.id] }
                };
            } else if (selectedOption(ValidoParaidTimbres).dataset.tipo == 0) {
                table = {
                    name: 'blm_multi_dia_timbre',
                    fields: ['*'],
                    conditions: { 'id_hora': ['=', itemData.id], 'id_timbre': ['=', ValidoParaidTimbres.value] }
                };
            } else {
                table = {
                    name: 'blm_multi_dia',
                    fields: ['*'],
                    conditions: { 'id_hora': ['=', itemData.id] }
                };
            }
            for (let index = 0; index < selectItemEscuela.dataset.dias; index++) {
                delete diasml[index].dataset.editar;
                delete diasml[index].dataset.dia;
                delete diasml[index].dataset.id;
                diasml[index].classList.remove('oculto');
                diasml[index].dataset.dia = index + 1;
                console.log(table);
                searchItemsDB(table).then((respuesta) => {
                    console.log(respuesta)
                    inputdias = diasml[index].querySelectorAll("input");
                    for (c in respuesta) {
                        if (diasml[index].dataset.dia == respuesta[c].id_dia) {
                            diasml[index].dataset.id = respuesta[c].id;
                            inputdias[0].value = respuesta[c].hora_inicio;
                            inputdias[1].value = respuesta[c].hora_fin;
                            diasml[index].dataset.editar = "1";
                        }
                    }
                }).catch((e) => {
                    console.log(e);
                });
            }
            peticionFrmPeriodo.value = 2;
            idFrmPeriodo.value = itemData.id;
            if (selectItemEscuela.dataset.tiempo_timbre_clases == 1 && selectedOption(ValidoParaidTimbres).dataset.tipo != 1) {
                if (ValidoParaidTimbres.value != 0) {
                    timbrePeriodo.value = ValidoParaidTimbres.value;
                    camposFrmPeriodo.value = JSON.stringify({ 'hora_inicio': 'horaInicio', 'hora_fin': 'horaFin', 'id_dia': 'idDay', 'id_hora': 'idHora', 'id_timbre': 'idTimbre' });
                    tablaFrmPeriodo.value = 'blm_multi_dia_timbre';
                    flag = true;
                } else {
                    data = {
                        'dataSets': {
                            action: 'Info',
                            modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                        },
                        'msg': langText.msg_alert_select_timbre,
                        'ico': 'info',
                        'closeDataSet': {
                            visible: 0
                        }
                    };
                    changeMsgModal(data);
                    $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                }
            } else if (selectItemEscuela.dataset.tiempo_timbre_clases == 0) {
                camposFrmPeriodo.value = JSON.stringify({ 'hora_inicio': 'horaInicio', 'hora_fin': 'horaFin', 'id_dia': 'idDay', 'id_hora': 'idHora' });
                tablaFrmPeriodo.value = 'blm_multi_dia';
                flag = true;
            } else if (selectItemEscuela.dataset.tiempo_timbre_clases == 1 && selectedOption(ValidoParaidTimbres).dataset.tipo == 1) {
                camposFrmPeriodo.value = JSON.stringify({ 'hora_inicio': 'horaInicio', 'hora_fin': 'horaFin', 'id_dia': 'idDay', 'id_hora': 'idHora' });
                tablaFrmPeriodo.value = 'blm_multi_dia';
                flag = true;
                console.log('Estoy entrando');
            }
            if (flag) {
                $('#ModalPeriodo').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalPeriodoAceptar.addEventListener('click', (e) => {
        let diasml = document.querySelectorAll(".diasMultiplesPeriodo");
        diasml.forEach((element, i) => {
            let data = element.dataset;
            if (e = data.hasOwnProperty('dia')) {
                let content = findElements('div', element).items;
                let inicio = findElements('input', content[0]).items[0].value;
                let fin = findElements('input', content[1]).items[0].value;
                if (inicio != "" && fin != "") {
                    console.log(inicio, fin);
                    let formData = new FormData();
                    let table = { name: tablaFrmPeriodo.value, campos: JSON.parse(camposFrmPeriodo.value) }
                    let peticion = 0;
                    let idDay = data.dia;
                    formData.append('idDay', idDay);
                    formData.append('horaInicio', inicio);
                    formData.append('horaFin', fin);
                    formData.append('idTimbre', timbrePeriodo.value);
                    formData.append('idHora', idFrmPeriodo.value);
                    if (e = data.hasOwnProperty('editar')) {
                        table.campos.id = 'idMulti';
                        formData.append('idMulti', data.id);
                        peticion = 3;
                    } else {
                        peticion = 2;
                    }
                    console.log(table);
                    printFormData(formData);
                    saveItemsDB(langText, peticion, formData, false, table).then(res => {
                        msGObj.loader.style.display = 'block';
                        if (res[0]) {
                            msGObj.labelLoader.style.color = '#28a745';
                            msGObj.labelLoader.innerHTML = langText.msg_saved;
                        } else {
                            msGObj.labelLoader.innerHTML = langText.msg_no_save;
                            msGObj.labelLoader.style.color = 'tomato';
                        }
                        infoDiffDaySchechule();
                    }).catch(e => {
                        msGObj.labelLoader.innerHTML = e;
                        msGObj.labelLoader.style.color = 'tomato';
                    });
                    closeWorkLand(msGObj);
                } else {
                    let table = {
                        name: tablaFrmPeriodo.value,
                        id: data.id
                    };
                    console.log(table, data.id)
                    deleteItemDB(table)
                }
            }
        });
        closeModal(JSON.stringify({ status: 1, close: ['ModalPeriodo'] }));
    });

    addItemTimbres.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectItem != 'none') {
            if (selectItem.dataset.tiempo_timbre_clases == 1) {
                tablaFrmAddTimbre.value = 'blm_timbres';
                idItemAddTimbre.value = selectItem.dataset.id;
                camposFrmAddTimbre.value = JSON.stringify({ 'nombre': 'nombreTimbreAddTimbre', 'id_horario': 'idItemAddTimbre' });
                peticionFrmAddTimbre.value = 2;
                $('#ModalAddTimbre').modal({ backdrop: 'static', keyboard: false });
            } else {
                data = {
                    'dataSets': {
                        action: 'Info',
                        modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    },
                    'msg': langText.msg_info_no_disponible,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 0,
                        modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0,
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalAddTimbreAgregar.addEventListener('click', () => {
        saveItemsDB(langText, 2, frmAddTimbre, true).then(res => {
            console.log(res);
            msGObj.loader.style.display = 'block';
            if (res[0]) {
                msGObj.labelLoader.style.color = '#28a745';
                msGObj.labelLoader.innerHTML = langText.msg_saved;
            } else {
                msGObj.labelLoader.innerHTML = langText.msg_no_save;
                msGObj.labelLoader.style.color = 'tomato';
            }
            closeModal(JSON.stringify({ status: 1, close: ['ModalAddTimbre', 'ModalTimbres'] }));
            closeWorkLand(msGObj);
        });
    });

    ValidoParaidTimbres.addEventListener('change', (e) => {
        resultadoDiasTimbres.innerHTML = '';
        if (ValidoParaidTimbres.value != 'none') {
            let selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
            table = {
                name: 'blm_vista_horas_recesos',
                fields: ['*'],
                conditions: { 'id_horario': ['=', selectItem.dataset.id] },
                order: { 'inicio': 'ASC' }
            };
            searchItemsDB(table).then((res) => {
                msGObj.loader.style.display = 'block';
                deleteAllChilds(tableBodyTimbres);
                if (Object.keys(res).length > 0) {
                    return orderDBresponse(res, 'inicio');
                } else {
                    msGObj.labelLoader.style.color = 'tomato';
                    msGObj.labelLoader.innerHTML = langText.msg_no_data;
                }
            }).then(quickSortData => {
                quickSortData.forEach((element, i) => {
                    console.log(element.nombre_timbre)
                    let cualquiera = element.nombre_timbre == null ? langText.msg_info_for_all : element.nombre_timbre;
                    data = {
                        'id': element.id,
                        'nombre': element.nombre,
                        'abreviatura': element.abreviatura,
                        'id_horario': element.id_horario,
                        'inicio': element.inicio,
                        'fin': element.fin,
                        'impresion': element.impresion,
                        'impresion_condicion_dos': element.impresion_condicion_dos,
                        'impresion_condicion_tres': element.impresion_condicion_tres,
                        'id_timbre': element.id_timbre,
                        'nombre_timbre': element.nombre_timbre,
                        'tipo': element.tipo,
                        'tiempo_timbre_dias': element.tiempo_timbre_dias,
                        'index': i + 1
                    };
                    items = [i + 1, element.nombre, element.abreviatura, element.inicio, element.fin, element.impresion, cualquiera];
                    tr = addRowTable(data, items, element.id, 'Timbres', tableBodyTimbres);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                });
                closeWorkLand(msGObj);
            }).then(() => {
                table = {
                    name: 'blm_vista_timbres',
                    fields: ['*'],
                    conditions: { 'id_timbre': ['=', ValidoParaidTimbres.value] }
                };
                return searchItemsDB(table);
            }).then((Horas) => {
                if (Object.keys(Horas).length > 0) {
                    let horasTbl = findElements('tr', tableBodyTimbres).items;
                    console.log(Horas, horasTbl);
                    for (const item in Horas) {
                        horasTbl.forEach(element => {
                            if (Horas[item].id_hora_receso == element.dataset.id) {
                                deleteAllChilds(element);
                                let cualquiera = element.dataset.nombre_timbre == "null" ? langText.msg_info_for_all : element.dataset.nombre_timbre;
                                items = [element.dataset.index, element.dataset.nombre, element.dataset.abreviatura, Horas[item].hora_inicio, Horas[item].hora_fin, element.dataset.impresion, cualquiera];
                                let newChildrenEqual = {
                                    'childrenConf': [true, 'equal'],
                                    'child': {
                                        'prefix': `tableRowFieldTimbres`,
                                        'father': element,
                                        'kind': 'td',
                                        'elements': items,
                                    }
                                };
                                element.dataset.new_inicio = Horas[item].hora_inicio;
                                element.dataset.new_fin = Horas[item].hora_fin;
                                buildNewElements(newChildrenEqual);
                            }
                        });
                    }
                }
            }).catch(error => {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = error;
            });
            table = {
                name: 'blm_vista_timbres_grupos',
                fields: ['*'],
                conditions: { 'id_timbre': ['=', ValidoParaidTimbres.value] }
            };
            searchItemsDB(table).then((grupos) => {
                if (Object.keys(grupos).length > 0) {
                    return orderDBresponse(grupos, 'abreviatura_grupo');
                }
            }).then(sortData => {
                let stringGrupo = new Array();
                sortData.forEach(element => {
                    stringGrupo.push(element.abreviatura_grupo);
                });
                resultadoGruposTimbres.innerHTML = stringGrupo.join(',');
            }).catch(e => {
                console.log(e);
                resultadoGruposTimbres.innerHTML = '';
            });
        }
    });

    ubicacionRecreoTimbre.addEventListener('change', (e) => {
        let option = selectedOption(ubicacionRecreoTimbre);
        inicioTimbre.value = option.dataset.inicio;
        finTimbre.value = option.dataset.fin;
    });

    numPeriodosDefPeriodos.addEventListener('change', (e) => {
        msGObj.loader.style.display = 'block';
        table = {
            name: 'blm_periodos',
            fields: ['*'],
            conditions: { 'id_horario': ['=', idFrmDefPeriodos.value], 'tipo': ['=', 0] }
        };
        searchItemsDB(table).then(response => {
            if (Object.keys(response).length > 0) {
                let table = { name: 'blm_periodos' };
                for (const item in response) {
                    table.id = response[item].id;
                    deleteItemDB(table).then(deleted => {
                        if (deleted) {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = langText.msg_deleted;
                        } else {
                            msGObj.labelLoader.innerHTML = langText.msg_no_deleted;
                        }
                        closeWorkLand(msGObj);
                    });
                }
            }
            return '0';
        }).then((r) => {
            let formData = new FormData();
            let periodos = new Array();
            for (let index = 2; index <= numPeriodosDefPeriodos.value; index++) {
                np = index <= 9 ? `0${index}` : index;
                let nom = `Periodo ${np}`;
                let abr = `P ${np}`;
                periodos.push({
                    nombre: nom,
                    abreviatura: abr,
                    id_horario: idFrmEscuela.value,
                    tipo: 0
                });
            }
            formData.append('camposObj', JSON.stringify(periodos));
            saveItemsDB(langText, 5, formData, false, { name: 'blm_periodos' }).then(res => console.log(res));
            closeModal(JSON.stringify({ status: 1, close: ['ModalDefPeriodos'] }));
        }).catch((error) => {
            msGObj.loader.style.display = 'block';
            if (error == "No se encontraron datos en la fuente..." || error == "No data on source...") {
                let formData = new FormData();
                let periodos = new Array();
                for (let index = 2; index <= numPeriodosDefPeriodos.value; index++) {
                    np = index <= 9 ? `0${index}` : index;
                    let nom = `Periodo ${np}`;
                    let abr = `P  ${np}`;
                    periodos.push({
                        nombre: nom,
                        abreviatura: abr,
                        id_horario: idFrmEscuela.value,
                        tipo: 0
                    });
                }
                formData.append('camposObj', JSON.stringify(periodos));
                saveItemsDB(langText, 5, formData, false, { name: 'blm_periodos' }).then(res => console.log(res), closeWorkLand(msGObj));
                closeModal(JSON.stringify({ status: 1, close: ['ModalDefPeriodos'] }));
            }
        });
    });

    numSemanaDefSemanas.addEventListener('change', (e) => {
        msGObj.loader.style.display = 'block';
        table = {
            name: 'blm_semanas',
            fields: ['*'],
            conditions: { 'id_horario': ['=', idFrmDefSemanas.value], 'tipo': ['=', 0] }
        };
        searchItemsDB(table).then(response => {
            if (Object.keys(response).length > 0) {
                let table = { name: 'blm_semanas' };
                for (const item in response) {
                    table.id = response[item].id;
                    deleteItemDB(table).then(deleted => {
                        if (deleted) {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = langText.msg_deleted;
                        } else {
                            msGObj.labelLoader.innerHTML = langText.msg_no_deleted;
                        }
                        closeWorkLand(msGObj);
                    });
                }
            }
            return '0';
        }).then((r) => {
            let formData = new FormData();
            let semanas = new Array();
            for (let index = 2; index <= numSemanaDefSemanas.value; index++) {
                np = index <= 9 ? `0${index}` : index;
                let nom = `Semana ${np}`;
                let abr = `S ${np}`;
                semanas.push({
                    nombre: nom,
                    abreviatura: abr,
                    id_horario: idFrmEscuela.value,
                    tipo: 0
                });
            }
            formData.append('camposObj', JSON.stringify(semanas));
            saveItemsDB(langText, 5, formData, false, { name: 'blm_semanas' }).then(res => console.log(res));
            closeModal(JSON.stringify({ status: 1, close: ['ModalDefSemanas'] }));
        }).catch((error) => {
            if (error == "No se encontraron datos en la fuente..." || error == "No data on source...") {
                msGObj.loader.style.display = 'block';
                let formData = new FormData();
                let semanas = new Array();
                for (let index = 2; index <= numSemanaDefSemanas.value; index++) {
                    ns = index <= 9 ? `0${index}` : index;
                    let nom = `Semana ${ns}`;
                    let abr = `S ${ns}`;
                    semanas.push({
                        nombre: nom,
                        abreviatura: abr,
                        id_horario: idFrmEscuela.value,
                        tipo: 0
                    });
                }
                formData.append('camposObj', JSON.stringify(semanas));
                saveItemsDB(langText, 5, formData, false, { name: 'blm_semanas' }).then(res => console.log(res), closeWorkLand(msGObj));
                closeModal(JSON.stringify({ status: 1, close: ['ModalDefSemanas'] }));
            }
        });
    });

    tableEleccion.addEventListener('click', (e) => {
        let selectItem = getChooseItem('tr', tableBodyEleccion, 'choosed')[1];
        idItemEleccion.value = selectItem.dataset.id;
        let table = {
            name: 'blm_timbres',
            fields: ['id', 'nombre', 'tipo'],
            exeption: ['nombre'],
            conditions: { 'id_horario': ['=', idFrmEscuela.value] }
        };
        searchItemsDB(table).then((respuesta) => {
            let key = respuesta[Object.keys(respuesta)];
            for (let c in respuesta) {
                if (respuesta[c].id != idFrmEleccion.value) {
                    console.log("datos en IF ", respuesta[c].id, idFrmEleccion.value)
                    let table1 = {
                        name: 'blm_vista_timbres_grupos',
                        fields: ['id', 'id_timbre', 'nombre_timbre', 'id_grupo'],
                        conditions: { 'id_timbre': ['=', respuesta[c].id] }
                    };
                    searchItemsDB(table1).then((respuesta1) => {
                        let key = respuesta[Object.keys(respuesta1)];
                        let formGruposTimbres = new FormData();
                        formGruposTimbres.append('peticion', 4);
                        let GruposTimbres = new Object();
                        GruposTimbres.target = '../blocks/planificacion/peticionesSql.php';
                        GruposTimbres.method = 'POST';
                        GruposTimbres.send = true;
                        msGObj.loader.style.display = 'block';
                        formGruposTimbres.append('tabla', 'blm_grupos_timbres');
                        for (let c in respuesta1) {
                            if (selectItem.dataset.id == respuesta1[c].id_grupo) {
                                formGruposTimbres.append('id', respuesta1[c].id);
                                GruposTimbres.form = [false, formGruposTimbres];
                                ajaxData(GruposTimbres).then((res) => {
                                    if (res) {
                                        msGObj.labelLoader.style.color = 'tomato';
                                        msGObj.labelLoader.innerHTML = langText.msg_deleted;
                                    } else {
                                        msGObj.labelLoader.innerHTML = langText.msg_no_deleted;
                                    }
                                    closeWorkLand(msGObj);
                                });
                            }
                        }
                    });
                } else {
                    closeWorkLand(msGObj);
                }
            }
        });
        var i, tr, temp;
        tr = [];
        temp = document.getElementById('tableBodyEleccioneleccion2').getElementsByTagName('TR');
        for (i in temp) {
            if (temp[i].hasOwnProperty) {
                tr.push(temp[i]);
            }
        }
        let insertar = true;
        for (let i = 0; i < tr.length - 3; i++) {
            if (tr[i].dataset.id == selectItem.dataset.id) {
                insertar = false;
                break
            }
        }
        if (insertar == true) {
            saveItemsDB(langText, 2, frmEleccion, true).then(res => {
                console.log(res);
                msGObj.loader.style.display = 'block';
                if (res[0]) {
                    let dataItem = selectItem.dataset;
                    data = { 'id': dataItem.id, 'nombre': dataItem.nombre, 'abreviatura': dataItem.abreviatura };
                    items = [dataItem.id, dataItem.nombre, dataItem.abreviatura];
                    tr = addRowTable(data, items, dataItem.id, 'Eleccion2', tableBodyEleccioneleccion2);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                    deleteItem(selectItem);
                    msGObj.labelLoader.style.color = '#28a745';
                    msGObj.labelLoader.innerHTML = langText.msg_saved;
                } else {
                    msGObj.labelLoader.innerHTML = langText.msg_no_save;
                    msGObj.labelLoader.style.color = 'tomato';
                }
                closeWorkLand(msGObj);
            });
        }
    });

    tableEleccioneleccion2.addEventListener('click', (e) => {
        let selectItem = getChooseItem('tr', tableBodyEleccioneleccion2, 'choosed')[1];
        item = selectItem.dataset.id_grupo_timbre;
        var i, tr, temp;
        tr = [];
        temp = document.getElementById('tableBodyEleccion').getElementsByTagName('TR');
        for (i in temp) {
            if (temp[i].hasOwnProperty) {
                tr.push(temp[i]);
            }
        }
        let insertar = true;
        for (let i = 0; i < tr.length - 3; i++) {
            if (tr[i].dataset.id == selectItem.dataset.id) {
                insertar = false;
                break
            }
        }
        idItemEleccion.value = selectItem.dataset.id;
        let table = {
            name: 'blm_timbres',
            fields: ['id', 'nombre', 'tipo'],
            exeption: ['nombre'],
            conditions: { 'id_horario': ['=', idFrmEscuela.value] }
        };
        searchItemsDB(table).then((respuesta) => {
            for (let c in respuesta) {
                let table1 = {
                    name: 'blm_vista_timbres_grupos',
                    fields: ['id', 'id_timbre', 'nombre_timbre', 'id_grupo'],
                    conditions: { 'id_timbre': ['=', respuesta[c].id] }
                };
                console.log(respuesta);
                searchItemsDB(table1).then((respuesta1) => {
                    for (let n in respuesta1) {
                        let timbre = respuesta1[n].nombre_timbre == 'Timbre 1' ? 1 : 0;
                        if (timbre == 1) {
                            console.log(respuesta1[n])
                            let formData = new FormData();
                            let idFrm = item;
                            formData.append('idTimbre', respuesta1[n].id_timbre);
                            formData.append('idFrm', idFrm);
                            console.log(item, respuesta1[n].id_timbre)
                            saveItemsDB(langText, 3, formData, false, { name: 'blm_grupos_timbres', campos: { 'id': 'idFrm', 'id_timbre': 'idTimbre' } }).then(res => {
                                if (res) {
                                    if (tableBodyEleccioneleccion2.childNodes.length != 0) {
                                        tableBodyEleccioneleccion2.removeChild(selectItem);
                                    }
                                    if (insertar == true) {
                                        let dataItem = selectItem.dataset;
                                        data = { 'id': dataItem.id, 'nombre': dataItem.nombre, 'abreviatura': dataItem.abreviatura };
                                        items = [dataItem.id, dataItem.nombre, dataItem.abreviatura];
                                        tr = addRowTable(data, items, dataItem.id, 'Eleccion2', tableBodyEleccion);
                                        tr.child.addEventListener('click', (e) => {
                                            selectRow(e);
                                        });
                                    }
                                    msGObj.labelLoader.style.color = 'tomato';
                                } else {}
                                if (res[0]) {
                                    msGObj.loader.style.display = 'block';
                                    msGObj.labelLoader.style.color = '#28a745';
                                    msGObj.labelLoader.innerHTML = langText.msg_saved;
                                } else {
                                    msGObj.labelLoader.innerHTML = langText.msg_no_save;
                                    msGObj.labelLoader.style.color = 'tomato';
                                }
                            });
                            closeWorkLand(msGObj);
                        }
                    }
                });
            }
        });
    });

    ValidoVPreviaVPrevia.addEventListener('change', (e) => {
        let timbreselect = selectedOption(ValidoVPreviaVPrevia);
        if (ValidoVPreviaVPrevia.value != 0) {
            table = {
                name: 'blm_dias_tl',
                fields: ['*'],
                limit: [selectItem.dataset.dias],
                order: { 'id': 'ASC' }
            }
            searchItemsDB(table).then((dias) => {
                //console.log(dias, '---->');
                return orderDBresponse(dias, 'id');
            }).then((sortData) => {
                if (Object.keys(sortData).length > 0) {
                    let stringDia = new Array();
                    let stringIdDia = new Array();
                    sortData.forEach(element => {
                        stringDia.push(`${element.dia}`);
                        stringIdDia.push(`${element.id}`);
                    });
                    let timbreselect = selectedOption(ValidoVPreviaVPrevia);
                    let hrs = tableTitleVPrevia.childNodes;
                    //Llenar matriz horas por dias modificadas
                    deleteAllChilds(tableBodyVPrevia);
                    for (let a = 0; a < stringIdDia.length; a++) {
                        let it = new Array();
                        it[0] = stringDia[a];
                        for (const k in hrs) {
                            if (k != 0 && k < hrs.length) {
                                it[k] = '';
                                console.log(selectedOption(ValidoVPreviaVPrevia).dataset.tipo)
                                if (selectedOption(ValidoVPreviaVPrevia).dataset.tipo == 1) {
                                    if (hrs[k].dataset.ttd == 1) {
                                        table = {
                                            name: 'blm_multi_dia',
                                            fields: ['*'],
                                            conditions: { 'id_hora': ['=', hrs[k].dataset.id], 'id_dia': ['=', stringIdDia[a]] }
                                        }
                                        searchItemsDB(table).then((rs) => {
                                            for (const hd in rs) {
                                                tableBodyVPrevia.rows[a].cells[k].innerHTML = `<center>${rs[hd].hora_inicio} - ${rs[hd].hora_fin}</center>`;
                                                table = {
                                                    name: 'blm_timbres_hr',
                                                    fields: ['*'],
                                                    conditions: { 'id_horas_recesos': ['=', hrs[k].dataset.id] }
                                                }
                                                searchItemsDB(table).then((res1) => {
                                                    for (const h1 in res1) {
                                                        if (res1[h1].id_timbre != selectedOption(ValidoVPreviaVPrevia).dataset.id) {
                                                            tableBodyVPrevia.rows[a].cells[k].innerHTML = '';
                                                        }
                                                    }
                                                }).catch(error => {
                                                    console.log(error);
                                                });
                                            }
                                            return it;
                                        }).catch(error => {
                                            console.log(error);
                                        });
                                    }
                                } else {
                                    table = {
                                        name: 'blm_active_tiempo_timbre_dias',
                                        fields: ['*'],
                                        conditions: { 'id_horas_recesos': ['=', hrs[k].dataset.id], 'id_timbre': ['=', ValidoVPreviaVPrevia.value] }
                                    }
                                    searchItemsDB(table).then((rse) => {
                                        for (const et in rse) {
                                            if (rse[et].tiempo_timbre_dias == 1) {
                                                table = {
                                                    name: 'blm_multi_dia_timbre',
                                                    fields: ['*'],
                                                    conditions: { 'id_hora': ['=', hrs[k].dataset.id], 'id_timbre': ['=', ValidoVPreviaVPrevia.value], 'id_dia': ['=', stringIdDia[a]], }
                                                }
                                                searchItemsDB(table).then((rs) => {
                                                    for (const hd in rs) {
                                                        tableBodyVPrevia.rows[a].cells[k].innerHTML = `<center>${rs[hd].hora_inicio} - ${rs[hd].hora_fin}</center>`;
                                                        console.log(`<center>${rs[hd].hora_inicio} - ${rs[hd].hora_fin}</center>`)
                                                        table = {
                                                            name: 'blm_timbres_hr',
                                                            fields: ['*'],
                                                            conditions: { 'id_horas_recesos': ['=', hrs[k].dataset.id] }
                                                        }
                                                        searchItemsDB(table).then((res1) => {
                                                            for (const h1 in res1) {
                                                                if (res1[h1].id_timbre != selectedOption(ValidoVPreviaVPrevia).dataset.id) {
                                                                    tableBodyVPrevia.rows[a].cells[k].innerHTML = '';
                                                                }
                                                            }
                                                        }).catch(error => {
                                                            console.log(error);
                                                        });
                                                    }
                                                    return it;
                                                }).catch(error => {
                                                    console.log(error);
                                                });
                                            }
                                        }
                                    }).catch(error => {
                                        console.log(error);
                                    });
                                }
                            }
                        }
                        data = {
                            'id': stringIdDia[a],
                            'dia': stringDia[a]
                                //'data_horas': JSON.stringify(horas)
                        }
                        td = addRowTable(data, it, 'vp', 'Dias', tableBodyVPrevia);
                        td.child.addEventListener('click', (e) => {
                            selectRow(e);
                        });
                    }
                    //Fin Llenar matriz horas por dias modificadas
                    //llenar cabecera
                    for (const a in hrs) {
                        if (a != 0 && a < (hrs.length - 2)) {
                            tableTitleVPrevia.cells[a].innerHTML = '<center>' + hrs[a].dataset.nombre + '<br/>' + hrs[a].dataset.i + ' - ' + hrs[a].dataset.f + '</center>';
                            table = {
                                name: 'blm_vista_timbres',
                                fields: ['*'],
                                conditions: { 'id_hora_receso': ['=', hrs[a].dataset.id], 'id_timbre': ['=', timbreselect.dataset.id] }
                            }
                            searchItemsDB(table).then((res2) => {
                                for (const h2 in res2) {
                                    tableTitleVPrevia.cells[a].innerHTML = '<center>' + res2[h2].nombre_hora + '<br/>' + res2[h2].hora_inicio + ' - ' + res2[h2].hora_fin + '</center>';
                                }
                            }).catch(error => {});
                            table = {
                                name: 'blm_timbres_hr',
                                fields: ['*'],
                                conditions: { 'id_horas_recesos': ['=', hrs[a].dataset.id] }
                            }
                            searchItemsDB(table).then((res1) => {
                                for (const h1 in res1) {
                                    if (res1[h1].id_timbre != selectedOption(ValidoVPreviaVPrevia).dataset.id) {
                                        tableTitleVPrevia.cells[a].innerHTML = '';
                                    } else {
                                        tableTitleVPrevia.cells[a].innerHTML = '<center>' + hrs[a].dataset.nombre + '<br/>' + hrs[a].dataset.i + ' - ' + hrs[a].dataset.f + '</center>';
                                        table = {
                                            name: 'blm_vista_timbres',
                                            fields: ['*'],
                                            conditions: { 'id_hora_receso': ['=', hrs[a].dataset.id], 'id_timbre': ['=', timbreselect.dataset.id] }
                                        }
                                        searchItemsDB(table).then((res2) => {
                                            for (const h2 in res2) {
                                                tableTitleVPrevia.cells[a].innerHTML = '<center>' + res2[h2].nombre_hora + '<br/>' + res2[h2].hora_inicio + ' - ' + res2[h2].hora_fin + '</center>';
                                            }
                                        }).catch(error => {
                                            console.log(error);
                                        });
                                    }
                                }
                            }).catch(error => {});
                        }
                    }
                    //Fin de llenar cabecera
                }
            }).catch(error => {
                resultadoDiasTimbres.innerHTML = '';
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = error;
            });
        }
    });


    labelBtnModalEscuelaTablaVPrevia.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyEscuelaTabla, 'choosed')[1];
        if (selectItem != "none") {
            msGObj.loader.style.display = 'block';
            let dfprevia = selectItem.dataset;
            console.log(dfprevia)
            let hide = [ValidoVPreviaVPrevia.parentNode];
            if (dfprevia.tiempo_timbre_clases == 1) {
                hideElement(hide, 1);
                select = {
                    item: ValidoVPreviaVPrevia,
                    prefix: `${ValidoVPreviaVPrevia.id}Op`
                };
                table = {
                    name: 'blm_timbres',
                    fields: ['id', 'nombre', 'tipo'],
                    exeption: ['nombre'],
                    conditions: { 'id_horario': ['=', dfprevia.id] }
                };
                fillSelectItem(select, table, { firstItem: false });
            } else {
                hideElement(hide, 0);
            }
            if (dfprevia.periodo_bool == 1) {
                table = {
                    name: 'blm_periodos',
                    conditions: { id_horario: dfprevia.id, tipo: 0 }
                };
                countRecordsDB(table).then(records => {
                    if (records > 0) {
                        select = {
                            item: ValidoVPreviaPeriodosVPrevia,
                            prefix: `${ValidoVPreviaVPrevia.id}Op`
                        };
                        table = {
                            name: 'blm_periodos',
                            fields: ['id', 'nombre'],
                            exeption: ['nombre'],
                            conditions: { 'id_horario': ['=', dfprevia.id] }
                        };
                        fillSelectItem(select, table, { firstItem: false });
                        hideElement([ValidoVPreviaPeriodosVPrevia.parentNode], 1);
                    } else {
                        hideElement([ValidoVPreviaPeriodosVPrevia.parentNode], 0);
                    }
                }).catch(e => console.error(e));
            } else {
                hideElement([ValidoVPreviaPeriodosVPrevia.parentNode], 0);
            }
            if (dfprevia.semana_bool == 1) {
                table = {
                    name: 'blm_semanas',
                    conditions: { id_horario: dfprevia.id, tipo: 0 }
                };
                countRecordsDB(table).then(records => {
                    if (records > 0) {
                        select = {
                            item: ValidoVPreviaSemanasVPrevia,
                            prefix: `${ValidoVPreviaVPrevia.id}Op`
                        };
                        table = {
                            name: 'blm_semanas',
                            fields: ['id', 'nombre'],
                            exeption: ['nombre'],
                            conditions: { 'id_horario': ['=', dfprevia.id] }
                        };
                        fillSelectItem(select, table, { firstItem: false });
                        hideElement([ValidoVPreviaSemanasVPrevia.parentNode], 1);
                    } else {
                        hideElement([ValidoVPreviaSemanasVPrevia.parentNode], 0);
                    }
                }).catch(e => console.error(e));
            } else {
                hideElement([ValidoVPreviaSemanasVPrevia.parentNode], 0);
            }
            deleteAllChilds(tableBodyVPrevia);
            table = {
                name: 'blm_vista_horas_recesos',
                fields: ['*'],
                conditions: { 'id_horario': ['=', selectItem.dataset.id] }
            }
            searchItemsDB(table).then((horas) => {
                return orderDBresponse(horas, 'inicio');
            }).then((sortData) => {
                if (Object.keys(sortData).length > 0) {
                    deleteAllChilds(tableTitleVPrevia, ['tableVPrevia#']);
                    let stringId = new Array();
                    let stringNom = new Array();
                    let stringHi = new Array();
                    let stringHf = new Array();
                    let stringTp = new Array();
                    let stringIdT = new Array();
                    let stringTTD = new Array();
                    sortData.forEach(element => {
                        stringId.push(`${element.id}`);
                        stringNom.push(`${element.nombre}`);
                        stringHi.push(`${element.inicio}`);
                        stringHf.push(`${element.fin}`);
                        stringTp.push(`${element.tipo}`);
                        stringIdT.push(`${element.id_timbre}`);
                        stringTTD.push(`${element.tiempo_timbre_dias}`);
                    });
                    console.log(stringId)
                    setTimeout(() => {
                        for (const z in stringId) {
                            console.log(z)
                            let hr;
                            console.log(stringIdT[z], selectedOption(ValidoVPreviaVPrevia).dataset.id);
                            if (stringIdT[z] == 'null' || stringIdT[z] == selectedOption(ValidoVPreviaVPrevia).dataset.id) {
                                console.log(`<center>${stringNom[z]}<br/> ${stringHi[z]} - ${stringHf[z]}</center>`)
                                hr = `<center>${stringNom[z]}<br/> ${stringHi[z]} - ${stringHf[z]}</center>`;
                            } else {
                                hr = '';
                            }
                            let newChild = {
                                'childrenConf': [false],
                                'child': {
                                    'prefix': 'VPrevia',
                                    'name': stringNom[z],
                                    'father': tableTitleVPrevia,
                                    'kind': 'th',
                                    'innerHtml': hr,
                                    'data': { 'id': stringId[z], 'tipo': stringTp[z], 'nombre': stringNom[z], 'i': stringHi[z], 'f': stringHf[z], 'ttd': stringTTD[z] },
                                    'classes': ['nowrap', 'test'],
                                    'attributes': { 'scope': 'col' }
                                }
                            };
                            buildNewElements(newChild);
                        }
                    }, 2000);
                }
            }).catch(error => {});
            setTimeout(() => {
                closeWorkLand(msGObj);
                $('#ModalVPrevia').modal({ backdrop: 'static', keyboard: false }).css("padding-right", 0);
                $("#ModalVPrevia").hover(function() {
                    $(".modal-dialog", this).addClass("modal-dialog-full-widthVP");
                    $(".modal-content", this).addClass("modal-content-full-widthVP");
                    $(".modal-body", this).addClass("modal-body-fullVP");
                    $(".modal-footer", this).addClass("modal-footer-full-widthVP");
                });
                let hrs = tableTitleVPrevia.childNodes;
                table = {
                    name: 'blm_dias_tl',
                    fields: ['*'],
                    limit: [selectItem.dataset.dias],
                    order: { 'id': 'ASC' }
                }
                searchItemsDB(table).then((dias) => {
                    return orderDBresponse(dias, 'id');
                }).then((sortData) => {
                    if (Object.keys(sortData).length > 0) {
                        let stringDia = new Array();
                        let stringIdDia = new Array();
                        sortData.forEach(element => {
                            stringDia.push(`${element.dia}`);
                            stringIdDia.push(`${element.id}`);
                        });
                        deleteAllChilds(tableBodyVPrevia);
                        for (let a = 0; a < stringIdDia.length; a++) {
                            let it = new Array();
                            it[0] = stringDia[a];
                            console.log(stringDia[a]);
                            for (const k in hrs) {
                                if (k != 0 && k < hrs.length) {
                                    it[k] = '';
                                    if (hrs[k].dataset.ttd == 1) {
                                        table = {
                                            name: 'blm_multi_dia',
                                            fields: ['*'],
                                            conditions: { 'id_hora': ['=', hrs[k].dataset.id], 'id_dia': ['=', stringIdDia[a]] }
                                        }
                                        searchItemsDB(table).then((rs) => {
                                            for (const hd in rs) {
                                                console.log(`<center>${rs[hd].hora_inicio} - ${rs[hd].hora_fin}</center>`);
                                                tableBodyVPrevia.rows[a].cells[k].innerHTML = `<center>${rs[hd].hora_inicio} - ${rs[hd].hora_fin}</center>`;
                                                table = {
                                                    name: 'blm_timbres_hr',
                                                    fields: ['*'],
                                                    conditions: { 'id_horas_recesos': ['=', hrs[k].dataset.id] }
                                                }
                                                searchItemsDB(table).then((res1) => {
                                                    for (const h1 in res1) {
                                                        console.log(selectedOption(ValidoVPreviaVPrevia).dataset.id, res1[h1].id_timbre)
                                                        if (res1[h1].id_timbre != selectedOption(ValidoVPreviaVPrevia).dataset.id) {
                                                            tableBodyVPrevia.rows[a].cells[k].innerHTML = '';
                                                        }
                                                    }
                                                }).catch(error => {
                                                    console.log(error);
                                                });
                                            }
                                            return it;
                                        }).catch(error => {
                                            console.log(error);
                                        });
                                    }
                                }
                            }
                            data = {
                                'id': stringIdDia[a],
                                'dia': stringDia[a]
                                    //'data_horas': JSON.stringify(horas)
                            }
                            td = addRowTable(data, it, 'vp', 'Dias', tableBodyVPrevia);
                            td.child.addEventListener('click', (e) => {
                                selectRow(e);
                            });
                        }
                    }
                }).catch(error => {
                    resultadoDiasTimbres.innerHTML = '';
                    msGObj.labelLoader.style.color = 'tomato';
                    msGObj.labelLoader.innerHTML = error;
                });
            }, 3000);
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalVPreviaClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalVPrevia'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalRdiasClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalRdias'] })
            },
            'msg': langText.msg_alert_Cancel,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalAddTimbreClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalAddTimbre'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalEleccionClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalEleccion'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalDefSemanasClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalDefSemanas'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalTimbresCerrar.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalTimbres'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalObjetoClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalObjeto'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalDefPeriodosClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalDefPeriodos'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalTimbreClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalTimbre'] })
            },
            'msg': langText.msg_alert_Cancel,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalPeriodoClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalPeriodo'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });
});