window.addEventListener('load', () => {
    let horario = titleModulePlaneacion.dataset;
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    //Buscar en el código :
    //- Posible cambio
    //- Revisar
    //- Se quito el id de los td
    //- En proceso

    //Create dataTable
    btnPlaneacionTablaP.addEventListener('click', (e) => {
        abrirModlVistaTablero();
    });

    $("#selectByTPrincipal").change(function () {
        table = {
            name: 'blm_vista_tablero',
            fields: ['*'],
            conditions: {
                'id_horario': ['=', horario.id]
            },
            rs: true
        };
        searchItemsDB(table).then((fichas) => {
            return fichas;
        }).catch(e => {
            return [];
        }).then(fichas => {
            table = {
                name: 'blm_vista_tablero_na',
                fields: ['*'],
                conditions: {
                    'id_horario': ['=', horario.id]
                },
                rs: true
            };
            searchItemsDB(table).then((fichasNoA) => {
                return fichasNoA
            }).catch(e => {
                return [];
            }).then(fichasNoA => {
                let arrFichas = Object.values(fichas);
                let arrFichasNoA = Object.values(fichasNoA);
                let tipo = parseInt($(this).val());
                table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                    tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                        tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                            tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                scheduleMaker(table, arrFichas);
                notAssigned(table, arrFichasNoA);
            });
        });
    });

    //JavaScript Effects
    $("#ModalTPrincipal").on("mouseover", ".fichasHover, .fichaNoAHover", function (e) {
        /* let tipoAbr = table.selectBy == 1 ? "abreviatura_curso" :
            table.selectBy == 2 ? "abreviatura_grupo" :
            table.selectBy == 3 ? "abreviatura_grupo" :
            table.selectBy == 4 ? "abreviatura_profesor" : "abreviatura_curso"; */
        let cmpF = $(this).data("ficha");
        let cmpL = $(this).data("id_leccion");
        table = {
            fields: ['group_concat(distinct abreviatura_profesor) profmerge', 'group_concat(distinct abreviatura_grupo) groupmerge'],
            conditions: {
                'id_horario': ['=', horario.id],
                'id_leccion': ['=', cmpL],
                'ficha': ['=', cmpF]
            },
            rs: true
        };
        if (e.target.className == "fichaNoAHover") {
            table.name = 'blm_vista_tablero_na';
        } else {
            table.name = 'blm_vista_tablero';
        }
        searchItemsDB(table).then((abr) => {
            let tipoAbr = e.target.textContent;
            let aula = e.target.dataset.abreviatura_aula == "null" || e.target.className == "fichaNoAHover" ? "" : e.target.dataset.abreviatura_aula;
            let receso = e.target.dataset.texto_recesos == "undefined" || !e.target.dataset.texto_recesos ? "" : e.target.dataset.texto_recesos;
            $(".recuadroFicha").replaceWith(`<div class="recuadroFicha" style="background:${e.target.style.background}" title="${tipoAbr}">${tipoAbr}</div>`);
            $(".datoAsignatura").replaceWith(`<p class="datoAsignatura font-weight-bolder">${e.target.dataset.abreviatura_curso} - ${e.target.dataset.nombre_curso}</p>`);
            $(".datoClase").replaceWith(`<p class="datoClase">${abr[0].groupmerge}</p>`);
            $(".datoProfesor").replaceWith(`<p class="datoProfesor">${abr[0].profmerge}</p>`);
            $(".datoAula").replaceWith(`<p class="datoAula">${aula}</p>`);
            $(".datoDuracion").replaceWith(`<p class="datoDuracion">${e.target.dataset.duracion} lecciones</p>`);
            $(".datoReceso").replaceWith(`<p class="datoReceso">Receso entre: ${receso}</p>`);
        })
    });

    $("#ModalTPrincipal").on("click", ".fichasHover, .fichaNoAHover", function (e) {
        $("#ModalTPrincipal tr:eq(1),#ModalTPrincipal th").removeAttr("style");
        let lButtonMove = document.getElementById("labelBtnTPrincipalMoverFicha")
        lButtonMove.dataset.type_card = e.target.className;
        cmpIdHorarioTPrincipal.value = $(this).data('id_horario');
        cmpIdTPrincipal.value = $(this).data('id');
        cmpFichasTPrincipal.value = $(this).data('ficha');
        cmpIdLeccionTPrincipal.value = $(this).data('id_leccion');
        cmpIdCursoTPrincipal.value = $(this).data('id_curso');
        cmpIdGrupoTPrincipal.value = $(this).data('id_grupo');
        cmpDuracionTPrincipal.value = $(this).data('duracion');
        cmpAulaTPrincipal.value = $(this).data('id_aula');

        if ((e.target.className == "fichasHover")) {
            quitSwitch.removeAttribute("style");
            quitClassroom.removeAttribute("style");
            deleteCard.setAttribute("style", "display:block");
            moveCard.classList.replace("col-12", "col-6");
            let BlockProperties = document.getElementById("Block");
            if (e.target.dataset.activo == "0") {
                bloquearFTPrincipal.checked = true;
                BlockProperties.setAttribute("style", "pointer-events: none; opacity: 0.4");
            } else {
                bloquearFTPrincipal.checked = false;
                BlockProperties.removeAttribute("style");
            }
        } else {
            Block.removeAttribute("style");
            quitSwitch.setAttribute("style", "display:none");
            quitClassroom.setAttribute("style", "display:none");
            deleteCard.setAttribute("style", "display:none");
            moveCard.classList.replace("col-6", "col-12");
        }

        select = {
            item: cambiarAulaTPrincipal,
            prefix: `${cambiarAulaTPrincipal.id}Op`
        };
        table = {
            name: 'blm_vista_aula_curso',
            fields: ['id', 'id_aula', 'aula'],
            exeption: ['aula'],
            conditions: { 'id_curso': ['=', $(this).data("id_curso")] }
        };

        fillSelectItem(select, table, { firstItem: true })

        showMenu(e);

        tableLights = {
            name: 'blm_vista_tablero',
            fields: ['*'],
            conditions: {
                'id_horario': ['=', horario.id]
            },
            rs: true
        };
        searchItemsDB(tableLights).then((fichas) => {
            $("#ModalTPrincipal tr:eq(1)").attr("style", "background: #49FF33");

            let arrFichas = Object.values(fichas);
            let read = [];
            let teacherRead = [];
            let arrHours = [];
            //let idRowRead = [];
            //let idRow = selectByTPrincipal.value == "1" ? "id_grupo":
            selectByTPrincipal.value == "2" ? "id_profesor" :
                selectByTPrincipal.value == "3" ? "id_aula" : "id_curso";
            let teachersFilter = e.target.className == "fichasHover" ? arrFichas.filter(filtP => filtP.id_leccion == `${e.target.dataset.id_leccion}` && filtP.ficha == `${e.target.dataset.ficha}`) :
                arrFichas.filter(filtP => filtP.id_leccion == `${e.target.dataset.id_leccion}`);
            teachersFilter.forEach(el => {
                /* 
                Semaforo lateral // Prueba
                if(!idRowRead.includes(el[idRow])){
                    idRowRead.push(el[idRow]);
                    let rowHere = document.getElementById(`yRow${el[idRow]}`);
                    rowHere.setAttribute("style", "background: #49FF33 ;"); 
                } */
                arrHours.push(parseInt(el.hora));
                if (!teacherRead.includes(`Day${el.id_profesor}`)) {
                    teacherRead.push(`Day${el.id_profesor}`);
                    arrFichas.filter(filtP => filtP.id_profesor == el.id_profesor).forEach(rT => {
                        if (!read.includes(`Day${rT.dia}Hour${rT.hora}`)) {
                            read.push(`Day${rT.dia}Hour${rT.hora}`);
                            let redTH = document.getElementById(`xCol${rT.dia}${rT.hora}`);
                            redTH.setAttribute("style", "background: red ;");
                        }
                    });
                }
            });
            if ((e.target.className == "fichasHover")) {
                cambiarAulaTPrincipal.value = 0;
                selectDiaTPrincipal.value = $(this).data("dia");
                selectHoraTPrincipal.value = Math.min(...arrHours);
                cmpHorasTPrincipal.value = Math.min(...arrHours);
                cmpDiasTPrincipal.value = selectDiaTPrincipal.value;
            } else {
                cambiarAulaTPrincipal.value = 0;
                selectDiaTPrincipal.value = 1;
                selectHoraTPrincipal.value = 1;
                cmpHorasTPrincipal.value = 1;
                cmpDiasTPrincipal.value = 1;
            }
        }).catch(err => {
            $("#ModalTPrincipal tr:eq(1)").attr("style", "background: #49FF33");
            cambiarAulaTPrincipal.value = 0;
            selectDiaTPrincipal.value = 1;
            selectHoraTPrincipal.value = 1;
            cmpHorasTPrincipal.value = 1;
            cmpDiasTPrincipal.value = 1;
        });
        e.stopPropagation();
    });

    $(document).on("click", function (event) {
        let contenedor = $("#menuTPrincipal");
        if (!contenedor.is(event.target) && contenedor.has(event.target).length == 0) {
            contenedor.removeClass("contextmenu-visible");
            $("#ModalTPrincipal tr:eq(1),#ModalTPrincipal th").removeAttr("style")
        }
    });

    //Tools
    labelBtnModalMsgAceptar.addEventListener('click', (e) => {
        selectByTPrincipal.value = 1
    });

    labelBtnModalTPrincipalClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({
                    status: 0,
                    close: ['ModalMsg', 'ModalTPrincipal']
                })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ['ModalMsg']
                })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    labelBtnTPrincipalBorrarFicha.addEventListener('click', (e) => {
        $("#ModalTPrincipal tr:eq(1),#ModalTPrincipal th").removeAttr("style");
        table = {
            name: 'blm_tablero_principal',
            fields: ['*'],
            conditions: {
                'id_horario': ['=', horario.id], 'ficha': ['=', cmpFichasTPrincipal.value], 'id_leccion': ['=', cmpIdLeccionTPrincipal.value]
            },
            rs: true
        };
        searchItemsDB(table).then((borrar) => {
            let cardsCommon = Object.values(borrar)
            let formData = new FormData();
            let addFichasNoA = [];
            document.querySelectorAll(`div[data-ficha = "${cmpFichasTPrincipal.value}"][data-id_leccion = "${cmpIdLeccionTPrincipal.value}"]`).forEach(dD => {
                let parent = dD.parentNode;
                if (parent.childNodes.length == 1) {
                    for (let dur = 0; dur < parseInt(cmpDuracionTPrincipal.value); dur++) {
                        let td = document.createElement("td")
                        parent.after(td);
                    }
                    deleteItem(parent)
                    return false;
                }
                deleteItem(dD);
            });

            let same = [];
            cardsCommon.forEach(elCC => {
                if (!same.includes(elCC.id)) {
                    same.push(elCC.id);
                    table = {
                        name: 'blm_tablero_principal',
                        id: elCC.id
                    }
                    deleteItemDB(table);
                    addFichasNoA.push({
                        id: elCC.id,
                        id_horario: elCC.id_horario,
                        id_leccion: elCC.id_leccion,
                        ficha: elCC.ficha,
                        duracion: elCC.duracion
                    });
                }
            });
            formData.append('camposObj', JSON.stringify(addFichasNoA));
            saveItemsDB(langText, 5, formData, false, {
                name: 'blm_tablero_principal_na'
            }).then(res => {
                table = {
                    name: 'blm_vista_tablero_na',
                    fields: ['*'],
                    conditions: {
                        'id_horario': ['=', horario.id]
                    },
                    rs: true
                };
                searchItemsDB(table).then((fichasNoA) => {
                    let arrFichasNoA = Object.values(fichasNoA);
                    let tipo = selectByTPrincipal.value;
                    table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                        tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                            tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                                tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                    notAssigned(table, arrFichasNoA);
                });
            });
        });
        let contenedor = $("#menuTPrincipal");
        contenedor.removeClass("contextmenu-visible");
    });

    bloquearFTPrincipal.addEventListener('click', (e) => {
        let BlockProperties = document.getElementById("Block");
        let CardsBlock = document.querySelectorAll(`div[data-ficha = "${cmpFichasTPrincipal.value}"][data-id_leccion = "${cmpIdLeccionTPrincipal.value}"]`);
        //console.log(CardsBlock);
        if (bloquearFTPrincipal.checked == true) {
            //BlockProperties.setAttribute("style", "display:none");
            BlockProperties.setAttribute("style", "ponter-events: none; opacity: 0.4");
            CardsBlock.forEach(cB => {
                cB.dataset.activo = 0;
                table = {
                    name: 'blm_tablero_principal',
                    id: cB.dataset.id
                }
                let formDataBlock = new FormData();
                formDataBlock.append('id', cB.dataset.id);
                formDataBlock.append('activo', 0);
                saveItemsDB(langText, 3, formDataBlock, false, {
                    name: 'blm_tablero_principal',
                    campos: {
                        'id': 'id',
                        'activo': 'activo'

                    }
                });
            });
        } else {
            BlockProperties.removeAttribute("style");
            CardsBlock.forEach(cB => {
                cB.dataset.activo = 1;
                table = {
                    name: 'blm_tablero_principal',
                    id: cB.dataset.id
                }
                let formDataBlock = new FormData();
                formDataBlock.append('id', cB.dataset.id);
                formDataBlock.append('activo', 1);
                saveItemsDB(langText, 3, formDataBlock, false, {
                    name: 'blm_tablero_principal',
                    campos: {
                        'id': 'id',
                        'activo': 'activo'

                    }
                });
            });
        }
        let contenedor = $("#menuTPrincipal");
        contenedor.removeClass("contextmenu-visible");
    });

    labelBtnTPrincipalMoverFicha.addEventListener('click', (e) => {
        $("#ModalTPrincipal tr:eq(1),#ModalTPrincipal th").removeAttr("style");
        msGObj.loader.style.display = 'block';
        if (bloquearFTPrincipal.checked == false) {
            if (e.target.dataset.type_card == "fichasHover" && selectHoraTPrincipal.value == cmpHorasTPrincipal.value && selectDiaTPrincipal.value == cmpDiasTPrincipal.value) {
                // console.log("no cambio")
                closeWorkLand(msGObj);
            } else {
                if (parseInt(cmpDuracionTPrincipal.value) + parseInt(selectHoraTPrincipal.value) - 1 <= horario.lecciones) { //No exceder las horas mámimas
                    if (selectHoraTPrincipal.value == cmpHorasTPrincipal.value && selectDiaTPrincipal.value == cmpDiasTPrincipal.value && e.target.dataset.type_card == "fichasHover") { //En caso de que el dia y la hora sean los mismos no hace el proceso

                    } else {
                        let changeDay = selectDiaTPrincipal.value;
                        let changeHour = parseInt(selectHoraTPrincipal.value);
                        let duracion = parseInt(cmpDuracionTPrincipal.value);
                        let hMax = changeHour + (duracion - 1);
                        let cmpIdLec = cmpIdLeccionTPrincipal.value;
                        let cmpFicha = cmpFichasTPrincipal.value;


                        table = {
                            name: 'blm_vista_tablero',
                            fields: ['*'],
                            conditions: {
                                'id_horario': ['=', horario.id]
                            },
                            rs: true
                        };
                        searchItemsDB(table).then((fichas) => {
                            return fichas;
                        }).catch((fichas) => {
                            return [];
                        }).then((fichas) => {
                            let arrFichas = Object.values(fichas);
                            let cardsCommon = [];
                            let inconvenientes = [];
                            if (fichas.length == 0) {
                                table = {
                                    name: 'blm_vista_tablero_na',
                                    fields: ['*'],
                                    conditions: {
                                        'id_horario': ['=', horario.id],
                                        'ficha': ['=', cmpFicha],
                                        'id_leccion': ['=', cmpIdLec]
                                    },
                                    rs: true
                                };
                                searchItemsDB(table).then(cardsCommons => {
                                    cardsCommon = Object.values(cardsCommons);
                                    // console.log(cardsCommon, inconvenientes);
                                    confirmMoveCard(e, cardsCommon, inconvenientes);
                                });
                            } else {
                                for (const arF of arrFichas) {
                                    if (e.target.dataset.type_card == "fichaNoAHover") { } else {
                                        if (arF.ficha == cmpFicha && arF.id_leccion == cmpIdLec) {
                                            cardsCommon.push(arF);
                                        }
                                    }
                                    if (arF.dia == changeDay && (parseInt(arF.hora) >= changeHour && parseInt(arF.hora) <= hMax) && (arF.ficha != cmpFicha || arF.id_leccion != cmpIdLec)) {
                                        inconvenientes.push(arF);
                                    }
                                }

                                if (inconvenientes.length == 0) {
                                    // console.log("No hay problema");
                                    if (e.target.dataset.type_card == "fichasHover") {
                                        confirmMoveCard(e, cardsCommon, inconvenientes);
                                    } else {
                                        table = {
                                            name: 'blm_vista_tablero_na',
                                            fields: ['*'],
                                            conditions: {
                                                'id_horario': ['=', horario.id],
                                                'ficha': ['=', cmpFicha],
                                                'id_leccion': ['=', cmpIdLec]
                                            },
                                            rs: true
                                        };
                                        searchItemsDB(table).then(cardsCommons => {
                                            cardsCommon = Object.values(cardsCommons);
                                            confirmMoveCard(e, cardsCommon, inconvenientes);
                                        });
                                    }

                                } else if (inconvenientes.length > 0) {
                                    if (e.target.dataset.type_card == "fichaNoAHover") {
                                        table = {
                                            name: 'blm_vista_tablero_na',
                                            fields: ['*'],
                                            conditions: {
                                                'id_horario': ['=', horario.id],
                                                'ficha': ['=', cmpFicha],
                                                'id_leccion': ['=', cmpIdLec]
                                            },
                                            rs: true
                                        };
                                        searchItemsDB(table).then(cardsCommons => {
                                            cardsCommon = Object.values(cardsCommons);
                                            let inconvenientes2 = [];
                                            for (const cC of cardsCommon) {
                                                for (const incts of inconvenientes) {
                                                    if (cC.id_grupo == incts.id_grupo) {
                                                        inconvenientes2.push(incts);
                                                        // console.log("grupo");
                                                    }
                                                    if (cC.id_profesor == incts.id_profesor) {
                                                        inconvenientes2.push(incts);
                                                        // console.log("misma profesor");
                                                    }
                                                    if (cC.id_aula == incts.id_aula && cC.id_aula) {
                                                        //inconvenientes2.push(incts);
                                                        //console.log("misma aula");
                                                    }
                                                }
                                            }
                                            // console.log(cardsCommon, inconvenientes2);
                                            confirmMoveCard(e, cardsCommon, inconvenientes2);
                                        })
                                    } else {
                                        let inconvenientes2 = [];
                                        for (const cC of cardsCommon) {
                                            for (const incts of inconvenientes) {
                                                if (cC.id_grupo == incts.id_grupo) {
                                                    inconvenientes2.push(incts);
                                                    // console.log("grupo");
                                                }
                                                if (cC.id_profesor == incts.id_profesor) {
                                                    inconvenientes2.push(incts);
                                                    // console.log("misma profesor");
                                                }
                                                if (cC.id_aula == incts.id_aula && cC.id_aula) {
                                                    inconvenientes2.push(incts);
                                                    // console.log("misma aula");
                                                }
                                            }
                                        }
                                        // console.log(cardsCommon, inconvenientes2);
                                        confirmMoveCard(e, cardsCommon, inconvenientes2);
                                    }
                                }
                            }
                        })
                    }
                    let contenedor = $("#menuTPrincipal");
                    contenedor.removeClass("contextmenu-visible");
                } else {
                    let cardsCommon = document.querySelectorAll(`div[data-ficha = "${cmpFichasTPrincipal.value}"][data-id_leccion = "${cmpIdLeccionTPrincipal.value}"]`);
                    closeWorkLand(msGObj);
                    // console.log(cardsCommon);
                    // console.log("El número de lecciones excede la cantidad de horas por día.");
                }
            }
        }
    });

    labelBtnModalTPrincipalBH.addEventListener('click', (e) => {
        $('#ModalEliminarHorario').modal({
            backdrop: 'static',
            keyboard: false
        });
        FAcomodadasEliminarHorario.checked = false;
        FNAcomodadasEliminarHorario.checked = false;
    });

    FAcomodadasEliminarHorario.addEventListener('change', (e) => {
        FAcomodadasEliminarHorario.checked = true;
        FNAcomodadasEliminarHorario.checked = false;
    });

    FNAcomodadasEliminarHorario.addEventListener('change', (e) => {
        FNAcomodadasEliminarHorario.checked = true;
        FAcomodadasEliminarHorario.checked = false;
    })

    labelBtnModalEliminarHorarioAgregar.addEventListener('click', (e) => {
        msGObj.loader.style.display = 'block';
        let activo = {};
        let formData = new FormData();
        let addFichasNoA = [];
        if (FAcomodadasEliminarHorario.checked == true) {
            activo = { 'id_horario': ['=', horario.id], 'activo': ['=', 1] }
        } else if (FNAcomodadasEliminarHorario.checked == true) {
            activo = { 'id_horario': ['=', horario.id] }
        }

        //No bloqueados 
        table = {
            name: 'blm_tablero_principal',
            fields: ['*'],
            conditions: activo,
            rs: true
        }
        searchItemsDB(table).then((cardsCommon) => {
            for (const Na in cardsCommon) {
                if (cardsCommon.hasOwnProperty(Na)) {
                    addFichasNoA.push({
                        id: cardsCommon[Na].id,
                        id_horario: cardsCommon[Na].id_horario,
                        id_leccion: cardsCommon[Na].id_leccion,
                        ficha: cardsCommon[Na].ficha,
                        duracion: cardsCommon[Na].duracion
                    });
                    tDelete = {
                        name: 'blm_tablero_principal',
                        id: cardsCommon[Na].id
                    }
                    deleteItemDB(tDelete);
                }
            }
        }).then(() => {
            setTimeout(() => {
                formData.append('camposObj', JSON.stringify(addFichasNoA));
                saveItemsDB(langText, 5, formData, false, {
                    name: 'blm_tablero_principal_na'
                }).then(res => {
                    table = {
                        name: 'blm_vista_tablero',
                        fields: ['*'],
                        conditions: {
                            'id_horario': ['=', horario.id]
                        },
                        rs: true
                    };
                    searchItemsDB(table).then((fichas) => {
                        return fichas;
                    }).catch((fichas) => {
                        return [];
                    }).then((fichas) => {
                        table = {
                            name: 'blm_vista_tablero_na',
                            fields: ['*'],
                            conditions: {
                                'id_horario': ['=', horario.id]
                            },
                            rs: true
                        };
                        searchItemsDB(table).then((fichasNoA) => {
                            return fichasNoA;
                        }).catch((fichasNoA) => {
                            return [];
                        }).then((fichasNoA) => {
                            let arrFichasNoA = Object.values(fichasNoA);
                            let arrFichas = Object.values(fichas);
                            let tipo = selectByTPrincipal.value;
                            table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                                tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                                    tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                                        tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                            let arr = []
                            arr.push(scheduleMaker(table, arrFichas));
                            arr.push(notAssigned(table, arrFichasNoA));
                            Promise.allSettled(arr).then((promes) => {
                                closeWorkLand(msGObj);
                            });
                        });
                    });
                });
            }, 5000);
        }).catch(() => {
            // console.log("No hay datos para eliminar");
            closeWorkLand(msGObj);
        });

        let contenedor = $("#menuTPrincipal");
        contenedor.removeClass("contextmenu-visible");


    });

    labelBtnModalEliminarHorarioClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({
                    status: 0,
                    close: ['ModalMsg', 'ModalEliminarHorario']
                })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ['ModalMsg']
                })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $(document).on("change", "#cambiarAulaTPrincipal", function (e) {
        msGObj.loader.style.display = 'block';
        closeWorkLand(msGObj);
        if (e.target.value != 0) {
            let changeAula = e.target.options[e.target.selectedIndex].dataset.id_aula;
            let nameAula = e.target.options[e.target.selectedIndex].textContent;
            let horaNoDis = parseInt(cmpHorasTPrincipal.value) + parseInt(cmpDuracionTPrincipal.value) - 1;
            table = {
                name: 'blm_tablero_principal',
                fields: ['*'],
                conditions: {
                    'id_horario': ['=', horario.id],
                    'ficha': ['<>', parseInt(cmpFichasTPrincipal.value)],
                    'id_aula': ['=', changeAula],
                    'id_dia': ['=', parseInt(cmpDiasTPrincipal.value)],
                    'id_hora': ['>=', `${parseInt(cmpHorasTPrincipal.value)}' AND id_hora <= '${horaNoDis}`]
                },
                rs: true
            };
            searchItemsDB(table).then((fichas) => {
                return fichas;
            }).catch(() => {
                return []
            }).then(fichas => {
                let inconveniente = Object.values(fichas);
                tableA = {
                    name: 'blm_tablero_principal',
                    fields: ['*'],
                    conditions: {
                        'id_horario': ['=', horario.id], 'ficha': ['=', cmpFichasTPrincipal.value], 'id_leccion': ['=', cmpIdLeccionTPrincipal.value]
                    },
                    rs: true
                };
                searchItemsDB(tableA).then((change) => {
                    let cardsCommonAulas = Object.values(change);
                    //let cardsCommonAulas = document.querySelectorAll(`div[data-ficha = "${cmpFichasTPrincipal.value}"][data-id_leccion = "${cmpIdLeccionTPrincipal.value}"]`);
                    // console.log(inconveniente);
                    if (inconveniente.length == 0) {
                        let prom = []
                        for (const elCC of cardsCommonAulas) {
                            //deleteItem(elCC);
                            table = {
                                name: 'blm_tablero_principal',
                                id: elCC.id
                            }
                            let formDataDayAula = new FormData();
                            formDataDayAula.append('id', elCC.id);
                            formDataDayAula.append('id_aula', changeAula);
                            prom.push(saveItemsDB(langText, 3, formDataDayAula, false, {
                                name: 'blm_tablero_principal',
                                campos: {
                                    'id': 'id',
                                    'id_aula': 'id_aula'
                                }
                            }));
                            document.querySelectorAll(`div[data-ficha = "${elCC.ficha}"][data-id_leccion = "${elCC.id_leccion}"]`).forEach(ch => {
                                ch.dataset.abreviatura_aula = nameAula;
                                ch.dataset.nombre_aula = nameAula;
                                ch.dataset.id_aula = changeAula
                            });
                        }
                        Promise.allSettled(prom).then(() => {
                            let tipo = selectByTPrincipal.value;
                            if (tipo == 3) {
                                //setTimeout(() => {
                                table = {
                                    name: 'blm_vista_tablero',
                                    fields: ['*'],
                                    conditions: {
                                        'id_horario': ['=', horario.id]
                                    },
                                    rs: true
                                };
                                searchItemsDB(table).then((fichas) => {
                                    return fichas;
                                }).then(fichas => {
                                    let arrFichas = Object.values(fichas);
                                    table = { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' };
                                    scheduleMaker(table, arrFichas);
                                });
                                //}, 2200);
                            }
                        })
                    } else {
                        // console.log("Hay inconvenientes para signar el aula")
                    }
                })
            });
            //let cardsCommonAulas = document.querySelectorAll(`div[data-ficha = "${cmpFichasTPrincipal.value}"][data-id_leccion = "${cmpIdLeccionTPrincipal.value}"]`);          
        }
        let contenedor = $("#menuTPrincipal");
        contenedor.removeClass("contextmenu-visible");
    });

    //Generar PDF :D
    btnPDF.addEventListener('click', (e) => {
        btnPDF.classList.add('oculto');
        const $elementoParaConvertir = imprHorario;
        html2pdf()
            .set({
                margin: 1,
                filename: 'horario.pdf',
                image: {
                    type: 'jpeg',
                    quality: 0.98
                },
                html2canvas: {
                    scale: 3,
                    letterRendering: true
                },
                jsPDF: {
                    unit: "in",
                    format: "a3",
                    orientation: 'landscape'
                }

            })
            .from($elementoParaConvertir)
            .save()
            .catch(error => console.log(error)).
            then(() => {
                btnPDF.classList.remove('oculto');
            });
    });

});

function scheduleMaker(table, arrFichas) {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let horario = titleModulePlaneacion.dataset;
    $('#ModalTPrincipal').modal({
        backdrop: 'static',
        keyboard: false
    });

    searchItemsDB(table).then(allTables => {
        let brdCGr = []
        tables = {
            name: 'blm_vista_horas_recesos',
            fields: ['*'],
            conditions: {
                'id_horario': ['=', horario.id]
            },
        };
        searchItemsDB(tables).then((hrs) => {
            return orderDBresponse(hrs, 'inicio');
        }).catch(() => {
            return [];
        }).then((sort) => {
            let sortData = Object.values(sort);
            let reverseSortData = [...sortData].reverse();
            let n = 1;
            let resolveNewSort = new Array();
            let newSort = new Array();
            for (const sr of sortData) {
                if (sr.tipo == 0) {
                    sr.enum = n;
                    n++
                } else {
                    sr.enum = 0;
                }
            }
            for (const [index, srt] of sortData.entries()) {
                if (srt.tipo == 1) {
                    // console.log(srt.tipo);
                    tables = {
                        name: 'blm_vista_timbres_grupos',
                        fields: ['id_grupo'],
                        conditions: {
                            'id_timbre': ['=', srt.id_timbre]
                        }
                    };
                    let despues = sortData.findIndex(sortData => sortData.id == srt.id);
                    let antes = reverseSortData.findIndex(reverseSortData => reverseSortData.id == srt.id);
                    let hora = sortData.findIndex((sortData, i) => sortData.tipo == 0 && i > despues);
                    let horaR = reverseSortData.findIndex((reverseSortData, i) => reverseSortData.tipo == 0 && i > antes);
                    if (hora != -1 && horaR != -1) {
                        sortData[index].hora_d = sortData[hora].enum;
                        sortData[index].hora_a = reverseSortData[horaR].enum;
                    } else {
                        if (hora != -1) {
                            sortData[index].hora_d = sortData[hora].enum;
                            sortData[index].hora_a = sortData[hora].enum;
                        } else {
                            sortData[index].hora_d = reverseSortData[horaR].enum;
                            sortData[index].hora_a = reverseSortData[horaR].enum;
                        }
                    }
                    const resProm = searchItemsDB(tables).then((grupo) => {
                        let grupos = Object.values(grupo);
                        return grupos;
                    }).catch(() => {
                        return [];
                    }).then((grupos) => {
                        let arr = []
                        for (const grp in grupos) {
                            if (grupos.hasOwnProperty(grp)) {
                                arr.push(grupos[grp].id_grupo)
                            }
                        }
                        return arr;
                    });
                    let p = Promise.resolve(resProm)
                    resolveNewSort.push(
                        p.then(grp => {
                            sortData[index].grupos = grp
                            newSort.push(sortData[index]);
                        })
                    );
                }
            }
            return Promise.allSettled(resolveNewSort).then(() => {
                return newSort;
            });
        }).then((newSort) => {
            // console.log(newSort)
            let tableBody = document.getElementById("bodyTodoTPrincipal");
            tableBody.innerHTML = "";
            //let yRow = 0;
            let concatenar = [];
            let yaLeido = [];
            for (const aT in allTables) {
                if (allTables.hasOwnProperty(aT)) {
                    //yRow++;
                    let trBody = document.createElement("tr"); //por cada grupo crea un tr
                    let tdBodyGroup = document.createElement("td"); //crea el primer td (identifica el grupo, clase, ...)
                    let txtBodyGroup = document.createTextNode(allTables[aT].abreviatura); //Crea el texto al primer td
                    tdBodyGroup.setAttribute("id", `yRow${allTables[aT].id}`);
                    tdBodyGroup.setAttribute("style", `overflow: hidden;`);
                    tdBodyGroup.appendChild(txtBodyGroup); //Asigna el texto al td ejem: G1
                    trBody.appendChild(tdBodyGroup); //Asigna el elemento td dentro del primer tr 
                    trBody.classList.add("font-weight-bold", "small", "text-center"); //Agrega clases
                    tableBody.appendChild(trBody); //Agrega el tr con los td al body de la tabla
                    for (let tDias = 1; tDias <= horario.dias; tDias++) { //1 dia
                        for (let tHoras = 1; tHoras <= horario.lecciones; tHoras++) { //hora 1{
                            let blanksTd = document.createElement("td");
                            let id = parseInt(allTables[aT].id)
                            let buscarDiaHora = (arrFichas).findIndex(arrFichas => arrFichas.dia == tDias && arrFichas.hora == tHoras && arrFichas[table.typeId] == id);
                            if (buscarDiaHora != -1) {
                                //let aux = buscarDiaHora[0];
                                let cardsCommon = document.querySelector(`div[data-ficha = "${arrFichas[buscarDiaHora].ficha}"][data-id_leccion = "${arrFichas[buscarDiaHora].id_leccion}"][data-${table.typeId}= "${arrFichas[buscarDiaHora][table.typeId]}"][class="fichasHover"]`);
                                if (cardsCommon != null) {
                                    //buscarDiaHora.forEach(bDH=>{
                                    let tipo = [arrFichas[buscarDiaHora][table.typeId], table.abrCard];
                                    if (tipo[0] == parseInt(allTables[aT].id)) {
                                        let buttonF = document.createElement("div");
                                        buttonF.className += `fichasHover`;
                                        let noVeces = parseInt(arrFichas[buscarDiaHora].duracion);
                                        //let porcentaje = (100 * noVeces) + (2 * noVeces);
                                        //buttonF.setAttribute("style", `width:${porcentaje}%;`);
                                        setDatato(arrFichas[buscarDiaHora], buttonF);
                                        cardsCommon.parentNode.appendChild(buttonF);
                                        //trBody.appendChild(blanksTd);
                                    }
                                    //})
                                } else {

                                    let tipo = [arrFichas[buscarDiaHora][table.typeId], table.abrCard];
                                    if (tipo[0] == parseInt(allTables[aT].id)) {
                                        let buttonF = document.createElement("div");
                                        let conc = "";
                                        let txtSome = "";
                                        let horasText = "";
                                        let fic = arrFichas[buscarDiaHora].ficha;
                                        let lec = arrFichas[buscarDiaHora].id_leccion;
                                        let hash = {};
                                        let array = arrFichas.filter(arrFichas => hash[arrFichas.id_leccion + "" + arrFichas.id_profesor] ? false : hash[arrFichas.id_leccion + "" + arrFichas.id_profesor] = true);
                                        let buscarCoincidencias = (array).filter(array => array.id_leccion == parseInt(lec));
                                        let noVeces = parseInt(arrFichas[buscarDiaHora].duracion);
                                        //let porcentaje = (100 * noVeces) + (2 * noVeces);
                                        if (buscarCoincidencias.length > 1) {
                                            let gradient = "";
                                            buscarCoincidencias.forEach(bC => {
                                                gradient += "," + bC.color;
                                            });
                                            //buttonF.setAttribute("style", `background: linear-gradient(to right ${gradient});width:${porcentaje}%; z-index:1000;`);
                                            buttonF.setAttribute("style", `background: linear-gradient(to right ${gradient}); z-index:1000;`);
                                        } else {
                                            //buttonF.setAttribute("style", `background: ${arrFichas[buscarDiaHora].color};width:${porcentaje}%;  z-index:1000;`);
                                            buttonF.setAttribute("style", `background: ${arrFichas[buscarDiaHora].color}; z-index:1000;`);
                                        }
                                        let hash2 = {};
                                        let array2 = arrFichas.filter(arrFichas => hash2[arrFichas.id_leccion + "" + arrFichas[tipo[1]]] ? false : hash2[arrFichas.id_leccion + "" + arrFichas[tipo[1]]] = true);
                                        let buscarCoincidencias2 = (array2).filter(array2 => array2.id_leccion == parseInt(lec));
                                        if (buscarCoincidencias2.length > 1) {
                                            buscarCoincidencias2.forEach(bC2 => {
                                                txtSome += bC2[tipo[1]] + ",";
                                            });
                                            txtSome = txtSome.slice(0, -1)
                                        } else {
                                            txtSome = arrFichas[buscarDiaHora][tipo[1]];
                                        }
                                        buttonF.className += `fichasHover`;
                                        let txtButton = document.createTextNode(txtSome);
                                        buttonF.appendChild(txtButton);
                                        setDatato(arrFichas[buscarDiaHora], buttonF);
                                        blanksTd.appendChild(buttonF);
                                        blanksTd.colSpan = arrFichas[buscarDiaHora].duracion;
                                        trBody.appendChild(blanksTd);
                                        /////Recesos/////
                                        if (!yaLeido.includes(`lec${lec}:fic${fic}`)) {
                                            yaLeido.push(`lec${lec}:fic${fic}`);
                                            for (const [ind, ns] of newSort.entries()) {
                                                let grp = ns.grupos;
                                                let prome = (ns.hora_a + ns.hora_d) / 2;
                                                if (parseInt(arrFichas[buscarDiaHora].hora) < prome && (parseInt(arrFichas[buscarDiaHora].duracion) + parseInt(arrFichas[buscarDiaHora].hora) - 1) > prome) {
                                                    if (ns.id_timbre == null) {
                                                        let find = arrFichas.findIndex(arrFichas => arrFichas.id_leccion == lec && arrFichas.ficha == fic && parseInt(arrFichas.hora) < prome && (parseInt(arrFichas.duracion) + parseInt(arrFichas.hora) - 1) > prome && parseInt(arrFichas.hora) == tHoras && parseInt(arrFichas.dia) == tDias)
                                                        if (find != -1) {
                                                            horasText += ` ${ns.hora_a}-${ns.hora_d},`;
                                                            conc = arrFichas[find];
                                                        }
                                                    } else {
                                                        let fichaYaLeida = [];
                                                        for (const grr of grp) {
                                                            let find = arrFichas.findIndex(arrFichas => arrFichas.id_leccion == lec && arrFichas.ficha == fic && arrFichas.id_grupo == grr && parseInt(arrFichas.hora) < prome && (parseInt(arrFichas.duracion) + parseInt(arrFichas.hora) - 1) > prome && parseInt(arrFichas.hora) == tHoras && parseInt(arrFichas.dia) == tDias)
                                                            if (!fichaYaLeida.includes(`lec${lec}:fic${fic}`)) {
                                                                if (find != -1) {
                                                                    fichaYaLeida.push(`lec${lec}:fic${fic}`);
                                                                    horasText += ` ${ns.hora_a}-${ns.hora_d},`;
                                                                    conc = arrFichas[find];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (ind == newSort.length - 1) {
                                                    conc.texto_recesos = horasText;
                                                    concatenar = concatenar.concat(conc);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                trBody.appendChild(blanksTd);
                            }
                        }
                    }
                }
            }
            let read = [];
            for (const redLine of concatenar) {
                if (!read.includes(redLine.id)) {
                    read.push(redLine.id)
                    document.querySelectorAll(`div[data-ficha="${redLine.ficha}"][data-id_leccion="${redLine.id_leccion}"][class="fichasHover"]`).forEach(el => {
                        let redL = el.getAttribute("style");
                        let trimRec = redLine.texto_recesos
                        el.dataset.texto_recesos = trimRec.slice(0, -1);
                        el.setAttribute("style", `${redL} border-right: solid red 5px !important;`);
                    });
                }
            }
        })
    });
}

function notAssigned(table, arrFichasNoA) {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let horario = titleModulePlaneacion.dataset;
    $(".agregarNoAcomodadas")[0].innerHTML = "";
    table.name = 'blm_leccion';
    table.fields = ['*'];
    table.conditions = { 'id_horario': ['=', horario.id] };
    searchItemsDB(table).then(lecciones => {
        for (const l in lecciones) {
            let search = arrFichasNoA.findIndex(arrF => arrF.id_leccion == lecciones[l].id);
            if (search != -1) {
                let classNo = document.getElementsByClassName("agregarNoAcomodadas")[0];
                let divAsigFuera = document.createElement("div");
                divAsigFuera.className += "asignaturaFuera col-3 m-3 offset-1";
                classNo.appendChild(divAsigFuera);
                let auxPila = 0;
                arrFichasNoA.forEach(arrFNA => {
                    if (arrFNA.id_leccion == lecciones[l].id) {
                        let fichaAsigNoA = document.createElement("div");
                        let nameAsigNoA = table.abrCard;
                        let hash = {};
                        let lec = arrFNA.id_leccion;
                        let array = arrFichasNoA.filter(arrFichasNoA => hash[arrFichasNoA.id_leccion + "" + arrFichasNoA.id_profesor] ? false : hash[arrFichasNoA.id_leccion + "" + arrFichasNoA.id_profesor] = true);
                        let buscarCoincidencias = (array).filter(array => array.id_leccion == parseInt(lec));
                        let background = "";
                        let gradient = "";
                        let txtSome = "";
                        if (buscarCoincidencias.length > 1) {
                            buscarCoincidencias.forEach(bC => {
                                gradient += "," + bC.color;
                            });
                            background = `background: linear-gradient(to right ${gradient});`;
                        } else {
                            background = `background: ${arrFNA.color};`;
                        }
                        let hash2 = {};
                        let array2 = arrFichasNoA.filter(arrFichasNoA => hash2[arrFichasNoA.id_leccion + "" + arrFichasNoA[nameAsigNoA]] ? false : hash2[arrFichasNoA.id_leccion + "" + arrFichasNoA[nameAsigNoA]] = true);
                        let buscarCoincidencias2 = (array2).filter(array2 => array2.id_leccion == parseInt(lec));
                        if (buscarCoincidencias2.length > 1) {
                            buscarCoincidencias2.forEach(bC2 => {
                                txtSome += bC2[nameAsigNoA] + ",";
                            });
                            txtSome = document.createTextNode(txtSome.slice(0, -1));
                        } else {
                            txtSome = document.createTextNode(arrFNA[nameAsigNoA]);
                        }
                        fichaAsigNoA.setAttribute("style", `${background} width:100%; position: absolute; left: ${auxPila}px; top: ${auxPila}px;`);
                        setDatato(arrFNA, fichaAsigNoA);
                        fichaAsigNoA.className += "fichaNoAHover";
                        fichaAsigNoA.appendChild(txtSome);
                        divAsigFuera.appendChild(fichaAsigNoA);
                        auxPila += 0.05;
                    }
                });
            }
        }
    });
}

function confirmMoveCard(event, cardsCommon, inconvenientes) {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let horario = titleModulePlaneacion.dataset;
    let changeDay = selectDiaTPrincipal.value;
    let changeHour = selectHoraTPrincipal.value;
    let auxChange = 0;
    let eliminar = [];
    let hash = {};
    cardsCommon = cardsCommon.filter(cardsCommon => hash[cardsCommon.id] ? false : hash[cardsCommon.id] = true);
    let hash2 = {};
    inconvenientes = inconvenientes.filter(inconvenientes => hash2[inconvenientes.id_leccion + "" + inconvenientes.ficha] ? false : hash2[inconvenientes.id_leccion + "" + inconvenientes.ficha] = true);
    // console.log(cardsCommon, inconvenientes)
    if (event.target.dataset.type_card == "fichasHover") {
        for (const [index, elCC] of cardsCommon.entries()) {
            elCC.dia = changeDay;
            elCC.hora = parseInt(changeHour) + auxChange;
            table = {
                name: 'blm_tablero_principal',
                id: elCC.id
            }
            let formDataDayHour = new FormData();
            formDataDayHour.append('id', elCC.id);
            formDataDayHour.append('id_dia', changeDay);
            formDataDayHour.append('id_hora', parseInt(changeHour) + auxChange);
            saveItemsDB(langText, 3, formDataDayHour, false, {
                name: 'blm_tablero_principal',
                campos: {
                    'id': 'id',
                    'id_dia': 'id_dia',
                    'id_hora': 'id_hora'
                }
            }).then(() => {
                if (cardsCommon.length == index + 1) {
                    // console.log(inconvenientes)
                    if (inconvenientes.length == 0) {
                        let formData = new FormData();
                        formData.append('camposObj', JSON.stringify(eliminar));
                        saveItemsDB(langText, 5, formData, false, { name: 'blm_tablero_principal_na' }).then(() => {
                            //Se recarga la tabla
                            table = {
                                name: 'blm_vista_tablero',
                                fields: ['*'],
                                conditions: {
                                    'id_horario': ['=', horario.id]
                                },
                                rs: true
                            };
                            searchItemsDB(table).then((fichas) => {
                                return fichas;
                            }).catch(e => {
                                return [];
                            }).then(fichas => {
                                let arrFichas = Object.values(fichas);
                                let tipo = selectByTPrincipal.value;
                                table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                                    tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                                        tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                                            tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                                scheduleMaker(table, arrFichas);
                            }).then(() => {
                                closeWorkLand(msGObj);
                            });
                        })
                    } else {
                        for (const [i, inc] of inconvenientes.entries()) {
                            table = {
                                name: 'blm_tablero_principal',
                                fields: ['*'],
                                conditions: {
                                    'id_horario': ['=', horario.id],
                                    'ficha': ['=', inc.ficha],
                                    'id_leccion': ['=', inc.id_leccion]
                                },
                                rs: true
                            };
                            searchItemsDB(table).then((fichasInc) => {
                                for (const fInc in fichasInc) {
                                    if (fichasInc.hasOwnProperty(fInc)) {
                                        eliminar.push({
                                            id: fichasInc[fInc].id,
                                            id_horario: fichasInc[fInc].id_horario,
                                            id_leccion: fichasInc[fInc].id_leccion,
                                            ficha: fichasInc[fInc].ficha,
                                            duracion: fichasInc[fInc].duracion
                                        });
                                        table = {
                                            name: 'blm_tablero_principal',
                                            id: fichasInc[fInc].id
                                        }
                                        deleteItemDB(table);
                                    }
                                }
                            }).then(() => {
                                if (inconvenientes.length == i + 1) {
                                    let formData = new FormData();
                                    formData.append('camposObj', JSON.stringify(eliminar));
                                    saveItemsDB(langText, 5, formData, false, { name: 'blm_tablero_principal_na' }).then(() => {
                                        //Se recarga la tabla
                                        table = {
                                            name: 'blm_vista_tablero',
                                            fields: ['*'],
                                            conditions: {
                                                'id_horario': ['=', horario.id]
                                            },
                                            rs: true
                                        };
                                        searchItemsDB(table).then((fichas) => {
                                            return fichas;
                                        }).catch(e => {
                                            return [];
                                        }).then(fichas => {
                                            // console.log("aqui");
                                            table = {
                                                name: 'blm_vista_tablero_na',
                                                fields: ['*'],
                                                conditions: {
                                                    'id_horario': ['=', horario.id]
                                                },
                                                rs: true
                                            };
                                            searchItemsDB(table).then((fichasNoA) => {
                                                return fichasNoA
                                            }).catch(e => {
                                                return [];
                                            }).then(fichasNoA => {
                                                let arrFichas = Object.values(fichas);
                                                let arrFichasNoA = Object.values(fichasNoA);
                                                let tipo = selectByTPrincipal.value;
                                                table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                                                    tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                                                        tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                                                            tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                                                scheduleMaker(table, arrFichas);
                                                notAssigned(table, arrFichasNoA);
                                            }).then(() => {
                                                closeWorkLand(msGObj);
                                            });
                                        });
                                    })
                                    //}, 3000);
                                }
                            });
                        }
                    }
                }
            });
            auxChange++;
        };
    }
    if (event.target.dataset.type_card == "fichaNoAHover") {
        // console.log("Desde no acomodadas")
        let formDataAcomodar = new FormData();
        let FichasNAcomodadasATablero = [];
        let incluye = [];
        //document.querySelectorAll(`div[data-ficha = "${cmpFichasTPrincipal.value}"][data-id_leccion = "${cmpIdLeccionTPrincipal.value}"]`).forEach(e => e.parentNode.removeChild(e));
        for (const elCC of cardsCommon) {
            if (!incluye.includes(elCC.id)) {
                incluye.push(elCC.id);
                table = {
                    name: 'blm_tablero_principal_na',
                    id: elCC.id
                }
                deleteItemDB(table);
                FichasNAcomodadasATablero.push({
                    id: elCC.id,
                    id_horario: elCC.id_horario,
                    id_hora: parseInt(changeHour) + auxChange,
                    id_leccion: elCC.id_leccion,
                    id_dia: changeDay,
                    ficha: elCC.ficha,
                    duracion: elCC.duracion,
                    activo: 1
                });
                auxChange++;
            }
            //deleteItem(elCC);
        };
        if (inconvenientes.length == 0) {
            formDataAcomodar.append('camposObj', JSON.stringify(FichasNAcomodadasATablero));
            saveItemsDB(langText, 5, formDataAcomodar, false, {
                name: 'blm_tablero_principal'
            }).then(() => {
                table = {
                    name: 'blm_vista_tablero',
                    fields: ['*'],
                    conditions: {
                        'id_horario': ['=', horario.id]
                    },
                    rs: true
                };
                searchItemsDB(table).then((fichas) => {
                    return fichas;
                }).catch(e => {
                    return [];
                }).then(fichas => {
                    table = {
                        name: 'blm_vista_tablero_na',
                        fields: ['*'],
                        conditions: {
                            'id_horario': ['=', horario.id]
                        },
                        rs: true
                    };
                    searchItemsDB(table).then((fichasNoA) => {
                        return fichasNoA
                    }).catch(e => {
                        return [];
                    }).then(fichasNoA => {
                        let arrFichas = Object.values(fichas);
                        let arrFichasNoA = Object.values(fichasNoA);
                        let tipo = selectByTPrincipal.value;
                        table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                            tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                                tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                                    tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                        scheduleMaker(table, arrFichas);
                        notAssigned(table, arrFichasNoA);
                    }).then(() => {
                        closeWorkLand(msGObj);
                    });
                });
            })
        } else {
            for (const [i, inc] of inconvenientes.entries()) {
                table = {
                    name: 'blm_tablero_principal',
                    fields: ['*'],
                    conditions: {
                        'id_horario': ['=', horario.id],
                        'ficha': ['=', inc.ficha],
                        'id_leccion': ['=', inc.id_leccion]
                    },
                    rs: true
                };
                searchItemsDB(table).then((fichasInc) => {
                    for (const fInc in fichasInc) {
                        if (fichasInc.hasOwnProperty(fInc)) {
                            eliminar.push({
                                id: fichasInc[fInc].id,
                                id_horario: fichasInc[fInc].id_horario,
                                id_leccion: fichasInc[fInc].id_leccion,
                                ficha: fichasInc[fInc].ficha,
                                duracion: fichasInc[fInc].duracion
                            });
                            table = {
                                name: 'blm_tablero_principal',
                                id: fichasInc[fInc].id
                            }
                            deleteItemDB(table);
                        }
                    }
                }).then(() => {
                    if (inconvenientes.length == i + 1) {
                        formDataAcomodar.append('camposObj', JSON.stringify(FichasNAcomodadasATablero));
                        saveItemsDB(langText, 5, formDataAcomodar, false, { name: 'blm_tablero_principal' })
                        //Se insertan los datos  nuevos
                        let formData = new FormData();
                        formData.append('camposObj', JSON.stringify(eliminar));
                        saveItemsDB(langText, 5, formData, false, { name: 'blm_tablero_principal_na' }).then(() => {
                            table = {
                                name: 'blm_vista_tablero',
                                fields: ['*'],
                                conditions: {
                                    'id_horario': ['=', horario.id]
                                },
                                rs: true
                            };
                            searchItemsDB(table).then((fichas) => {
                                return fichas;
                            }).catch(e => {
                                return [];
                            }).then(fichas => {
                                table = {
                                    name: 'blm_vista_tablero_na',
                                    fields: ['*'],
                                    conditions: {
                                        'id_horario': ['=', horario.id]
                                    },
                                    rs: true
                                };
                                searchItemsDB(table).then((fichasNoA) => {
                                    return fichasNoA
                                }).catch(e => {
                                    return [];
                                }).then(fichasNoA => {
                                    let arrFichas = Object.values(fichas);
                                    let arrFichasNoA = Object.values(fichasNoA);
                                    let tipo = selectByTPrincipal.value;
                                    table = tipo == 1 ? { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' } :
                                        tipo == 2 ? { name: 'blm_profesores', fields: ['*'], selectBy: 2, typeId: 'id_profesor', abrCard: 'abreviatura_grupo' } :
                                            tipo == 3 ? { name: 'blm_aulas', fields: ['*'], selectBy: 3, typeId: 'id_aula', abrCard: 'abreviatura_grupo' } :
                                                tipo == 4 ? { name: 'blm_cursos', fields: ['*'], selectBy: 4, typeId: 'id_curso', abrCard: 'abreviatura_profesor' } : { name: 'blm_grupos', fields: ['*'], selectBy: 1, typeId: 'id_grupo', abrCard: 'abreviatura_curso' };
                                    scheduleMaker(table, arrFichas);
                                    notAssigned(table, arrFichasNoA);
                                }).then(() => {
                                    closeWorkLand(msGObj);
                                });
                            });
                        })
                    }
                })
            }
        }
        let contenedor = $("#menuTPrincipal");
        contenedor.removeClass("contextmenu-visible");
    }
}

function showMenu(e) {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let horario = titleModulePlaneacion.dataset;
    let mouseX = getMousePoint(e).x;
    let mouseY = getMousePoint(e).y;
    let wrapperX = $("#ModalTPrincipal").offset().left;
    let wrapperY = $("#ModalTPrincipal").offset().top;
    let MtoW_X = Math.floor(mouseX - wrapperX);
    let MtoW_Y = Math.floor(mouseY - wrapperY);
    let W_w = $("#ModalTPrincipal").width();
    let W_h = $("#ModalTPrincipal").height();
    let M_w = $('#menuTPrincipal').width();
    let M_h = $('#menuTPrincipal').height();
    let P_top = 0;
    let P_left = 0;
    let W_scroll_top = $("#ModalTPrincipal").scrollTop();
    let W_scroll_left = $("#ModalTPrincipal").scrollLeft();
    P_top = (W_h - MtoW_Y >= M_h && W_h - MtoW_Y < W_h) ? MtoW_Y - 3 : MtoW_Y - M_h + 3;
    P_left = (W_w - MtoW_X >= M_w && W_w - MtoW_X < W_w) ? MtoW_X - 3 : MtoW_X - M_w + 3;
    $('#menuTPrincipal').addClass('contextmenu-visible');
    $('#menuTPrincipal').css({
        left: P_left + W_scroll_left + 'px',
        top: P_top + W_scroll_top + 'px'
    });
}

function getMousePoint(ev) {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let horario = titleModulePlaneacion.dataset;
    let point = {
        x: 0,
        y: 0
    };
    if (typeof window.pageYOffset != 'undefined') {
        point.x = window.pageXOffset;
        point.y = window.pageYOffset;
    } else if (typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
        point.x = document.documentElement.scrollLeft;
        point.y = document.documentElement.scrollTop;
    } else if (typeof document.body != 'undefined') {
        point.x = document.body.scrollLeft;
        point.y = document.body.scrollTop;
    }
    point.x += ev.clientX;
    point.y += ev.clientY;
    return point;
}

function abrirModlVistaTablero() {
    //logica de las horas de la cabecera para los PDFs
    let horasCabecera = new Array();
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let horario = titleModulePlaneacion.dataset;
    table = {
        name: 'blm_grupos',
        fields: ['*']
    }
    searchItemsDB(table).then((grupos) => {
        return grupos;
    }).then((grupos) => {
        for (const g in grupos) {
            if (Object.hasOwnProperty.call(grupos, g)) {
                table = {
                    name: 'blm_vista_horas_recesos',
                    fields: ['*'],
                    conditions: { 'id_horario': ['=', horario.id] }
                }
                searchItemsDB(table).then((hrs) => {
                    return orderDBresponse(hrs, 'inicio');
                }).then((hrs) => {
                    let horasGrupo = new Array();
                    for (const h in hrs) {
                        if (Object.hasOwnProperty.call(hrs, h)) {
                            horasGrupo.push({ id: hrs[h].id, id_timbre: hrs[h].id_timbre, nombre: hrs[h].nombre, hora_inicio: hrs[h].inicio, hora_fin: hrs[h].fin });
                        }
                    }
                    for (let hg = 0; hg < horasGrupo.length; hg++) {
                        if (horasGrupo[hg].id_timbre != null) {
                            table = {
                                name: 'blm_vista_timbres_grupos',
                                fields: ['*'],
                                conditions: { 'id_timbre': ['=', horasGrupo[hg].id_timbre], 'id_grupo': ['=', grupos[g].id] }
                            }
                            searchItemsDB(table).then((timbre_grupo) => {
                                return timbre_grupo;
                            }).then((timbre_grupo) => { }).catch(error => {
                                horasGrupo[hg].hora_inicio = null;
                                horasGrupo[hg].hora_fin = null;
                                horasGrupo[hg].id = null;
                            });
                        }

                    }

                    horasCabecera.push({ id_grupo: grupos[g].id, grupo: grupos[g].nombre, horasCabecera: horasGrupo });
                    if (horario.tiempo_timbre_clases == 1) {
                        table = {
                            name: 'blm_vista_timbres',
                            fields: ['*'],
                            conditions: { 'id_horario': ['=', horario.id] }
                        }
                        searchItemsDB(table).then((hrsM) => { return hrsM; })
                            .then((hrsM) => {
                                for (const hM in hrsM) {
                                    if (Object.hasOwnProperty.call(hrsM, hM)) {
                                        table = {
                                            name: 'blm_vista_timbres_grupos',
                                            fields: ['*'],
                                            conditions: { 'id_timbre': ['=', hrsM[hM].id_timbre], 'id_grupo': ['=', grupos[g].id] }
                                        }
                                        searchItemsDB(table).then((timbre_grupo) => {
                                            return timbre_grupo;
                                        }).then((timbre_grupo) => {
                                            let filtroHora = horasGrupo.findIndex(hora => hora.id == hrsM[hM].id_hora_receso);
                                            if (filtroHora != -1) {
                                                horasGrupo[filtroHora].hora_inicio = hrsM[hM].hora_inicio;
                                                horasGrupo[filtroHora].hora_fin = hrsM[hM].hora_fin;
                                            }
                                        }).catch(error => { });
                                    }
                                }
                            }).catch(error => { });
                    }
                }).catch(error => {
                    console.log(error);
                });
            }

        }
    }).catch(error => {
        console.log(error);
    });
    console.log(horasCabecera);
    //Fin logica de las horas de la cabecera para los PDFs

    if (isset(horario.id)) {
        msGObj.loader.style.display = 'block';
        closeWorkLand(msGObj);
        $(".contenedorBoton", "#ModalTPrincipal").prepend();
        $(".modal-dialog", "#ModalTPrincipal").addClass("modal-dialog-full-width");
        $(".modal-content", "#ModalTPrincipal").addClass("modal-content-full-width");
        $(".modal-header", "#ModalTPrincipal").addClass("modal-header-full-width");
        $("#menuTPrincipal").appendTo(".modal-content-full-width");
        $(".modal-body", "#ModalTPrincipal").addClass("modal-body-full");
        //Se quito la clase de modal-footer para que no causara conflicto con la nueva clase 
        $(".modal-footer", "#ModalTPrincipal").removeClass("modal-footer").addClass("row modal-footer-full-width").addClass("p-0");
        if ($(".infoFichas")) {
            $(".infoFichas", "#ModalTPrincipal").remove();
            $(".noAcomodadas", "#ModalTPrincipal").remove();
        }
        //.modal-footer se cambio por: .modal-footer-full-width
        $(".modal-footer-full-width", "#ModalTPrincipal").prepend(`<div class='col-7 noAcomodadas'>
                                                    <div class="row justify-content-around agregarNoAcomodadas"></div>
                                                </div>`);
        $(".modal-footer-full-width", "#ModalTPrincipal").prepend(`<div class='col-3 infoFichas row'>
                                                            <div class="col-md-4"><div class="recuadroFicha"></div></div>
                                                            <div class="datosFicha col-md-8">
                                                                <p class="datoAsignatura"></p>
                                                                <p class="datoClase"></p>
                                                                <p class="datoProfesor"></p>
                                                                <p class="datoAula"></p>
                                                                <p class="datoDuracion"></p>
                                                                <p class="datoReceso"></p>
                                                            </div></div>`);
        $(".contenedorBoton", "#ModalTPrincipal").removeClass("col-12").addClass("col-2 p-l-0");
        $(".contenedorBoton", "#ModalTPrincipal").prepend();

        let tableHead = document.getElementById("diaHoraTPrincipal");
        tableHead.innerHTML = "";

        select = {
            item: selectDiaTPrincipal,
            prefix: `${selectDiaTPrincipal.id}Op`
        };
        table = {
            name: 'blm_dias_tl',
            fields: ['id', 'dia'],
            exeption: ['dia'],
            conditions: { 'id': ['<=', parseInt(horario.dias)] }
        };
        fillSelectItem(select, table, { firstItem: false });

        select = {
            item: selectHoraTPrincipal,
            prefix: `${selectHoraTPrincipal.id}Op`
        };
        table = {
            name: 'blm_horas_tl',
            fields: ["id", 'hora'],
            exeption: ['hora'],
            conditions: { 'id': ['<=', horario.lecciones] }
        };
        fillSelectItem(select, table, { firstItem: false });

        table = {
            name: 'blm_dias_tl',
            fields: ['id', 'dia']
            /* ,exeption: ['dia'] */
        };
        searchItemsDB(table).then(resD => {
            let resDias = Object.values(resD)
            // codigo que se puede borrar si no funciona ;D
            /*table = {
                name: 'blm_vista_horas_recesos',
                fields: ['*'],
                conditions: {
                    'id_horario': ['=', horario.id]
                },
            };
            searchItemsDB(table).then((hrs) => {
                return orderDBresponse(hrs, 'inicio');
            }).catch(() => {
                return [];
            }).then((sort) => {
                let sortData = Object.values(sort);
                let n = 1;
                for (const srt of sortData) {
                    if (srt.tipo == 0) {
                        srt.enum = n;
                        n++
                    } else {
                        srt.enum = 0;
                    }
                }
                return sortData;
            }).then((sortData) => {*/
            for (let tr = 0; tr < 2; tr++) {
                if (tr == 0) {
                    let diasTRowH = document.createElement("tr");
                    for (let th = 0; th <= horario.dias; th++) {
                        if (th == 0) {
                            let diasTh = document.createElement("th");
                            diasTh.rowSpan = "2";
                            diasTRowH.appendChild(diasTh);
                        } else {
                            let diasTh = document.createElement("th");
                            let nombreDia = resDias.findIndex(resDias => parseInt(resDias.id) == th);
                            let dia = document.createTextNode(resDias[nombreDia].dia);
                            diasTh.colSpan = horario.lecciones;
                            diasTh.appendChild(dia);
                            diasTRowH.appendChild(diasTh);
                        }
                    }
                    tableHead.appendChild(diasTRowH);
                } else {
                    let diasTRowH = document.createElement("tr");
                    for (let tDias = 1; tDias <= horario.dias; tDias++) {
                        for (let tHoras = 1; tHoras <= horario.lecciones; tHoras++) {
                            //let searchEnum = (sortData).findIndex(sortData => sortData.enum == tHoras);
                            let horasTh = document.createElement("th");
                            let hora = document.createTextNode(tHoras);
                            /* if (sortData[searchEnum + 1] != -1 && sortData[searchEnum + 1] != undefined) {
                                if (sortData[searchEnum + 1].enum == 0) {
                                    horasTh.setAttribute("class", "horaReceso");
                                }
                            } */
                            horasTh.setAttribute("id", `xCol${tDias}${tHoras}`);
                            horasTh.appendChild(hora);
                            diasTRowH.appendChild(horasTh);

                        }
                    }
                    tableHead.appendChild(diasTRowH);
                }
            }
            //});
            //Fin
        });

        table = {
            name: 'blm_vista_tablero',
            fields: ['*'],
            conditions: {
                'id_horario': ['=', horario.id]
            },
            rs: true
        };
        searchItemsDB(table).then((fichas) => {
            return fichas;
        }).catch(e => {
            return [];
        }).then(fichas => {
            table = {
                name: 'blm_vista_tablero_na',
                fields: ['*'],
                conditions: {
                    'id_horario': ['=', horario.id]
                },
                rs: true
            };
            searchItemsDB(table).then((fichasNoA) => {
                return fichasNoA;
            }).catch(e => {
                return [];
            }).then(fichasNoA => {
                let arrFichasNoA = Object.values(fichasNoA);
                let arrFichas = Object.values(fichas);
                table = {
                    name: 'blm_grupos',
                    fields: ['*'],
                    selectBy: 1,
                    typeId: 'id_grupo',
                    abrCard: 'abreviatura_curso'
                };
                scheduleMaker(table, arrFichas);
                notAssigned(table, arrFichasNoA);
            });
        });
    } else {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
            },
            'msg': langText.msg_info_select_horario,
            'ico': 'info',
            'closeDataSet': {
                visible: 0
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    }
}