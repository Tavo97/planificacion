window.addEventListener('load', (e) => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    labelBtnModalAvisoAgregar.addEventListener('click', (e) => { });

    function chooseLevels(id, leccionesList, sumLeccionesHoras, leccion) {
        if (leccionesList.hasOwnProperty(id)) {
            leccionesList[id].push({ id: leccion.id_leccion, horas: leccion.lecciones_max, periodo: leccion.periodo, semana: leccion.semana });
        } else {
            leccionesList[id] = [{ id: leccion.id_leccion, horas: leccion.lecciones_max, periodo: leccion.periodo, semana: leccion.semana }];
        }
        if (sumLeccionesHoras.hasOwnProperty(id)) {
            sumLeccionesHoras[id] += parseInt(leccion.lecciones_max);
        } else {
            sumLeccionesHoras[id] = parseInt(leccion.lecciones_max);
        }
        return [leccionesList, sumLeccionesHoras];
    }

    btnPlaneacionPrueba.addEventListener('click', e => {
        msGObj.loader.style.display = 'block';
        msGObj.labelLoader.style.color = 'black';
        msGObj.labelLoader.innerHTML = langText.msg_make_test;
        deleteAllChilds(criticosAsesor);
        let horario = titleModulePlaneacion.dataset;
        let errors = new Array();
        if (isset(horario.id)) {
            table = {
                name: 'blm_vista_prueba_lecciones',
                fields: ['id_leccion', 'lecciones_max', 'id_periodo', 'periodo', 'tipo_periodo', 'id_semana', 'semana', 'tipo_semana', 'id_grupo', 'nombre_grupo'],
                conditions: { 'id_horario': ['=', horario.id] },
            };
            searchItemsDB(table).then((lecciones) => {
                if (Object.keys(lecciones).length > 0) {
                    console.log(lecciones);
                    let flag = true;
                    let arrayListLeccionesId = new Array();
                    // Separar las lecciones por grupos
                    let leccionesGrupos = new Object();
                    let groups = new Object();
                    for (const leccion in lecciones) {
                        if (leccionesGrupos.hasOwnProperty(lecciones[leccion].id_grupo)) {
                            leccionesGrupos[lecciones[leccion].id_grupo][leccion] = lecciones[leccion];
                        } else {
                            leccionesGrupos[lecciones[leccion].id_grupo] = { [leccion]: lecciones[leccion] };
                        }
                    }
                    for (const LG in leccionesGrupos) {
                        let leccionesList = new Object();
                        let sumLeccionesHoras = new Object();
                        let nombre;
                        for (const leccion in leccionesGrupos[LG]) {
                            if (!arrayListLeccionesId.includes(leccionesGrupos[LG][leccion].id_leccion))
                                arrayListLeccionesId.push(leccionesGrupos[LG][leccion].id_leccion);
                            if (horario.crear_horarios == 1) {
                                if (leccionesGrupos[LG][leccion].tipo_periodo == 1 && leccionesGrupos[LG][leccion].tipo_semana == 1) {
                                    let res = chooseLevels('base', leccionesList, sumLeccionesHoras, leccionesGrupos[LG][leccion], LG);
                                    leccionesList = res[0];
                                    sumLeccionesHoras = res[1];
                                } else {
                                    let id = `${leccionesGrupos[LG][leccion].id_periodo}->${leccionesGrupos[LG][leccion].id_semana}`;
                                    let res = chooseLevels(id, leccionesList, sumLeccionesHoras, leccionesGrupos[LG][leccion], LG);
                                    leccionesList = res[0];
                                    sumLeccionesHoras = res[1];
                                }
                            } else {
                                let res = chooseLevels('base', leccionesList, sumLeccionesHoras, leccionesGrupos[LG][leccion], LG);
                                leccionesList = res[0];
                                sumLeccionesHoras = res[1];
                            }
                            nombre = leccionesGrupos[LG][leccion].nombre_grupo
                        }
                        groups[LG] = { leccionesList, sumLeccionesHoras, nombre };
                    }
                    for (const group in groups) {
                        for (const hora in groups[group].sumLeccionesHoras) {
                            if (groups[group].sumLeccionesHoras[hora] > horario.horas_maximas) {
                                flag = false;
                                let name = langText.msg_unique_schedule;
                                if (groups[group].leccionesList[hora][0].periodo != null && groups[group].leccionesList[hora][0].semana != null) {
                                    name = hora == 'base' ? `Base ${groups[group].leccionesList[hora][0].periodo} -> ${groups[group].leccionesList[hora][0].semana}` : `${groups[group].leccionesList[hora][0].periodo} -> ${groups[group].leccionesList[hora][0].semana}`;
                                    errors.push({ msg: `${langText.msg_error_horario_horas_max} (${name} - ${groups[group].nombre})` });
                                } else {
                                    errors.push({ msg: `${langText.msg_error_horario_horas_max} (${name} - ${groups[group].nombre}) (${langText.msg_test_time_over} ${groups[group].sumLeccionesHoras[hora] - horario.horas_maximas})` });
                                }
                            }
                        }
                    }
                    return [groups, arrayListLeccionesId, leccionesGrupos, errors];
                    // if (flag) {
                    //     return [groups, arrayListLeccionesId, leccionesGrupos];
                    // } else {
                    //     fillModalErrorsTest(errors);
                    //     return flag;
                    // }
                }
            }).then(lecciones => {
                if (lecciones != false) {
                    let newConditions = new Object();
                    let errorsAux = lecciones[3];
                    newConditions.id_leccion = ['IN', `${lecciones[1].join()}`];
                    table = {
                        name: 'blm_vista_prueba_lecciones',
                        fields: ['id_leccion', 'id_profesor', 'id_grupo', 'id_curso', 'id_aula', 'lecciones_max as horas', 'nombre as profesor', 'nombre_curso as curso', 'nombre_aula as aula', 'nombre_grupo as grupo', 'id_periodo', 'periodo', 'tipo_periodo', 'id_semana', 'semana', 'tipo_semana'],
                        conditions: newConditions,
                        rs: true,
                        order: { 'id_leccion': 'ASC' }
                    };
                    let groupData = lecciones[0];
                    let leccionesGrupos = lecciones[2];
                    return searchItemsDB(table).then(lecciones => {
                        if (lecciones.length > 0) {
                            let profesores = new Array();
                            let aulas = new Array();
                            let grupos = new Array();
                            let cursos = new Array();
                            lecciones.forEach(leccion => {
                                if (!profesores.includes(leccion.id_profesor)) {
                                    profesores.push(leccion.id_profesor);
                                }
                                if (!aulas.includes(leccion.id_aula)) {
                                    aulas.push(leccion.id_aula);
                                }
                                if (!grupos.includes(leccion.id_grupo)) {
                                    grupos.push(leccion.id_grupo);
                                }
                                if (!cursos.includes(leccion.id_curso)) {
                                    cursos.push(leccion.id_curso);
                                }
                            });
                            console.log(errorsAux);
                            return {
                                'leccionesGrupos': leccionesGrupos,
                                'groupData': groupData,
                                'lecciones': lecciones,
                                'profesores': profesores,
                                'aulas': aulas,
                                'grupos': grupos,
                                'cursos': cursos,
                                'errors': errorsAux
                            };
                        } else {
                            return false;
                        }
                    });
                } else {
                    return false;
                }
            }).then(data => {
                if (data != false) {
                    let newProfesores = new Object();
                    let arrayPromises = new Array();
                    for (const profesor of data.profesores) {
                        let info = {
                            element: 'profesores',
                            horario: horario.id,
                            item: profesor,
                            multiple: horario.crear_horarios,
                            periodo: horario.periodo_bool,
                            semana: horario.semana_bool
                        };
                        arrayPromises.push(searchBuildFreeTime(info).then(response => { newProfesores[profesor] = response; }).catch(e => console.log(e)));
                    }
                    return Promise.allSettled(arrayPromises).then(() => {
                        data.profesores = newProfesores;
                        return data;
                    });
                } else {
                    return false;
                }
            }).then(data => {
                if (data != false) {
                    let newAulas = new Object();
                    let arrayPromises = new Array();
                    for (const aula of data.aulas) {
                        let info = {
                            element: 'aulas',
                            horario: horario.id,
                            item: aula,
                            multiple: horario.crear_horarios,
                            periodo: horario.periodo_bool,
                            semana: horario.semana_bool
                        };
                        arrayPromises.push(searchBuildFreeTime(info).then(response => { newAulas[aula] = response; }).catch(e => console.log(e)));
                    }
                    return Promise.allSettled(arrayPromises).then(() => {
                        data.aulas = newAulas;
                        return data;
                    });
                } else {
                    return false;
                }
            }).then(data => {
                if (data != false) {
                    let newCursos = new Object();
                    let arrayPromises = new Array();
                    for (const curso of data.cursos) {
                        let info = {
                            element: 'cursos',
                            horario: horario.id,
                            item: curso,
                            multiple: horario.crear_horarios,
                            periodo: horario.periodo_bool,
                            semana: horario.semana_bool
                        };
                        arrayPromises.push(searchBuildFreeTime(info).then(response => { newCursos[curso] = response; }).catch(e => console.log(e)));
                    }
                    return Promise.allSettled(arrayPromises).then(() => {
                        data.cursos = newCursos;
                        return data;
                    });
                } else {
                    return false;
                }
            }).then(data => {
                if (data != false) {
                    let newGrupos = new Object();
                    let arrayPromises = new Array();
                    for (const grupo of data.grupos) {
                        let info = {
                            element: 'clases',
                            horario: horario.id,
                            item: grupo,
                            multiple: horario.crear_horarios,
                            periodo: horario.periodo_bool,
                            semana: horario.semana_bool
                        };
                        arrayPromises.push(searchBuildFreeTime(info).then(response => { newGrupos[grupo] = response; }).catch(e => console.log(e)));
                    }
                    return Promise.allSettled(arrayPromises).then(() => {
                        data.grupos = newGrupos;
                        return data;
                    });
                } else {
                    return false;
                }
            }).then(data => {
                console.log(data);
                if (data != false) {
                    let leccionesXFreeTime = new Object();
                    for (const leccion of data.lecciones) {
                        keysList = Object.keys(leccionesXFreeTime);
                        objLevel = {
                            periodo: { id: leccion.id_periodo, nombre: leccion.periodo, tipo: leccion.tipo_periodo },
                            semana: { id: leccion.id_semana, nombre: leccion.semana, tipo: leccion.tipo_semana }
                        };
                        if (keysList.includes(leccion.id_leccion)) {
                            lessonId = leccion.id_leccion;
                            obj = {
                                // aulas: { id: leccion.id_aula, nombre: leccion.aula },
                                cursos: { id: leccion.id_curso, nombre: leccion.curso },
                                grupos: { id: leccion.id_grupo, nombre: leccion.grupo },
                                profesores: { id: leccion.id_profesor, nombre: leccion.profesor }
                            }
                            leccionesXFreeTime = checkExistElemetLessons(leccionesXFreeTime, lessonId, obj, data);
                        } else {
                            obj = {
                                // aulas: { id: leccion.id_aula, nombre: leccion.aula },
                                cursos: { id: leccion.id_curso, nombre: leccion.curso },
                                grupos: { id: leccion.id_grupo, nombre: leccion.grupo },
                                profesores: { id: leccion.id_profesor, nombre: leccion.profesor }
                            }
                            obj = getFreeTimeElemetLessons(data, obj, objLevel);
                            obj.horas = leccion.horas;
                            leccionesXFreeTime[leccion.id_leccion] = Object.assign(obj, objLevel);
                        }
                    }
                    for (const grupo in data.leccionesGrupos) {
                        for (const leccion in data.leccionesGrupos[grupo]) {
                            leccionesXFreeTime[leccion].GID = grupo;
                        }
                    }
                    // hasta aqui todo bien
                    
                    let errors = new Array();
                    console.log(data.errors);
                    if (data.errors.length != 0 ) {
                        errors= errors.concat(data.errors);
                    }

                    for (const profe in data.profesores) {
                        let IgnoreLeccion = [];
                        let leccP = 0;
                        let kind = 'profesor';
                        let nameProfesor = '';
                        if (Object.hasOwnProperty.call(data.profesores, profe)) {
                            console.log(profe)
                            for (const lec of data.lecciones) {
                                if (lec.id_profesor == profe && !IgnoreLeccion.includes(lec.id_leccion)) {
                                    leccP = leccP + parseInt(lec.horas);
                                    nameProfesor = lec.profesor;
                                    IgnoreLeccion.push(lec.id_leccion);
                                }
                            }
                            if (leccP > data.profesores[profe].base.Status.Green) {
                                flagTeacher = false;
                                let textGeneral = langText.msg_test_max_lecciones.replace(':D', langText[kind])
                                    .replace('"${kind}"', kind)
                                    .replace('"${name}"', nameProfesor)
                                    .replace('"${lecciones}"', leccP)
                                    .replace('"${green}"', data.profesores[profe].base.Status.Green)
                                let objErr = { msg: textGeneral, type: langText[kind], id: profe, subError: [{ msg: 'hola', type: langText[kind], id: profe }] };
                                errors.push(objErr);
                            }
                        }
                    }

                    fillModalErrorsTest(errors);
                }
            }).catch(e => {
                errors.push({ msg: `${e} (${langText.lecciones})` });
                fillModalErrorsTest(errors);
            });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    function checkExistElemetLessons(leccionesXFreeTime, lessonId, obj, dataFreeTime) {
        for (const key in obj) {
            let flag = false;
            for (const item of leccionesXFreeTime[lessonId][key]) {
                if (item.id == obj[key].id) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                let getFreeTime = getFreeTimeElemetLessons(dataFreeTime[key], obj[key], objLevel, true);
                obj[key].key = getFreeTime[1];
                obj[key].level = getFreeTime[2];
                obj[key].freeTime = getFreeTime[0];
                leccionesXFreeTime[lessonId][key].push(obj[key]);
            }
        }
        return leccionesXFreeTime;
    }

    function getFreeTimeElemetLessons(dataFreeTime, obj, objLevel, insert = false) {
        let horario = titleModulePlaneacion.dataset;
        let keyFreeTime = 'base';
        let level = 'none';
        if (parseInt(horario.crear_horarios) == 1) {
            if (parseInt(horario.periodo_bool) == 1) {
                level = 'periodo';
                if (parseInt(objLevel.periodo.tipo) != 1)
                    keyFreeTime = objLevel.periodo.id;
            }
            if (parseInt(horario.semana_bool) == 1) {
                level = 'semana';
                if (parseInt(objLevel.semana.tipo) != 1)
                    keyFreeTime = objLevel.semana.id;
            }
            if (parseInt(horario.periodo_bool) == 1 && parseInt(horario.semana_bool) == 1) {
                level = 'periodo_semana';
                if (!(parseInt(objLevel.periodo.tipo) == 1 && parseInt(objLevel.semana.tipo) == 1))
                    keyFreeTime = `${objLevel.periodo.id}->${objLevel.semana.id}`;
            }
        }
        if (insert) {
            return [getFT(dataFreeTime[obj.id], keyFreeTime), keyFreeTime, level];
        } else {
            let newObj = new Object();
            for (const key in obj) {
                let FreeTimeItem = dataFreeTime[key][obj[key].id];
                obj[key].key = keyFreeTime;
                obj[key].level = level;
                obj[key].freeTime = getFT(FreeTimeItem, keyFreeTime);
                newObj[key] = [obj[key]];
            }
            return newObj;
        }
        function getFT(FreeTimeItem, keyFreeTime) {
            if (FreeTimeItem.hasOwnProperty('Error')) {
                return FreeTimeItem['Error'];
            } else {
                if (FreeTimeItem.hasOwnProperty(keyFreeTime)) {
                    return FreeTimeItem[keyFreeTime];
                } else {
                    return FreeTimeItem['base'];
                }
            }
        }
    }

    labelBtnModalAvisoClose.addEventListener('click', (e) => {
        let langText = globalVariable.langText;
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({
                    status: 0,
                    close: ['ModalMsg', 'ModalAviso']
                })
            },
            'msg': langText.msg_only_alert,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ['ModalMsg']
                })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    labelBtnModalAsesorClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({
                    status: 0,
                    close: ['ModalMsg', 'ModalAsesor', 'ModalAviso']
                })
            },
            'msg': langText.msg_only_alert,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ['ModalMsg']
                })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
});

function fillModalErrorsTest(errors) {
    containerAsesor.classList.remove('border-danger', 'border-success');
    console.log('Entro', errors);
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    if (errors.length > 0) {
        containerAsesor.classList.add('border-danger');
        errors.forEach(element => {
            let newChild = {
                'childrenConf': [false],
                'child': {
                    'prefix': 'AcesorErrors',
                    'name': element.type,
                    'father': criticosAsesor,
                    'kind': 'li',
                    'innerHtml': element.msg,
                    'classes': ['list-group-item', 'li-custome', 'm-1', 'msg-test', 'msg-error'],
                    'data': { type: element.type, idChild: `AcesorSubErrors${element.type}${element.id}` }
                }
            };
            newChild = buildNewElements(newChild).child;
            if (e = element.hasOwnProperty('level')) {
                let levelItem = {
                    'childrenConf': [false],
                    'child': {
                        'prefix': 'AcesorErrorsLevel',
                        'name': element.type,
                        'father': newChild,
                        'kind': 'label',
                        'innerHtml': element.level,
                        'classes': ['levelCardCustome']
                    }
                };
                buildNewElements(levelItem);
            }
            if (e = element.hasOwnProperty('subErrors')) {
                if (element.subErrors.length > 0) {
                    console.log(element);
                    let contentSubError = {
                        'childrenConf': [false],
                        'child': {
                            'prefix': 'AcesorSubErrors',
                            'name': `${element.type}${element.id}`,
                            'father': criticosAsesor,
                            'kind': 'li',
                            'classes': ['list-group-item', 'li-custome', 'm-1', 'msg-test', 'msg-error', 'fadeOut'],
                            'data': { type: element.type }
                        }
                    };
                    contentSubError = buildNewElements(contentSubError);
                    if (contentSubError.status) {
                        let SubErrorList = {
                            'childrenConf': [false],
                            'child': {
                                'prefix': `AcesorSubErrorsList${element.id}`,
                                'name': element.type,
                                'father': contentSubError.child,
                                'kind': 'ul',
                                'classes': ['list-group', 'p-0'],
                            }
                        };
                        SubErrorList = buildNewElements(SubErrorList);
                        if (SubErrorList.status) {
                            element.subErrors.forEach(subError => {
                                let SubErrorListItem = {
                                    'childrenConf': [false],
                                    'child': {
                                        'prefix': 'AcesorSubErrorsList',
                                        'name': subError.type,
                                        'father': SubErrorList.child,
                                        'kind': 'li',
                                        'innerHtml': subError.msg,
                                        'classes': ['list-group-item', 'li-custome', 'm-1', 'msg-test', 'msg-error'],
                                        'data': { type: element.type }
                                    }
                                };
                                SubErrorListItem = buildNewElements(SubErrorListItem);
                            });
                        }
                    }
                }
            }
        });
    } else {
        containerAsesor.classList.add('border-success');
        let newChild = {
            'childrenConf': [false],
            'child': {
                'prefix': 'AcesorErrors',
                'name': 'Done',
                'father': criticosAsesor,
                'kind': 'li',
                'innerHtml': langText.msg_info_done,
                'classes': ['list-group-item', 'li-custome', 'm-1', 'msg-test', 'msg-done'],
                'data': { type: 'Done', idChild: `AcesorSubErrors${'unknow'}` }
            }
        };
        newChild = buildNewElements(newChild).child;
    }
    $('#ModalAsesor').modal({ backdrop: 'static', keyboard: false });
    let lis = findElements('li', criticosAsesor);
    if (lis.state) {
        lis.items.forEach((element, i) => {
            let classes = ['icon-custome-menu'];
            switch (element.dataset.type) {
                case 'Teacher':
                case 'Profesor':
                    classes.push('fas', 'fa-graduation-cap');
                    break;

                case 'Subject':
                case 'Curso':
                    classes.push('fas', 'fa-book');
                    break;

                case 'Group':
                case 'Grupo':
                    classes.push('fas', 'fa-user-friends');
                    break;

                case 'Classroom':
                case 'Aula':
                    classes.push('fas', 'fa-door-open');
                    break;

                case 'Done':
                    classes.push('fas', 'fa-check-double');
                    break;

                default:
                    classes.push('fas', 'fa-bug');
                    break;
            }
            let newChild = {
                'childrenConf': [false],
                'child': {
                    'prefix': 'Acesor',
                    'name': element.dataset.type + i,
                    'father': element,
                    'kind': 'i',
                    'classes': classes,
                }
            };
            newC = buildNewElements(newChild);
            if (newC.status) {
                newC.child.addEventListener('click', (e) => {
                    let contentSubErrors = document.getElementById(e.target.parentNode.dataset.idchild);
                    if (contentSubErrors !== null) {
                        contentSubErrors.classList.toggle('fadeOut');
                    }
                });
            }
        });
    }
    closeWorkLand(msGObj);
}