window.addEventListener('load', () => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let groups = document.querySelectorAll(`[data-group]`);
    groups.forEach(element => {
        element.addEventListener('change', (e) => {
            checkGroup(e.target);
            if (fichasSemanaRestriccionesClases.checked == true) {
                configuracionRestriccionesClases.disabled = false;
            } else { configuracionRestriccionesClases.disabled = true; }
        });
    });

    btnPlaneacionRelaciones.addEventListener('click', (e) => {
        let dataHorario = titleModulePlaneacion.dataset;
        dataRelaciones.innerHTML = `${dataHorario.id}-${dataHorario.nombre} - ${dataHorario.centro}`;
        deleteAllChilds(tableBodyRelaciones);
        if (isset(dataHorario.id)) {
            let formData = new FormData();
            formData.append('peticion', 1);
            formData.append('select', JSON.stringify(['*']));
            formData.append('tabla', 'blm_relaciones');
            formData.append('conditions', JSON.stringify({
                'id_horario': ['=', dataHorario.id]
            }));
            let data = new Object();
            data.target = '../blocks/planificacion/peticionesSql.php';
            data.method = 'POST';
            data.send = true;
            data.form = [false, formData];
            ajaxData(data).then((res) => {
                msGObj.loader.style.display = 'block';
                deleteAllChilds(tableBodyRelaciones);
                if (Object.keys(res).length > 0) {
                    orderDBresponse(res, 'id', 'number').then((array) => {
                        let m = 0;
                        array.forEach((element, i) => {
                            let datainsert = new Object();
                            table = {
                                name: 'blm_vista_grupo_relacion',
                                fields: ['*'],
                                conditions: { 'id_relacion': ['=', element.id] }
                            };
                            searchItemsDB(table).then((grupos) => {
                                console.log(grupos)
                                let gruposList = new Array();
                                let gruposData = new Array();
                                for (const grupo in grupos) {
                                    gruposList.push(grupos[grupo].grupo);
                                    gruposData.push({
                                        id: grupos[grupo].id,
                                        id_grupo: grupos[grupo].id_grupo,
                                        grupo: grupos[grupo].grupo
                                    });
                                }
                                datainsert.groups = JSON.stringify(gruposData);
                                return gruposList;
                            }).then(grupos => {
                                return grupos.join(', ');
                            }).catch(e => {
                                // datainsert.groups = JSON.stringify([{}]);
                                return langText.all;
                            }).then(grupo => {
                                table = {
                                    name: 'blm_vista_curso_relacion',
                                    fields: ['*'],
                                    conditions: { 'id_relacion': ['=', element.id] }
                                };
                                return searchItemsDB(table).then((cursos) => {
                                    let cursosList = new Array();
                                    let cursosData = new Array();
                                    for (const curso in cursos) {
                                        cursosList.push(cursos[curso].curso);
                                        cursosData.push({
                                            id: cursos[curso].id,
                                            id_curso: cursos[curso].id_curso,
                                            curso: cursos[curso].curso
                                        });
                                    }
                                    datainsert.courses = JSON.stringify(cursosData);
                                    return [cursosList.join(', '), grupo];
                                }).catch(e => {
                                    return [langText.msg_no_data, grupo];
                                });
                            }).then(datosMultiple => {
                                datainsert.id = element.id;
                                datainsert.estado_grupo = element.estado_grupo;
                                datainsert.nota = element.nota;
                                datainsert.condicion = element.condicion;
                                datainsert.orden = element.orden;
                                datainsert.posicion = element.posicion;
                                datosMultiple[1] = element.estado_grupo == 1 ? langText.all : datosMultiple[1];
                                items = [++m, datosMultiple[0], datosMultiple[1], element.nota];
                                console.log(items, datainsert)
                                tr = addRowTable(datainsert, items, element.id, 'Relaciones', tableBodyRelaciones);
                                tr.child.addEventListener('click', (e) => {
                                    selectRow(e);
                                });
                            });
                        });
                    });
                } else {
                    msGObj.labelLoader.style.color = 'tomato';
                    msGObj.labelLoader.innerHTML = langText.msg_no_data;
                }
                closeWorkLand(msGObj);
            });
            $("#ModalRelaciones").modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_horario,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalRelacionesAñadir.addEventListener('click', (e) => {
        frmRestriccionesClases.reset();
        deleteAllChilds(coursesContainerRestriccionesClases);
        deleteAllChilds(groupsContainerRestriccionesClases);
        delete asignaturaRestriccionesClases.dataset.itemsListAux;
        delete asignaturaRestriccionesClases.dataset.itemsList;
        delete claseRestriccionesClases.dataset.itemsList;
        delete claseRestriccionesClases.dataset.itemsListAux;
        configuracionRestriccionesClases.disabled = true;
        // cambiarOrdenRestriccionesClases.disabled = true;
        // ImpInicioFinalRestriccionesClases.disabled = true;
        eleccionRestriccionesClases.checked = false;
        todasRestriccionesClases.checked = true;
        if (eleccionRestriccionesClases.checked == true) {
            claseRestriccionesClases.disabled = false;
        } else {
            claseRestriccionesClases.disabled = true;
        }
        let selectItems = [{
            select: {
                item: asignaturaRestriccionesClases,
                prefix: `${asignaturaRestriccionesClases.id}Op`
            },
            table: {
                name: 'blm_cursos',
                fields: ['id', 'nombre'],
                exeption: ['nombre']
            }
        },
        {
            select: {
                item: claseRestriccionesClases,
                prefix: `${claseRestriccionesClases.id}Op`
            },
            table: {
                name: 'blm_grupos',
                fields: ['id', 'nombre'],
                exeption: ['nombre']
            }
        }];
        selectItems.forEach(element => {
            fillSelectItem(element.select, element.table, { firstItem: false }, element.type);
        });
        tablaFrmRestriccionesClases.value = 'blm_relaciones';
        peticionFrmRestriccionesClases.value = 2;
        idItemHorarioRestriccionesClases.value = titleModulePlaneacion.dataset.id;
        camposFrmRestriccionesClases.value = JSON.stringify({
            'estado_grupo': 'clasesRestriccionesClases',
            'nota': 'notaRestriccionesClases',
            'condicion': 'condicionRestriccionesClases',
            'orden': 'ordenRestriccionesClases',
            'posicion': 'ImpInicioFinalRestriccionesClases',
            'id_horario': 'idItemHorarioRestriccionesClases'
        });
        $('#ModalRestriccionesClases').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalRestriccionesClasesAgregar.addEventListener('click', (e) => {
        msGObj.loader.style.display = 'block';
        let data = new Object();
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [true, frmRestriccionesClases];
        console.log('antesde ajax')
        ajaxData(data).then((res) => {
            console.log(res)
            msGObj.labelLoader.style.color = '#28a745';
            if (res[0] != false) {
                let op = parseInt(peticionFrmRestriccionesClases.value);
                switch (op) {
                    case 2:
                        let flag = true;
                        if (asignaturaRestriccionesClases.dataset.hasOwnProperty('itemsList')) {
                            let formData = new FormData();
                            let camposObj = new Array();
                            let itemsList = JSON.parse(asignaturaRestriccionesClases.dataset.itemsList);
                            if (itemsList.length > 0) {
                                itemsList.forEach(element => {
                                    camposObj.push({
                                        id_curso: element,
                                        id_relacion: res[0]
                                    });
                                });
                                formData.append('camposObj', JSON.stringify(camposObj));
                                saveItemsDB(langText, 5, formData, false, { name: 'blm_curso_relacion' }).then(response => console.log(response)).catch(e => {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = e;
                                });
                            } else {
                                msGObj.labelLoader.style.color = 'tomato';
                                msGObj.labelLoader.innerHTML = 'Courses no selected, for add go to edit';
                            }
                        } else {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = 'Courses no selected, for add go to edit';
                        }
                        if (claseRestriccionesClases.dataset.hasOwnProperty('itemsList')) {
                            if (eleccionRestriccionesClases.checked == 1) {
                                let formData = new FormData();
                                let camposObj = new Array();
                                let itemsList = JSON.parse(claseRestriccionesClases.dataset.itemsList);
                                if (itemsList.length > 0) {
                                    itemsList.forEach(element => {
                                        camposObj.push({
                                            id_grupo: element,
                                            id_relacion: res[0]
                                        });
                                    });
                                    formData.append('camposObj', JSON.stringify(camposObj));
                                    saveItemsDB(langText, 5, formData, false, { name: 'blm_grupo_relacion' }).then(response => console.log(response)).catch(e => {
                                        msGObj.labelLoader.style.color = 'tomato';
                                        msGObj.labelLoader.innerHTML = e;
                                    });
                                } else {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = 'Group no selected, for add go to edit';
                                }
                            }
                        }
                        if (fichasSemanaRestriccionesClases.checked == 1) {
                            let frdata = new FormData();
                            frdata.append('tabla', 'blm_distribucion_asignatura');
                            frdata.append('peticion', 2);
                            frdata.append('campos', JSON.stringify({
                                'dias_consecutivos': 'FichasDivididasFrmRestriccionesClases',
                                'lecciones_consecutivos': 'SHMDCFrmRestriccionesClases',
                                'distribucion_dias_lecciones': 'DLeccionesRestriccionesClases',
                                'ficha_desde': 'dfndd1FrmRestriccionesClases',
                                'ficha_hasta': 'dfndh1FrmRestriccionesClases',
                                'leccion_desde': 'nldid2FrmRestriccionesClases',
                                'leccion_hasta': 'nldih2FrmRestriccionesClases',
                                'id_relacion': 'relacionRestriccionesClases'
                            }));
                            frdata.append('relacionRestriccionesClases', res[0]);
                            frdata.append('FichasDivididasFrmRestriccionesClases', FichasDivididasFrmRestriccionesClases.value);
                            frdata.append('SHMDCFrmRestriccionesClases', SHMDCFrmRestriccionesClases.value);
                            frdata.append('DLeccionesRestriccionesClases', DLeccionesRestriccionesClases.value);
                            frdata.append('dfndd1FrmRestriccionesClases', dfndd1FrmRestriccionesClases.value);
                            frdata.append('dfndh1FrmRestriccionesClases', dfndh1FrmRestriccionesClases.value);
                            frdata.append('nldid2FrmRestriccionesClases', nldid2FrmRestriccionesClases.value);
                            frdata.append('nldih2FrmRestriccionesClases', nldih2FrmRestriccionesClases.value);
                            data.form = [false, frdata];
                            ajaxData(data).then((resp) => {
                                msGObj.labelLoader.style.color = '#28a745';
                                if (resp[0] != false) {
                                    msGObj.labelLoader.innerHTML = langText.msg_relacion_fichas_divididas;
                                } else {
                                    flag = false;
                                }
                            });
                        }
                        if (flag) {
                            msGObj.labelLoader.innerHTML = langText.msg_added;
                        } else {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = langText.msg_error;
                        }
                        break;

                    case 3:
                        console.log('editar grupos y horas XD')
                        let flagEdit = true;
                        if (asignaturaRestriccionesClases.dataset.hasOwnProperty('itemsList')) {
                            let formData = new FormData();
                            let camposObj = new Array();
                            let itemsList = JSON.parse(asignaturaRestriccionesClases.dataset.itemsList);
                            console.log(asignaturaRestriccionesClases.dataset)
                            if (asignaturaRestriccionesClases.dataset.itemsList) {
                                itemsList = JSON.parse(asignaturaRestriccionesClases.dataset.itemsList);
                            }
                            let itemsListAux = JSON.parse(asignaturaRestriccionesClases.dataset.itemsListAux);
                            let diff = itemsListAux.diff(itemsList)
                            console.log(diff.length)
                            if (diff.length > 0) {
                                diff.forEach(element => {
                                    camposObj.push({
                                        id_curso: element,
                                        id_relacion: res[2].id
                                    });
                                });
                                console.log(res, camposObj)
                                formData.append('camposObj', JSON.stringify(camposObj));
                                saveItemsDB(langText, 5, formData, false, { name: 'blm_curso_relacion' }).then(response => console.log(response)).catch(e => {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = e;
                                });
                            } else {
                                console.log('error longitud')
                                msGObj.labelLoader.style.color = 'tomato';
                                msGObj.labelLoader.innerHTML = 'Courses no selected, for add go to edit';
                                closeWorkLand(msGObj);
                            }
                        } else {
                            console.log('error itemsList')
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = 'Courses no selected, for add go to edit';
                            closeWorkLand(msGObj);
                        }
                        if (claseRestriccionesClases.dataset.hasOwnProperty('itemsList')) {
                            if (eleccionRestriccionesClases.checked == 1) {
                                let formData = new FormData();
                                let camposObj = new Array();
                                let itemsListAux;
                                let diff
                                let itemsList = JSON.parse(claseRestriccionesClases.dataset.itemsList);
                                if (claseRestriccionesClases.dataset.itemsListAux) {
                                    itemsListAux = JSON.parse(claseRestriccionesClases.dataset.itemsListAux);
                                    diff = itemsListAux.diff(itemsList);
                                } else {
                                    diff = itemsList;
                                }
                                console.log(diff, itemsList)
                                // let itemsListAux = JSON.parse(claseRestriccionesClases.dataset.itemsListAux);
                                if (diff.length > 0) {
                                    diff.forEach(element => {
                                        camposObj.push({
                                            id_grupo: element,
                                            id_relacion: res[2].id
                                        });
                                    });
                                    console.log(res, camposObj)
                                    formData.append('camposObj', JSON.stringify(camposObj));
                                    saveItemsDB(langText, 5, formData, false, { name: 'blm_grupo_relacion' }).then(response => console.log(response)).catch(e => {
                                        msGObj.labelLoader.style.color = 'tomato';
                                        msGObj.labelLoader.innerHTML = e;
                                    });
                                } else {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = 'Groups no selected, for add go to edit';
                                }
                            }
                        } else {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = 'Groups no selected, for add go to edit';
                        }
                        if (flagEdit) {
                            msGObj.labelLoader.innerHTML = langText.msg_added;
                        } else {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = langText.msg_error;
                        }
                        break;
                }
                closeWorkLand(msGObj);
                closeModal(JSON.stringify({ status: 0, close: ['ModalRelaciones', 'ModalRestriccionesClases'] }));
            } else {
                msGObj.labelLoader.style.color = 'tomato';
                msGObj.labelLoader.innerHTML = langText.msg_condicion;
                closeWorkLand(msGObj);
            }
        });
    });

    labelBtnModalRelacionesEditar.addEventListener('click', (e) => {
        deleteAllChilds(coursesContainerRestriccionesClases);
        deleteAllChilds(groupsContainerRestriccionesClases);
        delete asignaturaRestriccionesClases.dataset.itemsListAux;
        delete asignaturaRestriccionesClases.dataset.itemsList;
        delete claseRestriccionesClases.dataset.itemsList;
        delete claseRestriccionesClases.dataset.itemsListAux;
        frmRestriccionesClases.reset();
        let groups = document.querySelectorAll(`[data-group]`);
        groups.forEach(element => {
            element.addEventListener('change', e => checkGroup(e.target));
        });
        labelBtnModalLeccionAgregar.innerHTML = langText.btn_guardar_frm;
        selectItem = getChooseItem('tr', tableBodyRelaciones, 'choosed')[1];
        if (selectItem != 'none') {
            let dataItem = selectItem.dataset;
            let courses = JSON.parse(selectItem.dataset.courses);
            console.log(selectItem.dataset)
            let idAsignaturas = new Array();
            courses.forEach(element => {
                newChildData = {
                    'data': { 'id': element.id_curso, 'nombre': element.curso, 'id_item': element.id },
                    'father': coursesContainerRestriccionesClases,
                    'prefix': 'asignaturasAdded'
                };
                addEvent = {
                    'father': asignaturaRestriccionesClases,
                    'prefix': 'asignaturaRestriccionesClasesOp',
                    'container': coursesContainerRestriccionesClases
                };
                idAsignaturas.push(element.id_curso);
                createCoursesList(false, newChildData, addEvent, 'blm_curso_relacion');
            });
            asignaturaRestriccionesClases.dataset.itemsList = JSON.stringify(idAsignaturas);
            asignaturaRestriccionesClases.dataset.itemsListAux = JSON.stringify(idAsignaturas);
            if (isset(selectItem.dataset.groups)) {
                let groups = JSON.parse(selectItem.dataset.groups);
                let idGroups = new Array();
                groups.forEach(element => {
                    newChildData = {
                        'data': { 'id': element.id_grupo, 'nombre': element.grupo, 'id_item': element.id },
                        'father': groupsContainerRestriccionesClases,
                        'prefix': 'gruposAdded'
                    };
                    addEvent = {
                        'father': claseRestriccionesClases,
                        'prefix': 'claseRestriccionesClasesOp',
                        'container': groupsContainerRestriccionesClases
                    };
                    idGroups.push(element.id_grupo);
                    createCoursesList(false, newChildData, addEvent, 'blm_grupo_relacion');
                });
                claseRestriccionesClases.dataset.itemsList = JSON.stringify(idGroups);
                claseRestriccionesClases.dataset.itemsListAux = JSON.stringify(idGroups);
            }
            let selectItems = [{
                select: {
                    item: asignaturaRestriccionesClases,
                    prefix: `${asignaturaRestriccionesClases.id}Op`,
                    exclude: asignaturaRestriccionesClases.dataset.itemsList
                },
                table: {
                    name: 'blm_cursos',
                    fields: ['id', 'nombre'],
                    exeption: ['nombre']
                }
            },
            {
                select: {
                    item: claseRestriccionesClases,
                    prefix: `${claseRestriccionesClases.id}Op`,
                    exclude: claseRestriccionesClases.dataset.itemsList
                },
                table: {
                    name: 'blm_grupos',
                    fields: ['id', 'nombre'],
                    exeption: ['nombre']
                }
            }];
            selectItems.forEach(element => {
                fillSelectItem(element.select, element.table, { firstItem: false }, element.type);
            });
            let ordenGroup = document.querySelectorAll('[data-group="ordenRestriccionesClases"]');
            let condicionGroup = document.querySelectorAll('[data-group="condicionRestriccionesClases"]');
            let clasesGroup = document.querySelectorAll('[data-group="clasesRestriccionesClases"]');
            ordenGroup.forEach(element => {
                if (element.value == dataItem.orden) {
                    element.checked = 1;
                } else {
                    element.checked = 0;
                }
            });
            condicionGroup.forEach(element => {
                if (element.value == dataItem.condicion) {
                    element.checked = 1;
                } else {
                    element.checked = 0;
                }
            });
            clasesGroup.forEach(element => {
                console.log(element.value + "=" + dataItem.estado_grupo)
                if (element.value == dataItem.estado_grupo) {
                    element.checked = 1;
                    cambiarClaseRestriccionesClases.disabled = false;
                } else {
                    cambiarClaseRestriccionesClases.disabled = true;
                    element.checked = 0;
                }
            });
            notaRestriccionesClases.value = dataItem.nota;
            idItemCursoRestriccionesClases.value = dataItem.id_curso_relacion;
            idItemGrupoRestriccionesClases.value = dataItem.id_grupo_relacion;
            if (eleccionRestriccionesClases.checked == true) {
                claseRestriccionesClases.disabled = false;
            } else {
                claseRestriccionesClases.disabled = true;
            }
            if (fichasSemanaRestriccionesClases.checked == true) {
                configuracionRestriccionesClases.disabled = false;
            } else {
                configuracionRestriccionesClases.disabled = true;
            }
            tablaFrmRestriccionesClases.value = 'blm_relaciones';
            peticionFrmRestriccionesClases.value = 3;
            idFrmRestriccionesClases.value = dataItem.id;
            camposFrmRestriccionesClases.value = JSON.stringify({
                'id': 'idFrmRestriccionesClases',
                'estado_grupo': 'clasesRestriccionesClases',
                'nota': 'notaRestriccionesClases',
                'condicion': 'condicionRestriccionesClases',
                'orden': 'ordenRestriccionesClases'
            });
            $('#ModalRestriccionesClases').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            }
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalRelacionesEliminar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyRelaciones, 'choosed')[1];
        if (selectItem != 'none') {
            let msg = msgDeleteItem(langText.msg_alert_delete, selectItem.dataset.curso, langText.relacion);
            data = {
                'dataSets': {
                    'action': 'eliminar',
                    'data': JSON.stringify(selectItem.dataset),
                    'table': 'blm_relaciones',
                    'item': selectItem.id,
                    'modal': JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': msg,
                'ico': 'info',
                'closeDataSet': {
                    visible: 1,
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_item_table,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalRelacionesCerrar.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 0, close: ['ModalMsg', 'ModalRelaciones'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalRestriccionesClasesCancelar.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalRestriccionesClases'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    asignaturasRestriccionesClases.addEventListener('click', (e) => {
        let element = selectedOption(asignaturaRestriccionesClases);
        let data = element.dataset;
        let idAsignaturas = new Array();
        if (asignaturaRestriccionesClases.dataset.itemsList != undefined) {
            idAsignaturas = JSON.parse(asignaturaRestriccionesClases.dataset.itemsList);
            idAsignaturas.push(data.id);
        } else {
            idAsignaturas.push(data.id);
        }
        asignaturaRestriccionesClases.dataset.itemsList = JSON.stringify(idAsignaturas);
        newChildData = {
            'data': { 'id': data.id, 'nombre': data.nombre },
            'father': coursesContainerRestriccionesClases,
            'prefix': 'asignaturasAdded'
        };
        addEvent = {
            'father': asignaturaRestriccionesClases,
            'prefix': 'asignaturaRestriccionesClasesOp',
            'container': coursesContainerRestriccionesClases
        };
        createCoursesList(element, newChildData, addEvent);
    });

    cambiarClaseRestriccionesClases.addEventListener('click', (e) => {
        console.log('crear grupos')
        let element = selectedOption(claseRestriccionesClases);
        let data = element.dataset;
        let idGrupos = new Array();
        if (claseRestriccionesClases.dataset.itemsList != undefined) {
            console.log('definir itemsList')
            idGrupos = JSON.parse(claseRestriccionesClases.dataset.itemsList);
            idGrupos.push(data.id);
        } else {
            idGrupos.push(data.id);
        }
        claseRestriccionesClases.dataset.itemsList = JSON.stringify(idGrupos);
        newChildData = {
            'data': { 'id': data.id, 'nombre': data.nombre },
            'father': groupsContainerRestriccionesClases,
            'prefix': 'gruposAdded'
        };
        addEvent = {
            'father': claseRestriccionesClases,
            'prefix': 'claseRestriccionesClasesOp',
            'container': groupsContainerRestriccionesClases
        };
        console.log(addEvent)
        createCoursesList(element, newChildData, addEvent);
    });

    eleccionRestriccionesClases.addEventListener('change', (e) => {
        todasRestriccionesClases.removeAttribute('checked');
        cambiarClaseRestriccionesClases.disabled = false;
        eleccionRestriccionesClases.checked = true;
        todasRestriccionesClases.checked = false;
        claseRestriccionesClases.disabled = false;
    });

    todasRestriccionesClases.addEventListener('change', (e) => {
        todasRestriccionesClases.checked = true;
        eleccionRestriccionesClases.checked = false;
        claseRestriccionesClases.disabled = true;
    });

});

function createCoursesList(element, newChildData, addEvent, tableName = false) {
    let newChild = {
        'childrenConf': [false],
        'child': {
            'prefix': newChildData.prefix,
            'name': newChildData.data.id,
            'father': newChildData.father,
            'kind': 'li',
            'innerHtml': `${newChildData.data.nombre}`,
            'data': newChildData.data,
            'classes': ['list-group-item', 'li-custome', 'hand'],
            'attributes': { 'scope': 'col' }
        }
    };
    let newItem = buildNewElements(newChild);
    if (newItem.status) {
        newItem.child.addEventListener('click', e => {
            let data = e.target.dataset;
            let itemsList = JSON.parse(addEvent.father.dataset.itemsList);
            console.log(addEvent.father.dataset.itemsListAux)
            let itemsListAux = JSON.parse(addEvent.father.dataset.itemsListAux);
            let itemsContainer = findElements('li', addEvent.container).items;
            itemsContainer.forEach(itemContainer => {
                if (itemContainer.dataset.id == data.id) {
                    let newChild = {
                        'childrenConf': [false],
                        'child': {
                            'prefix': addEvent.prefix,
                            'name': itemContainer.dataset.id,
                            'father': addEvent.father,
                            'kind': 'option',
                            'innerHtml': `${itemContainer.dataset.nombre}`,
                            'data': { 'id': itemContainer.dataset.id, 'nombre': itemContainer.dataset.nombre },
                            'attributes': { 'value': itemContainer.dataset.id }
                        }
                    };
                    buildNewElements(newChild);
                    if (e = itemContainer.dataset.hasOwnProperty('id_item')) {
                        let table = {
                            name: tableName,
                            id: itemContainer.dataset.id_item
                        };
                        deleteItemDB(table).then(res => console.log(res)).catch(e => console.log(e));
                    }
                    deleteItem(itemContainer);
                }
            });
            removeItemFromArr(itemsList, data.id);
            removeItemFromArr(itemsListAux, data.id);
            addEvent.father.dataset.itemsList = JSON.stringify(itemsList);
            addEvent.father.dataset.itemsListAux = JSON.stringify(itemsListAux);
        });
    }
    if (element != false) {
        deleteItem(element);
    }
}