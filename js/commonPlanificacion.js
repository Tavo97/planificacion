function fillSelectItem(select, table, settings, type) {
    /*
    info for settings object
    settings = {
        firstItem: true,
        textFirstItem: 'Some text here',
        previous: 'Some text here',
        subsequently: 'Some text here'
    }
    */
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    deleteAllChilds(select.item);
    if (settings.firstItem) {
        let textFirst = langText.choose;
        if (e = settings.hasOwnProperty('textFirstItem')) {
            textFirst = settings.textFirstItem;
        }
        let newChild = {
            'childrenConf': [false],
            'child': {
                'prefix': select.prefix,
                'name': 'chooseFirst',
                'father': select.item,
                'kind': 'option',
                'innerHtml': textFirst,
                'attributes': { 'selected': 'true', 'value': '0' }
            }
        };
        buildNewElements(newChild);
    }
    searchItemsDB(table).then(res => {
        if (Object.keys(res).length > 0) {
            for (const item in res) {
                let objData = new Object();
                let innertext = '';
                table.fields.forEach(element => {
                    objData[element] = res[item][element];
                    if (table.exeption.includes(element)) {
                        innertext += `${res[item][element]} `;
                    }
                });
                let newOption = {
                    'childrenConf': [false],
                    'child': {
                        'prefix': select.prefix,
                        'name': res[item].id,
                        'father': select.item,
                        'kind': 'option',
                        'data': objData,
                        'attributes': { 'value': res[item].id }
                    }
                };
                if (e = settings.hasOwnProperty('previous')) {
                    innertext = `${settings.previous} ${innertext}`;
                }
                if (e = settings.hasOwnProperty('subsequently')) {
                    innertext = `${innertext} ${settings.previous}`;
                }
                if (e = settings.hasOwnProperty('activeLengthRes')) {
                    if (Object.keys(res).length <= settings.activeLengthRes) {
                        select.item.setAttribute('readonly', true);
                    } else {
                        select.item.removeAttribute('readonly');
                    }
                } else {
                    select.item.removeAttribute('readonly');
                }
                if (e = select.hasOwnProperty('exclude')) {
                    let exc = [];
                    try {
                        exc = JSON.parse(select.exclude);
                        if (select.exclude.includes(res[item].id)) { } else {
                            newOption.child.innerHtml = `${innertext}`;
                            buildNewElements(newOption);
                        }
                    } catch (error) {
                        newOption.child.innerHtml = `${innertext}`;
                        buildNewElements(newOption);
                    }
                } else {
                    newOption.child.innerHtml = `${innertext}`;
                    buildNewElements(newOption);
                }
                innertext = '';
                if (e = select.hasOwnProperty('currentOption')) {
                    select.item.value = select.currentOption;
                }
            }
            // if (e = select.hasOwnProperty('idItem')) {
            //     $(`#${select.idItem}`).select2({
            //         dropdownParent: $('#ModalPlaneacion')
            //     });
            // }
        }
    }).catch(e => {
        console.error(e);
        msGObj.labelLoader.style.color = 'tomato';
        msGObj.labelLoader.innerHTML = e;
    });
}

function getChooseItem(kind, parent, key) {
    let choose = '',
        select = 'none';
    let i = 0;
    choose = findElements(kind, parent);
    if (choose.state) {
        choose.items.forEach(element => {
            if (element.classList.value.includes(key)) {
                i++;
                select = element;
            }
        });
    }
    return [i, select];
}

function selecItemMenu(item) {
    let hijos = item.parentNode.childNodes;
    hijos.forEach(element => {
        if (element.tagName == 'I') {
            element.classList.remove('choosed-font');
        }
    });
    item.classList.toggle('choosed-font');
}

function addRowTable(dataRow, itemsRow, id, prefix, tableBody) { //Planeacion  tableBodyPlaneacion
    let newChild = {
        'childrenConf': [false],
        'child': {
            'prefix': `tableRow${prefix}`,
            'name': id,
            'father': tableBody,
            'kind': 'tr',
            'data': dataRow,
            'classes': ['hand'],
        }
    };
    let tr = buildNewElements(newChild);
    if (tr.status) {
        let newChildrenEqual = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': `tableRowField${prefix}`,
                'father': tr.child,
                'kind': 'td',
                'elements': itemsRow,
            }
        };
        buildNewElements(newChildrenEqual);
    }
    return tr;
}

function selectRow(e, multiple = false) {
    if (multiple == false) {
        let hijos = e.target.parentNode.parentNode.childNodes;
        hijos.forEach(element => {
            element.classList.remove('choosed');
        });
    }
    e.target.parentNode.classList.toggle('choosed');
}

function hideElement(elements, option = "") {
    switch (option) {
        case 0:
            console.log("Ocultar")
            elements.forEach(element => {
                element.classList.add('oculto', 'col-sm-6');
            });
            break;
        case 1:
            console.log("Mostrar")
            elements.forEach(element => {
                element.classList.remove('oculto', 'col-sm-6');
            });
            break;
        default:
            console.log("Toggle")
            elements.forEach(element => {
                element.classList.toggle('oculto');
                element.classList.toggle('col-sm-6');
            });
            break;
    }
}

/*Only search the first element for every tr*/
function searchColor(parent) {
    let children = parent.childNodes;
    children.forEach(item => {
        dataColor = item.dataset.color;
        itemcolor = item.childNodes;
        for (let i = 0; i < itemcolor.length; i++) {
            if (itemcolor[i].id.includes(dataColor)) {
                itemcolor[i].style.backgroundColor = dataColor;
                break;
            }
        }
    });
}

function selectChildOption(select, valueItem) {
    let children = select.childNodes;
    if (select.selectedIndex != -1) {
        select.options[select.selectedIndex].removeAttribute('selected');
    }
    for (let i = 0; i < children.length; i++) {
        if (children[i].tagName == 'OPTION') {
            if (children[i].value == valueItem) {
                children[i].setAttribute('selected', 'true');
            }
        }
    }
}

function changeMsgModal(data) {
    if (e = data.hasOwnProperty('ico')) {
        switch (data.ico) {
            case 'info':
                modalIcoMsg.setAttribute('class', 'fas fa-info-circle info-custome');
                break;
            case 'error':
                modalIcoMsg.setAttribute('class', 'fas fa-times-circle error-custome');
                break;
        }
    }
    if (e = data.hasOwnProperty('title')) {
        switch (data.title) {
            case 'info':
                modalTitleMsg.innerHTML = langText.msg_title_modal_info;
                break;
            case 'error':
                modalTitleMsg.innerHTML = langText.msg_title_modal_error;
                break;
        }
    }
    if (e = data.hasOwnProperty('msg')) {
        modalBodyMsg.innerHTML = `<p>${data.msg}</p>`;
    }
    if (e = data.hasOwnProperty('dataSets')) {
        setDatato(data.dataSets, labelBtnModalMsgAceptar);
    }
    if (e = data.hasOwnProperty('closeDataSet')) {
        if (data.closeDataSet.visible == 0) {
            labelBtnModalMsgCancelar.style.display = 'none';
        } else if (data.closeDataSet.visible == 1) {
            labelBtnModalMsgCancelar.style.display = 'block';
            setDatato(data.closeDataSet, labelBtnModalMsgCancelar);
        }
    }
}

function recreateFrmValidations() {
    let alert = document.querySelectorAll('.alert-validate');
    let alertInput = document.querySelectorAll('.alert-validate-input');
    if (alert.length > 0) {
        alert.forEach(element => {
            element.parentNode.removeChild(element);
        });
    }
    if (alertInput.length > 0) {
        alertInput.forEach(element => {
            element.classList.remove('alert-validate-input');
        });
    }
}

function assignValidations(validations) {
    let elements = document.querySelectorAll(`[data-validate]`)
    elements.forEach(element => {
        element.removeAttribute('data-validate');
    });
    for (const key in validations) {
        document.getElementById(`${key}`).dataset.validate = JSON.stringify(validations[key]);
    }
}

function checkGroup(check) {
    let group = check.dataset.group;
    if (isset(group)) {
        let siblings = document.querySelectorAll(`[data-group="${group}"]`);
        siblings.forEach(element => {
            if (element.id != check.id) {
                console.log(element);
                element.checked = 0;
            }
        });
    }
}

function searchItemsDB(table) {
    let langText = globalVariable.langText;
    return new Promise(function (resolve, reject) {
        let formData = objectToFormData({ 'peticion': 1, 'select': JSON.stringify(table.fields), 'tabla': table.name });
        if (e = table.hasOwnProperty('conditions')) {
            formData.append('conditions', JSON.stringify(table.conditions));
        } else {
            // console.info('No conditions set up', e);
        }
        if (e = table.hasOwnProperty('limit')) {
            formData.append('limit', JSON.stringify(table.limit));
        } else {
            // console.info('No limit set up', e);
        }
        if (e = table.hasOwnProperty('order')) {
            formData.append('order', JSON.stringify(table.order));
        } else {
            // console.info('No order set up', e);
        }
        if (e = table.hasOwnProperty('rs')) {
            formData.append('rs', table.rs);
        } else {
            // console.info('No rs set up', e);
        }
        let data = new Object();
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [false, formData];
        ajaxData(data).then((response) => {
            if (Object.keys(response).length > 0) {
                resolve(response);
            } else {
                reject(langText.msg_no_data_source);
            }
        });
    });
}

async function searchItemsDBAsync(table) {
    try {
        let res = await searchItemsDB(table);
        return res;
    } catch (e) {
        return e;
    }
}

// formBoolean an true is form type (sent a html element of type form)
function saveItemsDB(langText, kind, dataToSave, formBoolean = true, table = '') {
    return new Promise(function (resolve, reject) {
        let data = new Object();
        let go = false;
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        if (formBoolean) {
            if (kind == 2 || kind == 3) {
                go = true;
            } else {
                console.error('save with multi row and form type isn´t allowed')
            }
        } else {
            dataToSave.append('tabla', table.name);
            switch (kind) {
                case 2:
                    dataToSave.append('peticion', kind);
                    dataToSave.append('campos', JSON.stringify(table.campos));
                    go = true;
                    break;
                case 3:
                    dataToSave.append('peticion', kind);
                    dataToSave.append('campos', JSON.stringify(table.campos));
                    go = true;
                    break;
                case 5:
                    dataToSave.append('peticion', kind);
                    go = true;
                    break;
                default:
                    console.info('We can´t create items on db with that option.');
                    break;
            }
        }
        if (go) {
            data.form = [formBoolean, dataToSave];
            ajaxData(data).then((response) => {
                if (Object.keys(response).length > 0) {
                    resolve(response);
                } else {
                    reject(langText.msg_no_data_source);
                }
            }).catch(e => {
                reject(e);
            });
        } else {
            reject(langText.msg_error);
        }
    });
}

function closeWorkLand(msGObj) {
    setTimeout(() => {
        msGObj.loader.style.display = 'none';
        msGObj.labelLoader.style.color = 'black';
        msGObj.labelLoader.innerHTML = 'Working';
    }, 1000);
};

function selectedOption(selectItem) {
    let selectedOption = selectItem.options[selectItem.selectedIndex];
    return selectedOption;
}

function reemplazarCadena(cadenaVieja, cadenaNueva, cadenaCompleta) {
    for (var i = 0; i < cadenaCompleta.length; i++) {
        if (cadenaCompleta.substring(i, i + cadenaVieja.length) == cadenaVieja) {
            cadenaCompleta = cadenaCompleta.substring(0, i) + cadenaNueva + cadenaCompleta.substring(i + cadenaVieja.length, cadenaCompleta.length);
        }
    }
    return cadenaCompleta;
}

function orderDBresponse(response, field, type) {
    return new Promise(function (resolve, reject) {
        let element = new Array();
        if (Object.keys(response).length > 0) {
            for (const item in response) {
                element.push(response[item]);
            }
        }
        let sortData = quickSortObj(element, field, type);
        if (sortData.length > 0) {
            resolve(sortData);
        } else {
            reject(Error(sortData.length));
        }
    });
}

function orderDBresponseLR(response, field, type) {
    let sortData = quickSortObj(response, field, type);
    if (sortData.length > 0) {
        return sortData
    } else {
        return Error(sortData.length);
    }
}

function quickSortObj(array, field, type = 'string') {
    let pivote = array[0];
    let izq = new Array();
    let der = new Array();
    if (array.length < 1) {
        return array;
    }
    array.forEach((element, i) => {
        if (i > 0) {
            dataKind = type == 'number' ? parseInt(element[`${field}`]) : element[`${field}`].toUpperCase();
            pivoteVar = type == 'number' ? parseInt(pivote[`${field}`]) : pivote[`${field}`].toUpperCase();
            if (dataKind < pivoteVar) {
                izq.push(element);
            } else {
                der.push(element);
            }
        }
    });
    return quickSortObj(izq, field, type).concat([pivote]).concat(quickSortObj(der, field, type));
}

function modalSpot() {
    let bodyModal = document.querySelector('#page-my-index');
    setTimeout(() => {
        bodyModal.classList.add('modal-open');
    }, 550);
}

function deleteItemDB(table) {
    return new Promise(function (resolve, reject) {
        let data = new Object();
        let formData = objectToFormData({ 'peticion': 4, 'id': table.id, 'tabla': table.name });
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [false, formData];
        ajaxData(data).then((res) => {
            resolve(res);
        }).catch(e => {
            reject(e);
        });
    });
}

function countRecordsDB(table) {
    return new Promise(function (resolve, reject) {
        let data = new Object();
        let formData = objectToFormData({ 'peticion': 6, 'conditions': JSON.stringify(table.conditions), 'tabla': table.name });
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [false, formData];
        ajaxData(data).then((res) => {
            resolve(res);
        }).catch(e => {
            reject(e);
        });
    });
}

function validationExe(validations) {
    let Approve = true;
    let item;
    validations.forEach(element => {
        for (const key in element) {
            if (key == 'input') {
                item = element[key];
                continue;
            }
            if (element[key] != true) {
                Approve = false;
                console.log(`element ${key}: ${element[key].msg}`);
                item.classList.add('alert-validate-input');
                label = document.createElement('label');
                label.classList.add('alert-validate');
                label.innerHTML = element[key].msg;
                let newChildrenEqualBefore = {
                    'childrenConf': [false],
                    'child': {
                        'name': `alert-${key}`,
                        'father': item.parentNode,
                        'kind': 'label',
                        'innerHtml': element[key].msg,
                        'classes': ['alert-validate', 'flex-fill', 'col-12'],
                    }
                };
                buildNewElements(newChildrenEqualBefore);
            }
        }
    });
    return Approve;
}

function msgDeleteItem(msg, data1, data2) {
    msg = msg.replace('${op}', data1 == undefined ? '' : data1);
    msg = msg.replace('${op2}', data2);
    return msg
}

function fillLessonTable(res, langText) {
    res.forEach((element, i) => {
        let dataHorario = titleModulePlaneacion.dataset;
        let data = new Object();
        data = {
            'id': element.id,
            'sesiones_semana': element.sesiones_semana,
            'leccion': element.leccion,
            'id_curso': element.id_curso,
            'curso': element.curso,
            'id_horario': element.id_horario
        };
        let items = [i + 1, element.id];
        let table = {
            name: 'blm_vista_leccion_profesor',
            fields: ['*'],
            conditions: { 'id_leccion': ['=', element.id] }
        };
        searchItemsDB(table).then((profesores) => {
            return orderDBresponse(profesores, 'nombre');
        }).then(sortProfesores => {
            let namesTeachers = new Array();
            let objTeacher = new Array();
            sortProfesores.forEach(element => {
                objTeacher.push({
                    id: element.id_profesor,
                    nombre: element.nombre,
                    apellido_paterno: element.apellido_paterno,
                    apellido_materno: element.apellido_materno,
                    id_leccion: element.id_leccion
                });
                namesTeachers.push(`${element.nombre} ${element.apellido_paterno} ${element.apellido_materno}`);
            });
            data.idProfesores = JSON.stringify(objTeacher);
            namesTeachers = namesTeachers.sort().join(',<br/>');
            items.push(namesTeachers);
        }).then(() => {
            table = {
                name: 'blm_vista_leccion_grupo',
                fields: ['*'],
                conditions: { 'id_leccion': ['=', element.id] }
            };
            return searchItemsDB(table).then((grupos) => {
                return orderDBresponse(grupos, 'nombre');
            });
        }).then(sortGrupos => {
            let namesGroups = new Array();
            let objGroups = new Array();
            sortGrupos.forEach(element => {
                namesGroups.push(`${element.nombre}`);
                objGroups.push({
                    id: element.id_grupo,
                    nombre: element.nombre,
                    id_leccion: element.id_leccion
                });
            });
            data.idGrupos = JSON.stringify(objGroups);
            namesGroups = namesGroups.sort().join(',<br/>');
            items.push(namesGroups, element.curso, element.sesiones_semana, element.leccion);
        }).then(() => {
            table = {
                name: 'blm_vista_aula_curso',
                fields: ['*'],
                conditions: { 'id_curso': ['=', element.id_curso] }
            };
            return searchItemsDB(table).then((aulas) => {
                console.log(aulas);
                return orderDBresponse(aulas, 'aula');
            }).catch(error => {
                return error;
            });
        }).then(sortAulas => {
            if (Array.isArray(sortAulas)) {
                console.log(sortAulas);
                let nameClassroom = new Array();
                sortAulas.forEach(element => {
                    nameClassroom.push(`${element.aula}`);
                });
                nameClassroom = nameClassroom.sort().join(',<br/>');
                items.push(nameClassroom);
            } else {
                items.push(sortAulas);
            }
            return items;
        }).then(item => {
            // console.info(item);
            if (isset(dataHorario.id) && dataHorario.crear_horarios == 1) {
                item.push(element.periodo, element.semana);
                data.id_semana = element.id_semana;
                data.semana = element.semana;
                data.id_periodo = element.id_periodo;
                data.periodo = element.periodo;
            }
            tr = addRowTable(data, item, element.id, 'Lecciones', tableBodyLecciones);
            tr.child.addEventListener('click', (e) => {
                selectRow(e, true);
            });
        }).catch(error => console.error(error));
    });
}

function fillTeacherGroup(leccion, teachers, groups) {
    let formData = new FormData();
    formData.append('peticion', 5);
    let data = new Object();
    data.target = '../blocks/planificacion/peticionesSql.php';
    data.method = 'POST';
    data.send = true;
    let obj = new Array();
    let campos = "";
    if (teachers != undefined) {
        campos = JSON.parse(teachers);
        campos.forEach(element => {
            obj.push({
                id_leccion: leccion,
                id_profesor: element
            });
        });
        formData = objectToFormData({ 'tabla': 'blm_leccion_profesor', 'camposObj': JSON.stringify(obj) }, formData);
        data.form = [false, formData];
        ajaxData(data).then((res) => {
            console.log(res);
        });
    }
    let objGrupo = new Array();
    let camposGrupos = "";
    if (groups != undefined) {
        camposGrupos = JSON.parse(groups);
        camposGrupos.forEach(element => {
            objGrupo.push({
                id_leccion: leccion,
                id_grupo: element
            });
        });
        formData = objectToFormData({ 'tabla': 'blm_leccion_grupo', 'camposObj': JSON.stringify(objGrupo) }, formData);
        data.form = [false, formData];
        ajaxData(data).then((res) => {
            console.log(res);
        });
    }
}

function closeModal(data) {
    let dataModal = JSON.parse(data);
    if (dataModal.close.length > 0) {
        if (dataModal.close.length > 1) {
            dataModal.close.forEach(element => {
                $(`#${element}`).modal('hide');
            });
        } else {
            $(`#${dataModal.close[0]}`).modal('hide');
        }
        if (dataModal.status == 1) {
            modalSpot()
        }
    }
}

function getIdItems(items) {
    items = JSON.parse(items);
    let ids = new Array();
    items.forEach(element => {
        ids.push(element.id);
    });
    return JSON.stringify(ids);
}

function searchBuildFreeTime(info) {
    info.peticion = 7;
    let langText = globalVariable.langText;
    return new Promise(function (resolve, reject) {
        let formData = objectToFormData(info);
        let data = new Object();
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        data.form = [false, formData];
        ajaxData(data).then((response) => {
            if (Object.keys(response).length > 0) {
                resolve(response);
            } else {
                reject(langText.msg_no_data_source);
            }
        });
    });
}