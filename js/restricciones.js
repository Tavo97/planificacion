window.addEventListener('load', () => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let elementTohide = [];

    btnPlaneacionRestricciones.addEventListener('click', (e) => {
        gestionFrmRestricciones.reset();
        hideElement(elementTohide);
        // Check item selected on table
        selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        // Check item selected on left menu
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        let dataHorario = titleModulePlaneacion.dataset;
        if (isset(dataHorario.id)) {
            if (selectMenu != 'none') {
                let op = selectMenu.dataset.title;
                labelBtnModalRestriccionesAgregar.innerHTML = langText.btn_guardar_frm;
                labelBtnModalRestriccionesAgregar.dataset.menu = op;
                if (op == 'Profesores') {
                    if (selectItem != 'none') {
                        peticionFrmRestricciones.value = 2;
                        idItemRestricciones.value = selectItem.dataset.id;
                        idItemHorarioRestricciones.value = titleModulePlaneacion.dataset.id; 
                        let abrir = true;
                        msGObj.loader.style.display = 'block';
                        msGObj.labelLoader.style.color = '#20c997';
                        let text = langText.msg_get_data_restrictions;
                        let labelMenu = selectMenu.title;
                        msGObj.labelLoader.innerHTML = text.replace('${op}', labelMenu);
                        switch (op) {
                            case 'Profesores':
                                let elements = document.querySelectorAll(`#gestionFrmRestricciones [type="checkbox"]`);
                                let checks = new Array();
                                elements.forEach(element => {
                                    element.removeAttribute('checked');
                                    checks.push(element.id);
                                });
                                let inputNumber = document.querySelectorAll(`#gestionFrmRestricciones [type="number"]`);
                                inputNumber.forEach(element => {
                                    element.setAttribute('disabled', true);
                                });
                                let idItem = selectItem.dataset.id;
                                let table = ['blm_restricciones_profesores', 'id_profesor'];
                                let data = new Object();
                                let formData = new FormData();
                                tablaFrmRestricciones.value = table[0];
                                let elementFrm = {
                                    'check_lim_huecos_horario': 'limitehuecosFrmRestricciones',
                                    'max_huecos': 'maximahuecosFrmRestricciones',
                                    'limite_num_dias': 'limitardiasFrmRestricciones',
                                    'input_num_dias': 'diasenqueFrmRestricciones',
                                    'min_max_lecciones_dia': 'leccionesxdiaFrmRestricciones',
                                    'lecciones_por_dia_1': 'numeroleccionesxdiaFrmRestricciones',
                                    'lecciones_por_dia_2': 'numero2leccionesxdiaFrmRestricciones',
                                    'fin_semana_min_max': 'noctrlfindeFrmRestricciones',
                                    'lecciones_consecutivas': 'leccionesconsecutivasFrmRestricciones',
                                    'input_lecciones_consecutivas': 'maxleccionesconsecutivasFrmRestricciones',
                                    'agotamiento_fines': 'agotamientoFrmRestricciones',
                                    'tres_huecos': 'tresHuecosFrmRestricciones',
                                    'dos_huecos': 'dosHuecosFrmRestricciones',
                                    'id_profesor': 'idItemRestricciones',
                                    'id_horario': 'idItemHorarioRestricciones'
                                };
                                data.target = '../blocks/planificacion/peticionesSql.php';
                                data.method = 'POST';
                                data.send = true;
                                formData.append('peticion', 1);
                                formData.append('select', JSON.stringify(['*']));
                                formData.append('tabla', table[0]);
                                formData.append('conditions', JSON.stringify({
                                    [table[1]]: ['=', idItem]
                                }));
                                data.form = [false, formData];
                                ajaxData(data).then((res) => {
                                    if (Object.keys(res).length > 0) {
                                        peticionFrmRestricciones.value = 3;
                                        elementFrm.id = 'idFrmRestricciones';
                                        for (const item in res) {
                                            for (const key in res[item]) {
                                                if (checks.includes(elementFrm[key])) {
                                                    if (parseInt(res[item][key]) == 1) {
                                                        checkCurrent = document.getElementById(elementFrm[key]);
                                                        checkCurrent.setAttribute('checked', true);
                                                        if (isset(checkCurrent.dataset.targetTo)) {
                                                            getTargetsCheck = checkCurrent.dataset.targetTo.split('&');
                                                            if (getTargetsCheck.length > 1) {
                                                                getTargetsCheck.forEach(element => {
                                                                    document.getElementById(element).removeAttribute('disabled');
                                                                });
                                                            } else {
                                                                document.getElementById(getTargetsCheck[0]).removeAttribute('disabled');
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    document.getElementById(elementFrm[key]).value = res[item][key];
                                                }
                                            }
                                        }
                                    }
                                    camposFrmRestricciones.value = JSON.stringify(elementFrm);
                                });
                                break;
                        }
                        setTimeout(() => {
                            if (abrir) {
                                $('#ModalRestricciones').modal({ backdrop: 'static', keyboard: false });
                            }
                            hideElement(elementTohide);
                            msGObj.loader.style.display = 'none';
                            msGObj.labelLoader.style.color = 'black';
                            msGObj.labelLoader.innerHTML = langText.msg_working;
                        }, 3000);
                    } else {
                        data = {
                            'dataSets': {
                                action: 'Info',
                                modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                            },
                            'msg': langText.msg_info_select_item_table,
                            'ico': 'info',
                            'closeDataSet': {
                                visible: 0
                            }
                        };
                        changeMsgModal(data);
                        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                    }
                } else {
                    data = {
                        'dataSets': {
                            action: 'Info',
                            modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                        },
                        'msg': langText.msg_info_no_disponible,
                        'ico': 'info',
                        'closeDataSet': {
                            visible: 0
                        }
                    };
                    changeMsgModal(data);
                    $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                }
            } else {
                data = {
                    'dataSets': {
                        action: 'Info',
                        modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                    },
                    'msg': langText.msg_info_no_disponible,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 0
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_horario,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalRestriccionesAgregar.addEventListener('click', (e) => {
        e.preventDefault();
        msGObj.loader.style.display = 'block';
        let data = new Object();
        let send;
        data.target = '../blocks/planificacion/peticionesSql.php';
        data.method = 'POST';
        data.send = true;
        if (e.target.dataset.menu = "Profesores") {
            send = [false, getCheckboxData({ name: 'gestionFrmRestricciones', item: gestionFrmRestricciones })];
        } else {
            send = [true, gestionFrmRestricciones];
        }
        data.form = send;
        printFormData(send[1]);
        ajaxData(data).then((res) => {
            msGObj.labelLoader.style.color = '#28a745';
            if (res[0] != false) {
                let op = parseInt(peticionFrmRestricciones.value);
                switch (op) {
                    case 2:
                        msGObj.labelLoader.innerHTML = langText.msg_added;
                        break;
                    case 3:
                        msGObj.labelLoader.innerHTML = langText.msg_saved;
                        break;
                }
                searchColor(tableBodyPlaneacion);
                closeModal(JSON.stringify({ status: 0, close: ['ModalRestricciones'] }));
                closeWorkLand(msGObj);
            }
        });
    });

    labelBtnModalRestriccionesCancelar.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 0, close: ['ModalMsg', 'ModalRestricciones'] })
            },
            'msg': langText.msg_alert_Cancel,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    leccionesxdiaFrmRestricciones.addEventListener('click', (e) => {
        if (leccionesxdiaFrmRestricciones.checked == true) {
            numeroleccionesxdiaFrmRestricciones.disabled = false;
            numero2leccionesxdiaFrmRestricciones.disabled = false;
        } else {
            numeroleccionesxdiaFrmRestricciones.disabled = true;
            numero2leccionesxdiaFrmRestricciones.disabled = true;
        }
    });

    limitehuecosFrmRestricciones.addEventListener('click', (e) => {
        if (limitehuecosFrmRestricciones.checked == true) {
            maximahuecosFrmRestricciones.disabled = false;
        } else {
            maximahuecosFrmRestricciones.disabled = true;
        }
    });

    limitardiasFrmRestricciones.addEventListener('click', (e) => {
        if (limitardiasFrmRestricciones.checked == true) {
            diasenqueFrmRestricciones.disabled = false;
        } else {
            diasenqueFrmRestricciones.disabled = true;
        }
    });

    leccionesconsecutivasFrmRestricciones.addEventListener('click', (e) => {
        if (leccionesconsecutivasFrmRestricciones.checked == true) {
            maxleccionesconsecutivasFrmRestricciones.disabled = false;
        } else {
            maxleccionesconsecutivasFrmRestricciones.disabled = true;
        }
    });

});