window.addEventListener('load', (e) => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    btnPlaneacionTiempoLibre.addEventListener('click', (e) => {
        recreateFrmValidations();
        deleteAllChilds(tableBodyTiempo);
        deleteAllChilds(tableTitleTiempo, ['tableTiempo#']);
        let dataHorario = titleModulePlaneacion.dataset;
        if (isset(dataHorario.id)) {
            // Check item selected on table
            selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
            // Check item selected on left menu
            selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
            if (selectMenu != 'none') {
                if (selectMenu.dataset.title == 'Profesores' || selectMenu.dataset.title == 'Aulas' || selectMenu.dataset.title == 'Clases' || selectMenu.dataset.title == 'Cursos') {
                    if (selectItem != 'none') {
                        (dataHorario.crear_horarios == 0) ?
                            grpPeriodoSemanaTiempo.style.display = 'none' :
                            grpPeriodoSemanaTiempo.style.display = 'block';
                        let selectItems = [{
                            select: {
                                item: periodoTLTiempo,
                                prefix: `${periodoTLTiempo.id}Op`
                            },
                            table: {
                                name: 'blm_periodos',
                                fields: ['id', 'nombre', 'tipo'],
                                exeption: ['nombre'],
                                conditions: {
                                    'id_horario': ['=', dataHorario.id],
                                    'nombre': ['multiple', ['!=', 'Todo el año'], ['!=', 'Cualquier periodo']]
                                }
                            }
                        },
                        {
                            select: {
                                item: semanaTLTiempo,
                                prefix: `${semanaTLTiempo.id}Op`
                            },
                            table: {
                                name: 'blm_semanas',
                                fields: ['id', 'nombre', 'tipo'],
                                exeption: ['nombre'],
                                conditions: {
                                    'id_horario': ['=', dataHorario.id],
                                    'nombre': ['multiple', ['!=', 'Todas las semanas'], ['!=', 'Cualquier semana']]
                                }
                            }
                        }];
                        selectItems.forEach(element => {
                            fillSelectItem(element.select, element.table, { firstItem: false }, element.type);
                        });
                        table = {
                            name: 'blm_periodos',
                            conditions: { id_horario: dataHorario.id, tipo: 0 }
                        };
                        countRecordsDB(table).then(records => {
                            if (records > 0) {
                                hideElement([grpPeriodosTiempo], 1);
                            } else {
                                hideElement([grpPeriodosTiempo], 0);
                            }
                        }).catch(e => console.error(e));
                        table = {
                            name: 'blm_semanas',
                            conditions: { id_horario: dataHorario.id, tipo: 0 }
                        };
                        countRecordsDB(table).then(records => {
                            if (records > 0) {
                                hideElement([grpSemanasTiempo], 1);
                            } else {
                                hideElement([grpSemanasTiempo], 0);
                            }
                        }).catch(e => console.error(e));
                        let validations = new Object();
                        let titleModal = document.getElementById('Modal-title-Tiempo');
                        titleModal.innerHTML = `${selectMenu.title} &#8594; ${titleModal.dataset.title} &#8594; ${dataHorario.id} - ${dataHorario.centro} &#8594; ${selectItem.dataset.nombre}`;
                        let text = langText.msg_get_data_tiempo;
                        let labelMenu = selectMenu.title;
                        msGObj.loader.style.display = 'block';
                        msGObj.labelLoader.style.color = '#20c997';
                        msGObj.modalTitle.innerHTML = labelMenu;
                        msGObj.labelLoader.innerHTML = text.replace('${op}', labelMenu);
                        let op = selectMenu.dataset.title;
                        let tableSetting = new Object();
                        let idHorario = dataHorario.id;
                        let idItem = selectItem.dataset.id;
                        tableSetting.values = {
                            id_horario: idHorario,
                            id_item: idItem
                        };
                        periodosCheckTiempo.checked = parseInt(dataHorario.periodo_bool);
                        semanasCheckTiempo.checked = parseInt(dataHorario.semana_bool);
                        if (parseInt(dataHorario.periodo_bool) == 1) {
                            periodoTLTiempo.disabled = false;
                            validations.periodoTLTiempo = {
                                'value': {
                                    symbol: '>',
                                    value: 0
                                }
                            };
                        } else { periodoTLTiempo.disabled = true; }
                        if (parseInt(dataHorario.semana_bool) == 1) {
                            semanaTLTiempo.disabled = false;
                            validations.semanaTLTiempo = {
                                'value': {
                                    symbol: '>',
                                    value: 0
                                }
                            };
                        } else { semanaTLTiempo.disabled = true; }
                        assignValidations(validations);
                        tableSetting = pathSettingFreeTime(op, tableSetting);
                        table = {
                            name: 'blm_horas_tl',
                            fields: ['id', 'hora']
                        };
                        searchItemsDB(table).then(horas => {
                            let arrHoras = new Array();
                            for (const item in horas) {
                                if (parseInt(dataHorario.lecciones) < parseInt(horas[item].id)) {
                                    break;
                                }
                                arrHoras.push(horas[item]);
                                newTh = {
                                    'childrenConf': [false],
                                    'child': {
                                        'prefix': 'tableTiempo',
                                        'name': horas[item].id,
                                        'father': tableTitleTiempo,
                                        'kind': 'th',
                                        'innerHtml': `${horas[item].hora}`,
                                        'data': {
                                            'state': 'unknow',
                                            'id': horas[item].id,
                                            'hora': horas[item].hora
                                        },
                                        'classes': ['nowrap', 'hand'],
                                        'attributes': { 'scope': 'col' },
                                    }
                                };
                                newThItem = buildNewElements(newTh).child;
                                // cabecera de la tabla tiempo libre
                                newThItem.addEventListener('click', (e) => {
                                    let dataValidate = document.querySelectorAll('[data-validate]');
                                    let validations = validation(dataValidate, langText);
                                    let Approve = validationExe(validations);
                                    if (Approve) {
                                        let children = findElements('TR', tableBodyTiempo).items;
                                        let father = e.target; //Elemento de la cabecera
                                        children.forEach(child => {
                                            let tds = findElements('TD', child).items;
                                            let itemChange = binarySearch(tds, father.dataset.id);
                                            tableSetting = freeTimeMultipleValidations(tableSetting);
                                            if (e = tableSetting.hasOwnProperty('multiTable')) {
                                                switch (tableSetting.multiTable.fieldIdMult) {
                                                    case 'id_periodo':
                                                        tableSetting.values.fieldIdMult = periodoTLTiempo.value;
                                                        break;

                                                    case 'id_semana':
                                                        tableSetting.values.fieldIdMult = semanaTLTiempo.value;
                                                        break;
                                                }
                                                if (e = tableSetting.multiTable.hasOwnProperty('fieldIdMult2')) {
                                                    switch (tableSetting.multiTable.fieldIdMult2) {
                                                        case 'id_periodo':
                                                            tableSetting.values.fieldIdMult2 = periodoTLTiempo.value;
                                                            break;

                                                        case 'id_semana':
                                                            tableSetting.values.fieldIdMult2 = semanaTLTiempo.value;
                                                            break;
                                                    }
                                                }
                                            }
                                            // 1 => configuracion de la tabla segun sea la que se debe usar (debido a la bifurcacion entre Aulas, Profesores, Grupos, Curso)
                                            // 2 => Elemento individual de la tabla Tiempo Libre,
                                            // 3 => Se toma el estado de la cabecera para poder cambiar el estado de la columna correspondiente
                                            changeStateDB(tableSetting, itemChange, father.dataset.state).then(res => {
                                                if (res[0][0] != false) {
                                                    father.dataset.state = res[1][0];
                                                    notifyTiempo.innerHTML = langText.msg_saved;
                                                    notifyTiempo.classList.remove('fadeOut');
                                                    notifyTiempo.classList.add('alert-success', 'fadeIn');
                                                    fadeOut({ time: 3000, item: notifyTiempo });
                                                }
                                            }).catch(e => {
                                                notifyTiempo.innerHTML = `${langText.msg_no_save} ${e}`;
                                                notifyTiempo.classList.add('alert-danger', 'fadeIn');
                                                fadeOut({ time: 3000, item: notifyTiempo });
                                            });
                                        });
                                    }
                                });
                            }
                            table = {
                                name: 'blm_dias_tl',
                                fields: ['id', 'dia']
                            };
                            return searchItemsDB(table).then(dias => {
                                return [dias, arrHoras];
                            });
                        }).then(data => {
                            let trs = new Array();
                            let dias = data[0];//Obj
                            let horas = data[1];//Arr
                            for (const dia in dias) {
                                if (parseInt(dataHorario.dias) < parseInt(dias[dia].id)) {
                                    break;
                                }
                                newChild = {
                                    'childrenConf': [false],
                                    'child': {
                                        'prefix': 'tableTiempoTr',
                                        'name': dias[dia].id,
                                        'father': tableBodyTiempo,
                                        'kind': 'tr',
                                        'data': { 'state': 'unknow' }
                                    }
                                };
                                // crea cada fila dentro de este for que recorre los dias
                                newTr = buildNewElements(newChild);
                                trs.push(newTr);
                                if (newTr.status) {
                                    newTd = {
                                        'childrenConf': [false],
                                        'child': {
                                            'prefix': 'tableTiempoTd',
                                            'name': dias[dia].id,
                                            'father': newTr.child,
                                            'kind': 'th',
                                            'innerHtml': `${dias[dia].dia}`,
                                            'data': {
                                                'id': dias[dia].id,
                                                'dia': dias[dia].dia
                                            },
                                            'classes': ['table-dark', 'hand']
                                        }
                                    };
                                    // columna inicial que contiene los dias de la tabla tiempo libre
                                    lineStateTd = buildNewElements(newTd);
                                    lineStateTd.child.addEventListener('click', (e) => {
                                        let dataValidate = document.querySelectorAll('[data-validate]');
                                        let validations = validation(dataValidate, langText);
                                        let Approve = validationExe(validations);
                                        if (Approve) {
                                            let diaClick = e.target;
                                            let father = diaClick.parentNode;
                                            let brothers = findElements('TD', father).items;
                                            brothers.forEach(brother => {
                                                tableSetting = freeTimeMultipleValidations(tableSetting);
                                                if (e = tableSetting.hasOwnProperty('multiTable')) {
                                                    switch (tableSetting.multiTable.fieldIdMult) {
                                                        case 'id_periodo':
                                                            tableSetting.values.fieldIdMult = periodoTLTiempo.value;
                                                            break;

                                                        case 'id_semana':
                                                            tableSetting.values.fieldIdMult = semanaTLTiempo.value;
                                                            break;
                                                    }
                                                    if (e = tableSetting.multiTable.hasOwnProperty('fieldIdMult2')) {
                                                        switch (tableSetting.multiTable.fieldIdMult2) {
                                                            case 'id_periodo':
                                                                tableSetting.values.fieldIdMult2 = periodoTLTiempo.value;
                                                                break;

                                                            case 'id_semana':
                                                                tableSetting.values.fieldIdMult2 = semanaTLTiempo.value;
                                                                break;
                                                        }
                                                    }
                                                }
                                                changeStateDB(tableSetting, brother, father.dataset.state).then(res => {
                                                    if (res[0][0] != false) {
                                                        father.dataset.state = res[1][0];
                                                        notifyTiempo.innerHTML = langText.msg_saved;
                                                        notifyTiempo.classList.remove('fadeOut');
                                                        notifyTiempo.classList.add('alert-success', 'fadeIn');
                                                        fadeOut({ time: 3000, item: notifyTiempo });
                                                    }
                                                }).catch(e => {
                                                    notifyTiempo.innerHTML = `${langText.msg_no_save} ${e}`;
                                                    notifyTiempo.classList.add('alert-danger', 'fadeIn');
                                                    fadeOut({ time: 3000, item: notifyTiempo });
                                                });
                                            });
                                        }
                                    });
                                    // For que recorre las horas necesarias para generar el contenido de la tabla estas se crean en el estado inicial unknow
                                    horas.forEach(hora => {
                                        newTd = {
                                            'childrenConf': [false],
                                            'child': {
                                                'prefix': 'tableTiempoTd',
                                                'name': `${dias[dia].id}-${hora.id}`,
                                                'father': newTr.child,
                                                'kind': 'td',
                                                'data': {
                                                    'td-state': 'unknow',
                                                    'id-hora': hora.id,
                                                    'hora': hora.hora,
                                                    'id-dia': dias[dia].id,
                                                    'dia': dias[dia].dia
                                                },
                                                'classes': ['unknow', 'hand']
                                            }
                                        };
                                        stateTd = buildNewElements(newTd);
                                        stateTd.child.addEventListener('click', (item) => {
                                            let dataValidate = document.querySelectorAll('[data-validate]');
                                            let validations = validation(dataValidate, langText);
                                            let Approve = validationExe(validations);
                                            if (Approve) {
                                                console.log('Estados unicos');
                                                tableSetting = freeTimeMultipleValidations(tableSetting);
                                                if (e = tableSetting.hasOwnProperty('multiTable')) {
                                                    switch (tableSetting.multiTable.fieldIdMult) {
                                                        case 'id_periodo':
                                                            tableSetting.values.fieldIdMult = periodoTLTiempo.value;
                                                            break;

                                                        case 'id_semana':
                                                            tableSetting.values.fieldIdMult = semanaTLTiempo.value;
                                                            break;
                                                    }
                                                    if (e = tableSetting.multiTable.hasOwnProperty('fieldIdMult2')) {
                                                        switch (tableSetting.multiTable.fieldIdMult2) {
                                                            case 'id_periodo':
                                                                tableSetting.values.fieldIdMult2 = periodoTLTiempo.value;
                                                                break;

                                                            case 'id_semana':
                                                                tableSetting.values.fieldIdMult2 = semanaTLTiempo.value;
                                                                break;
                                                        }
                                                    }
                                                }
                                                let element = item.target;
                                                changeStateDB(tableSetting, element).then(res => {
                                                    if (res[0][0] != false) {
                                                        notifyTiempo.innerHTML = langText.msg_saved;
                                                        notifyTiempo.classList.remove('fadeOut');
                                                        notifyTiempo.classList.add('alert-success', 'fadeIn');
                                                        fadeOut({ time: 3000, item: notifyTiempo });
                                                    }
                                                }).catch(e => {
                                                    notifyTiempo.innerHTML = `${langText.msg_no_save} ${e}`;
                                                    notifyTiempo.classList.add('alert-danger', 'fadeIn');
                                                    fadeOut({ time: 3000, item: notifyTiempo });
                                                });
                                            }
                                        });
                                    });
                                }
                            }
                            console.log(trs);
                            return trs;
                        }).then(trs => {
                            let newTrs = new Array();
                            for (const tr in trs) {
                                if (trs[tr].child != undefined) {
                                    newTrs.push(trs[tr].child);
                                }
                            }
                            table = {
                                name: tableSetting.baseTable.table,
                                fields: ['id', tableSetting.baseTable.field, 'id_dia_tl', 'id_hora_tl', 'id_estado_tl'],
                                conditions: {
                                    [tableSetting.baseTable.field]: ['=', idItem],
                                    'id_horario': ['=', idHorario]
                                }
                            };
                            getStatesDB(newTrs, table).catch(e => { console.log(e) });
                        }).catch(error => {
                            msGObj.labelLoader.style.color = 'tomato';
                            msGObj.labelLoader.innerHTML = error;
                        });
                        closeWorkLand(msGObj);
                        $('#ModalTiempo').modal({ backdrop: 'static', keyboard: false });
                    } else {
                        data = {
                            'dataSets': {
                                action: 'Info',
                                modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                            },
                            'msg': langText.msg_info_select_item_table,
                            'ico': 'info',
                            'closeDataSet': {
                                visible: 0
                            }
                        };
                        changeMsgModal(data);
                        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                    }
                } else {
                    data = {
                        'dataSets': {
                            action: 'Info',
                            modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                        },
                        'msg': langText.msg_info_no_disponible,
                        'ico': 'info',
                        'closeDataSet': {
                            visible: 0
                        }
                    };
                    changeMsgModal(data);
                    $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                }
            } else {
                data = {
                    'dataSets': {
                        action: 'Info',
                        modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                    },
                    'msg': langText.msg_info_select_main_menu, //Seleccionar una seccion del menu principal
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 0
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_horario,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    periodosCheckTiempo.addEventListener('change', (e) => {
        let dataHorario = titleModulePlaneacion.dataset;
        let selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        let tableSetting = new Object();
        tableSetting.values = {
            id_horario: dataHorario.id,
            id_item: selectItem.dataset.id
        };
        pathSettingFreeTime(getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1].dataset.title, tableSetting);
        msGObj.loader.style.display = 'block';
        let periodo_status;
        let formData = new FormData();
        if (periodosCheckTiempo.checked) {
            periodoTLTiempo.disabled = false;
            periodo_status = 1;
        } else {
            periodoTLTiempo.disabled = true;
            periodo_status = 0;
        }
        let table = {
            name: 'blm_horarios',
            campos: {
                'id': 'id_horario',
                'periodo_bool': 'periodo_status'
            }
        };
        formData.append('periodo_status', periodo_status);
        formData.append('id_horario', dataHorario.id);
        saveItemsDB(langText, 3, formData, false, table).then(response => {
            if (response[0] != false) {
                msGObj.labelLoader.style.color = '#20c997';
                dataHorario.periodo_bool = periodo_status;
                msGObj.labelLoader.innerHTML = langText.msg_saved;
            }
        }).catch(e => {
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = e;
        });
        closeWorkLand(msGObj);
    });

    semanasCheckTiempo.addEventListener('click', (e) => {
        let dataHorario = titleModulePlaneacion.dataset;
        let selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        let tableSetting = new Object();
        tableSetting.values = {
            id_horario: dataHorario.id,
            id_item: selectItem.dataset.id
        };
        pathSettingFreeTime(getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1].dataset.title, tableSetting);
        msGObj.loader.style.display = 'block';
        let semama_status;
        let formData = new FormData();
        if (semanasCheckTiempo.checked) {
            semanaTLTiempo.disabled = false;
            semama_status = 1;
        } else {
            semanaTLTiempo.disabled = true;
            semama_status = 0;
        }
        let table = {
            name: 'blm_horarios',
            campos: {
                'id': 'id_horario',
                'semana_bool': 'semama_status'
            }
        };
        formData.append('semama_status', semama_status);
        formData.append('id_horario', dataHorario.id);
        saveItemsDB(langText, 3, formData, false, table).then(response => {
            if (response[0] != false) {
                msGObj.labelLoader.style.color = '#20c997';
                dataHorario.semana_bool = semama_status;
                msGObj.labelLoader.innerHTML = langText.msg_saved;
            }
        }).catch(e => {
            msGObj.labelLoader.style.color = 'tomato';
            msGObj.labelLoader.innerHTML = e;
        });
        closeWorkLand(msGObj);
    });

    periodoTLTiempo.addEventListener('change', (e) => {
        changeStatesMultiples(e.target);
    });

    semanaTLTiempo.addEventListener('change', (e) => {
        changeStatesMultiples(e.target);
    });

});

// funciones fuera del ciclo de inicio window load

function changeStatesMultiples(item) {
    let trs = findElements('tr', tableBodyTiempo);
    let langText = globalVariable.langText;
    let selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1].dataset;
    let dataHorario = titleModulePlaneacion.dataset;
    let dataItemBase = JSON.parse(item.dataset.baseTable);
    tableBase = {
        name: dataItemBase.table,
        fields: ['id', dataItemBase.field, 'id_dia_tl', 'id_hora_tl', 'id_estado_tl'],
        conditions: {
            [dataItemBase.field]: ['=', selectItem.id],
            'id_horario': ['=', dataHorario.id]
        }
    };
    getStatesDB(trs.items, tableBase).then(res => {
        if (res) {
            if (periodosCheckTiempo.checked && semanasCheckTiempo.checked) {
                if (periodoTLTiempo.value == 0 || semanaTLTiempo.value == 0) {
                    notifyTiempo.innerHTML = langText.msg_info_select_multiple_valido;
                    notifyTiempo.classList.remove('fadeOut');
                    notifyTiempo.classList.add('alert-dark', 'fadeIn');
                    fadeOut({ time: 3000, item: notifyTiempo });
                } else {
                    if (selectedOption(periodoTLTiempo).dataset.tipo == 1 && selectedOption(semanaTLTiempo).dataset.tipo == 1) {
                        return getStatesDB(trs.items, tableBase);
                    } else {
                        dataItem = JSON.parse(item.dataset.multiTable);
                        table = {
                            name: dataItem.tableView,
                            fields: ['id', 'id_tlm', dataItem.fieldIdMult, dataItem.fieldIdMult2, 'id_dia_tl', 'id_hora_tl', 'id_estado_tl'],
                            conditions: {
                                [dataItemBase.field]: ['=', selectItem.id],
                                'id_horario': ['=', dataHorario.id],
                                [dataItem.fieldIdMult]: ['=', periodoTLTiempo.value],
                                [dataItem.fieldIdMult2]: ['=', semanaTLTiempo.value]
                            }
                        };
                        return getStatesDB(trs.items, table, 1);
                    }
                }
            } else {
                if (item.value == 0) {
                    notifyTiempo.innerHTML = langText.msg_info_select_multiple_valido;
                    notifyTiempo.classList.remove('fadeOut');
                    notifyTiempo.classList.add('alert-dark', 'fadeIn');
                    fadeOut({ time: 3000, item: notifyTiempo });
                } else {
                    if (selectedOption(item).dataset.tipo == 1) {
                        return getStatesDB(trs.items, tableBase);
                    } else {
                        dataItem = JSON.parse(item.dataset.multiTable);
                        table = {
                            name: dataItem.tableView,
                            fields: ['id', 'id_tlm', dataItem.fieldIdMult, 'id_dia_tl', 'id_hora_tl', 'id_estado_tl'],
                            conditions: {
                                [dataItemBase.field]: ['=', selectItem.id],
                                'id_horario': ['=', dataHorario.id],
                                [dataItem.fieldIdMult]: ['=', item.value]
                            }
                        };
                        return getStatesDB(trs.items, table, 1);
                    }
                }
            }
        }
    }).catch(e => { console.log(e) });
}

function getStatesDB(trs, table, multi = 0) {
    return new Promise(function (resolve, reject) {
        trs.forEach(element => {
            thDia = findElements('TH', element).items[0].dataset.id;
            table.conditions.id_dia_tl = ['=', thDia];
            searchItemsDB(table).then(horasDiasItems => {
                let tds = findElements('TD', element).items;
                for (const ft in horasDiasItems) {
                    tds.forEach(td => {
                        delete td.dataset.idBranch;
                        if (parseInt(horasDiasItems[ft].id_hora_tl) == parseInt(td.dataset.idHora)) {
                            td.dataset.idTblDb = horasDiasItems[ft].id;
                            if (multi == 1) { td.dataset.idBranch = horasDiasItems[ft].id_tlm };
                            switch (parseInt(horasDiasItems[ft].id_estado_tl)) {
                                case 1:
                                    text = 'Bien';
                                    td.setAttribute('class', `hand ${text}`);
                                    td.dataset.tdState = text;
                                    break;
                                case 2:
                                    text = 'Mala';
                                    td.setAttribute('class', `hand ${text}`);
                                    td.dataset.tdState = text;
                                    break;
                                case 3:
                                    text = 'Pregunta';
                                    td.setAttribute('class', `hand ${text}`);
                                    td.dataset.tdState = text;
                                    break;
                            }
                        }
                    });
                }
                resolve(true);
            }).catch(e => {
                notifyTiempo.innerHTML = e;
                notifyTiempo.classList.remove('fadeOut');
                notifyTiempo.classList.add('alert-danger', 'fadeIn');
                fadeOut({ time: 3000, item: notifyTiempo });
                reject(e);
            });
        });
    });
}

function freeTimeMultipleValidations(tableSetting) {
    if (periodosCheckTiempo.checked && semanasCheckTiempo.checked) {
        if (selectedOption(semanaTLTiempo).dataset.tipo == 1 && selectedOption(periodoTLTiempo).dataset.tipo == 1) {
            delete tableSetting.multiTable;
        } else {
            tableSetting.multiTable = JSON.parse(semanaTLTiempo.dataset.multiTable);
        }
        return tableSetting;
    } else {
        delete tableSetting.multiTable;
        if (periodosCheckTiempo.checked && selectedOption(periodoTLTiempo).dataset.tipo != 1) {
            tableSetting.multiTable = JSON.parse(periodoTLTiempo.dataset.multiTable);
            return tableSetting;
        }
        if (semanasCheckTiempo.checked && selectedOption(semanaTLTiempo).dataset.tipo != 1) {
            tableSetting.multiTable = JSON.parse(semanaTLTiempo.dataset.multiTable);
            return tableSetting;
        }
    }
    return tableSetting;
}

function changeStateTimeFree(element, state = false) {
    let fatherState = '';
    let elementState = '';
    let res = new Object();
    elementState = getState(element.dataset.tdState);
    if (state != false) {
        fatherState = getState(state);
        res = {
            state: elementState,
            stateFather: fatherState
        };
    } else {
        res = { state: elementState };
    }

    function getState(op) {
        let ns = '';
        switch (op) {
            case 'Bien':
                ns = { name: 'Mala', number: 2 };
                break;
            case 'Mala':
                ns = { name: 'Pregunta', number: 3 };

                break;
            case 'Pregunta':
                ns = { name: 'Bien', number: 1 };

                break;
            case 'unknow':
                ns = { name: 'Bien', number: 1 };

                break;
        }
        return ns;
    }
    return res;
}

function changeStateDB(table, element, fatherState = false) {
    return new Promise(function (resolve, reject) {
        let langText = globalVariable.langText;
        let flagMultiple = false;
        let currentState = element.dataset.tdState;
        let idBranch = element.dataset.idBranch;
        // changeStateTimeFree(item)[0] regresa estado del elemento en la tabla tiempo libre
        // changeStateTimeFree(item)[1] regresa estado del padre del elemento en la tabla tiempo libre (header o fila estado)
        let newState = (fatherState == false) ? changeStateTimeFree(element).state : changeStateTimeFree(element, fatherState).stateFather;
        console.log(newState);
        // se comienza proceso de guardado
        let peticion = '';
        let formData = new FormData();
        let tableDB = new Object();
        if (e = table.hasOwnProperty('multiTable')) {
            flagMultiple = true;
            // Se cambia el direccionamiento del guardado a las tablas rama
            tableDB = {
                name: table.multiTable.table,
                campos: {
                    [table.multiTable.fieldId]: `${table.multiTable.fieldId}_tbl`,
                    [table.multiTable.fieldIdMult]: `${[table.multiTable.fieldIdMult]}_tbl`,
                    'id_estado_tl': 'id_estado_tl_tbl'
                }
            };
            if (currentState == 'unknow') {
                reject(langText.msg_info_save_base_free_time);
            } else {
                if (idBranch != undefined) {
                    // item on branch
                    peticion = 3;
                    tableDB.campos.id = 'id_tbl';
                    formData.append('id_tbl', element.dataset.idBranch);
                } else {
                    // no hay item on branch
                    peticion = 2;
                }
            }
            formData.append(`${table.multiTable.fieldId}_tbl`, element.dataset.idTblDb);
            formData.append(`${table.multiTable.fieldIdMult}_tbl`, table.values.fieldIdMult);
            formData.append('id_estado_tl_tbl', newState.number);
            console.log(newState.number);
            if (table.multiTable.hasOwnProperty('fieldIdMult2')) {
                tableDB.campos[table.multiTable.fieldIdMult2] = `${[table.multiTable.fieldIdMult2]}_tbl`;
                formData.append(`${table.multiTable.fieldIdMult2}_tbl`, table.values.fieldIdMult2);
            }
        } else {
            // Se configura el direccionamiento a la tabla base
            tableDB = {
                name: table.baseTable.table,
                campos: {
                    [table.baseTable.field]: `${table.baseTable.field}_tbl`,
                    'id_dia_tl': 'id_dia_tl_tbl',
                    'id_hora_tl': 'id_hora_tl_tbl',
                    'id_estado_tl': 'id_estado_tl_tbl',
                    'id_horario': 'id_horario_tbl'
                }
            };
            if (currentState == 'unknow') {
                peticion = 2;
            } else {
                peticion = 3;
                tableDB.campos.id = 'id_tbl';
                formData.append('id_tbl', element.dataset.idTblDb);
            }
            formData.append(`${table.baseTable.field}_tbl`, table.values.id_item);
            formData.append('id_dia_tl_tbl', element.dataset.idDia);
            formData.append('id_hora_tl_tbl', element.dataset.idHora);
            formData.append('id_estado_tl_tbl', newState.number);
            formData.append('id_horario_tbl', table.values.id_horario);
        }
        printFormData(formData);
        saveItemsDB(langText, peticion, formData, false, tableDB).then(response => {
            console.log(response);
            if (response[0] != false) {
                element.classList.remove('Bien', 'Pregunta', 'Mala', 'unknow');
                element.dataset.tdState = newState.name;
                element.classList.add(newState.name);
                switch (peticion) {
                    case 2:
                        if (flagMultiple) {
                            element.dataset.idBranch = response[0];
                        } else {
                            element.dataset.idTblDb = response[0];
                        }
                        break;
                    case 3:
                        break;
                }
                resolve([response, [newState.name, newState.number]]);
            }
        }).catch(e => reject(e));
    });
}

function pathSettingFreeTime(kind, tableSetting) {
    switch (kind) {
        case 'Clases':
            tableSetting.baseTable = {
                table: 'blm_tiempo_libre_grupos',
                field: 'id_grupo'
            };
            if (periodosCheckTiempo.checked) {
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_clases_periodo',
                    tableView: 'blm_vista_tl_clase_periodo',
                    fieldId: 'id_tl_clase',
                    fieldIdMult: 'id_periodo'
                });
            }
            if (semanasCheckTiempo.checked) {
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_clases_semana',
                    tableView: 'blm_vista_tl_clase_semana',
                    fieldId: 'id_tl_clase',
                    fieldIdMult: 'id_semana'
                });
            }
            if (periodosCheckTiempo.checked && semanasCheckTiempo.checked) {
                let dataTable = {
                    table: 'blm_tl_clases_periodo_semana',
                    tableView: 'blm_vista_tl_clase_periodo_semana',
                    fieldId: 'id_tl_clase',
                    fieldIdMult: 'id_periodo',
                    fieldIdMult2: 'id_semana'
                };
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
            }
            break;

        case 'Aulas':
            tableSetting.baseTable = {
                table: 'blm_tiempo_libre_aulas',
                field: 'id_aula'
            };
            if (periodosCheckTiempo.checked) {
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_aulas_periodo',
                    tableView: 'blm_vista_tl_aula_periodo',
                    fieldId: 'id_tl_aula',
                    fieldIdMult: 'id_periodo'
                });
            }
            if (semanasCheckTiempo.checked) {
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_aulas_semana',
                    tableView: 'blm_vista_tl_aula_semana',
                    fieldId: 'id_tl_aula',
                    fieldIdMult: 'id_semana'
                });
            }
            if (periodosCheckTiempo.checked && semanasCheckTiempo.checked) {
                let dataTable = {
                    table: 'blm_tl_aulas_periodo_semana',
                    tableView: 'blm_vista_tl_aula_periodo_semana',
                    fieldId: 'id_tl_aula',
                    fieldIdMult: 'id_periodo',
                    fieldIdMult2: 'id_semana'
                };
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
            }
            break;

        case 'Profesores':
            tableSetting.baseTable = {
                table: 'blm_tiempo_libre_profesores',
                field: 'id_profesor'
            };
            if (periodosCheckTiempo.checked) {
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_profesores_periodo',
                    tableView: 'blm_vista_tl_profesor_periodo',
                    fieldId: 'id_tl_profesor',
                    fieldIdMult: 'id_periodo'
                });
            }
            if (semanasCheckTiempo.checked) {
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_profesores_semana',
                    tableView: 'blm_vista_tl_profesor_semana',
                    fieldId: 'id_tl_profesor',
                    fieldIdMult: 'id_semana'
                });
            }
            if (periodosCheckTiempo.checked && semanasCheckTiempo.checked) {
                let dataTable = {
                    table: 'blm_tl_profesores_periodo_semana',
                    tableView: 'blm_vista_tl_profesor_periodo_semana',
                    fieldId: 'id_tl_profesor',
                    fieldIdMult: 'id_periodo',
                    fieldIdMult2: 'id_semana'
                };
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
            }
            break;

        case 'Cursos':
            tableSetting.baseTable = {
                table: 'blm_tiempo_libre_cursos',
                field: 'id_curso'
            };
            if (periodosCheckTiempo.checked) {
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_cursos_periodo',
                    tableView: 'blm_vista_tl_curso_periodo',
                    fieldId: 'id_tl_curso',
                    fieldIdMult: 'id_periodo'
                });
            }
            if (semanasCheckTiempo.checked) {
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify({
                    table: 'blm_tl_cursos_semana',
                    tableView: 'blm_vista_tl_curso_semana',
                    fieldId: 'id_tl_curso',
                    fieldIdMult: 'id_semana'
                });
            }
            if (periodosCheckTiempo.checked && semanasCheckTiempo.checked) {
                let dataTable = {
                    table: 'blm_tl_cursos_periodo_semana',
                    tableView: 'blm_vista_tl_curso_periodo_semana',
                    fieldId: 'id_tl_curso',
                    fieldIdMult: 'id_periodo',
                    fieldIdMult2: 'id_semana'
                };
                semanaTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                periodoTLTiempo.dataset.baseTable = JSON.stringify(tableSetting.baseTable);
                semanaTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
                periodoTLTiempo.dataset.multiTable = JSON.stringify(dataTable);
            }
            break;
    }
    return tableSetting;
}