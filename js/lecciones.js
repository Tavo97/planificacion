window.addEventListener('load', () => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;

    btnPlaneacionLecciones.addEventListener('click', (e) => {
        deleteAllChilds(tableTitleLecciones, ['tableLecciones#', 'tableLeccionesNLeccion', 'tableLeccionesProfesor', 'tableLeccionesClase', 'tableLeccionesAsignatura', 'tableLeccionesSessionSemana', 'tableLeccionesLeccion', 'tableLeccionesAulaAsignatura']);
        deleteAllChilds(tableBodyLecciones);
        let dataHorario = titleModulePlaneacion.dataset;
        // Check item selected on table
        selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        // Check item selected on left menu
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        console.log(isset(selectMenu));
        if (isset(dataHorario.id)) {
            if (selectMenu != 'none') {
                if (selectMenu.dataset.title == 'Profesores' || selectMenu.dataset.title == 'Clases' || selectMenu.dataset.title == 'Cursos' || selectMenu.dataset.title == 'Aulas') {
                    if (selectItem != 'none') {
                        msGObj.loader.style.display = 'block';
                        if (dataHorario.crear_horarios == 1) {
                            let newChildrenEqual = {
                                'childrenConf': [true, 'equal'],
                                'child': {
                                    'prefix': 'tableLecciones',
                                    'father': tableTitleLecciones,
                                    'kind': 'th',
                                    'elements': [langText.periodo, langText.semana],
                                    'classes': ['nowrap', 'test'],
                                    'attributes': { 'scope': 'col' }
                                }
                            };
                            buildNewElements(newChildrenEqual);
                        }
                        let table;
                        switch (selectMenu.dataset.title) {
                            case "Profesores":
                                table = {
                                    name: 'blm_vista_leccion_profesor',
                                    fields: ['*'],
                                    conditions: { 'id_profesor': ['=', selectItem.dataset.id], 'id_horario': ['=', dataHorario.id] }
                                };
                                searchItemsDB(table).then((lecciones) => {
                                    let newConditions = new Object();
                                    let newValues = new Array();
                                    for (const leccion in lecciones) {
                                        newValues.push(lecciones[leccion].id_leccion);
                                    }
                                    newConditions.id = ['IN', `${newValues.join()}`];
                                    return newConditions;
                                }).then(newConditions => {
                                    table = {
                                        name: 'blm_vista_leccion',
                                        fields: ['*'],
                                        conditions: newConditions
                                    };
                                    return searchItemsDB(table).then((res) => {
                                        if (Object.keys(res).length > 0) {
                                            return orderDBresponse(res, 'id', 'number');
                                        }
                                    });
                                }).then(sortData => {
                                    fillLessonTable(sortData, langText);
                                }).catch(error => {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = error;
                                });
                                break;

                            case "Clases":
                                table = {
                                    name: 'blm_vista_leccion_grupo',
                                    fields: ['*'],
                                    conditions: { 'id_grupo': ['=', selectItem.dataset.id], 'id_horario': ['=', dataHorario.id] }
                                };
                                searchItemsDB(table).then((lecciones) => {
                                    let newConditions = new Object();
                                    let newValues = new Array();
                                    for (const leccion in lecciones) {
                                        newValues.push(lecciones[leccion].id_leccion);
                                    }
                                    newConditions.id = ['IN', `${newValues.join()}`];
                                    console.log(newConditions);
                                    table = {
                                        name: 'blm_vista_leccion',
                                        fields: ['*'],
                                        conditions: newConditions
                                    };
                                    return searchItemsDB(table).then((res) => {
                                        if (Object.keys(res).length > 0) {
                                            return orderDBresponse(res, 'id', 'number');
                                        }
                                    });
                                }).then(sortData => {
                                    fillLessonTable(sortData, langText);
                                }).catch(error => {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = error;
                                });
                                break;

                            case "Cursos":
                                table = {
                                    name: 'blm_vista_leccion',
                                    fields: ['*'],
                                    conditions: { 'id_curso': ['=', selectItem.dataset.id], 'id_horario': ['=', dataHorario.id] }
                                };
                                searchItemsDB(table).then((res) => {
                                    if (Object.keys(res).length > 0) {
                                        return orderDBresponse(res, 'id', 'number');
                                    }
                                }).then(sortData => {
                                    fillLessonTable(sortData, langText);
                                }).catch(error => {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = error;
                                });
                                break;

                            case "Aulas":
                                table = {
                                    name: 'blm_aula_curso',
                                    fields: ['*'],
                                    conditions: { 'id_aula': ['=', selectItem.dataset.id] }
                                };
                                searchItemsDB(table).then((cursosAulas) => {
                                    let cursosIds = new Array();
                                    let newConditions = new Object();
                                    for (const item of Object.keys(cursosAulas)) {
                                        cursosIds.push(cursosAulas[item].id_curso);
                                    }
                                    newConditions = { 'id_curso': ['IN', `${cursosIds.unique().join()}`], 'id_horario': ['=', dataHorario.id] };
                                    table = {
                                        name: 'blm_vista_leccion',
                                        fields: ['*'],
                                        conditions: newConditions
                                    };
                                    return searchItemsDB(table).then((res) => {
                                        if (Object.keys(res).length > 0) {
                                            return orderDBresponse(res, 'id', 'number');
                                        }
                                    });
                                }).then(sortData => {
                                    fillLessonTable(sortData, langText);
                                }).catch(error => {
                                    msGObj.labelLoader.style.color = 'tomato';
                                    msGObj.labelLoader.innerHTML = error;
                                });
                                break;
                            default:
                                break;
                        }
                        closeWorkLand(msGObj);
                        $("#ModalLecciones").modal({ backdrop: 'static', keyboard: false });
                    } else {
                        data = {
                            'dataSets': {
                                action: 'Info',
                                modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                            },
                            'msg': langText.msg_info_select_item_table,
                            'ico': 'info',
                            'closeDataSet': {
                                visible: 0
                            }
                        };
                        changeMsgModal(data);
                        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                    }
                } else {
                    data = {
                        'dataSets': {
                            action: 'Info',
                            modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                        },
                        'msg': langText.msg_info_no_disponible,
                        'ico': 'info',
                        'closeDataSet': {
                            visible: 0
                        }
                    };
                    changeMsgModal(data);
                    $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
                }
            } else {
                data = {
                    'dataSets': {
                        action: 'Info',
                        modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                    },
                    'msg': langText.msg_info_no_disponible,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 0
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                },
                'msg': langText.msg_info_select_horario,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalLeccionesNueva.addEventListener('click', (e) => {
        let validations = {
            'asignaturaLeccion': {
                'type': 'number',
                'required': true
            }
        };
        assignValidations(validations);

        let selectItems = [{
            select: {
                item: asignaturaLeccion,
                prefix: `${asignaturaLeccion.id}Op`
            },
            table: {
                name: 'blm_cursos',
                fields: ['id', 'nombre'],
                exeption: ['nombre']
            }
        },
        {
            select: {
                item: profesorLeccion,
                prefix: `${profesorLeccion.id}Op`
            },
            table: {
                name: 'blm_profesores',
                fields: ['id', 'nombre', 'apellido_paterno', 'apellido_materno'],
                exeption: ['nombre', 'apellido_paterno', 'apellido_materno']
            }
        },
        {
            select: {
                item: grupoLeccion,
                prefix: `${grupoLeccion.id}Op`
            },
            table: {
                name: 'blm_grupos',
                fields: ['id', 'nombre'],
                exeption: ['nombre']
            }
        }
        ];
        selectItems.forEach(element => {
            fillSelectItem(element.select, element.table, { firstItem: true }, element.type);
        });

        let horario = titleModulePlaneacion.dataset;
        let hide = [periodoLeccion.parentNode, semanaLeccion.parentNode];
        if (horario.crear_horarios == 1) {
            let validations = {
                'periodoLeccion': {
                    'type': 'number',
                    'required': true
                },
                'semanaLeccion': {
                    'type': 'number',
                    'required': true
                }
            };
            assignValidations(validations);
            hideElement(hide, 1);
            let selectItems = [{
                select: {
                    item: periodoLeccion,
                    prefix: `${periodoLeccion.id}Op`
                },
                table: {
                    name: 'blm_periodos',
                    fields: ['id', 'nombre'],
                    exeption: ['nombre'],
                    conditions: { 'id_horario': ['=', horario.id] },
                }
            },
            {
                select: {
                    item: semanaLeccion,
                    prefix: `${semanaLeccion.id}Op`
                },
                table: {
                    name: 'blm_semanas',
                    fields: ['id', 'nombre'],
                    exeption: ['nombre'],
                    conditions: { 'id_horario': ['=', horario.id] },
                }
            }
            ];
            selectItems.forEach(element => {
                fillSelectItem(element.select, element.table, { firstItem: false, activeLengthRes: 3 }, element.type);
            });
        } else {
            hideElement(hide, 0);
        }
        tablaFrmLeccion.value = 'blm_leccion';
        peticionFrmLeccion.value = 2;
        idItemLeccion.value = titleModulePlaneacion.dataset.id;
        camposFrmLeccion.value = JSON.stringify({
            'sesiones_semana': 'SesionSemanaLeccion',
            'leccion': 'seleccionarLeccionLeccion',
            'id_curso': 'asignaturaLeccion',
            'id_horario': 'idItemLeccion'
        });
        deleteAllChilds(aulasDisplayLeccion);
        deleteAllChilds(profesoresContainerLeccion);
        if (profesorLeccion.dataset.profesores != undefined) {
            profesorLeccion.removeAttribute('data-profesores');
        }
        deleteAllChilds(grupoContainerLeccion);
        if (grupoLeccion.dataset.grupos != undefined) {
            grupoLeccion.removeAttribute('data-grupos');
        }
        $('#ModalLeccion').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalLeccionAgregar.addEventListener('click', (e) => {
        recreateFrmValidations();
        data = document.querySelectorAll('[data-validate]');
        let validations = validation(data, langText);
        let Approve = validationExe(validations);
        let groups = findElements('li', grupoContainerLeccion).items.length;
        let teachers = findElements('li', profesoresContainerLeccion).items.length;
        if (groups > 0 && teachers > 0 && Approve) {
            msGObj.loader.style.display = 'block';
            saveItemsDB(langText, 2, frmLeccion).then(res => {
                console.log(res);
                if (res[0] != false) {
                    msGObj.labelLoader.style.color = '#28a745';
                    let op = parseInt(peticionFrmLeccion.value);
                    switch (op) {
                        case 2:
                            fillTeacherGroup(res[0], profesorLeccion.dataset.profesores, grupoLeccion.dataset.grupos);
                            msGObj.labelLoader.innerHTML = langText.msg_added;
                            let horario = titleModulePlaneacion.dataset;
                            if (horario.crear_horarios == 1) {
                                tablaFrmLeccion.value = 'blm_leccion_periodo_semana';
                                idFrmLeccion.value = res[0];
                                camposFrmLeccion.value = JSON.stringify({
                                    'id_periodo': 'periodoLeccion',
                                    'id_semana': 'semanaLeccion',
                                    'id_leccion': 'idFrmLeccion'
                                });
                                saveItemsDB(langText, 2, frmLeccion).then(res => {
                                    if (res[0] != false) {
                                        console.log(res);
                                    }
                                }).catch(e => { console.log(e) });
                            }
                            break;

                        case 3:
                            fillTeacherGroup(idFrmLeccion.value, profesorLeccion.dataset.profesores, grupoLeccion.dataset.grupos);
                            msGObj.labelLoader.innerHTML = langText.msg_saved;
                            break;
                    }
                    closeModal(JSON.stringify({ status: 0, close: ['ModalLeccion', 'ModalLecciones'] }));
                }
                closeWorkLand(msGObj);
            }).catch(e => { console.log(e) });
        } else {
            if (groups > 0) {
                grupoLeccion.classList.remove('alert-validate-input');
            } else {
                grupoLeccion.classList.add('alert-validate-input');
            }
            if (teachers > 0) {
                profesorLeccion.classList.remove('alert-validate-input');
            } else {
                profesorLeccion.classList.add('alert-validate-input');
            }
        }
    });

    semanaLeccion.addEventListener('change', e => {
        if (peticionFrmLeccion.value == 3) {
            let formData = new FormData();
            let idFrm = e.target.dataset.id_lps;
            formData.append('semanaLeccion', semanaLeccion.value);
            formData.append('idFrm', idFrm);
            saveItemsDB(langText, 3, formData, false, { name: 'blm_leccion_periodo_semana', campos: { 'id': 'idFrm', 'id_semana': 'semanaLeccion' } }).then(res => {
                if (res[0]) {
                    msGObj.loader.style.display = 'block';
                    msGObj.labelLoader.style.color = '#28a745';
                    msGObj.labelLoader.innerHTML = langText.msg_saved;
                } else {
                    msGObj.labelLoader.innerHTML = langText.msg_no_save;
                    msGObj.labelLoader.style.color = 'tomato';
                }
            }).catch(e => {
                msGObj.labelLoader.innerHTML = e;
                msGObj.labelLoader.style.color = 'tomato';
            });
            closeWorkLand(msGObj);
        }
    });

    periodoLeccion.addEventListener('change', e => {
        if (peticionFrmLeccion.value == 3) {
            let formData = new FormData();
            let idFrm = e.target.dataset.id_lps;
            formData.append('periodoLeccion', periodoLeccion.value);
            formData.append('idFrm', idFrm);
            saveItemsDB(langText, 3, formData, false, { name: 'blm_leccion_periodo_semana', campos: { 'id': 'idFrm', 'id_periodo': 'periodoLeccion' } }).then(res => {
                if (res[0]) {
                    msGObj.loader.style.display = 'block';
                    msGObj.labelLoader.style.color = '#28a745';
                    msGObj.labelLoader.innerHTML = langText.msg_saved;
                } else {
                    msGObj.labelLoader.innerHTML = langText.msg_no_save;
                    msGObj.labelLoader.style.color = 'tomato';
                }
            }).catch(e => {
                msGObj.labelLoader.innerHTML = e;
                msGObj.labelLoader.style.color = 'tomato';
            });
            closeWorkLand(msGObj);
        }
    });

    masProfesorLeccion.addEventListener('click', (e) => {
        let id = profesorLeccion.value;
        let element = document.getElementById(`profesorLeccionOp${id}`);
        let data = element.dataset;
        let idProfesores = new Array();
        if (profesorLeccion.dataset.profesores != undefined) {
            idProfesores = JSON.parse(profesorLeccion.dataset.profesores);
            idProfesores.push(id);
        } else {
            idProfesores.push(id);
        }
        profesorLeccion.dataset.profesores = JSON.stringify(idProfesores);
        let newChild = {
            'childrenConf': [false],
            'child': {
                'prefix': 'profesorAdded',
                'name': id,
                'father': profesoresContainerLeccion,
                'kind': 'li',
                'innerHtml': `${data.nombre} ${data.apellido_paterno} ${data.apellido_materno}`,
                'data': { 'id': id },
                'classes': ['list-group-item', 'li-custome'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChild);
        deleteItem(element);
    });

    claseUnidaLeccion.addEventListener('click', (e) => {
        let id = grupoLeccion.value;
        let element = document.getElementById(`grupoLeccionOp${id}`);
        let data = element.dataset;
        let idGrupos = new Array();
        if (grupoLeccion.dataset.grupos != undefined) {
            idGrupos = JSON.parse(grupoLeccion.dataset.grupos);
            idGrupos.push(id);
        } else {
            idGrupos.push(id);
        }
        grupoLeccion.dataset.grupos = JSON.stringify(idGrupos);
        let newChild = {
            'childrenConf': [false],
            'child': {
                'prefix': 'groupAdded',
                'name': id,
                'father': grupoContainerLeccion,
                'kind': 'li',
                'innerHtml': `${data.nombre}`,
                'data': { 'id': id },
                'classes': ['list-group-item', 'li-custome'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChild);
        deleteItem(element);
    });

    labelBtnModalLeccionClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalLeccion'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalLeccionesEditar.addEventListener('click', (e) => {
        let selectItem = getChooseItem('tr', tableBodyLecciones, 'choosed')[1];
        let selectItemNum = getChooseItem('tr', tableBodyLecciones, 'choosed')[0];
        if (selectItemNum > 0 && selectItemNum == 1) {
            let horario = titleModulePlaneacion.dataset;
            let hide = [periodoLeccion.parentNode, semanaLeccion.parentNode];
            if (horario.crear_horarios == 1) {
                let validations = {
                    'periodoLeccion': {
                        'type': 'number',
                        'required': true
                    },
                    'semanaLeccion': {
                        'type': 'number',
                        'required': true
                    }
                };
                assignValidations(validations);
                hideElement(hide, 1);
                table = {
                    name: 'blm_leccion_periodo_semana',
                    fields: ['*'],
                    conditions: { 'id_leccion': ['=', selectItem.dataset.id] }
                };
                msGObj.loader.style.display = 'block';
                searchItemsDB(table).then((lps) => {
                    console.log(lps);
                    if (Object.keys(lps).length > 0) {
                        for (const item in lps) {
                            periodoLeccion.dataset.id_lps = lps[item].id;
                            semanaLeccion.dataset.id_lps = lps[item].id;
                            let selectItems = [{
                                select: {
                                    item: periodoLeccion,
                                    prefix: `${periodoLeccion.id}Op`,
                                    currentOption: lps[item].id_periodo
                                },
                                table: {
                                    name: 'blm_periodos',
                                    fields: ['id', 'nombre'],
                                    exeption: ['nombre'],
                                    conditions: { 'id_horario': ['=', horario.id] }
                                }
                            },
                            {
                                select: {
                                    item: semanaLeccion,
                                    prefix: `${semanaLeccion.id}Op`,
                                    currentOption: lps[item].id_semana
                                },
                                table: {
                                    name: 'blm_semanas',
                                    fields: ['id', 'nombre'],
                                    exeption: ['nombre'],
                                    conditions: { 'id_horario': ['=', horario.id] }
                                }
                            }
                            ];
                            selectItems.forEach(element => {
                                fillSelectItem(element.select, element.table, { firstItem: false }, element.type);
                            });
                        }
                    }
                }).catch(e => console.log(e));
            } else {
                hideElement(hide, 0);
            }
            labelBtnModalLeccionAgregar.innerHTML = langText.btn_guardar_frm;
            let validations = {
                'asignaturaLeccion': {
                    'type': 'number',
                    'required': true
                }
            };
            assignValidations(validations);
            deleteAllChilds(profesorLeccion);
            deleteAllChilds(asignaturaLeccion);
            deleteAllChilds(grupoLeccion);
            deleteAllChilds(profesoresContainerLeccion);
            if (profesorLeccion.dataset.profesores != undefined) {
                profesorLeccion.removeAttribute('data-profesores');
            }
            deleteAllChilds(grupoContainerLeccion);
            if (grupoLeccion.dataset.grupos != undefined) {
                grupoLeccion.removeAttribute('data-grupos');
            }
            let chooseOption = {
                'childrenConf': [false],
                'child': {
                    'prefix': 'profesorLeccionOp',
                    'name': 0,
                    'father': profesorLeccion,
                    'kind': 'option',
                    'innerHtml': langText.choose,
                    'attributes': { 'value': '' }
                }
            };
            buildNewElements(chooseOption);
            chooseOption.child.prefix = 'asignaturaLeccionOp';
            chooseOption.child.father = asignaturaLeccion;
            buildNewElements(chooseOption);
            chooseOption.child.prefix = 'grupoLeccionOp';
            chooseOption.child.father = grupoLeccion;
            buildNewElements(chooseOption);

            if (selectItem != 'none') {
                let dataItem = selectItem.dataset;

                let formData = new FormData();
                formData.append('peticion', 1);
                formData.append('select', JSON.stringify(['id', 'nombre', 'apellido_paterno', 'apellido_materno']));
                formData.append('tabla', 'blm_profesores');
                let data = new Object();
                data.target = '../blocks/planificacion/peticionesSql.php';
                data.method = 'POST';
                data.send = true;
                data.form = [false, formData];
                ajaxData(data).then((res) => {
                    if (Object.keys(res).length > 0) {
                        let teachers = JSON.parse(dataItem.idprofesores);
                        let idTeachers = new Array();
                        teachers.forEach(teacher => {
                            let newChild = {
                                'childrenConf': [false],
                                'child': {
                                    'prefix': 'profesorAdded',
                                    'name': teacher.id,
                                    'father': profesoresContainerLeccion,
                                    'kind': 'li',
                                    'innerHtml': `${teacher.nombre} ${teacher.apellido_paterno} ${teacher.apellido_materno}`,
                                    'data': { 'id': teacher.id, saved: true },
                                    'classes': ['list-group-item', 'li-custome'],
                                    'attributes': { 'scope': 'col' }
                                }
                            };
                            buildNewElements(newChild);
                            idTeachers.push(teacher.id);
                        });
                        for (const item in res) {
                            if (!idTeachers.includes(res[item].id)) {
                                let newOption = {
                                    'childrenConf': [false],
                                    'child': {
                                        'prefix': 'profesorLeccionOp',
                                        'name': res[item].id,
                                        'father': profesorLeccion,
                                        'kind': 'option',
                                        'innerHtml': `${res[item].nombre} ${res[item].apellido_paterno} ${res[item].apellido_materno}`,
                                        'data': {
                                            'nombre': res[item].nombre,
                                            'apellido1': res[item].apellido_paterno,
                                            'apellido2': res[item].apellido_materno
                                        },
                                        'attributes': { 'value': res[item].id }
                                    }
                                };
                                buildNewElements(newOption);
                            }
                        }
                    } else {
                        msGObj.labelLoader.style.color = 'tomato';
                        msGObj.labelLoader.innerHTML = langText.msg_no_data_source
                    }
                    closeWorkLand(msGObj);
                });

                formData.append('select', JSON.stringify(['id', 'nombre']));
                formData.append('tabla', 'blm_cursos');
                ajaxData(data).then((res) => {
                    if (Object.keys(res).length > 0) {
                        for (const item in res) {
                            let newOption = {
                                'childrenConf': [false],
                                'child': {
                                    'prefix': 'asignaturaLeccionOp',
                                    'name': res[item].id,
                                    'father': asignaturaLeccion,
                                    'kind': 'option',
                                    'innerHtml': `${res[item].nombre}`,
                                    'data': {
                                        'nombre': res[item].nombre
                                    },
                                    'attributes': { 'value': res[item].id }
                                }
                            };
                            buildNewElements(newOption);
                        }
                        asignaturaLeccion.value = dataItem.id_curso;
                    } else {
                        msGObj.labelLoader.style.color = 'tomato';
                        msGObj.labelLoader.innerHTML = langText.msg_no_data_source
                    }
                    closeWorkLand(msGObj);
                });

                formData.append('select', JSON.stringify(['id', 'nombre']));
                formData.append('tabla', 'blm_grupos');
                ajaxData(data).then((res) => {
                    if (Object.keys(res).length > 0) {
                        let groups = JSON.parse(dataItem.idgrupos);
                        let idGroups = new Array();
                        groups.forEach(group => {
                            let newChild = {
                                'childrenConf': [false],
                                'child': {
                                    'prefix': 'groupAdded',
                                    'name': group.id,
                                    'father': grupoContainerLeccion,
                                    'kind': 'li',
                                    'innerHtml': `${group.nombre}`,
                                    'data': { 'id': group.id, saved: true },
                                    'classes': ['list-group-item', 'li-custome'],
                                    'attributes': { 'scope': 'col' }
                                }
                            };
                            buildNewElements(newChild);
                            idGroups.push(group.id);
                        });
                        for (const item in res) {
                            if (!idGroups.includes(res[item].id)) {
                                let newOption = {
                                    'childrenConf': [false],
                                    'child': {
                                        'prefix': 'grupoLeccionOp',
                                        'name': res[item].id,
                                        'father': grupoLeccion,
                                        'kind': 'option',
                                        'innerHtml': `${res[item].nombre}`,
                                        'data': {
                                            'nombre': res[item].nombre
                                        },
                                        'attributes': { 'value': res[item].id }
                                    }
                                };
                                buildNewElements(newOption);
                            }
                        }
                    } else {
                        msGObj.labelLoader.style.color = 'tomato';
                        msGObj.labelLoader.innerHTML = langText.msg_no_data_source
                    }
                    closeWorkLand(msGObj);
                });

                SesionSemanaLeccion.value = dataItem.sesiones_semana;
                seleccionarLeccionLeccion.value = dataItem.leccion;
                tablaFrmLeccion.value = 'blm_leccion';
                peticionFrmLeccion.value = 3;
                idFrmLeccion.value = dataItem.id;
                camposFrmLeccion.value = JSON.stringify({
                    'id': 'idFrmLeccion',
                    'sesiones_semana': 'SesionSemanaLeccion',
                    'leccion': 'seleccionarLeccionLeccion',
                    'id_curso': 'asignaturaLeccion'
                });
                $('#ModalLeccion').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_only_one,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalLeccionesEliminar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyLecciones, 'choosed')[1];
        selectItemNum = getChooseItem('tr', tableBodyLecciones, 'choosed')[0];
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        if (selectItemNum > 0 && selectItemNum == 1) {
            if (selectItem != 'none') {
                let msg = msgDeleteItem(langText.msg_alert_delete, selectItem.dataset.nombre, selectMenu.title);
                data = {
                    'dataSets': {
                        'action': 'eliminar',
                        'data': JSON.stringify(selectItem.dataset),
                        'table': 'blm_leccion',
                        'item': selectItem.id,
                        'modal': JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    },
                    'msg': msg,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 1,
                        modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_only_one,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalLeccionesCopiar.addEventListener('click', (e) => {
        let selectItemNum = getChooseItem('tr', tableBodyLecciones, 'choosed')[0];
        if (selectItemNum > 0) {
            deleteAllChilds(itemsContainerCopy);
            chooseElementCopy.removeAttribute('data-items');
            chooseElementCopy.setAttribute('disabled', 'true');
            deleteAllChilds(chooseElementCopy);
            let newChild = {
                'childrenConf': [false],
                'child': {
                    'name': 'chooseFirst',
                    'father': chooseElementCopy,
                    'kind': 'option',
                    'innerHtml': langText.choose,
                    'attributes': { 'selected': 'true' }
                }
            };
            buildNewElements(newChild);
            let childrenCopy = findElements('I', menuCopyCopy);
            if (childrenCopy.state) {
                childrenCopy.items.forEach(element => {
                    element.classList.remove('choosed-font');
                });
            }
            $('#ModalCopy').modal({ backdrop: 'static', keyboard: false });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_more_one,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    menuCopyClases.addEventListener('click', (e) => {
        let items = findElements('li', itemsContainerCopy).items.length;
        if (items <= 0) {
            chooseElementCopy.removeAttribute('disabled');
            selecItemMenu(e.target);
            let select = {
                item: chooseElementCopy,
                prefix: 'chooseElementCopy'
            };
            let table = {
                name: 'blm_grupos',
                fields: ['id', 'nombre'],
                exeption: ['nombre']
            };
            fillSelectItem(select, table, { firstItem: true });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_selected_active,
                'ico': 'info',
                'closeDataStatus': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    menuCopyProfesores.addEventListener('click', (e) => {
        let items = findElements('li', itemsContainerCopy).items.length;
        if (items <= 0) {
            chooseElementCopy.removeAttribute('disabled');
            selecItemMenu(e.target);
            let select = {
                item: chooseElementCopy,
                prefix: 'chooseElementCopy'
            };
            let table = {
                name: 'blm_profesores',
                fields: ['id', 'nombre', 'apellido_paterno', 'apellido_materno'],
                exeption: ['nombre', 'apellido_paterno', 'apellido_materno']
            };
            fillSelectItem(select, table, { firstItem: true });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_selected_active,
                'ico': 'info',
                'closeDataSet': {
                    visible: 0
                }
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    masItemCopy.addEventListener('click', (e) => {
        let id = chooseElementCopy.value;
        if (id != 'Choose an option') {
            let element = document.getElementById(`chooseElementCopy${id}`);
            let data = element.dataset;
            let idItems = new Array();
            if (chooseElementCopy.dataset.items != undefined) {
                idItems = JSON.parse(chooseElementCopy.dataset.items);
                idItems.push(id);
            } else {
                idItems.push(id);
            }
            chooseElementCopy.dataset.items = JSON.stringify(idItems);
            let newChild = {
                'childrenConf': [false],
                'child': {
                    'prefix': 'itemAdded',
                    'name': id,
                    'father': itemsContainerCopy,
                    'kind': 'li',
                    'innerHtml': `${data.nombre}`,
                    'data': { 'id': id },
                    'classes': ['list-group-item', 'li-custome'],
                    'attributes': { 'scope': 'col' }
                }
            };
            buildNewElements(newChild);
            deleteItem(element);
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_more_one,
                'ico': 'info',
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalCopyAgregar.addEventListener('click', (e) => {
        let items = findElements('li', itemsContainerCopy).items.length; // items that are choosed
        console.log(document.querySelector('#menuCopyCopy>.choosed-font'));
        let kind = document.querySelector('#menuCopyCopy>.choosed-font') != null ? document.querySelector('#menuCopyCopy>.choosed-font').dataset.title : false;
        if (items > 0 && kind != false) {
            msGObj.loader.style.display = 'block';
            let elementosForDB = new Array();
            let table = 'blm_leccion';
            let peticion = 2;
            let campos = {
                'id_curso': 'id_curso_frm',
                'leccion': 'leccion_frm',
                'sesiones_semana': 'sesiones_semana_frm',
                'id_horario': 'id_horario_frm'
            };
            let itemsValues = document.querySelectorAll('#tableBodyLecciones>.choosed');
            let cursos = [];
            itemsValues.forEach(element => {
                if (cursos.includes(element.dataset.id_curso)) {
                    let itemsId, mod;
                    switch (kind) {
                        case 'Clases':
                            itemsId = getIdItems(element.dataset.idprofesores);
                            mod = 1;
                            break;
                        case 'Profesores':
                            itemsId = getIdItems(element.dataset.idgrupos);
                            mod = 2;
                            break;
                    }
                    elementosForDB.forEach(dbi => {
                        if (dbi[3] == element.dataset.id_curso) {
                            dbi[4] = parseInt(dbi[4]) > parseInt(element.dataset.leccion) ? dbi[4] : element.dataset.leccion;
                            dbi[5] = parseInt(dbi[5]) > parseInt(element.dataset.sesiones_semana) ? dbi[5] : element.dataset.sesiones_semana;
                            switch (mod) {
                                case 1: //clases and mod profesores
                                    newId = JSON.parse(itemsId);
                                    oldId = JSON.parse(dbi[1]);
                                    fusion = oldId.concat(newId).unique().sort();
                                    dbi[1] = JSON.stringify(fusion);
                                    break;

                                case 2: //profesores and mod clases
                                    newId = JSON.parse(itemsId);
                                    oldId = JSON.parse(dbi[2]);
                                    fusion = oldId.concat(newId).unique().sort();
                                    dbi[2] = JSON.stringify(fusion);
                                    break;
                            }
                        }
                    });
                } else {
                    cursos.push(element.dataset.id_curso);
                    let formData = new FormData();
                    formData.append('tabla', table);
                    formData.append('peticion', peticion);
                    formData.append('campos', JSON.stringify(campos));
                    console.log(campos);
                    let groups, teachers;
                    switch (kind) {
                        case 'Clases':
                            teachers = getIdItems(element.dataset.idprofesores);
                            groups = chooseElementCopy.dataset.items;
                            break;
                        case 'Profesores':
                            teachers = chooseElementCopy.dataset.items;
                            groups = getIdItems(element.dataset.idgrupos);
                            break;
                    }
                    elementosForDB.push([formData, teachers, groups, element.dataset.id_curso, element.dataset.leccion, element.dataset.sesiones_semana]);
                }
            });
            console.log(elementosForDB);
            elementosForDB.forEach(element => {
                element[0].append('id_curso_frm', element[3]);
                element[0].append('leccion_frm', element[4]);
                element[0].append('sesiones_semana_frm', element[5]);
                element[0].append('id_horario_frm', titleModulePlaneacion.dataset.id);
                let data = new Object();
                data.target = '../blocks/planificacion/peticionesSql.php';
                data.method = 'POST';
                data.send = true;
                data.form = [false, element[0]];
                ajaxData(data).then((res) => {
                    msGObj.labelLoader.style.color = '#28a745';
                    if (res[0] != false) {
                        let op = parseInt(peticion);
                        switch (op) {
                            case 2:
                                fillTeacherGroup(res[0], element[1], element[2]);
                                msGObj.labelLoader.innerHTML = langText.msg_added;
                                break;

                            case 3:
                                fillTeacherGroup(idFrmLeccion.value, element[1], element[2]);
                                msGObj.labelLoader.innerHTML = langText.msg_saved;
                                break;
                        }
                        closeModal(JSON.stringify({ status: 0, close: ['ModalCopy', 'ModalLecciones'] }));
                        closeWorkLand(msGObj);
                    }
                });
            });
        } else {
            data = {
                'dataSets': {
                    action: 'Info',
                    modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
                },
                'msg': langText.msg_alert_more_one,
                'ico': 'info',
            };
            changeMsgModal(data);
            $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalCopyClose.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 1, close: ['ModalMsg', 'ModalCopy'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    labelBtnModalLeccionesCerrar.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 0, close: ['ModalMsg', 'ModalLecciones'] })
            },
            'msg': langText.msg_alert_Close,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });

    asignaturaLeccion.addEventListener('change', (e) => {
        let aulas = new Array();
        let formDataAulas = new FormData();
        formDataAulas.append('peticion', 1);
        formDataAulas.append('select', JSON.stringify(['*']));
        formDataAulas.append('tabla', 'blm_vista_aula_curso');
        formDataAulas.append('conditions', JSON.stringify({ 'id_curso': ['=', e.target.value] })); //f
        let dataAula = new Object();
        dataAula.target = '../blocks/planificacion/peticionesSql.php';
        dataAula.method = 'POST';
        dataAula.send = true;
        dataAula.form = [false, formDataAulas];
        ajaxData(dataAula).then((response) => {
            if (Object.keys(response).length > 0) {
                for (const itemA in response) {
                    aulas.push(response[itemA].aula);
                }
                aulas = aulas.join(', ');
                console.log(aulas);
            } else {
                aulas = langText.msg_no_data_source;
            }
            aulasDisplayLeccion.innerHTML = `<p>${aulas}</p>`;
        });
    });

});