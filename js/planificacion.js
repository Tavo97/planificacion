window.addEventListener('load', (w) => {
    // loadScript('https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js', () => { loadScript('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js') });
    const loader = document.querySelector('.containerLoader');
    const labelLoader = document.querySelector('.labelLoader');
    const modalTitle = document.getElementById('Modal-title-Planeacion');
    const language = document.getElementsByTagName('html')[0].lang;
    let langText = languages[language];
    let msGObj = { 'loader': loader, 'labelLoader': labelLoader, 'modalTitle': modalTitle };
    window.globalVariable = { 'langText': langText, 'msGObj': msGObj };
    mainMenuGrados.addEventListener('click', (e) => {
        let validations = {
            'nombreFrmPlaneacion': {
                'type': 'textnumber',
                'long': 50,
                'required': true
            },
            'abreviaturaFrmPlaneacion': {
                'type': 'text',
                'long': 25,
                'required': true
            },
            'colorFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            },
        };
        assignValidations(validations);
        tablaPlaneacion.innerHTML = e.target.title;
        deleteAllChilds(tableBodyPlaneacion);
        deleteAllChilds(tableTitlePlaneacion, ['tablePlaneacion#', 'tablePlaneacionTitulo', 'tablePlaneacionAbreviatura', 'tablePlaneacionColor']);
        selecItemMenu(e.target);

        table = {
            name: 'blm_grados',
            fields: ['id', 'nombre', 'abreviatura', 'color']
        };
        loader.style.display = 'block';
        searchItemsDB(table).then((res) => {
            deleteAllChilds(tableBodyPlaneacion);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = { 'id': res[item].id, 'nombre': res[item].nombre, 'abreviatura': res[item].abreviatura, 'color': res[item].color };
                    items = [res[item].id, res[item].nombre, res[item].abreviatura, res[item].color];
                    tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyPlaneacion);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
                searchColor(tableBodyPlaneacion);
            }
            closeWorkLand(msGObj);
        }).catch(e => {
            labelLoader.style.color = 'tomato';
            labelLoader.innerHTML = e;
            closeWorkLand(msGObj);
        });
    });

    mainMenuProfesores.addEventListener('click', (e) => {
        let validations = {
            'nombreFrmPlaneacion': {
                'type': 'text2',
                'long': 50,
                'required': true
            },
            'apellidoPatFrmPlaneacion': {
                'type': 'text',
                'long': 50,
                'required': true
            },
            'apellidoMatFrmPlaneacion': {
                'type': 'text',
                'long': 50,
                'required': true
            },
            'abreviaturaFrmPlaneacion': {
                'type': 'text',
                'long': 10,
                'required': true
            },
            'emailFrmPlaneacion': {
                'type': 'email',
                'long': 50,
                'required': true
            },
            'telefonoFrmPlaneacion': {
                'type': 'number',
                'long': 10,
                'required': true
            },
            'colorFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            },
        };
        assignValidations(validations);
        tablaPlaneacion.innerHTML = e.target.title;
        deleteAllChilds(tableBodyPlaneacion);
        deleteAllChilds(tableTitlePlaneacion, ['tablePlaneacion#', 'tablePlaneacionTitulo', 'tablePlaneacionAbreviatura', 'tablePlaneacionColor']);
        let newChildrenEqualBefore = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': 'tablePlaneacion',
                'father': tableTitlePlaneacion,
                'kind': 'th',
                'elements': [langText.apellido_pat, langText.apellido_mat],
                'classes': ['nowrap', 'test'],
                'attributes': { 'scope': 'col' },
                'before': tablePlaneacionAbreviatura
            }
        };
        buildNewElements(newChildrenEqualBefore);
        let newChildrenEqual = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': 'tablePlaneacion',
                'father': tableTitlePlaneacion,
                'kind': 'th',
                'elements': [langText.email, langText.telefono],
                'classes': ['nowrap', 'test'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChildrenEqual);
        selecItemMenu(e.target);

        table = {
            name: 'blm_profesores',
            fields: ['id', 'nombre', 'apellido_paterno', 'apellido_materno', 'abreviatura', 'color', 'email', 'telefono', 'nomina']
        };
        loader.style.display = 'block';
        searchItemsDB(table).then((res) => {
            deleteAllChilds(tableBodyPlaneacion);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = { 'id': res[item].id, 'nombre': res[item].nombre, 'apellido_paterno': res[item].apellido_paterno, 'apellido_materno': res[item].apellido_materno, 'color': res[item].color, 'abreviatura': res[item].abreviatura, 'email': res[item].email, 'telefono': res[item].telefono, 'nomina': res[item].nomina };
                    items = [res[item].id, res[item].nombre, res[item].apellido_paterno, res[item].apellido_materno, res[item].abreviatura, res[item].color, res[item].email, res[item].telefono];
                    tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyPlaneacion);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
                searchColor(tableBodyPlaneacion);
                // $('#tablePlaneacion').DataTable();
            }
            closeWorkLand(msGObj);
        }).catch(e => {
            labelLoader.style.color = 'tomato';
            labelLoader.innerHTML = e;
            closeWorkLand(msGObj);
        });
    });

    mainMenuAulas.addEventListener('click', (e) => {
        let validations = {
            'idChooseFrmPlaneacion': {
                'type': 'number',
                'required': true,
                'value': {
                    symbol: '>',
                    value: 0
                }
            },
            'nombreFrmPlaneacion': {
                'type': 'text',
                'long': 255,
                'required': true
            },
            'abreviaturaFrmPlaneacion': {
                'type': 'text',
                'long': 20,
                'required': true
            },
            'colorFrmPlaneacion': {
                'type': 'especial',
                'long': 7,
                'required': true
            },
            'capacidadFrmPlaneacion': {
                'type': 'especial',
                'long': 20,
                'required': true
            },

        };
        assignValidations(validations);
        tablaPlaneacion.innerHTML = e.target.title;
        deleteAllChilds(tableBodyPlaneacion);
        deleteAllChilds(tableTitlePlaneacion, ['tablePlaneacion#', 'tablePlaneacionTitulo', 'tablePlaneacionAbreviatura', 'tablePlaneacionColor']);
        let newChildrenEqual = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': 'tablePlaneacion',
                'father': tableTitlePlaneacion,
                'kind': 'th',
                'elements': [langText.capacidad, langText.id_centro],
                'classes': ['nowrap', 'test'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChildrenEqual);
        selecItemMenu(e.target);
        table = {
            name: 'blm_aulas',
            fields: ['id', 'nombre', 'abreviatura', 'color', 'capacidad', 'id_centro']
        };
        loader.style.display = 'block';
        searchItemsDB(table).then((res) => {
            deleteAllChilds(tableBodyPlaneacion);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = { 'id': res[item].id, 'nombre': res[item].nombre, 'abreviatura': res[item].abreviatura, 'color': res[item].color, 'capacidad': res[item].capacidad, 'id_centro': res[item].id_centro };
                    items = [res[item].id, res[item].nombre, res[item].abreviatura, res[item].color, res[item].capacidad, res[item].id_centro];
                    tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyPlaneacion);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
                searchColor(tableBodyPlaneacion);
            }
            closeWorkLand(msGObj);
        }).catch(e => {
            labelLoader.style.color = 'tomato';
            labelLoader.innerHTML = e;
            closeWorkLand(msGObj);
        });
    });

    mainMenuCentros.addEventListener('click', (e) => {
        let validations = {
            'nombreFrmPlaneacion': {
                'type': 'text',
                'long': 50,
                'required': true
            },
            'abreviaturaFrmPlaneacion': {
                'type': 'text',
                'long': 25,
                'required': true
            },
            'colorFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            },
            'horasFrmPlaneacion': {
                'type': 'number',
                'long': 10,
                'required': true
            }
        };
        assignValidations(validations);
        tablaPlaneacion.innerHTML = e.target.title;
        deleteAllChilds(tableBodyPlaneacion);
        deleteAllChilds(tableTitlePlaneacion, ['tablePlaneacion#', 'tablePlaneacionTitulo', 'tablePlaneacionAbreviatura', 'tablePlaneacionColor']);
        let newChildrenEqual = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': 'tablePlaneacion',
                'father': tableTitlePlaneacion,
                'kind': 'th',
                'elements': [langText.horas],
                'classes': ['nowrap', 'test'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChildrenEqual);
        selecItemMenu(e.target);

        table = {
            name: 'blm_centros',
            fields: ['id', 'nombre', 'abreviatura', 'color', 'horas']
        };
        loader.style.display = 'block';
        searchItemsDB(table).then((res) => {
            deleteAllChilds(tableBodyPlaneacion);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = { 'id': res[item].id, 'nombre': res[item].nombre, 'abreviatura': res[item].abreviatura, 'color': res[item].color, 'horas': res[item].horas };
                    items = [res[item].id, res[item].nombre, res[item].abreviatura, res[item].color, res[item].horas];
                    tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyPlaneacion);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
                searchColor(tableBodyPlaneacion);
            }
            closeWorkLand(msGObj);
        }).catch(e => {
            labelLoader.style.color = 'tomato';
            labelLoader.innerHTML = e;
            closeWorkLand(msGObj);
        });
    });

    mainMenuClases.addEventListener('click', (e) => {
        let validations = {
            'nombreFrmPlaneacion': {
                'type': 'text',
                'long': 50,
                'required': true
            },
            'abreviaturaFrmPlaneacion': {
                'type': 'text',
                'long': 25,
                'required': true
            },
            'colorFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            },
            'fechaInFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            },
            'fechaFinFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            },
            'gradoFrmPlaneacion': {
                'type': 'number',
                'value': {
                    symbol: '>',
                    value: 0
                },
                'required': true
            }
        };
        assignValidations(validations);
        tablaPlaneacion.innerHTML = e.target.title;
        deleteAllChilds(tableBodyPlaneacion);
        deleteAllChilds(tableTitlePlaneacion, ['tablePlaneacion#', 'tablePlaneacionTitulo', 'tablePlaneacionAbreviatura', 'tablePlaneacionColor']);
        selecItemMenu(e.target);
        let newChildrenEqual = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': 'tablePlaneacion',
                'father': tableTitlePlaneacion,
                'kind': 'th',
                'elements': [langText.fecha_inicio, langText.fecha_fin, langText.id_grado],
                'classes': ['nowrap', 'test'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChildrenEqual);
        selecItemMenu(e.target);
        table = {
            name: 'blm_grupos',
            fields: ['id', 'nombre', 'abreviatura', 'color', 'fecha_inicio', 'fecha_fin', 'id_grado']
        };
        loader.style.display = 'block';
        searchItemsDB(table).then((res) => {
            deleteAllChilds(tableBodyPlaneacion);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = { 'id': res[item].id, 'nombre': res[item].nombre, 'abreviatura': res[item].abreviatura, 'color': res[item].color, 'fecha_inicio': res[item].fecha_inicio, 'fecha_fin': res[item].fecha_fin, 'id_grado': res[item].id_grado };
                    items = [res[item].id, res[item].nombre, res[item].abreviatura, res[item].color, res[item].fecha_inicio, res[item].fecha_fin, res[item].id_grado];
                    tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyPlaneacion);
                    //console.log(tr);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
                searchColor(tableBodyPlaneacion);
            }
            closeWorkLand(msGObj);
        }).catch(e => {
            labelLoader.style.color = 'tomato';
            labelLoader.innerHTML = e;
            closeWorkLand(msGObj);
        });
    });

    mainMenuCursos.addEventListener('click', (e) => {
        let validations = {
            'nombreFrmPlaneacion': {
                'type': 'text',
                'long': 50,
                'required': true
            },
            'abreviaturaFrmPlaneacion': {
                'type': 'text',
                'long': 25,
                'required': true
            },
            'colorFrmPlaneacion': {
                'type': 'especial',
                'long': 10,
                'required': true
            }
        };
        assignValidations(validations);
        tablaPlaneacion.innerHTML = e.target.title;
        deleteAllChilds(tableBodyPlaneacion);
        deleteAllChilds(tableTitlePlaneacion, ['tablePlaneacion#', 'tablePlaneacionTitulo', 'tablePlaneacionAbreviatura', 'tablePlaneacionColor']);
        selecItemMenu(e.target);
        table = {
            name: 'blm_cursos',
            fields: ['id', 'nombre', 'abreviatura', 'color']
        };
        loader.style.display = 'block';
        searchItemsDB(table).then((res) => {
            deleteAllChilds(tableBodyPlaneacion);
            if (Object.keys(res).length > 0) {
                for (const item in res) {
                    data = { 'id': res[item].id, 'nombre': res[item].nombre, 'abreviatura': res[item].abreviatura, 'color': res[item].color };
                    items = [res[item].id, res[item].nombre, res[item].abreviatura, res[item].color];
                    tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyPlaneacion);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                    });
                }
                searchColor(tableBodyPlaneacion);
            }
            closeWorkLand(msGObj);
        }).catch(e => {
            labelLoader.style.color = 'tomato';
            labelLoader.innerHTML = e;
            closeWorkLand(msGObj);
        });
    });

    $("#idChooseFrmPlaneacion").change((e) => {
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        if (selectMenu != 'none') {
            labelLoader.style.color = '#13b5ea';
            loader.style.display = 'block';
            let op = selectMenu.dataset.title;
            switch (op) {
                case 'Test':
                    labelLoader.innerHTML = `${langText.msg_get_data_source} ${op}`;
                    let datos = e.target.options[e.target.selectedIndex];
                    nombreFrmPlaneacion.value = datos.dataset.nombre;
                    apellidoPatFrmPlaneacion.value = datos.dataset.apellido1;
                    apellidoMatFrmPlaneacion.value = datos.dataset.apellido2;
                    capacidadFrmPlaneacion.value = datos.value;
                    break;

                case 'Cursos':
                    labelLoader.innerHTML = `${langText.msg_get_data_source} ${op}`;
                    let datos1 = e.target.options[e.target.selectedIndex];
                    nombreFrmPlaneacion.value = datos1.dataset.fullname;
                    abreviaturaFrmPlaneacion.value = datos1.dataset.shortname;
                    break;

                case 'Clases':
                    labelLoader.innerHTML = `${langText.msg_get_data_source} ${op}`;
                    let datacl = e.target.options[e.target.selectedIndex];
                    gradoFrmPlaneacion.value = datacl.dataset.nombre;
                    break;

                case 'Centros':
                    labelLoader.innerHTML = `${langText.msg_get_data_source} ${op}`;
                    let datosCentros = e.target.options[e.target.selectedIndex];
                    nombreFrmPlaneacion.value = datosCentros.dataset.nombre;
                    break;

                case 'Profesores':
                    labelLoader.innerHTML = `Getting data of ${op}`;
                    alert('hola')
                    let datosProfesores = e.target.options[e.target.selectedIndex];
                    nombreFrmPlaneacion.value = datosProfesores.dataset.firstname;
                    apellidoPatFrmPlaneacion.value = datosProfesores.dataset.lastname;
                    apellidoMatFrmPlaneacion.value = datosProfesores.dataset.apellidomaterno;
                    emailFrmPlaneacion.value = datosProfesores.dataset.email;
                    break;
            }
            closeWorkLand(msGObj);
        }
    });

    labelBtnModalMsgCancelar.addEventListener('click', (e) => {
        let dta = e.target.dataset;
        closeModal(dta.modal);
    });

    labelBtnModalMsgAceptar.addEventListener('click', (e) => {
        let dta = e.target.dataset;
        let op = dta.action;
        switch (op) {
            case 'Info':
                closeModal(dta.modal);
                if (e = dta.hasOwnProperty('extrado')) {
                    setFunctionOnMsg(dta.extrado);
                }
                break;

            case 'eliminar':
                loader.style.display = 'block';
                let dataItem = JSON.parse(dta.data);
                console.log(dataItem);
                let table = {
                    name: dta.table,
                    id: dataItem.id
                };
                deleteItemDB(table).then(deleted => {
                    console.log(deleted);
                    if (deleted) {
                        deleteItem(document.querySelector(`#${dta.item}`));
                        labelLoader.style.color = '#28a745';
                        labelLoader.innerHTML = langText.msg_deleted;
                        closeModal(dta.modal);
                    } else {
                        labelLoader.style.color = 'tomato';
                        labelLoader.innerHTML = langText.msg_no_deleted;
                    }
                    closeWorkLand(msGObj);
                }).catch(e => { console.error(e) });
                break;
        }
    });
});

function setFunctionOnMsg(data) {
    data = JSON.parse(data);
    console.log(data);
    switch (data.functionDo) {
        case 'destroySelect2':
            destroySelect2(data.parameters.id, data.parameters.petition);
            break;

        case 'abrirModlVistaTablero':
            abrirModlVistaTablero();
            break;
    }
    if (e = labelBtnModalMsgAceptar.dataset.hasOwnProperty('extrado')) {
        delete labelBtnModalMsgAceptar.dataset.extrado;
    }
}

function destroySelect2(nameId, petition) {
    item = document.getElementById(nameId);
    console.log(item.value);
    if (petition == 2) {
        $(`#${nameId}`).select2('destroy');
        console.log(nameId);
    }
}