function ajaxData(data) {
    return new Promise(function (resolve, reject) {
        let xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else {
            xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
        // localhos:80//archiv0?hola="6" en GET
        xhr.open(data.method, data.target, true);
        // cabecera
        xhr.addEventListener('load', (res) => {
            res = res.target;
            if (res.status == 200) {
                resolve(JSON.parse(res.responseText));
            } else {
                reject(Error(res.Error));
            }
        });
        if (data.send) {
            let setData = '';
            // console.log(`Data-> ${data.form[0]} - ${data.form[1]}`);
            if (data.form[0]) {
                setData = new FormData(data.form[1]);
            } else {
                setData = data.form[1];
            }
            // printFormData(setData);
            // console.log(`Aqui-> ${setData}`);
            xhr.send(setData);
        } else {
            xhr.send();
        }
    });
}

function printFormData(formData) {
    for (var ob of formData.entries()) {
        console.log(`[Key: ${ob[0]} -> Value: ${ob[1]}]`);
    }
}

function objectToFormData(object, frmd = false) {
    let formData = (frmd == false) ? new FormData() : frmd;
    for (var ob of Object.entries(object)) {
        if (frmd != false) {
            if (u = formData.has(ob[0])) {
                console.log(u);
                formData.set(ob[0], ob[1])
            } else {
                formData.append(ob[0], ob[1]);
            }
        } else {
            formData.append(ob[0], ob[1]);
        }
    }
    return formData;
}

function deleteAllChilds(father, exep = '') {
    if (father.hasChildNodes()) {
        if (exep == '') {
            while (father.childNodes.length >= 1) {
                father.removeChild(father.firstChild);
            }
        } else {
            let children = father.childNodes;
            while (children.length > exep.length) {
                children.forEach(child => {
                    if (exep.includes(child.id)) { } else {
                        father.removeChild(child);
                    }
                });
            }
        }
    }
}

function buildNewElements(newItem) {
    if (newItem.childrenConf[0]) {
        if (newItem.childrenConf[1] == 'diff') {
            newItem.children.forEach(element => {
                return createChild(element);
            });
        }
        if (newItem.childrenConf[1] == 'equal') {
            let children = newItem.child.elements;
            children.forEach(element => {
                newItem.child.innerHtml = element;
                newItem.child.name = element;
                return createChild(newItem);
            });
        }
    } else {
        return createChild(newItem);
    }
}

function createChild(itemObject) {
    let nItem, father, nameItem;
    if (e = itemObject.hasOwnProperty('child')) {
        // console.info('Check child', e);
        if (e = itemObject.child.hasOwnProperty('father')) {
            father = itemObject.child.father;
            // console.info('Creating father', e);
        } else {
            console.error('Creating father', e);
            return false;
        }
        if (e = itemObject.child.hasOwnProperty('kind')) {
            nItem = document.createElement(itemObject.child.kind);
            // console.info('Creating kind', e);
        } else {
            console.error('Creating kind', e);
            return false;
        }
        if (e = itemObject.child.hasOwnProperty('name')) {
            nameItem = itemObject.child.name;
            // console.info('Check name', e);
        } else {
            console.error('Check name', e);
            return false;
        }
        if (e = itemObject.child.hasOwnProperty('prefix')) {
            nItem.id = `${itemObject.child.prefix}${nameItem}`;
            // console.info('Creating ID with prefix', e);
        } else {
            nItem.id = `${nameItem}`;
            // console.error('Creating ID with prefix (we create the id with no prefix)', e);
        }
        if (e = itemObject.child.hasOwnProperty('innerHtml')) {
            nItem.innerHTML = itemObject.child.innerHtml;
            // console.info('Check innerHtml', e);
        } else {
            // console.error('Check innerHtml', e);
        }
        if (e = itemObject.child.hasOwnProperty('data')) {
            let data = itemObject.child.data;
            setDatato(data, nItem);
            // console.info('Check data', e);
        } else {
            // console.error('Check data', e);
        }
        if (e = itemObject.child.hasOwnProperty('classes')) {
            let classes = itemObject.child.classes;
            classes.forEach(element => {
                nItem.classList.add(element);
            });
            // console.info('Check classes', e);
        } else {
            // console.error('Check classes', e);
        }
        if (e = itemObject.child.hasOwnProperty('attributes')) {
            let attributes = itemObject.child.attributes;
            let keys = Object.keys(attributes);
            keys.forEach(key => {
                nItem.setAttribute(`${key}`, attributes[key]);
            });
            // console.info('Check attributes', e);
        } else {
            // console.error('Check attributes', e);
        }
        if (e = itemObject.child.hasOwnProperty('before')) {
            father.insertBefore(nItem, itemObject.child.before);
        } else {
            father.append(nItem);
        }
        return { 'status': true, 'child': nItem };
    } else {
        console.error('Check child', e);
        return false;
    }
}

function binarySearch(arr, item) {
    let start = 0,
        end = arr.length - 1,
        element;
    while (start <= end) {
        element = Math.floor((start + end) / 2);
        if (parseInt(arr[element].dataset.idHora) == parseInt(item)) {
            return arr[element];
        } else if (parseInt(arr[element].dataset.idHora) < parseInt(item)) {
            start = element + 1;
        } else {
            end = element - 1;
        }
    }
    return false;
}

function setDatato(data, element) {
    let keys = Object.keys(data);
    keys.forEach(key => {
        element.setAttribute(`data-${key}`, data[key]);
    });
}

function isset(variable) {
    if (typeof (variable) != "undefined") {
        return true;
    } else {
        return false;
    }
}

function loadScript(src, callback) {
    var scriptItem, r, t;
    r = false;
    scriptItem = document.createElement('script');
    scriptItem.type = 'text/javascript';
    scriptItem.src = src;
    scriptItem.onload = scriptItem.onreadystatechange = function () {
        //console.log( this.readyState ); //uncomment this line to see which ready states are called.
        if (!r && (!this.readyState || this.readyState == 'complete')) {
            r = true;
            if (callback !== undefined) {
                callback();
            }
        }
    };
    t = document.getElementsByTagName('script')[0];
    t.parentNode.insertBefore(scriptItem, t);
}
Array.prototype.diff = function (arr) {
    var mergedArr = this.concat(arr);
    return mergedArr.filter(function (e) {
        return mergedArr.indexOf(e) === mergedArr.lastIndexOf(e);
    });
};

Array.prototype.unique = function (a) {
    return function () { return this.filter(a) }
}(function (a, b, c) {
    return c.indexOf(a, b + 1) < 0
});

function getCheckboxData(form) {
    let elements = document.querySelectorAll(`#${form.name} [type="checkbox"]`);
    let positiveChecks = new Array();
    let checks = new Array();
    elements.forEach(element => {
        checks.push(element.id);
    });
    let formData = new FormData(form.item);
    for (var ob of formData.entries()) {
        if (checks.includes(ob[0])) { positiveChecks.push(ob[0]) }
    }
    var diff = positiveChecks.diff(checks);
    for (let i = 0; i < diff.length; i++) {
        formData.append(diff[i], 0);
    }
    return formData;
}

function findElements(tag, parent) {
    let res = new Object();
    let children = parent.childNodes;
    let items = Array();
    res.state = false;
    children.forEach(element => {
        if (element.tagName == tag.toUpperCase()) {
            res.state = true;
            items.push(element);
        }
    });
    res.children = children;
    res.items = items;
    return res;
}

function validation(data, langText) {
    response = true;
    let expreg = '';
    let array = new Array();
    let item;
    data.forEach(element => {
        item = new Object();
        item.input = element;
        v = JSON.parse(element.dataset.validate);
        for (const key in v) {
            let msg = 'No';
            switch (key) {
                case 'type':
                    switch (v[key]) {
                        case 'text':
                            expreg = new RegExp("[ñA-Za-z_]");
                            msg = langText.text_validate;
                            break;
                        case 'text2': //para 2 nombres ///esta bien ya la pobre 
                            expreg = new RegExp("[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$");
                            msg = langText.text_validate;
                            break;
                        case 'number': //solo numeros
                            expreg = new RegExp("[0-9]"); ///esta bien ya la pobre
                            msg = langText.number_validate;
                            break;
                        case 'textnumber':
                            expreg = new RegExp("[ñA-Za-z0-9]*[ñA-Za-z0-9][ñA-Za-z0-9 _]*$");
                            msg = langText.email_validate;
                            break;
                        case 'email':
                            expreg = new RegExp("[ñA-Za-z0-9._%-]+@[ñA-Za-z0-9.-]");
                            msg = langText.textnumber;
                            break;
                        case 'especial':
                            expreg = new RegExp("\w*")
                            msg = langText.special_validate;
                            break;
                    }
                    (expreg.test(element.value)) ? (item[key] = true) : (item[v[key]] = { 'msg': msg });
                    break;
                case 'long':
                    msg = langText.text_longitud;
                    (element.value.length <= v[key]) ? (item[key] = true) : (item[key] = { 'msg': msg });
                    break;
                case 'required':
                    msg = langText.required;
                    (element.value != '') ? (item[key] = true) : (item[key] = { 'msg': msg });
                    break;
                case 'value':
                    switch (v[key].symbol) {
                        case '>':
                            msg = langText.value_greater;
                            (parseInt(element.value) > v[key].value) ? (item[key] = true) : (item[key] = { 'msg': msg });
                            break;
                        case '<':
                            msg = langText.value_shorter;
                            (parseInt(element.value) < v[key].value) ? (item[key] = true) : (item[key] = { 'msg': msg });
                            break;
                        case '>=':
                            msg = langText.value_equal_greater;
                            (parseInt(element.value) >= v[key].value) ? (item[key] = true) : (item[key] = { 'msg': msg });
                            break;
                        case '<=':
                            msg = langText.value_equal_shorter;
                            (parseInt(element.value) <= v[key].value) ? (item[key] = true) : (item[key] = { 'msg': msg });
                            break;
                        case '==':
                            msg = langText.value_equal;
                            (parseInt(element.value) == v[key].value) ? (item[key] = true) : (item[key] = { 'msg': msg });
                            break;
                        case '!=':
                            msg = langText.value_diff;
                            (parseInt(element.value) != v[key].value) ? (item[key] = true) : (item[key] = { 'msg': msg });
                            break;
                    }
                    break;
            };
        }
        array.push(item);
    });
    return array;
}

function randomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function randomNumber(startZero = false, limit) {
    limit = startZero ? limit++ : limit;
    zero = startZero ? 0 : 1;
    let x = Math.floor((Math.random() * limit) + zero);
    return x;
}

function deleteItem(item) {
    item.parentNode.removeChild(item);
}

function currentDate(format, separador) {
    let currentDate = new Array();
    let date = new Date();
    format.forEach(element => {
        switch (element.toLowerCase()) {
            case 'd':
                currentDate.push((date.getDate()) < 10 ? `0${date.getDate() + 1}` : date.getDate());
                break;
            case 'm':
                currentDate.push((date.getMonth() + 1) < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1);
                break;
            case 'y':
                currentDate.push(date.getFullYear());
                break;
        }
    });
    // document.getElementById('fechaActual').value = year + "-" + mes + "-" + dia;
    return currentDate.join(separador);
}

// current time = 00:00 = hh:mm
// time to add = 00:00 = hh:mm
function addHours(currentTime, newTimeAdd) {
    let time = currentTime.split(':');
    let timeAdd = newTimeAdd.split(':');
    let newHour, newMin;
    console.log(time, timeAdd);
    if (time[0] < 24 && timeAdd[0] < 24 && time[0] >= 0 && timeAdd[0] >= 0) {
        if (time[1] < 60 && timeAdd[1] < 60 && time[1] >= 0 && timeAdd[1] >= 0) {
            newHour = parseInt(time[0]) + parseInt(timeAdd[0]);
            newMin = parseInt(time[1]) + parseInt(timeAdd[1]);
            console.log(newHour, newMin);
            if (newMin > 60) {
                newMin -= 60;
                newHour += 1;
            }
            if (newHour > 24) {
                while (newHour > 24) {
                    newHour -= 24;
                }
            }
            if (newHour == 24) {
                newHour = 0;
            }
            return `${newHour.toString().length == 1 ? '0' : ''}${newHour}:${newMin.toString().length == 1 ? '0' : ''}${newMin}`;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function subHours(currentTime, newTimeSub) {
    let time = currentTime.split(':');
    let timeSub = newTimeSub.split(':');
    let newHour, newMin;
    console.log(time, timeSub);
    if (time[0] < 24 && timeSub[0] < 24 && time[0] >= 0 && timeSub[0] >= 0) {
        if (time[1] < 60 && timeSub[1] < 60 && time[1] >= 0 && timeSub[1] >= 0) {
            newHour = parseInt(time[0]) - parseInt(timeSub[0]);
            newMin = parseInt(time[1]) - parseInt(timeSub[1]);
            console.log(newHour, newMin);
            if (newMin < 0) {
                newMin += 60;
                newHour -= 1;
            }
            if (newHour < 0) {
                while (newHour < 0) {
                    newHour += 24;
                }
            }
            if (newHour == 24) {
                newHour = 0;
            }
            return `${newHour.toString().length == 1 ? '0' : ''}${newHour}:${newMin.toString().length == 1 ? '0' : ''}${newMin}`;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function quickSort(array) {
    let pivote = array[0];
    let izq = new Array();
    let der = new Array();
    if (array.length < 1) {
        return array;
    }
    array.forEach((element, i) => {
        if (i > 0) {
            if (element < pivote) {
                izq.push(element);
            } else {
                der.push(element);
            }
        }
    });
    return quickSort(izq).concat([pivote]).concat(quickSort(der));
}

function fadeOut(data) {
    setTimeout(() => {
        data.item.classList.remove('fadeIn', 'alert-success', 'alert-danger');
        data.item.classList.add('fadeOut');
    }, data.time);
}

function removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);
    if (i !== -1) {
        arr.splice(i, 1);
    }
}

function clone(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }
    var temp = obj.constructor();
    for (var key in obj) {
        temp[key] = clone(obj[key]);
    }
    return temp;
}