window.addEventListener('load', () => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;
    let elementTohide = [];
    btnPlaneacionAgregar.addEventListener('click', (e) => {
        recreateFrmValidations();
        gestionFrmPlaneacion.reset();
        labelBtnModalPlaneacionAgregar.innerHTML = langText.btn_agregar_frm;
        hideElement(elementTohide);
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        if (selectMenu != 'none') {
            colorFrmPlaneacion.value = randomColor();
            peticionFrmPlaneacion.value = 2;
            msGObj.loader.style.display = 'block';
            let op = selectMenu.dataset.title;
            msGObj.modalTitle.innerHTML = selectMenu.title;
            switch (op) {
                case 'Cursos':
                    elementTohide = [
                        telefonoFrmPlaneacion.parentNode,
                        apellidoPatFrmPlaneacion.parentNode,
                        apellidoMatFrmPlaneacion.parentNode,
                        emailFrmPlaneacion.parentNode,
                        claseSegFrmPlaneacion,
                        aulaSegFrmPlaneacion,
                        centroSegFrmPlaneacion,
                        selectFrmProfesores
                    ];
                    hideElement(elementTohide);
                    tablaFrmPlaneacion.value = 'blm_cursos';
                    camposFrmPlaneacion.value = JSON.stringify({
                        'nombre': 'nombreFrmPlaneacion',
                        'abreviatura': 'abreviaturaFrmPlaneacion',
                        'color': 'colorFrmPlaneacion'
                    });
                    select = {
                        item: idChooseFrmPlaneacion,
                        prefix: `${idChooseFrmPlaneacion.id}Op`,
                        idItem: idChooseFrmPlaneacion.id
                    };
                    table = {
                        name: 'course',
                        fields: ['id', 'fullname', 'shortname'],
                        exeption: ['shortname']
                    };
                    fillSelectItem(select, table, { firstItem: true });
                    break;

                case 'Clases':
                    document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.curso;
                    elementTohide = [
                        idChooseFrmPlaneacion.parentNode,
                        telefonoFrmPlaneacion.parentNode,
                        apellidoPatFrmPlaneacion.parentNode,
                        apellidoMatFrmPlaneacion.parentNode,
                        emailFrmPlaneacion.parentNode,
                        aulaSegFrmPlaneacion,
                        centroSegFrmPlaneacion,
                        tutorFrmPlaneacion.parentNode,
                        selectFrmProfesores
                    ];
                    hideElement(elementTohide);
                    tablaFrmPlaneacion.value = 'blm_grupos';
                    camposFrmPlaneacion.value = JSON.stringify({
                        'nombre': 'nombreFrmPlaneacion',
                        'abreviatura': 'abreviaturaFrmPlaneacion',
                        'color': 'colorFrmPlaneacion',
                        'fecha_inicio': 'fechaInFrmPlaneacion',
                        'fecha_fin': 'fechaFinFrmPlaneacion',
                        'id_grado': 'gradoFrmPlaneacion'
                    });
                    select = {
                        item: gradoFrmPlaneacion,
                        prefix: `${gradoFrmPlaneacion.id}Op`,
                        idItem: idChooseFrmPlaneacion.id
                    };
                    table = {
                        name: 'blm_grados',
                        fields: ['id', 'nombre'],
                        exeption: ['nombre']
                    };
                    fillSelectItem(select, table, { firstItem: true });
                    break;

                case 'Centros':
                    document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.centro;
                    elementTohide = [
                        telefonoFrmPlaneacion.parentNode,
                        apellidoPatFrmPlaneacion.parentNode,
                        apellidoMatFrmPlaneacion.parentNode,
                        emailFrmPlaneacion.parentNode,
                        claseSegFrmPlaneacion,
                        aulaSegFrmPlaneacion,
                        selectFrmProfesores
                    ];
                    hideElement(elementTohide);
                    tablaFrmPlaneacion.value = 'blm_centros';
                    camposFrmPlaneacion.value = JSON.stringify({
                        'nombre': 'nombreFrmPlaneacion',
                        'abreviatura': 'abreviaturaFrmPlaneacion',
                        'color': 'colorFrmPlaneacion',
                        'horas': 'horasFrmPlaneacion'
                    });
                    select = {
                        item: idChooseFrmPlaneacion,
                        prefix: `${idChooseFrmPlaneacion.id}Op`,
                        idItem: idChooseFrmPlaneacion.id
                    };
                    table = {
                        name: 'empresas',
                        fields: ['id', 'nombre'],
                        exeption: ['nombre'],
                        conditions: { 'nombre': ['!=', 'undefined'], 'nombre': ['!=', 'null'] }
                    };
                    fillSelectItem(select, table, { firstItem: true });
                    break;

                case 'Aulas':
                    document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.centro;
                    elementTohide = [
                        telefonoFrmPlaneacion.parentNode,
                        apellidoPatFrmPlaneacion.parentNode,
                        apellidoMatFrmPlaneacion.parentNode,
                        emailFrmPlaneacion.parentNode,
                        addClassroomPlaneacion.parentNode,
                        claseSegFrmPlaneacion,
                        centroSegFrmPlaneacion,
                        selectFrmProfesores
                    ];
                    hideElement(elementTohide);
                    tablaFrmPlaneacion.value = 'blm_aulas';
                    camposFrmPlaneacion.value = JSON.stringify({
                        'nombre': 'nombreFrmPlaneacion',
                        'abreviatura': 'abreviaturaFrmPlaneacion',
                        'color': 'colorFrmPlaneacion',
                        'capacidad': 'capacidadFrmPlaneacion',
                        'id_centro': 'idChooseFrmPlaneacion'
                    });
                    select = {
                        item: idChooseFrmPlaneacion,
                        prefix: `${idChooseFrmPlaneacion.id}Op`,
                        idItem: idChooseFrmPlaneacion.id
                    };
                    table = {
                        name: 'blm_centros',
                        fields: ['id', 'nombre'],
                        exeption: ['nombre']
                    };
                    fillSelectItem(select, table, { firstItem: true });
                    break;

                case 'Profesores':
                    // document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.nomina;
                    msGObj.modalTitle.innerHTML = op;
                    elementTohide = [
                        claseSegFrmPlaneacion,
                        aulaSegFrmPlaneacion,
                        centroSegFrmPlaneacion,
                        idChooseFrmPlaneacion.parentNode
                    ];
                    hideElement(elementTohide);
                    tablaFrmPlaneacion.value = 'blm_profesores';
                    camposFrmPlaneacion.value = JSON.stringify({
                        'nombre': 'nombreFrmPlaneacion',
                        'apellido_paterno': 'apellidoPatFrmPlaneacion',
                        'apellido_materno': 'apellidoMatFrmPlaneacion',
                        'abreviatura': 'abreviaturaFrmPlaneacion',
                        'color': 'colorFrmPlaneacion',
                        'email': 'emailFrmPlaneacion',
                        'telefono': 'telefonoFrmPlaneacion',
                        'nomina': 'idProfesorFrmPlaneacion'
                    });
                    break;

                case 'Grados':
                    elementTohide = [
                        telefonoFrmPlaneacion.parentNode,
                        apellidoPatFrmPlaneacion.parentNode,
                        apellidoMatFrmPlaneacion.parentNode,
                        emailFrmPlaneacion.parentNode,
                        aulaSegFrmPlaneacion,
                        centroSegFrmPlaneacion,
                        idChooseFrmPlaneacion.parentNode,
                        fechaInFrmPlaneacion.parentNode,
                        fechaFinFrmPlaneacion.parentNode,
                        tutorFrmPlaneacion.parentNode,
                        claseSegFrmPlaneacion,
                        selectFrmProfesores
                    ];
                    hideElement(elementTohide);
                    tablaFrmPlaneacion.value = 'blm_grados';
                    camposFrmPlaneacion.value = JSON.stringify({
                        'nombre': 'nombreFrmPlaneacion',
                        'abreviatura': 'abreviaturaFrmPlaneacion',
                        'color': 'colorFrmPlaneacion'
                    });
                    break;
            }
            closeWorkLand(msGObj);
            $('#ModalPlaneacion').modal({ backdrop: 'static', keyboard: false });
        }
    });

    labelBtnModalPlaneacionAgregar.addEventListener('click', (e) => {
        recreateFrmValidations();
        data = document.querySelectorAll('[data-validate]');
        let validations = validation(data, langText);
        let Approve = validationExe(validations);
        console.log(validations);
        if (Approve) {
            msGObj.loader.style.display = 'block';
            let data = new Object();
            data.target = '../blocks/planificacion/peticionesSql.php';
            data.method = 'POST';
            data.send = true;
            data.form = [true, gestionFrmPlaneacion];
            let newForeing = false;
            let selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
            let dataToUser = new Object();
            if (selectMenu.dataset.title == 'Profesores' && idChooseFrmPlaneacion.value == 'null') {
                newForeing = true;
                dataToUser.nombre = nombreFrmPlaneacion.value;
                dataToUser.apellidoPaterno = apellidoPatFrmPlaneacion.value;
                dataToUser.apellidoMaterno = apellidoMatFrmPlaneacion.value;
                dataToUser.email = emailFrmPlaneacion.value;
                dataToUser.operacion = "A";
                dataToUser.fechaIngreso = currentDate(['y', 'm', 'd'], '/');
                dataToUser.fechaNacimiento = currentDate(['y', 'm', 'd'], '/');
                dataToUser.planta = "Externos";
                dataToUser.departamento = "Externos";
                dataToUser.puesto = "Externos";
                dataToUser.nominaJefe = null;
                dataToUser.unidad = null;
                dataToUser.CeCo = null;
                dataToUser.NoCentros = null;
                dataToUser.dueno = null;
                dataToUser.user = "BLMovil20";
                dataToUser.pass = "Admin4321$";
            }
            ajaxData(data).then((res) => {
                console.log(res);
                msGObj.labelLoader.style.color = '#28a745';
                if (res[0] != false) {
                    let op = parseInt(peticionFrmPlaneacion.value);
                    switch (op) {
                        case 2:
                            msGObj.labelLoader.innerHTML = langText.msg_added;
                            let keys = Object.keys(res[1]);
                            let data = new Object();
                            let items = new Array();
                            let tr;
                            data.id = res[0];
                            items.push(res[0]);
                            keys.forEach(key => {
                                let value = document.getElementById(`${res[1][key]}`).value;
                                data[key] = value;
                                if (key != 'nomina')
                                    items.push(value);
                            });
                            tr = addRowTable(data, items, res[0], 'Planeacion', tableBodyPlaneacion);
                            tr.child.addEventListener('click', (e) => {
                                selectRow(e);
                            });
                            if (newForeing) {
                                let nomina = `ext-${res[0]}`;
                                dataToUser.noNomina = nomina;
                                let formDataToUser = new FormData();
                                formDataToUser.append('tabla', 'blm_profesores');
                                formDataToUser.append('peticion', 3);
                                formDataToUser.append('id', res[0]);
                                formDataToUser.append('campos', JSON.stringify({ id: 'id', nomina: "nominaGen" }));
                                formDataToUser.append('nominaGen', nomina);
                                let dataUserNomina = new Object();
                                dataUserNomina.target = '../blocks/planificacion/peticionesSql.php';
                                dataUserNomina.method = 'POST';
                                dataUserNomina.send = true;
                                dataUserNomina.form = [false, formDataToUser];
                                ajaxData(dataUserNomina).then((res) => {
                                    if (res[0]) {
                                        let formDataSend = new FormData();
                                        formDataSend.append('data', JSON.stringify(dataToUser));
                                        msGObj.labelLoader.innerHTML = langText.msg_alert_set_user;
                                        let dataSendUser = new Object();
                                        dataSendUser.target = '../blocks/planificacion/user.php';
                                        dataSendUser.method = 'POST';
                                        dataSendUser.send = true;
                                        dataSendUser.form = [false, formDataSend];
                                        ajaxData(dataSendUser).then(res => console.log(res)).catch(error => console.log(error));
                                    } else {
                                        msGObj.labelLoader.innerHTML = langText.msg_added;
                                    }
                                }).catch((error) => {
                                    msGObj.labelLoader.innerHTML = error;
                                });
                            }
                            break;

                        case 3:
                            msGObj.labelLoader.innerHTML = langText.msg_saved;
                            let dataK = Object.keys(res[1]);
                            let itemsI = new Array();
                            let trI;
                            dataK.forEach(key => {
                                let value = document.getElementById(`${res[1][key]}`).value;
                                if (key == 'id') {
                                    trI = document.getElementById(`tableRowPlaneacion${value}`);;
                                } else {
                                    trI.setAttribute(`data-${key}`, value);
                                }
                                itemsI.push(value);
                            });
                            deleteAllChilds(trI);
                            let newChildrenEqual = {
                                'childrenConf': [true, 'equal'],
                                'child': {
                                    'prefix': 'tableRowFieldPlaneacion',
                                    'father': trI,
                                    'kind': 'td',
                                    'elements': itemsI,
                                }
                            };
                            buildNewElements(newChildrenEqual);
                            break;
                    }
                    searchColor(tableBodyPlaneacion);
                    closeModal(JSON.stringify({ status: 0, close: ['ModalPlaneacion'] }));
                    closeWorkLand(msGObj);
                } else {
                    let elementTrate = res[1];
                    let errorTrate = res[2];
                    console.log(elementTrate, errorTrate);
                    msGObj.labelLoader.style.color = 'tomato';
                    switch (errorTrate[0]) {
                        case 'dmlwriteexception':
                            let errorItem = elementTrate[errorTrate[2]];
                            document.getElementById(`${errorItem}`).classList.add('alert-validate-input');
                            switch (parseInt(errorTrate[1])) {
                                case 1:
                                    msg = langText.msg_db_duplicate;
                                    msg = msg.replace('${op}', errorTrate[2]);
                                    msGObj.labelLoader.innerHTML = msg;
                                    break;
                                case 2:
                                    msg = langText.msg_db_too_long;
                                    msg = msg.replace('${op}', errorTrate[2]);
                                    msGObj.labelLoader.innerHTML = msg;
                                    break;
                                default:
                                    msGObj.labelLoader.innerHTML = langText.msg_error;
                                    break;
                            }
                            break;
                    }
                    closeWorkLand(msGObj);
                }
            });
        } else {
            // mensajes
        }
    });

    // what we do when edit button is pressed fechaActualFrmPlaneacion
    btnPlaneacionEditar.addEventListener('click', (e) => {
        gestionFrmPlaneacion.reset()
        recreateFrmValidations();
        hideElement(elementTohide);
        // Check item selected on table
        selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        // Check item selected on left menu
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        labelBtnModalPlaneacionAgregar.innerHTML = langText.btn_guardar_frm;
        if (selectMenu != 'none') {
            if (selectItem != 'none') {
                let abrir = true;
                msGObj.loader.style.display = 'block';
                msGObj.labelLoader.style.color = '#20c997';
                // Elements on common in edit section (this section put values on form)
                peticionFrmPlaneacion.value = 3;
                idFrmPlaneacion.value = selectItem.dataset.id;
                //console.log(idChooseFrmPlaneacion.parentNode);
                // Elements on every case of different modules in edit section (this section put values on form)
                // In every case we need to change the form, like hide or show elements
                let op = selectMenu.dataset.title;
                let text = langText.msg_get_data;
                let labelMenu = selectMenu.title;
                msGObj.modalTitle.innerHTML = labelMenu;
                msGObj.labelLoader.innerHTML = text.replace('${op}', labelMenu);
                switch (op) {
                    case 'Test':
                        tablaFrmPlaneacion.value = 'blm_prueba';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        capacidadFrmPlaneacion.value = selectItem.dataset.cantidad;
                        // this input put elements that we send to PHP for set new values on DB.
                        // "key: value" where key is the name of column on DB, and value is the name of Post variable or name of every input of form.
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'cantidad': 'capacidadFrmPlaneacion'
                        });
                        elementTohide = [idChooseFrmPlaneacion.parentNode, telefonoFrmPlaneacion.parentNode];
                        hideElement(elementTohide);
                        break;

                    case 'Cursos':
                        tablaFrmPlaneacion.value = 'blm_cursos';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        colorFrmPlaneacion.value = selectItem.dataset.color;
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'color': 'colorFrmPlaneacion'
                        });
                        elementTohide = [
                            idChooseFrmPlaneacion.parentNode,
                            telefonoFrmPlaneacion.parentNode,
                            apellidoPatFrmPlaneacion.parentNode,
                            apellidoMatFrmPlaneacion.parentNode,
                            emailFrmPlaneacion.parentNode,
                            capacidadFrmPlaneacion.parentNode,
                            selectFrmProfesores,
                            claseSegFrmPlaneacion,
                            centroSegFrmPlaneacion
                        ];
                        hideElement(elementTohide);
                        break;

                    case 'Clases':
                        tablaFrmPlaneacion.value = 'blm_grupos';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        colorFrmPlaneacion.value = selectItem.dataset.color;
                        fechaInFrmPlaneacion.value = selectItem.dataset.fecha_inicio;
                        fechaFinFrmPlaneacion.value = selectItem.dataset.fecha_fin;
                        gradoFrmPlaneacion.value = selectItem.dataset.id_grado;
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'color': 'colorFrmPlaneacion',
                            'fecha_inicio': 'fechaInFrmPlaneacion',
                            'fecha_fin': 'fechaFinFrmPlaneacion',
                            'id_grado': 'gradoFrmPlaneacion'
                        });
                        elementTohide = [
                            idChooseFrmPlaneacion.parentNode,
                            telefonoFrmPlaneacion.parentNode,
                            apellidoPatFrmPlaneacion.parentNode,
                            apellidoMatFrmPlaneacion.parentNode,
                            emailFrmPlaneacion.parentNode,
                            aulaSegFrmPlaneacion,
                            centroSegFrmPlaneacion,
                            tutorFrmPlaneacion.parentNode,
                            selectFrmProfesores
                        ];
                        hideElement(elementTohide);
                        select = {
                            item: gradoFrmPlaneacion,
                            prefix: `${gradoFrmPlaneacion.id}Op`,
                            idItem: idChooseFrmPlaneacion.id
                        };
                        table = {
                            name: 'blm_grados',
                            fields: ['id', 'nombre'],
                            exeption: ['nombre']
                        };
                        fillSelectItem(select, table, { firstItem: true });
                        break;

                    case 'Centros':
                        document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.centro;
                        tablaFrmPlaneacion.value = 'blm_centros';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        colorFrmPlaneacion.value = selectItem.dataset.color;
                        horasFrmPlaneacion.value = selectItem.dataset.horas;
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'color': 'colorFrmPlaneacion',
                            'horas': 'horasFrmPlaneacion'
                        });
                        elementTohide = [
                            idChooseFrmPlaneacion.parentNode,
                            telefonoFrmPlaneacion.parentNode,
                            apellidoPatFrmPlaneacion.parentNode,
                            apellidoMatFrmPlaneacion.parentNode,
                            emailFrmPlaneacion.parentNode,
                            claseSegFrmPlaneacion,
                            aulaSegFrmPlaneacion,
                            selectFrmProfesores
                        ];
                        hideElement(elementTohide);
                        break;

                    case 'Aulas':
                        tablaFrmPlaneacion.value = 'blm_aulas';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        colorFrmPlaneacion.value = selectItem.dataset.color;
                        capacidadFrmPlaneacion.value = selectItem.dataset.capacidad;
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'color': 'colorFrmPlaneacion',
                            'capacidad': 'capacidadFrmPlaneacion',
                            'id_centro': 'idChooseFrmPlaneacion'
                        });
                        document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.centro;
                        elementTohide = [
                            telefonoFrmPlaneacion.parentNode,
                            apellidoPatFrmPlaneacion.parentNode,
                            apellidoMatFrmPlaneacion.parentNode,
                            emailFrmPlaneacion.parentNode,
                            addClassroomPlaneacion.parentNode,
                            claseSegFrmPlaneacion,
                            centroSegFrmPlaneacion,
                            selectFrmProfesores
                        ];
                        hideElement(elementTohide);
                        select = {
                            item: idChooseFrmPlaneacion,
                            prefix: `${idChooseFrmPlaneacion.id}Op`,
                            currentOption: selectItem.dataset.id_centro,
                            idItem: idChooseFrmPlaneacion.id
                        };
                        table = {
                            name: 'blm_centros',
                            fields: ['id', 'nombre'],
                            exeption: ['nombre']
                        };
                        fillSelectItem(select, table, { firstItem: true });
                        break;

                    case 'Profesores':
                        document.querySelector('[for="idChooseFrmPlaneacion"]').innerHTML = langText.nomina;
                        tablaFrmPlaneacion.value = 'blm_profesores';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        apellidoPatFrmPlaneacion.value = selectItem.dataset.apellido_paterno;
                        apellidoMatFrmPlaneacion.value = selectItem.dataset.apellido_materno;
                        emailFrmPlaneacion.value = selectItem.dataset.email;
                        telefonoFrmPlaneacion.value = selectItem.dataset.telefono;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        colorFrmPlaneacion.value = selectItem.dataset.color;
                        // nominaFrmPlaneacion.value = selectItem.dataset.nomina;
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'apellido_paterno': 'apellidoPatFrmPlaneacion',
                            'apellido_materno': 'apellidoMatFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'color': 'colorFrmPlaneacion',
                            'email': 'emailFrmPlaneacion',
                            'telefono': 'telefonoFrmPlaneacion'
                        });
                        elementTohide = [
                            idChooseFrmPlaneacion.parentNode,
                            claseSegFrmPlaneacion,
                            aulaSegFrmPlaneacion,
                            centroSegFrmPlaneacion,
                            selectFrmProfesores
                        ];
                        hideElement(elementTohide);
                        break;

                    case 'Grados':
                        tablaFrmPlaneacion.value = 'blm_grados';
                        nombreFrmPlaneacion.value = selectItem.dataset.nombre;
                        abreviaturaFrmPlaneacion.value = selectItem.dataset.abreviatura;
                        colorFrmPlaneacion.value = selectItem.dataset.color;
                        gradoFrmPlaneacion.value = selectItem.dataset.nivel;
                        camposFrmPlaneacion.value = JSON.stringify({
                            'id': 'idFrmPlaneacion',
                            'nombre': 'nombreFrmPlaneacion',
                            'abreviatura': 'abreviaturaFrmPlaneacion',
                            'color': 'colorFrmPlaneacion'
                        });
                        elementTohide = [
                            telefonoFrmPlaneacion.parentNode,
                            apellidoPatFrmPlaneacion.parentNode,
                            apellidoMatFrmPlaneacion.parentNode,
                            emailFrmPlaneacion.parentNode,
                            aulaSegFrmPlaneacion,
                            centroSegFrmPlaneacion,
                            idChooseFrmPlaneacion.parentNode,
                            fechaInFrmPlaneacion.parentNode,
                            fechaFinFrmPlaneacion.parentNode,
                            tutorFrmPlaneacion.parentNode,
                            claseSegFrmPlaneacion,
                            selectFrmProfesores
                        ];
                        hideElement(elementTohide);
                        break;
                }
                setTimeout(() => {
                    if (abrir) {
                        $('#ModalPlaneacion').modal({ backdrop: 'static', keyboard: false });
                    }
                    msGObj.loader.style.display = 'none';
                    msGObj.labelLoader.style.color = 'black';
                    msGObj.labelLoader.innerHTML = langText.msg_working;
                }, 3000);
            }
        }
    });

    btnPlaneacionEliminar.addEventListener('click', (e) => {
        selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        selectMenu = getChooseItem('i', mainMenuPlaneacion, 'choosed-font')[1];
        if (selectMenu != 'none') {
            if (selectItem != 'none') {
                let tableName = '';
                switch (selectMenu.dataset.title) {
                    case 'Cursos':
                        tableName = 'blm_cursos';
                        break;

                    case 'Clases':
                        tableName = 'blm_grupos';
                        break;

                    case 'Centros':
                        tableName = 'blm_centros';
                        break;
                    case 'Aulas':
                        tableName = 'blm_aulas';
                        break;

                    case 'Profesores':
                        tableName = 'blm_profesores';
                        break;

                    case 'Grados':
                        tableName = 'blm_grados';
                        break;
                }
                let msg = msgDeleteItem(langText.msg_alert_delete, selectItem.dataset.nombre, selectMenu.title);
                data = {
                    'dataSets': {
                        'action': 'eliminar',
                        'data': JSON.stringify(selectItem.dataset),
                        'table': tableName,
                        'item': selectItem.id,
                        'modal': JSON.stringify({ status: 0, close: ['ModalMsg'] }),
                    },
                    'msg': msg,
                    'ico': 'info',
                    'closeDataSet': {
                        visible: 1,
                        modal: JSON.stringify({ status: 0, close: ['ModalMsg'] })
                    }
                };
                changeMsgModal(data);
                $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
            }
        }
    });

    addClassroomPlaneacion.addEventListener('click', (e) => {
        delete chooseElementAddElement.dataset.itemsList;
        deleteAllChilds(tableBodyAddElement);
        document.querySelector('[for="chooseElementAddElement"]').innerHTML = langText.aula;
        peticionFrmAddElement.value = 2;
        tablaFrmAddElement.value = 'blm_aula_curso';
        idFrmAddElement.value = idFrmPlaneacion.value;
        camposFrmAddElement.value = JSON.stringify({
            'id_aula': 'chooseElementAddElement',
            'id_curso': 'idFrmAddElement'
        });
        deleteAllChilds(tableTitleAddElement, ['tableAddElement#']);
        let newChildrenEqual = {
            'childrenConf': [true, 'equal'],
            'child': {
                'prefix': 'tablePlaneacion',
                'father': tableTitleAddElement,
                'kind': 'th',
                'elements': [langText.aula, langText.curso],
                'classes': ['nowrap', 'test'],
                'attributes': { 'scope': 'col' }
            }
        };
        buildNewElements(newChildrenEqual);
        table = {
            name: 'blm_vista_aula_curso',
            fields: ['*'],
            conditions: { 'id_curso': ['=', idFrmAddElement.value] }
        };
        searchItemsDB(table).then(res => {
            console.log(res);
            let idAulas = new Array();
            msGObj.modalTitle.style.display = 'block';
            if (Object.keys(res).length > 0) {
                let i = 0;
                for (const item in res) {
                    ++i;
                    if (!idAulas.includes(res[item].id_aula))
                        idAulas.push(res[item].id_aula);
                    let data = { 'index': i, 'id': res[item].id, 'id_aula': res[item].id_aula, 'aula': res[item].aula, 'id_curso': res[item].id_curso, 'curso': res[item].curso };
                    let items = [i, res[item].aula, res[item].curso];
                    let tr = addRowTable(data, items, res[item].id, 'Planeacion', tableBodyAddElement);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                        tableToOption(e.target.parentNode, { name: tablaFrmAddElement.value }, chooseElementAddElement);
                    });
                }
            } else {
                msGObj.modalTitle.style.color = 'tomato';
                msGObj.modalTitle.innerHTML = langText.msg_no_data;
            }
            chooseElementAddElement.dataset.itemsList = JSON.stringify(idAulas);
        }).catch(e => console.log(e)).then(() => {
            select = {
                item: chooseElementAddElement,
                prefix: `${chooseElementAddElement.id}Op`,
                exclude: chooseElementAddElement.dataset.itemsList
            };
            table = {
                name: 'blm_aulas',
                fields: ['id', 'nombre'],
                exeption: ['nombre']
            };
            fillSelectItem(select, table, { firstItem: true });
            closeWorkLand(msGObj);
        });
        $('#ModalAddElement').modal({ backdrop: 'static', keyboard: false });
    });

    addItemAddElement.addEventListener('click', (e) => {
        let itemAdded = selectedOption(chooseElementAddElement);
        let selectItem = getChooseItem('tr', tableBodyPlaneacion, 'choosed')[1];
        if (itemAdded.value != 0) {
            msGObj.loader.style.display = 'block';
            saveItemsDB(langText, 2, frmAddElement, true).then(res => {
                msGObj.labelLoader.style.color = '#28a745';
                if (res[0] != false) {
                    let op = parseInt(peticionFrmAddElement.value);
                    switch (op) {
                        case 2:
                            msGObj.labelLoader.innerHTML = langText.msg_added;
                            break;

                        case 3:
                            msGObj.labelLoader.innerHTML = langText.msg_saved;
                            break;
                    }
                    let lastItem = findElements('tr', tableBodyAddElement);
                    let i = lastItem.state ? parseInt(lastItem.items[lastItem.items.length - 1].dataset.index) + 1 : 1;
                    let id_curso = selectItem.dataset.id;
                    let curso = selectItem.dataset.nombre;
                    let data = { 'index': i, 'id': res[0], 'id_aula': itemAdded.dataset.id, 'aula': itemAdded.dataset.nombre, 'id_curso': id_curso, 'curso': curso };
                    let items = [i, itemAdded.dataset.nombre, curso];
                    let tr = addRowTable(data, items, res[0], 'Planeacion', tableBodyAddElement);
                    tr.child.addEventListener('click', (e) => {
                        selectRow(e);
                        tableToOption(e.target.parentNode, { name: tablaFrmAddElement.value }, chooseElementAddElement);
                    });
                    deleteItem(itemAdded);
                }
            }).catch(error => {
                msGObj.labelLoader.innerHTML = error;
            });
        }
        closeWorkLand(msGObj);
    });

    labelBtnModalPlaneacionCancelar.addEventListener('click', (e) => {
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({ status: 0, close: ['ModalMsg', 'ModalPlaneacion'] }),
                // extraDo: JSON.stringify({ parameters: { id: 'idChooseFrmPlaneacion', petition: peticionFrmPlaneacion.value }, functionDo: 'destroySelect2' })
            },
            'msg': langText.msg_alert_Cancel,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({ status: 1, close: ['ModalMsg'] })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({ backdrop: 'static', keyboard: false });
    });
});

function tableToOption(tableRowItem, table, selectItem) {
    let data = tableRowItem.dataset;
    table.id = data.id;
    deleteItemDB(table).then(res => {
        console.log(res);
        let newItem = {
            childrenConf: [false],
            child: {
                prefix: "chooseElementAddElementOp",
                name: data.id_aula,
                father: selectItem,
                kind: "option",
                innerHtml: data.aula,
                attributes: { 'value': data.id_aula },
                data: { 'id': data.id_aula, 'nombre': data.aula }
            }
        };
        buildNewElements(newItem).child;
        deleteItem(tableRowItem);
    });
}