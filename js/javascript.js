$(document).ready(function () {
    //bucar por curso 1 reporte
    $('#selectProfesores').select2({
        width: '100%',
        placeholder: "Ingrese usuario/nomina",
        minimumInputLength: 2,
        ajax: {
            url: "http://" + window.location.hostname + "/moodle/blocks/planificacion/getUsuarios.php",
            // url:"../blocks/planificacion/getUsuarios.php",
            dataType: "json",
            type: "GET",
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    //error en foco por la ventan modal
    document.getElementById("ModalPlaneacion").removeAttribute("tabindex");

    $(document.body).on("change", "#selectProfesores", function () {
        var id_user = this.value;
        //alert("id_user: "+id_user);
        $.ajax({
            url: 'http://' + window.location.hostname + '/moodle/blocks/planificacion/getUsuarios2.php',
            type: 'POST',
            dataType: 'JSON',
            data: { id: id_user },
        })
            .done(function (data) {
                console.log(data[0].id);
                document.getElementById('idProfesorFrmPlaneacion').value = data[0].id;
                document.getElementById('nombreFrmPlaneacion').value = data[0].nombre;
                document.getElementById('apellidoPatFrmPlaneacion').value = data[0].ap1;
                document.getElementById('apellidoMatFrmPlaneacion').value = data[0].ap2;
                document.getElementById('emailFrmPlaneacion').value = data[0].email;
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () { });y
    });
});