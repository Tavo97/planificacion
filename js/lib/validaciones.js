function ValidacionTexto() {
    var text = document.getElementById("").value;
    var expreg = new RegExp("[A-Z][a-z]$");

    if (expreg.test(text))
        alert("Formato Correcto ");
    else
        alert("Solo se aceptan minusculas y mayusculas");

}

function ValidacionNumero() {
    var text = document.getElementById("").value;
    var expreg = new RegExp("[0-9]$");
    if (expreg.test(text))
        alert("Formato Correcto ");
    else
        alert("Solo se aceptan numeros");

}

function ValidacionLetraNumero() {
    var text = document.getElementById("").value;
    var expreg = new RegExp("[A-Z][a-z][0-9]$");
    if (expreg.test(text))
        alert("Formato Correcto ");
    else
        alert("Solo se aceptan minusculas y mayusculas e numeros");

}