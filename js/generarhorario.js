let testAulas = true;

window.addEventListener("load", (e) => {
    let langText = globalVariable.langText;
    let horario = titleModulePlaneacion.dataset;
    const modalTitle = document.getElementById("Modal-title-GenerarHorario");
    const labelLoader = document.querySelector(".labelLoader");
    const loader = document.querySelector(".containerLoader");
    let msGObj = {
        loader: loader,
        labelLoader: labelLoader,
        modalTitle: modalTitle,
    };

    btnPlaneacionGHorario.addEventListener("click", (e) => {
        contenedorVueltasGNuevHorario.classList.add('oculto');
        if (prcGNuevHorario.checked == false) {
            estrictoGNuevHorario.checked = true;
        }
        pruebaGNuevHorario.disabled = true;
        normalGNuevHorario.checked = true;
        grandeGNuevHorario.checked = false;
        muyGrandeGNuevHorario.checked = false;
        normalGNuevHorario.dataset.times = 1000;
        grandeGNuevHorario.dataset.times = 2500;
        muyGrandeGNuevHorario.dataset.times = 5000;
        if (isset(horario.id)) {
            let groups = document.querySelectorAll(`[data-group]`);
            groups.forEach((element) => {
                element.addEventListener("change", (e) => checkGroup(e.target));
            });
            $("#ModalGNuevHorario").modal({
                backdrop: "static",
                keyboard: false,
            });
        } else {
            data = {
                dataSets: {
                    action: "Info",
                    modal: JSON.stringify({
                        status: 0,
                        close: ["ModalMsg"],
                    }),
                },
                msg: langText.msg_info_select_horario,
                ico: "info",
                closeDataSet: {
                    visible: 0,
                },
            };
            changeMsgModal(data);
            $("#ModalMsg").modal({
                backdrop: "static",
                keyboard: false,
            });
        }
    });

    labelBtnModalGTerminadaClose.addEventListener("click", (e) => {
        let langText = globalVariable.langText;
        data = {
            dataSets: {
                action: "Info",
                modal: JSON.stringify({
                    status: 0,
                    close: ["ModalMsg", "ModalGNuevHorario", "ModalGTerminada"],
                }),
                extrado: JSON.stringify({ functionDo: "abrirModlVistaTablero" }),
            },
            msg: langText.msg_only_alert,
            ico: "info",
            closeDataSet: {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ["ModalMsg"],
                }),
            },
        };
        changeMsgModal(data);
        $("#ModalMsg").modal({
            backdrop: "static",
            keyboard: false,
        });
    });

    labelBtnModalGNuevHorarioClose.addEventListener("click", (e) => {
        let langText = globalVariable.langText;
        data = {
            dataSets: {
                action: "Info",
                modal: JSON.stringify({
                    status: 0,
                    close: ["ModalMsg", "ModalGNuevHorario"],
                }),
            },
            msg: langText.msg_alert_Close,
            ico: "info",
            closeDataSet: {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ["ModalMsg"],
                }),
            },
        };
        changeMsgModal(data);
        $("#ModalMsg").modal({
            backdrop: "static",
            keyboard: false,
        });
    });

    gnhorarioGNuevHorario.addEventListener("click", (e) => {
        deleteAllChilds(boxNotifyGTerminada);
        let relajado = document.getElementById("prcGNuevHorario");
        let estricto = document.getElementById("estrictoGNuevHorario");
        let condicion = 0;
        if (relajado.checked == true) {
            condicion = 1;
        } else if (estricto.checked == true) {
            condicion = 2;
        }
        if (condicion != 0) {
            let fechaHoraStart = new Date();
            loader.style.display = "block";
            tableD = {
                name: "blm_tablero_principal",
                fields: ["*"],
                conditions: {
                    id_horario: ["=", horario.id],
                    activo: ["=", 1],
                }
            };
            searchItemsDB(tableD).then((res) => {
                for (const r in res) {
                    if (Object.hasOwnProperty.call(res, r)) {
                        tabled1 = {
                            name: "blm_tablero_principal",
                            id: res[r].id,
                        };
                        deleteItemDB(tabled1);
                    }
                }
            }).catch((e) => {
                console.log(e);
            });
            tableD = {
                name: "blm_tablero_principal_na",
                fields: ["*"],
                conditions: {
                    id_horario: ["=", horario.id],
                },
            };
            searchItemsDB(tableD).then((res) => {
                for (const r in res) {
                    if (Object.hasOwnProperty.call(res, r)) {
                        tabled1 = {
                            name: "blm_tablero_principal_na",
                            id: res[r].id,
                        };
                        deleteItemDB(tabled1);
                    }
                }
            }).catch((e) => {
                console.log(e);
            });
            table = {
                name: "blm_leccion",
                fields: ["*"],
                conditions: {
                    id_horario: ["=", horario.id],
                },
            };
            searchItemsDB(table).then((lecciones) => {
                let tablas = new Array();
                tablas[0] = lecciones;
                return tablas;
            }).then((tablas) => {
                table.name = "blm_leccion_periodo_semana";
                table.conditions = "";
                return searchItemsDB(table).then((leccionPeriodoSemana) => {
                    tablas[1] = leccionPeriodoSemana;
                    return tablas;
                }).catch((e) => {
                    return tablas;
                });
            }).then((tablas) => {
                table.name = "blm_leccion_grupo";
                return searchItemsDB(table).then((gruposLec) => {
                    tablas[2] = gruposLec;
                    return tablas;
                });
            }).then((tablas) => {
                table.name = "blm_leccion_profesor";
                return searchItemsDB(table).then((profLec) => {
                    tablas[3] = profLec;
                    return tablas;
                });
            }).then((tablas) => {
                table.name = "blm_relaciones";
                table.conditions = {
                    id_horario: ["=", horario.id],
                };
                return searchItemsDB(table).then((relaciones) => {
                    tablas[4] = relaciones;
                    return tablas;
                }).catch(() => {
                    return tablas;
                });
            }).then((tablas) => {
                table.name = "blm_curso_relacion";
                table.conditions = "";
                return searchItemsDB(table).then((cursosRel) => {
                    tablas[5] = cursosRel;
                    return tablas;
                }).catch((e) => {
                    return tablas;
                });
            }).then((tablas) => {
                table.name = "blm_grupo_relacion";
                return searchItemsDB(table).then((cursosRel) => {
                    tablas[6] = cursosRel;
                    return tablas;
                }).catch((e) => {
                    return tablas;
                });
            }).then((tablas) => {
                table.name = "blm_aula_curso";
                return searchItemsDB(table).then((aulasCursos) => {
                    tablas[7] = aulasCursos;
                    return tablas;
                }).catch((e) => {
                    return tablas;
                });
            }).then((tablas) => {
                let lecciones = tablas[0];
                let leccionesPeriodoSemana = tablas[1];
                let gruposLecciones = tablas[2];
                let profesoresLecciones = tablas[3];
                let relaciones = tablas[4];
                let cursosRelaciones = tablas[5];
                let gruposRelaciones = tablas[6];
                let aulas = tablas[7];
                for (const l in lecciones) {
                    lecciones[l].grupos = new Array();
                    lecciones[l].profesores = new Array();
                    lecciones[l].aulas = new Array();
                    for (const aula in aulas) {
                        if (lecciones[l].id_curso == aulas[aula].id_curso) {
                            lecciones[l].aulas.push(aulas[aula].id_aula);
                        }
                    }
                    for (const gl in gruposLecciones) {
                        if (gruposLecciones[gl].id_leccion == lecciones[l].id) {
                            lecciones[l].grupos.push(gruposLecciones[gl].id_grupo);
                        }
                    }
                    for (const cl in profesoresLecciones) {
                        if (profesoresLecciones[cl].id_leccion == lecciones[l].id) {
                            lecciones[l].profesores.push({
                                id_profesor: profesoresLecciones[cl].id_profesor,
                            });
                        }
                    }
                    for (const lsm in leccionesPeriodoSemana) {
                        if (leccionesPeriodoSemana[lsm].id_leccion == lecciones[l].id) {
                            lecciones[l].periodo = leccionesPeriodoSemana[lsm].id_periodo;
                            lecciones[l].semana = leccionesPeriodoSemana[lsm].id_semana;
                        }
                    }
                }
                for (const r in relaciones) {
                    // console.log(relaciones[r]);
                    relaciones[r].cursos = new Array();
                    relaciones[r].grupos = new Array();
                    for (const cr in cursosRelaciones) {
                        if (cursosRelaciones[cr].id_relacion == relaciones[r].id) {
                            relaciones[r].cursos.push(cursosRelaciones[cr].id_curso);
                        }
                    }
                    for (const gr in gruposRelaciones) {
                        if (gruposRelaciones[gr].id_relacion == relaciones[r].id) {
                            relaciones[r].grupos.push(gruposRelaciones[gr].id_grupo);
                        }
                    }
                }
                return [lecciones, relaciones];
            }).then((tablas) => {
                let lecciones = tablas[0];
                let relaciones = tablas[1];
                let fichas = new Array();
                for (const l in lecciones) {
                    if (lecciones[l].leccion > 1) {
                        for (let ss = 0; ss < lecciones[l].sesiones_semana; ss++) {
                            for (let i = 0; i < lecciones[l].leccion; i++) {
                                fichas.push({
                                    ficha: ss + 1,
                                    leccion: lecciones[l].id,
                                    curso: lecciones[l].id_curso,
                                    duracion: lecciones[l].leccion,
                                    grupos: lecciones[l].grupos,
                                    dia: null,
                                    hora: null,
                                    periodo: lecciones[l].periodo,
                                    semana: lecciones[l].semana,
                                    aula: null,
                                    revisada: false
                                });
                            }
                        }
                    } else {
                        for (let ss = 0; ss < lecciones[l].sesiones_semana; ss++) {
                            fichas.push({
                                ficha: ss + 1,
                                leccion: lecciones[l].id,
                                curso: lecciones[l].id_curso,
                                duracion: lecciones[l].leccion,
                                grupos: lecciones[l].grupos,
                                dia: null,
                                hora: null,
                                periodo: lecciones[l].periodo,
                                semana: lecciones[l].semana,
                                aula: null,
                                revisada: false
                            });
                        }
                    }
                }
                let jAdvert = new Array();
                let jerarquias = { 3: 1, 13: 2, 6: 3, 5: 4, 2: 5, 10: 6, 4: 7, 11: 8, 1: 9 };
                if (relaciones != undefined) {
                    for (const relacion in relaciones) {
                        relaciones[relacion].jerarquia = jerarquias[relaciones[relacion].condicion];
                        for (const leccion in lecciones) {
                            if (relaciones[relacion].cursos.includes(lecciones[leccion].id_curso)) {
                                if (relaciones[relacion].estado_grupo == 1) {
                                    if (relaciones[relacion].hasOwnProperty("leccionesId")) {
                                        relaciones[relacion].leccionesId.push(
                                            lecciones[leccion].id
                                        );
                                    } else {
                                        relaciones[relacion].leccionesId = [
                                            lecciones[leccion].id,
                                        ];
                                    }
                                } else {
                                    if (checkGroups(lecciones[leccion].grupos, relaciones[relacion].grupos)) {
                                        if (relaciones[relacion].hasOwnProperty("leccionesId")) {
                                            relaciones[relacion].leccionesId.push(
                                                lecciones[leccion].id
                                            );
                                        } else {
                                            relaciones[relacion].leccionesId = [
                                                lecciones[leccion].id,
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (let jA = 0; jA < jAdvert.length; jA++) {
                    jerarquiaAdvertencia(jAdvert[jA].curso, jAdvert[jA].condiciones);
                }
                return [lecciones, relaciones, fichas];
            }).then((data) => {
                fichas = data[2];
                if (data[1] != undefined) {
                    data[1] = orderDBresponseLR(Object.values(data[1]), "jerarquia", "number");
                }
                let arrayPromise = new Array();
                for (const leccion in data[0]) {
                    let promise = tiempos_libres(data[0][leccion]).then((tl) => {
                        data[0][leccion].tl = tl;
                    }).catch((e) => console.log(e));
                    arrayPromise.push(promise);
                }
                if (data[1] != undefined) {
                    for (const relacion of data[1]) {
                        table = {
                            name: "blm_distribucion_asignatura",
                            fields: ["*"],
                            conditions: {
                                id_relacion: ["=", relacion.id],
                            },
                        }
                        let promise = searchItemsDB(table).then(distribucion => {
                            relacion.distribucion = distribucion;
                        });
                        arrayPromise.push(promise);
                    }
                }
                return Promise.allSettled(arrayPromise).then((res) => {
                    return [data[1], fichas, data[0]];
                });
            }).then((data) => {
                // console.log(data);
                let relaciones = data[0];
                let relacionesFunctions = {
                    1: constraintDiffDay,
                    2: noConsecutivas,
                    3: constraintCardDistribution,
                    4: MismoDia,
                    5: consecutivasf,
                    6: receso,
                    10: constraintSimultaneousSubjects,
                    11: mismaLeccion,
                    13: inicioFinal,
                };
                let parametros = {
                    dayCardCourse: new Array(),
                    fichas: data[1],
                    ocupados: new Array(),
                    ocupadoNoconsecutivas: new Array(),
                    consecutivas: new Array(),
                    lCardCourse: new Array(),
                    existLec: new Array(),
                    lecciones: data[2],
                    relacionesTotales: relaciones
                };
                let auxParametros = clone(parametros);
                let auxParametrosTemp = clone(parametros);
                let AuxFichasTemp = new Array();
                let flag = false;
                let globalConbinacion = 0;
                // variables de sueltas
                let relajado = document.getElementById("prcGNuevHorario");
                let estricto = document.getElementById("estrictoGNuevHorario");
                let condicion = 0;
                if (relajado.checked == true) {
                    condicion = 1;
                } else if (estricto.checked == true) {
                    condicion = 2;
                }
                // fin variables de sueltas
                let timesCheck = 0;
                if (grandeGNuevHorario.checked == true)
                    timesCheck = grandeGNuevHorario.dataset.times;
                if (muyGrandeGNuevHorario.checked == true) {
                    timesCheck = vueltasGNuevHorario.value;
                }
                if (normalGNuevHorario.checked == true)
                    timesCheck = normalGNuevHorario.dataset.times;
                let fichasVioladas = 0;
                let fichasVioladasTemp = 0;
                for (let conbinacion = 0; conbinacion < timesCheck; conbinacion++) {
                    globalConbinacion = conbinacion + 1;
                    if (relaciones != null) {
                        // console.log(relaciones);
                        for (const relacion of relaciones) {
                            parametros.relaciones = relacion;
                            relacionesFunctions[parseInt(relacion.condicion)](parametros);
                        }
                    }
                    parametros.todasRelaciones = relaciones;
                    parametros.condition = condicion;
                    fichasVioladas = sueltas(parametros);
                    let fichasAcomodadas = parametros.fichas.filter(ficha => ficha.dia == null);
                    if (fichasAcomodadas.length == 0) {
                        flag = false;
                        break;
                    }
                    if (conbinacion == 0) {
                        AuxFichasTemp = clone(parametros.fichas);
                        auxParametrosTemp = clone(parametros);
                        fichasVioladasTemp = fichasVioladas;
                    }
                    console.log(globalConbinacion, fichasAcomodadas.length, AuxFichasTemp.filter(ficha => ficha.dia == null).length);
                    if (fichasAcomodadas.length < AuxFichasTemp.filter(ficha => ficha.dia == null).length) {
                        // console.log(AuxFichasTemp);
                        AuxFichasTemp = clone(parametros.fichas);
                        auxParametrosTemp = clone(parametros);
                        fichasVioladasTemp = fichasVioladas;
                    }
                    parametros = clone(auxParametros);
                    flag = true;
                }
                if (flag) {
                    parametros = clone(auxParametrosTemp);
                    fichasVioladas = fichasVioladasTemp;
                }

                let reladasFichas = `${langText.msg_generar_horario_relajadas} ${fichasVioladas}`;
                let relajadas = {
                    childrenConf: [false],
                    child: {
                        prefix: "boxNotify",
                        name: "Relajadas",
                        father: boxNotifyGTerminada,
                        kind: "label",
                        innerHtml: reladasFichas,
                        classes: ["m-1", "msg-box-Notify"],
                    },
                };
                newChild = buildNewElements(relajadas).child;

                let horariosCreados = `${langText.msg_generar_horario_comprobados} ${globalConbinacion}`;
                horariosCreados = {
                    childrenConf: [false],
                    child: {
                        prefix: "boxNotify",
                        name: "Relajadas",
                        father: boxNotifyGTerminada,
                        kind: "label",
                        innerHtml: horariosCreados,
                        classes: ["m-1", "msg-box-Notify"],
                    },
                };
                newChild = buildNewElements(horariosCreados).child;
                return parametros.fichas;
            }).then((res) => {
                let fichas = res;
                let cursosAulas = new Object();
                let aulasReady = new Array();
                for (const ficha of fichas) {
                    if (!Object.keys(cursosAulas).includes(ficha.curso)) {
                        table = {
                            name: "blm_aula_curso",
                            fields: ["*"],
                            conditions: { id_curso: ["=", ficha.curso] },
                        };
                        let promise = searchItemsDB(table).then((aulas) => {
                            let aulasId = new Array();
                            for (const id in aulas) {
                                if (!aulasId.includes(aulas[id].id_aula)) {
                                    aulasId.push(aulas[id].id_aula);
                                }
                            }
                            cursosAulas[ficha.curso] = aulasId;
                        });
                        aulasReady.push(promise);
                    }
                }
                return Promise.allSettled(aulasReady).then((res) => {
                    let aulas = new Array();
                    for (const cursoId in cursosAulas) {
                        aulas = aulas.concat(cursosAulas[cursoId]).unique();
                    }
                    let table = {
                        name: "blm_tiempo_libre_aulas",
                        fields: ["*"],
                        condicion: {
                            id_horario: ["=", horario.id],
                            id_aula: ["IN", `${aulas.join()}`],
                        },
                    };
                    return searchItemsDB(table).then((freeTime) => {
                        return [fichas, cursosAulas, freeTime];
                    });
                });
            }).then((res) => {
                let fichasAulaTemp = new Array();
                let fichasAulaOptima = new Array();
                let fichas = res[0];
                let cursosAulas = res[1];
                let freeTime = Object.values(res[2]);
                let vueltas = testAulas ? 1000 : 1;
                for (let vueltaAula = 0; vueltaAula < vueltas; vueltaAula++) {
                    let aulasocupadas = new Array();
                    fichasAulaTemp = clone(fichas);
                    for (let f = 0; f < fichasAulaTemp.length; f++) {
                        let aux = f;
                        if (fichasAulaTemp[f].aula == null && fichasAulaTemp[f].dia != null && fichasAulaTemp[f].hora != null) {
                            let arrAulas = cursosAulas[fichas[f].curso].slice();
                            for (let i = 0; i < cursosAulas[fichasAulaTemp[f].curso].length; i++) {
                                let pa = Math.floor(Math.random() * arrAulas.length);
                                let aulaToUse = parseInt(arrAulas[pa]);
                                arrAulas.splice(pa, 1);
                                let searchAula = aulasocupadas.findIndex((aulasO) => aulasO.dia == fichasAulaTemp[f].dia && aulasO.hora >= parseInt(fichasAulaTemp[f].hora) && aulasO.hora <= parseInt(fichasAulaTemp[f].hora) + parseInt(fichasAulaTemp[f].duracion) - 1 && aulasO.id_aula == aulaToUse);
                                let searchFreeTimeAula = freeTime.findIndex((ft) => ft.id_aula == aulaToUse && ft.id_dia_tl == fichasAulaTemp[f].dia && ft.id_hora_tl >= parseInt(fichasAulaTemp[f].hora) && ft.id_hora_tl <= parseInt(fichasAulaTemp[f].hora) + parseInt(fichasAulaTemp[f].duracion) - 1 && ft.id_estado_tl == 2);
                                let duracion = parseInt(fichasAulaTemp[f].duracion);
                                if (searchAula == -1 && searchFreeTimeAula == -1) {
                                    for (let du = 0; du < duracion; du++) {
                                        fichasAulaTemp[f].aula = aulaToUse;
                                        aulasocupadas.push({
                                            id_aula: aulaToUse,
                                            dia: parseInt(fichasAulaTemp[f].dia),
                                            hora: parseInt(fichasAulaTemp[f].hora),
                                        });
                                        f++;
                                    }
                                    f--;
                                    break;
                                }
                            }
                            if (fichasAulaTemp[aux].aula == null) {
                                f = aux + parseInt(fichasAulaTemp[aux].duracion) - 1;
                            }
                        }
                    }
                    if (vueltaAula == 0) {
                        fichasAulaOptima = clone(fichasAulaTemp);
                    }
                    let fichasAcomodadasAula = fichasAulaTemp.filter(ficha => ficha.aula == null);
                    if (fichasAcomodadasAula.length == fichas.filter(ficha => ficha.dia == null).length) {
                        console.log(fichasAcomodadasAula, fichas.filter(ficha => ficha.dia == null).length);
                        break;
                    }
                    if (fichasAcomodadasAula.length < fichasAulaOptima.filter(ficha => ficha.aula == null).length) {
                        fichasAulaOptima = clone(fichasAulaTemp);
                    }
                    console.log(vueltaAula, fichasAulaOptima.filter(ficha => ficha.aula == null).length, fichasAcomodadasAula.length);
                }
                fichas = clone(fichasAulaOptima);
                let fichasDB = new Array();
                let fichasNA = new Array();
                guardarFichas(fichasDB, fichasNA, fichas, fechaHoraStart);
            }).catch((e) => console.log(e));
        } else {
            data = {
                dataSets: {
                    action: "Info",
                    modal: JSON.stringify({
                        status: 0,
                        close: ["ModalMsg"],
                    }),
                },
                msg: langText.msg_choose_mode,
                ico: "info",
                closeDataSet: {
                    visible: 0,
                },
            };
            changeMsgModal(data);
            $("#ModalMsg").modal({
                backdrop: "static",
                keyboard: false,
            });
        }
    });

    function checkGroups(leccionGroups, relacionGropus) {
        let res = false;
        for (const groupRelacionId of relacionGropus) {
            if (leccionGroups.includes(groupRelacionId)) {
                res = true;
                break;
            }
        }
        return res;
    }

    function sueltas({ lecciones, fichas, ocupados, todasRelaciones, condition }) {
        let fichasVioladas = 0;
        let leccionesSueltas = new Array();
        if (todasRelaciones != null) {
            for (const relacion of todasRelaciones) {
                for (const idLeccion of relacion.leccionesId) {
                    leccionesSueltas.push(idLeccion);
                }
            }
        }
        for (const leccionId in lecciones) {
            let arrTl = lecciones[leccionId].tl;
            let profes = lecciones[leccionId].profesores;
            let lecGroups = lecciones[leccionId].grupos;
            for (let f = 0; f < fichas.length; f++) {
                let cardsOnHold = new Array();
                if ((condition == 2 && fichas[f].dia == null && fichas[f].hora == null && !leccionesSueltas.includes(fichas[f].leccion) && fichas[f].leccion == leccionId) || (condition == 1 && fichas[f].dia == null && fichas[f].hora == null && fichas[f].leccion == leccionId)) {
                    let dayR = daysRandom([], horario.dias, 0);
                    let agrego = [];
                    for (let d = 0; d < dayR.length; d++) {
                        let lecR = leccRandom([], horario.lecciones, 0);
                        for (let l = 0; l < lecR.length; l++) {
                            let lecRAux = lecR[l];
                            for (let fd = 0; fd < fichas[f].duracion; fd++) {
                                if (lecRAux + parseInt(fichas[f].duracion) - 1 <= parseInt(horario.lecciones) || cardsOnHold.length > 0) {
                                    let nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == lecRAux && arrTl.d != false);
                                    let ocupadosG;
                                    let ocupadosP;
                                    for (const grupo of lecGroups) {
                                        ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecRAux && ocupados.grupo == grupo);
                                        if (ocupadosG != -1) {
                                            break;
                                        }
                                    }
                                    for (const profe of profes) {
                                        ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecRAux && ocupados.profesor == profe.id_profesor);
                                        if (ocupadosP != -1) {
                                            break;
                                        }
                                    }
                                    if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1) {
                                        cardsOnHold.push({
                                            dia: dayR[d],
                                            hora: lecRAux,
                                        });
                                        lecRAux++;
                                    } else {
                                        cardsOnHold = [];
                                        break;
                                    }
                                }
                                if (cardsOnHold.length == fichas[f].duracion) {
                                    if (condition == 1 && leccionesSueltas.includes(fichas[f].leccion)) {
                                        fichasVioladas++;
                                    }
                                    for (const [index, fEsp] of cardsOnHold.entries()) {
                                        fichas[f].dia = fEsp.dia;
                                        fichas[f].hora = fEsp.hora;
                                        for (const lecG of lecGroups) {
                                            for (const lecP of profes) {
                                                ocupados.push({
                                                    dia: fEsp.dia,
                                                    hora: fEsp.hora,
                                                    leccion: fichas[f].leccion,
                                                    curso: fichas[f].curso,
                                                    grupo: lecG,
                                                    profesor: lecP.id_profesor,
                                                });
                                            }
                                        }
                                        if (index + 1 < cardsOnHold.length) {
                                            f++;
                                        }
                                    }
                                    agrego.push(1);
                                    // auxDay++;
                                    break;
                                }
                            }
                            if (agrego.length > 0) {
                                break;
                            }
                        }
                        if (agrego.length > 0) {
                            break;
                        }
                    }
                }
            }
        }
        return fichasVioladas;
    }

    function guardarFichas(fichasDB, fichasNA, fichas, fechaHoraStart) {
        for (let index = 0; index < fichas.length; index++) {
            if (fichas[index].hora != null && fichas[index].dia != null) {
                fichasDB.push({
                    id_horario: horario.id,
                    id_hora: fichas[index].hora,
                    id_leccion: fichas[index].leccion,
                    id_dia: fichas[index].dia,
                    ficha: fichas[index].ficha,
                    duracion: fichas[index].duracion,
                    activo: 1,
                    id_aula: fichas[index].aula,
                });
            } else {
                fichasNA.push({
                    id_horario: horario.id,
                    id_leccion: fichas[index].leccion,
                    ficha: fichas[index].ficha,
                    duracion: fichas[index].duracion,
                });
            }
        }
        // fichasDB for trabajar aulas asignacion returna fichasDB
        let formData = new FormData();
        let formDataNA = new FormData();
        evaluate = fichasRestriction(fichasDB);
        let fichasToEvaluate = evaluate[0];
        let idsLessons = evaluate[1];
        let newConditions = new Object();
        newConditions.id_leccion = ["IN", `${idsLessons.join()}`];
        table = {
            name: "blm_vista_leccion_profesor",
            fields: ["*"],
            conditions: newConditions,
            rs: true,
            order: { id_leccion: "ASC" },
        };
        searchItemsDB(table).then((teachers) => {
            if (teachers.length > 0) {
                let idsTeachers = new Array();
                for (const teacher of teachers) {
                    if (!idsTeachers.includes(teacher.id_profesor))
                        idsTeachers.push(teacher.id_profesor);
                    if (fichasToEvaluate[teacher.id_leccion].hasOwnProperty("teachers")) {
                        fichasToEvaluate[teacher.id_leccion]["teachers"].push(
                            teacher.id_profesor
                        );
                    } else {
                        fichasToEvaluate[teacher.id_leccion].teachers = [
                            teacher.id_profesor,
                        ];
                    }
                }
                return [idsTeachers, fichasToEvaluate];
            }
        }).then((res) => {
            let spendTeacher = new Object();
            for (const ficha in res[1]) {
                for (const teacher of res[1][ficha].teachers) {
                    for (let day = 1; day < 8; day++) {
                        if (res[1][ficha].hasOwnProperty(day)) {
                            if (spendTeacher.hasOwnProperty(teacher)) {
                                if (spendTeacher[teacher].hasOwnProperty(day)) {
                                    spendTeacher[teacher][day] += res[1][ficha][day].length;
                                } else {
                                    spendTeacher[teacher][day] = res[1][ficha][day].length;
                                }
                            } else {
                                spendTeacher[teacher] = { [day]: res[1][ficha][day].length };
                            }
                        }
                    }
                }
            }
            table.name = "blm_restricciones_profesores";
            table.conditions = { id_profesor: ["IN", `${res[0].join()}`] };
            table.order = { id_profesor: "ASC" };
            return searchItemsDB(table).then((restrictions) => {
                if (restrictions.length > 0) {
                    let restrictionsTeachers = new Object();
                    for (const restriction of restrictions) {
                        if (restriction.min_max_lecciones_dia == 1) {
                            if (restrictionsTeachers.hasOwnProperty(restriction.id_profesor)) {
                                restrictionsTeachers[restriction.id_profesor].lessonsDay = {
                                    min: restriction.lecciones_por_dia_1,
                                    max: restriction.lecciones_por_dia_2,
                                };
                            } else {
                                restrictionsTeachers[restriction.id_profesor] = {
                                    lessonsDay: {
                                        min: restriction.lecciones_por_dia_1,
                                        max: restriction.lecciones_por_dia_2
                                    }
                                };
                            }
                        }
                        if (restriction.limite_num_dias == 1) {
                            if (restrictionsTeachers.hasOwnProperty(restriction.id_profesor)) {
                                restrictionsTeachers[restriction.id_profesor].limitDay = restriction.input_num_dias;
                            } else {
                                restrictionsTeachers[restriction.id_profesor] = { limitDay: restriction.input_num_dias };
                            }
                        }
                    }
                    for (const idTeacher in restrictionsTeachers) {
                        for (const restriction in restrictionsTeachers[idTeacher]) {
                            switch (restriction) {
                                case "lessonsDay":
                                    max = restrictionsTeachers[idTeacher].lessonsDay.max;
                                    for (const day in spendTeacher[idTeacher]) {
                                        while (spendTeacher[idTeacher][day] > max) {
                                            for (const ficha of fichasDB) {
                                                if (ficha.id_dia == day) {
                                                    fichasNA.push({
                                                        id_horario: ficha.id_horario,
                                                        id_leccion: ficha.id_leccion,
                                                        ficha: ficha.ficha,
                                                        duracion: ficha.duracion,
                                                    });
                                                    fichasDB.splice(fichasDB.indexOf(ficha), 1);
                                                    spendTeacher[idTeacher][day] -= 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "limitDay":
                                    days = restrictionsTeachers[idTeacher].limitDay;
                                    dayUsed = Object.keys(spendTeacher[idTeacher]);
                                    while (dayUsed.length > days) {
                                        i = randomNumber(true, dayUsed.length - 1);
                                        dayToDelete = dayUsed[i];
                                        for (const ficha of fichasDB) {
                                            if (ficha.id_dia == parseInt(dayToDelete) && res[1][ficha.id_leccion].teachers.includes(idTeacher)) {
                                                fichasNA.push({
                                                    id_horario: ficha.id_horario,
                                                    id_leccion: ficha.id_leccion,
                                                    ficha: ficha.ficha,
                                                    duracion: ficha.duracion,
                                                });
                                                fichasDB.splice(fichasDB.indexOf(ficha), 1);
                                            }
                                        }
                                        dayUsed.splice(i, 1);
                                    }
                                    break;
                            }
                        }
                    }
                    return [fichasDB, fichasNA];
                }
            }).catch((e) => {
                return [fichasDB, fichasNA];
            });
        }).catch((e) => {
            return [fichasDB, fichasNA];
        }).then((fichas) => {
            formData.append("camposObj", JSON.stringify(fichas[0]));
            saveItemsDB(langText, 5, formData, false, { name: "blm_tablero_principal" }).then((res) => console.log(res));
            formDataNA.append("camposObj", JSON.stringify(fichas[1]));
            saveItemsDB(langText, 5, formDataNA, false, { name: "blm_tablero_principal_na" }).then((res) => {
                closeWorkLand(msGObj);
                let fichasNoAcomodadas = new Array();
                for (const ficha of fichas[1]) {
                    if (!fichasNoAcomodadas.includes(`${ficha.id_leccion}->${ficha.ficha}`))
                        fichasNoAcomodadas.push(`${ficha.id_leccion}->${ficha.ficha}`);
                }
                let noAcomodadas = `${langText.msg_generar_horario_no_acomodadas} ${fichasNoAcomodadas.length}`;
                let noAcomodadasMsg = {
                    childrenConf: [false],
                    child: {
                        prefix: "boxNotify",
                        name: "NoAcomodadas",
                        father: boxNotifyGTerminada,
                        kind: "label",
                        innerHtml: noAcomodadas,
                        classes: ["m-1", "msg-box-Notify"],
                    },
                };
                newChild = buildNewElements(noAcomodadasMsg).child;
                let fechaHoraEnd = new Date();
                let timeSeconds = `${langText.msg_generar_horario_tiempo} ${Math.round((fechaHoraEnd.getTime() - fechaHoraStart.getTime()) / 1000)} ${langText.msg_seconds}`;
                let timer = {
                    childrenConf: [false],
                    child: {
                        prefix: "boxNotify",
                        name: "Timer",
                        father: boxNotifyGTerminada,
                        kind: "label",
                        innerHtml: timeSeconds,
                        classes: ["m-1", "msg-box-Notify"],
                    },
                };
                newChild = buildNewElements(timer).child;
                $("#ModalGTerminada").modal({
                    backdrop: "static",
                    keyboard: false,
                });
            });
        });
    }

    function tiempos_libres(leccion) {
        return new Promise(function (resolve, reject) {
            let tl = new Array();
            for (let dia = 1; dia <= horario.dias; dia++) {
                for (let hora = 1; hora <= horario.lecciones; hora++) {
                    tl.push({ dia: dia, hora: hora, d: true });
                }
            }
            let grupos = leccion.grupos;
            let profesores = leccion.profesores;
            table = {
                fields: ["*"],
            };
            table.name = "blm_tiempo_libre_cursos";
            table.conditions = {
                id_horario: ["=", horario.id],
                id_curso: ["=", leccion.id_curso],
            };
            searchItemsDB(table).then((res_c) => {
                for (const rc in res_c) {
                    if (Object.hasOwnProperty.call(res_c, rc)) {
                        tl.forEach((t) => {
                            if (res_c[rc].id_dia_tl == t.dia && res_c[rc].id_hora_tl == t.hora && res_c[rc].id_estado_tl == 2) {
                                t.d = false;
                            }
                        });
                    }
                }
                return tl;
            }).then((tl) => {
                let arrayPromise = new Array();
                for (let g = 0; g < grupos.length; g++) {
                    table.name = "blm_tiempo_libre_grupos";
                    table.conditions = {
                        id_horario: ["=", horario.id],
                        id_grupo: ["=", grupos[g]],
                    };
                    let promise = searchItemsDB(table).then((res_lg) => {
                        for (const rg in res_lg) {
                            if (Object.hasOwnProperty.call(res_lg, rg)) {
                                tl.forEach((t) => {
                                    if (res_lg[rg].id_dia_tl == t.dia && res_lg[rg].id_hora_tl == t.hora && res_lg[rg].id_estado_tl == 2) {
                                        t.d = false;
                                    }
                                });
                            }
                        }
                    }).catch((e) => {
                        console.log(e);
                    });
                    arrayPromise.push(promise);
                }
                return Promise.allSettled(arrayPromise).then(() => {
                    return tl;
                });
            }).then((tl) => {
                let arrayPromise = new Array();
                for (let pr = 0; pr < profesores.length; pr++) {
                    table.name = "blm_tiempo_libre_profesores";
                    table.conditions = {
                        id_horario: ["=", horario.id],
                        id_profesor: ["=", profesores[pr].id_profesor],
                    };
                    let promise = searchItemsDB(table).then((res_p) => {
                        for (const p in res_p) {
                            if (Object.hasOwnProperty.call(res_p, p)) {
                                tl.forEach((t) => {
                                    if (res_p[p].id_dia_tl == t.dia && res_p[p].id_hora_tl == t.hora && res_p[p].id_estado_tl == 2) {
                                        t.d = false;
                                    }
                                });
                            }
                        }
                    }).catch((e) => {
                        console.log(e);
                    });
                    arrayPromise.push(promise);
                }
                return Promise.allSettled(arrayPromise).then(() => {
                    resolve(tl);
                }).catch((e) => {
                    reject(e);
                });
            }).catch((e) => {
                reject(e);
            });
        });
    }

    function advertencias(cursoRes, aux2, relacion) {
        let auxrel = relacion.cursos;
        for (const key in auxrel) {
            if (Object.hasOwnProperty.call(auxrel, key)) {
                if (cursoRes.length > 0) {
                    for (let i = 0; i < cursoRes.length;) {
                        if (cursoRes[i].curso == auxrel[key]) {
                            if (!cursoRes[i].condiciones.includes(relacion.condicion)) {
                                let auxG = relacion.grupos;
                                if (relacion.estado_grupo == 1) {
                                    cursoRes[i].condiciones.push(relacion.condicion);
                                } else {
                                    for (let index = 0; index < auxG.length; index++) {
                                        if (cursoRes[i].grupos.includes(auxG[index])) {
                                            cursoRes[i].condiciones.push(relacion.condicion);
                                        } else {
                                            cursoRes.push({
                                                curso: auxrel[key],
                                                grupos: relacion.grupos,
                                                condiciones: [relacion.condicion],
                                            });
                                        }
                                    }
                                }
                            }
                            break;
                        } else {
                            if (!aux2.includes(auxrel[key])) {
                                aux2.push(auxrel[key]);
                                cursoRes.push({
                                    curso: auxrel[key],
                                    grupos: relacion.grupos,
                                    condiciones: [relacion.condicion],
                                });
                            }
                            i++;
                        }
                    }
                } else {
                    cursoRes.push({
                        curso: auxrel[key],
                        grupos: relacion.grupos,
                        condiciones: [relacion.condicion],
                    });
                }
            }
        }
        return cursoRes;
    }

    function jerarquiaAdvertencia(condicion) {
        let auxArr = condicion;
        for (let i = 0; i < condicion.length; i++) {
            let posicion = condicion[i];
            for (let j = 0; j < auxArr.length; j++) {
                comparar(posicion, auxArr[j]);
            }
        }
    }

    function comparar(posicion, compara) {
        if (posicion == compara) {
        } else if (posicion == 1) {
            switch (compara) {
                case "6":
                case "3":
                case "11":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 2) {
            switch (compara) {
                case "3":
                case "4":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 3) {
            switch (compara) {
                case "1":
                case "2":
                case "4":
                case "5":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 4) {
            switch (compara) {
                case "2":
                case "3":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 5) {
            switch (compara) {
                case "3":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 6) {
            switch (compara) {
                case "1":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 10) {
            switch (compara) {
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 11) {
            switch (compara) {
                case "1":
                    console.log("Aqui si se puede :D");
                    break;
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        } else if (posicion == 13) {
            switch (compara) {
                default:
                    console.log("aqui hay problemas revisar las restriciones");
            }
        }
    }

    function inicioFinal({ fichas, ocupados, lecciones, relaciones, relacionesTotales }) {
        console.log(relacionesTotales, relaciones);
        for (const leccionId of relaciones.leccionesId) {
            let arrTl = lecciones[leccionId].tl;
            let profes = lecciones[leccionId].profesores;
            let lecGroups = lecciones[leccionId].grupos;
            for (let f = 0; f < fichas.length; f++) {
                let cardsOnHold1 = new Array();
                if (fichas[f].leccion == leccionId && fichas[f].dia == null && fichas[f].hora == null && fichas[f].revisada == false) {
                    let dayR = daysRandom([], horario.dias, 0);
                    let agrego = [];
                    for (let rd = f; rd < fichas[f].duracion; rd++) {
                        fichas[rd].revisada = true;
                    }

                    for (let d = 0; d < dayR.length; d++) {
                        duracion1:
                        for (let fd = 1; fd <= fichas[f].duracion; fd++) {
                            let nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == fd && arrTl.d != false);
                            let ocupadosG;
                            let ocupadosP;
                            for (const grupo of lecGroups) {
                                ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == fd && ocupados.grupo == grupo);
                                if (ocupadosG != -1) {
                                    break;
                                }
                            }
                            for (const profe of profes) {
                                ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == fd && ocupados.profesor == profe.id_profesor);
                                if (ocupadosP != -1) {
                                    break;
                                }
                            }
                            if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1) {
                                let check = cardsOnHold1.length > 1 ? true : MismaMateriaMultipleRelacion({
                                    fichas: fichas,
                                    lecciones: lecciones,
                                    relaciones: relacionesTotales,
                                    relacion: relaciones,
                                    dia: dayR[d],
                                    hora: fd,
                                    horaFin: fichas[f].duracion > 1 ? fd + fichas[f].duracion - 1 : null,
                                    ficha: fichas[f]
                                });
                                console.log(`-------inicio ${check}`);
                                console.log(fichas[f]);

                                if (check) {
                                    cardsOnHold1.push({
                                        dia: dayR[d],
                                        hora: fd,
                                    });
                                }
                            } else {
                                cardsOnHold1 = [];
                                for (let fdx = horario.lecciones; fdx > horario.lecciones - fichas[f].duracion; fdx--) {
                                    nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == fdx && arrTl.d != false);
                                    let ocupadosG;
                                    let ocupadosP;
                                    for (const grupo of lecGroups) {
                                        ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == fdx && ocupados.grupo == grupo);
                                        if (ocupadosG != -1) {
                                            break;
                                        }
                                    }
                                    for (const profe of profes) {
                                        ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == fdx && ocupados.profesor == profe.id_profesor);
                                        if (ocupadosP != -1) {
                                            break;
                                        }
                                    }
                                    if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1) {
                                        let check = cardsOnHold1.length > 1 ? true : MismaMateriaMultipleRelacion({
                                            fichas: fichas,
                                            lecciones: lecciones,
                                            relaciones: relacionesTotales,
                                            relacion: relaciones,
                                            dia: dayR[d],
                                            hora: fdx,
                                            horaFin: fichas[f].duracion > 1 ? fdx + fichas[f].duracion - 1 : null,
                                            ficha: fichas[f]
                                        });
                                        console.log(fichas[f]);
                                        console.log(`-------final ${check}`);
                                        if (check) {
                                            cardsOnHold1.push({
                                                dia: dayR[d],
                                                hora: fdx,
                                            });
                                        } else {
                                            cardsOnHold1 = [];
                                            break duracion1;
                                        }
                                    } else {
                                        cardsOnHold1 = [];
                                        break;
                                    }
                                }
                            }
                        }
                        if (cardsOnHold1.length == fichas[f].duracion) {
                            let hInicioFicha = parseInt(cardsOnHold1[0].hora);
                            let hFinalFicha = parseInt(cardsOnHold1[0].hora) + parseInt(fichas[f].duracion) - 1;
                            for (const [index, fEsp] of cardsOnHold1.entries()) {
                                fichas[f].dia = fEsp.dia;
                                fichas[f].hora = fEsp.hora;
                                for (const lecG of lecGroups) {
                                    for (const lecP of profes) {
                                        ocupados.push({
                                            dia: fEsp.dia,
                                            hora: fEsp.hora,
                                            hora_inicio: hInicioFicha,
                                            hora_final: hFinalFicha,
                                            duracion: parseInt(fichas[f].duracion),
                                            ficha: fichas[f].ficha,
                                            leccion: fichas[f].leccion,
                                            curso: fichas[f].curso,
                                            grupo: lecG,
                                            profesor: lecP.id_profesor,
                                            id_relacion: relaciones.id
                                        });
                                    }
                                }
                                if (index + 1 < cardsOnHold1.length) {
                                    f++;
                                }
                            }
                            agrego.push(1);
                            break;
                        }
                        // if (agrego.length > 0) {
                        //     break;
                        // }
                    }
                }
            }
        }
        return true;
    }

    function daysRandom(dayRandom, dias, n) {
        if (n == dias) {
            return dayRandom;
        } else {
            let newDay = Math.floor(Math.random() * horario.dias) + 1;
            if (!dayRandom.includes(newDay)) {
                dayRandom.push(newDay);
                return daysRandom(dayRandom, dias, n + 1);
            } else {
                return daysRandom(dayRandom, dias, n);
            }
        }
    }

    function leccRandom(lecRandom, leccion, n) {
        if (n == leccion) {
            return lecRandom;
        } else {
            let newLec = Math.floor(Math.random() * horario.lecciones) + 1;
            if (!lecRandom.includes(newLec)) {
                lecRandom.push(newLec);
                return leccRandom(lecRandom, leccion, n + 1);
            } else {
                return leccRandom(lecRandom, leccion, n);
            }
        }
    }

    function constraintDiffDay({ fichas, ocupados, lecciones, relaciones, condition = 2, relacionesTotales }) {
        console.log('Entrooooo');
        let lecGroupsInc = relaciones.grupos;
        let leccionRelacion = relaciones.leccionesId;
        for (const leccionId of relaciones.leccionesId) {
            let arrTl = lecciones[leccionId].tl;
            let profes = lecciones[leccionId].profesores;
            let lecGroups = lecciones[leccionId].grupos;
            let read = []
            labelNewCard:
            for (let f = 0; f < fichas.length; f++) {
                let codeCard = `F${fichas[f].ficha}:L${fichas[f].leccion}`;
                let cardsOnHold = new Array();
                if (!read.includes(codeCard)) {
                    read.push(codeCard)
                    if (fichas[f].leccion == leccionId && fichas[f].dia == null && fichas[f].hora == null && fichas[f].revisada == false) {
                        for (let rd = f; rd < fichas[f].duracion; rd++) {
                            fichas[rd].revisada = true;
                        }
                        let dayR = daysRandom([], horario.dias, 0);
                        for (let d = 0; d < dayR.length; d++) {
                            let restrictDay;
                            if (relaciones.estado_grupo == "2") {
                                for (const cardG of lecGroups) {
                                    restrictDay = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.curso != fichas[f].curso && ocupados.grupo == cardG && leccionRelacion.includes(ocupados.leccion) && lecGroupsInc.includes(ocupados.grupo));
                                    if (restrictDay != -1) {
                                        break;
                                    }
                                }
                            } else {
                                for (const cardG of lecGroups) {
                                    restrictDay = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.curso != fichas[f].curso && ocupados.grupo == cardG && leccionRelacion.includes(ocupados.leccion));
                                    if (restrictDay != -1) {
                                        break;
                                    }
                                }
                            }
                            if (restrictDay == -1 || condition == 1) {
                                let lecR = leccRandom([], horario.lecciones, 0);
                                labelL:
                                for (let l = 0; l < lecR.length; l++) {
                                    if (lecR[l] + parseInt(fichas[f].duracion) - 1 <= parseInt(horario.lecciones)) {
                                        let nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora >= lecR[l] && arrTl.hora <= (lecR[l] + parseInt(fichas[f].duracion) - 1) && arrTl.d == false);
                                        let ocupadosG;
                                        let ocupadosP;
                                        if (nextAvailable != -1) {
                                            continue;
                                        }
                                        for (const grupo of lecGroups) {
                                            ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora >= lecR[l] && ocupados.hora <= (lecR[l] + parseInt(fichas[f].duracion) - 1) && ocupados.grupo == grupo);
                                            if (ocupadosG != -1) {
                                                continue;
                                            }
                                        }
                                        for (const profe of profes) {
                                            ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora >= lecR[l] && ocupados.hora <= (lecR[l] + parseInt(fichas[f].duracion) - 1) && ocupados.profesor == profe.id_profesor);
                                            if (ocupadosP != -1) {
                                                continue;
                                            }
                                        }
                                        if (nextAvailable == -1 && ocupadosG == -1 && ocupadosP == -1) {
                                            let lecL = lecR[l];
                                            let hInicioFicha = lecR[l];
                                            let hFinalFicha = lecR[l] + parseInt(fichas[f].duracion) - 1;
                                            let check = MismaMateriaMultipleRelacion({
                                                fichas: fichas,
                                                lecciones: lecciones,
                                                relaciones: relacionesTotales,
                                                relacion: relaciones,
                                                dia: dayR[d],
                                                hora: lecR[l],
                                                horaFin: fichas[f].duracion > 1 ? hFinalFicha : null,
                                                ficha: fichas[f]
                                            });
                                            console.log(check);
                                            if (!check) {
                                                continue;
                                            }
                                            for (let i = 0; i < parseInt(fichas[f].duracion); i++) {
                                                let sameCard = fichas.findIndex(ficha => ficha.ficha == fichas[f].ficha && ficha.leccion == fichas[f].leccion && ficha.dia == null && ficha.hora == null);
                                                fichas[sameCard].dia = dayR[d];
                                                fichas[sameCard].hora = lecL;
                                                for (const lecG of lecGroups) {
                                                    for (const lecP of profes) {
                                                        ocupados.push({
                                                            dia: dayR[d],
                                                            hora: lecL,
                                                            hora_inicio: hInicioFicha,
                                                            hora_final: hFinalFicha,
                                                            duracion: parseInt(fichas[f].duracion),
                                                            ficha: fichas[sameCard].ficha,
                                                            leccion: fichas[sameCard].leccion,
                                                            curso: fichas[sameCard].curso,
                                                            grupo: lecG,
                                                            profesor: lecP.id_profesor,
                                                            id_relacion: relaciones.id
                                                        });
                                                    }
                                                }
                                                lecL++;
                                            }
                                            continue labelNewCard;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    function noConsecutivas({ lecciones, fichas, ocupados, relaciones, relacionesTotales }) {
        let lecGroupsInc = relaciones.grupos;
        let leccionRelacion = relaciones.leccionesId;
        for (const leccionId of relaciones.leccionesId) {
            let arrTl = lecciones[leccionId].tl;
            let profes = lecciones[leccionId].profesores;
            let lecGroups = lecciones[leccionId].grupos;
            for (let f = 0; f < fichas.length; f++) {
                let cardsOnHold = new Array();
                if (fichas[f].leccion == leccionId && fichas[f].dia == null && fichas[f].hora == null && fichas[f].revisada == false) {
                    for (let rd = f; rd < fichas[f].duracion; rd++) {
                        fichas[rd].revisada = true;
                    }
                    let dayR = daysRandom([], horario.dias, 0);
                    let auxDay = 0;
                    let agrego = [];
                    for (let d = auxDay; d < dayR.length; d++) {
                        let lecR = leccRandom([], horario.lecciones, 0);
                        for (let l = 1; l <= lecR.length; l++) {
                            for (let fd = 0; fd < fichas[f].duracion; fd++) {
                                if (lecR[l] + parseInt(fichas[f].duracion) - 1 <= parseInt(horario.lecciones) || cardsOnHold.length > 0) {
                                    let nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == lecR[l] && arrTl.d != false);
                                    let horaAntes;
                                    let horaDespues;
                                    if (relaciones.estado_grupo == "2") {
                                        for (const grup of lecGroups) {
                                            horaAntes = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] - 1 && ocupados.curso != fichas[f].curso && leccionRelacion.includes(ocupados.leccion) && ocupados.grupo == grup && lecGroupsInc.includes(ocupados.grupo));
                                            horaDespues = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] + 1 && ocupados.curso != fichas[f].curso && leccionRelacion.includes(ocupados.leccion) && ocupados.grupo == grup && lecGroupsInc.includes(ocupados.grupo));
                                            if (horaAntes != -1 || horaDespues != -1) {
                                                break;
                                            }
                                        }
                                    } else {
                                        for (const grup of lecGroups) {
                                            horaAntes = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] - 1 && ocupados.curso != fichas[f].curso && leccionRelacion.includes(ocupados.leccion) && ocupados.grupo == grup);
                                            horaDespues = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] + 1 && ocupados.curso != fichas[f].curso && leccionRelacion.includes(ocupados.leccion) && ocupados.grupo == grup);
                                            if (horaAntes != -1 || horaDespues != -1) {
                                                break;
                                            }
                                        }
                                    }
                                    let ocupadosG;
                                    let ocupadosP;
                                    for (const grupo of lecGroups) {
                                        ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] && ocupados.grupo == grupo);
                                        if (ocupadosG != -1) {
                                            break;
                                        }
                                    }
                                    for (const profe of profes) {
                                        ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] && ocupados.profesor == profe.id_profesor);
                                        if (ocupadosP != -1) {
                                            break;
                                        }
                                    }
                                    if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1 && horaAntes == -1 && horaDespues == -1) {
                                        let check = cardsOnHold.length > 1 ? true : MismaMateriaMultipleRelacion({
                                            fichas: fichas,
                                            lecciones: lecciones,
                                            relaciones: relacionesTotales,
                                            relacion: relaciones,
                                            dia: dayR[d],
                                            hora: lecR[l],
                                            horaFin: fichas[f].duracion > 1 ? lecR[l] + fichas[f].duracion - 1 : null,
                                            ficha: fichas[f]
                                        });
                                        console.log(check);
                                        if (check) {
                                            cardsOnHold.push({
                                                dia: dayR[d],
                                                hora: lecR[l],
                                            });
                                            lecR[l]++;
                                        } else {
                                            cardsOnHold = [];
                                            break;
                                        }
                                    } else {
                                        cardsOnHold = [];
                                        break;
                                    }
                                }
                                if (cardsOnHold.length == fichas[f].duracion) {
                                    let hInicioFicha = parseInt(cardsOnHold[0].hora);
                                    let hFinalFicha = parseInt(cardsOnHold[0].hora) + parseInt(fichas[f].duracion) - 1;
                                    for (const [index, fEsp] of cardsOnHold.entries()) {
                                        fichas[f].dia = fEsp.dia;
                                        fichas[f].hora = fEsp.hora;
                                        for (const lecG of lecGroups) {
                                            for (const lecP of profes) {
                                                ocupados.push({
                                                    dia: fEsp.dia,
                                                    hora: fEsp.hora,
                                                    hora_inicio: hInicioFicha,
                                                    hora_final: hFinalFicha,
                                                    duracion: parseInt(fichas[f].duracion),
                                                    ficha: fichas[f].ficha,
                                                    leccion: fichas[f].leccion,
                                                    curso: fichas[f].curso,
                                                    grupo: lecG,
                                                    profesor: lecP.id_profesor,
                                                    id_relacion: relaciones.id
                                                });
                                            }
                                        }
                                        if (index + 1 < cardsOnHold.length) {
                                            f++;
                                        }
                                    }
                                    agrego.push(1);
                                    auxDay++;
                                    break;
                                }
                            }
                            if (agrego.length > 0) {
                                break;
                            }
                        }
                        if (agrego.length > 0) {
                            break;
                        }
                    }
                }
            }
        }
    }

    function MismoDia({ lecciones, fichas, ocupados, relaciones }) {
        let fichasLecciones = new Object();
        let pair = 0;
        let grupos = new Array();
        let cursos = new Array();
        for (const leccionId of relaciones.leccionesId) {
            grupos = quickSort(grupos.concat(lecciones[leccionId].grupos)).unique();
            cursos.push(lecciones[leccionId].id_curso);
            if (pair != 0) {
                pair = pair < lecciones[leccionId].sesiones_semana ? pair : lecciones[leccionId].sesiones_semana;
            } else {
                pair = lecciones[leccionId].sesiones_semana;
            }
            for (const ficha of fichas) {
                if (ficha.leccion === leccionId) {
                    if (fichasLecciones.hasOwnProperty(leccionId)) {
                        fichasLecciones[leccionId].push(ficha);
                    } else {
                        fichasLecciones[leccionId] = [ficha];
                    }
                }
            }
        }
        cursos = quickSort(cursos).unique();
        let matriz = new Object();
        for (const grupo of grupos) {
            let objCursos = new Object();
            for (const [i, curso] of cursos.entries()) {
                if (i < 2) {
                    objCursos[curso] = null;
                } else {
                    break;
                }
            }
            matriz[grupo] = objCursos;
        }
        let itemsNoPair = new Array();
        for (let index = 1; index <= pair; index++) {
            let daysRand = daysRandom([], horario.dias, 0);
            let hoursRand = leccRandom([], horario.lecciones, 0);
            let fichasGroup = new Object();
            for (const id in fichasLecciones) {
                ficha = fichasLecciones[id].filter((fichas) => fichas.ficha == index);
                fichasGroup[id] = ficha;
                for (const grupo of ficha[0].grupos) {
                    matriz[grupo][ficha[0].curso] = id;
                }
            }
            for (const idGrupo in matriz) {
                if (!Object.values(matriz[idGrupo]).includes(null)) {
                    let dayChoosed = new Object();
                    for (const idCurso in matriz[idGrupo]) {
                        let leccion = lecciones[matriz[idGrupo][idCurso]];
                        leccion.fichas = fichasGroup[matriz[idGrupo][idCurso]];
                        matriz[idGrupo][idCurso] = leccion;
                        dayChoosed[idCurso] = leccion.fichas[0].dia;
                    }
                    if (Object.values(dayChoosed).filter((item) => item == null).length == 2) {
                        throughMatriz({ matriz: matriz[idGrupo], daysRand: daysRand, hoursRand: hoursRand, ocupados: ocupados, fichas: fichas, relaciones: relaciones });
                    } else {
                        // console.log("Tiene valores de dia");
                        let dayBlock = Object.values(dayChoosed).filter((item) => item != null);
                        if (dayBlock.length == 1) {
                            let blockCourse;
                            dayBlock = dayBlock[0];
                            for (const course in dayChoosed) {
                                if (dayChoosed[course] == dayBlock) {
                                    blockCourse = course;
                                }
                            }
                            throughMatriz({ dayBlock: dayBlock, blockCourse: blockCourse, matriz: matriz[idGrupo], daysRand: daysRand, hoursRand: hoursRand, ocupados: ocupados, fichas: fichas, relaciones: relaciones });
                        } else {
                            // console.log("Ambos items estan acomodados");
                        }
                    }
                } else {
                    let itemAux = Object.values(matriz[idGrupo]).filter((item) => item != null)[0];
                    if (!itemsNoPair.includes(itemAux))
                        itemsNoPair.push(itemAux);
                }
                for (const idCurso in matriz[idGrupo]) {
                    matriz[idGrupo][idCurso] = null;
                }
            }
        }
        // console.log(itemsNoPair);
        for (const id of itemsNoPair) {
            // console.log(lecciones[id]);
        }
    }

    function consecutivasf({ fichas, ocupados, lecciones, relaciones }) {
        let pairCards = [];
        let readPair = [];
        for (const leccionId of relaciones.leccionesId) {
            let lessonsGroup = lecciones[leccionId].grupos;
            labelFichas:
            for (const iFichas of fichas) {
                if (iFichas.leccion == leccionId) {
                    let codeCard = `F${iFichas.ficha}:L${iFichas.leccion}`;
                    if (!readPair.includes(codeCard)) {
                        let searchToPair = -1;
                        for (const lessonG of lessonsGroup) {
                            searchToPair = pairCards.findIndex(pairC => !pairC['cursos'].includes(iFichas.curso) && pairC['grupos'].includes(lessonG));
                            if (searchToPair != -1) {
                                readPair.push(codeCard)
                                pairCards[searchToPair]['cursos'].push(iFichas.curso);
                                pairCards[searchToPair]['grupos'] = pairCards[searchToPair]['grupos'].concat(iFichas.grupos).unique();
                                pairCards[searchToPair]['fichas'].push(iFichas);
                                continue labelFichas;
                            }
                        }
                        readPair.push(codeCard)
                        pairCards.push({ 'cursos': [iFichas.curso], 'grupos': iFichas.grupos, 'fichas': [iFichas] });
                    }
                }
            }
        }
        for (let groupCards of pairCards) {
            let cardsUsed = [];
            let search = groupCards['fichas'].filter(gC => gC.dia != null && gC.hora != null);
            if (search.length > 0) {
                for (let srh of search) {
                    let searchHours = ocupados.findIndex(ocupados => ocupados.ficha == srh.ficha && ocupados.leccion == srh.leccion);
                    cardsUsed.push({ 'day': ocupados[searchHours].dia, 'firstHour': ocupados[searchHours].hora_inicio, 'lastHour': ocupados[searchHours].hora_final, 'card': srh });
                }
            }
            let allPairCards = groupCards['fichas'];
            if (allPairCards.length > 1) {
                let dayR = daysRandom([], horario.dias, 0);
                let lecR = leccRandom([], horario.lecciones, 0);

                inspectNewPair:
                for (let d = 0; d < dayR.length; d++) {
                    let cardsOnHold = [].concat(cardsUsed);
                    let ocupadosTemp = [];
                    for (let l = 0; l < lecR.length; l++) {
                        for (let i = 0; i < allPairCards.length; i++) {
                            if (allPairCards[i].dia == null && allPairCards[i].hora == null) {
                                let arrTl = lecciones[allPairCards[i].leccion].tl;
                                let teachersInspect = [];
                                for (let p of lecciones[allPairCards[i].leccion].profesores) {
                                    teachersInspect.push(p.id_profesor);
                                }
                                let groupsInspect = lecciones[allPairCards[i].leccion].grupos;
                                if (cardsOnHold.length == 0) {
                                    let rangeHour = lecR[l] + parseInt(allPairCards[i].duracion) - 1;
                                    let available = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora >= lecR[l] && arrTl.hora <= rangeHour && arrTl.d == false);
                                    if (available == -1 && rangeHour <= parseInt(horario.lecciones)) {
                                        let ocupadosA = validateAvailability(ocupados, ocupadosTemp, dayR[d], lecR[l], rangeHour, groupsInspect, teachersInspect);
                                        if (ocupadosA == true) {
                                            cardsOnHold.push({ 'day': dayR[d], 'firstHour': lecR[l], 'lastHour': rangeHour, 'card': allPairCards[i] });
                                            tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, dayR[d], lecR[l], parseInt(allPairCards[i].duracion));
                                        }
                                    }
                                } else {
                                    for (let cardsOH of cardsOnHold) {
                                        let dayCons = cardsOH.day;
                                        let firstHour = cardsOH.firstHour - parseInt(allPairCards[i].duracion); //Valida antes de la ficha con la que se compara
                                        let rangeFirstHour = firstHour + parseInt(allPairCards[i].duracion) - 1;
                                        let auxAvailableFirst = arrTl.findIndex((arrTl) => arrTl.dia == dayCons && arrTl.hora >= firstHour && arrTl.hora <= rangeFirstHour && arrTl.d == false);
                                        if (auxAvailableFirst == -1 && firstHour > 0) {
                                            let ocupadosA2 = validateAvailability(ocupados, ocupadosTemp, dayCons, firstHour, rangeFirstHour, groupsInspect, teachersInspect);
                                            if (ocupadosA2 == true) {
                                                cardsOnHold.push({ 'day': dayCons, 'firstHour': firstHour, 'lastHour': rangeFirstHour, 'card': allPairCards[i] });
                                                tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, dayCons, firstHour, parseInt(allPairCards[i].duracion));
                                                break;
                                            }
                                        } else {
                                            let lastHour = cardsOH.lastHour + 1; //Valida al final de la ficha con la que se compara 
                                            let rangeLastHour = lastHour + parseInt(allPairCards[i].duracion) - 1;
                                            let auxAvailableLast = arrTl.findIndex((arrTl) => arrTl.dia == dayCons && arrTl.hora >= lastHour && arrTl.hora <= rangeLastHour && arrTl.d == false);
                                            if (auxAvailableLast == -1 && rangeLastHour <= parseInt(horario.lecciones)) {
                                                let ocupadosA3 = validateAvailability(ocupados, ocupadosTemp, dayCons, lastHour, rangeLastHour, groupsInspect, teachersInspect);
                                                if (ocupadosA3 == true) {
                                                    cardsOnHold.push({ 'day': dayCons, 'firstHour': lastHour, 'lastHour': rangeLastHour, 'card': allPairCards[i] });
                                                    tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, dayCons, lastHour, parseInt(allPairCards[i].duracion));
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (cardsOnHold.length == allPairCards.length) {
                            for (let cards of cardsOnHold) {
                                if (cards['card'].dia == null && cards['card'].hora == null) {
                                    let mainCard = cards['card'];
                                    let day = cards.day;
                                    let hour = cards.firstHour;
                                    let hInicioFicha = cards.firstHour;
                                    let hFinalFicha = cards.lastHour;
                                    let sameCards = fichas.filter(fichas => fichas.ficha == mainCard.ficha && fichas.leccion == mainCard.leccion);
                                    for (const same of sameCards) {
                                        same.dia = day;
                                        same.hora = hour;
                                        for (const lecG of lecciones[mainCard.leccion].grupos) {
                                            for (const lecP of lecciones[mainCard.leccion].profesores) {
                                                ocupados.push({
                                                    dia: day,
                                                    hora: hour,
                                                    hora_inicio: hInicioFicha,
                                                    hora_final: hFinalFicha,
                                                    duracion: parseInt(mainCard.duracion),
                                                    ficha: mainCard.ficha,
                                                    leccion: mainCard.leccion,
                                                    curso: mainCard.curso,
                                                    grupo: lecG,
                                                    profesor: lecP.id_profesor,
                                                    id_relacion: relaciones.id
                                                });
                                            }
                                        }
                                        hour++;
                                    }
                                }
                            }
                            //Rompe el loop para inspeccionar un nuevo par.
                            break inspectNewPair;
                        } else {
                            ocupadosTemp = [];
                            cardsOnHold = [];
                        }
                    }
                }
            } else {
                //Si la ficha no tien alguna pareja, se saca
            }
        }
    }

    function constraintSimultaneousSubjects({ fichas, ocupados, lecciones, relaciones }) {
        let pairCards = [];
        let readPair = [];
        for (const leccionId of relaciones.leccionesId) {
            let lessonsGroup = lecciones[leccionId].grupos;

            readNewCard:
            for (const iFichas of fichas) {
                if (iFichas.leccion == leccionId) {
                    let codeCard = `F${iFichas.ficha}:L${iFichas.leccion}`;
                    if (!readPair.includes(codeCard)) {
                        let newPair = true;
                        for (const pair of pairCards) {
                            let addToPair = true;
                            for (const lessonG of lessonsGroup) {
                                if (pair['grupos'].includes(lessonG) || pair['cursos'].includes(iFichas.curso)) {
                                    addToPair = false; //Porque ya existe en ese lugar y no lo podría agregar ahí
                                    break
                                }
                            }
                            //Si en ese par no exite alguno de los grupos, entonces, eneaxa la ficha en ese par.
                            if (addToPair == true) {
                                readPair.push(codeCard)
                                pair['cursos'].push(iFichas.curso);
                                pair['grupos'] = pair['grupos'].concat(lessonsGroup).unique();
                                pair['fichas'].push(iFichas);
                                newPair = false
                                continue readNewCard;
                            }
                        }
                        //Terminó de leer, todos los pares y existe en todos, entonces, crea uno nuevo
                        if (pairCards.length == 0 || newPair == true) {
                            readPair.push(codeCard)
                            pairCards.push({ 'cursos': [iFichas.curso], 'grupos': lessonsGroup, 'fichas': [iFichas] });
                        }
                    }
                }
            }
        }

        for (let groupCards of pairCards) {
            let cardsUsed = [];
            let search = groupCards['fichas'].filter(gC => gC.dia != null && gC.hora != null);
            if (search.length > 0) {
                for (let srh of search) {
                    let searchHours = ocupados.findIndex(ocupados => ocupados.ficha == srh.ficha && ocupados.leccion == srh.leccion);
                    cardsUsed.push({ 'day': ocupados[searchHours].dia, 'firstHour': ocupados[searchHours].hora_inicio, 'lastHour': ocupados[searchHours].hora_final, 'card': srh });
                }
            }
            //Se puede cambiar para que utilice el inicio y el final de la fciha con mayor duración
            let allPairCards = groupCards['fichas'].sort((a, b) => { return a.value < b.value ? 1 : a.value > b.value ? -1 : 0 });
            if (allPairCards.length > 1) {
                let dayR = daysRandom([], horario.dias, 0);
                let lecR = leccRandom([], horario.lecciones, 0);

                inspectNewPair:
                for (let d = 0; d < dayR.length; d++) {
                    let cardsOnHold = [].concat(cardsUsed);
                    let ocupadosTemp = [];
                    for (let l = 0; l < lecR.length; l++) {
                        for (let i = 0; i < allPairCards.length; i++) {
                            if (allPairCards[i].dia == null && allPairCards[i].hora == null) {
                                let arrTl = lecciones[allPairCards[i].leccion].tl;
                                let teachersInspect = [];
                                for (let p of lecciones[allPairCards[i].leccion].profesores) {
                                    teachersInspect.push(p.id_profesor);
                                }
                                let groupsInspect = lecciones[allPairCards[i].leccion].grupos;
                                if (cardsOnHold.length == 0) {
                                    let rangeHour = lecR[l] + parseInt(allPairCards[i].duracion) - 1;
                                    let available = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora >= lecR[l] && arrTl.hora <= rangeHour && arrTl.d == false);
                                    if (available == -1 && rangeHour <= parseInt(horario.lecciones)) {
                                        let ocupadosA = validateAvailability(ocupados, ocupadosTemp, dayR[d], lecR[l], rangeHour, groupsInspect, teachersInspect);
                                        if (ocupadosA == true) {
                                            cardsOnHold.push({ 'day': dayR[d], 'firstHour': lecR[l], 'lastHour': rangeHour, 'card': allPairCards[i] });
                                            tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, dayR[d], lecR[l], parseInt(allPairCards[i].duracion));
                                        }
                                    }
                                } else {
                                    for (let cardsOH of cardsOnHold) {
                                        let dayCons = cardsOH.day;
                                        let firstHour = cardsOH.firstHour; //Valida antes de la ficha con la que se compara
                                        let rangeFirstHour = firstHour + parseInt(allPairCards[i].duracion) - 1;
                                        let auxAvailableFirst = arrTl.findIndex((arrTl) => arrTl.dia == dayCons && arrTl.hora >= firstHour && arrTl.hora <= rangeFirstHour && arrTl.d == false);
                                        if (auxAvailableFirst == -1 && rangeFirstHour <= parseInt(horario.lecciones)) {
                                            let ocupadosA2 = validateAvailability(ocupados, ocupadosTemp, dayCons, firstHour, rangeFirstHour, groupsInspect, teachersInspect);
                                            if (ocupadosA2 == true) {
                                                cardsOnHold.push({ 'day': dayCons, 'firstHour': firstHour, 'lastHour': rangeFirstHour, 'card': allPairCards[i] });
                                                tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, dayCons, firstHour, parseInt(allPairCards[i].duracion));
                                                break;
                                            }
                                        } else {
                                            let lastHour = cardsOH.lastHour - parseInt(allPairCards[i].duracion) + 1; //Valida al final de la ficha con la que se compara 
                                            let rangeLastHour = cardsOH.lastHour;
                                            let auxAvailableLast = arrTl.findIndex((arrTl) => arrTl.dia == dayCons && arrTl.hora >= lastHour && arrTl.hora <= rangeLastHour && arrTl.d == false);
                                            if (auxAvailableLast == -1 && rangeLastHour > 0) {
                                                let ocupadosA3 = validateAvailability(ocupados, ocupadosTemp, dayCons, lastHour, rangeLastHour, groupsInspect, teachersInspect);
                                                if (ocupadosA3 == true) {
                                                    cardsOnHold.push({ 'day': dayCons, 'firstHour': lastHour, 'lastHour': rangeLastHour, 'card': allPairCards[i] });
                                                    tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, dayCons, lastHour, parseInt(allPairCards[i].duracion));
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (cardsOnHold.length == allPairCards.length) {
                            for (let cards of cardsOnHold) {
                                if (cards['card'].dia == null && cards['card'].hora == null) {
                                    let mainCard = cards['card'];
                                    let day = cards.day;
                                    let hour = cards.firstHour;
                                    let hInicioFicha = cards.firstHour;
                                    let hFinalFicha = cards.lastHour;
                                    let sameCards = fichas.filter(fichas => fichas.ficha == mainCard.ficha && fichas.leccion == mainCard.leccion);
                                    for (const same of sameCards) {
                                        same.dia = day;
                                        same.hora = hour;
                                        for (const lecG of lecciones[mainCard.leccion].grupos) {
                                            for (const lecP of lecciones[mainCard.leccion].profesores) {
                                                ocupados.push({
                                                    dia: day,
                                                    hora: hour,
                                                    hora_inicio: hInicioFicha,
                                                    hora_final: hFinalFicha,
                                                    duracion: parseInt(mainCard.duracion),
                                                    ficha: mainCard.ficha,
                                                    leccion: mainCard.leccion,
                                                    curso: mainCard.curso,
                                                    grupo: lecG,
                                                    profesor: lecP.id_profesor,
                                                    id_relacion: relaciones.id
                                                });
                                            }
                                        }
                                        hour++;
                                    }
                                }
                            }
                            //Rompe el loop para inspeccionar un nuevo par.
                            break inspectNewPair;
                        } else {
                            ocupadosTemp = [];
                            cardsOnHold = [];
                        }
                    }
                }
            } else {
                //Si la ficha no tien alguna pareja, se saca
            }
        }
    }

    function mismaLeccion({ lecciones, fichas, ocupados, relaciones }) {
        for (const leccionId of relaciones.leccionesId) {
            let arrTl = lecciones[leccionId].tl;
            let profes = lecciones[leccionId].profesores;
            let lecGroups = lecciones[leccionId].grupos;
            let dh = new Array();
            for (let h = 1; h <= horario.lecciones; h++) {
                let contD = 0;
                let hmax = h + parseInt(lecciones[leccionId].leccion) - 1;
                for (let d = 1; d <= horario.dias; d++) {
                    let hoursOnHold = new Array();
                    for (let du = h; du <= hmax; du++) {
                        if (h + parseInt(lecciones[leccionId].leccion) - 1 <= parseInt(horario.lecciones)) {
                            let diaHora = arrTl.findIndex((arrTl) => arrTl.dia == d && arrTl.hora == du && arrTl.d != false);
                            let ocupadosG;
                            let ocupadosP;
                            for (const grupo of lecGroups) {
                                ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == d && ocupados.hora == du && ocupados.grupo == grupo);
                                if (ocupadosG != -1) {
                                    break;
                                }
                            }
                            for (const profe of profes) {
                                ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == d && ocupados.hora == du && ocupados.profesor == profe.id_profesor);
                                if (ocupadosP != -1) {
                                    break;
                                }
                            }
                            if (diaHora != -1 && ocupadosG == -1 && ocupadosP == -1) {
                                hoursOnHold.push(":D");
                            } else {
                                hoursOnHold = [];
                            }
                            if (hoursOnHold.length == lecciones[leccionId].leccion) {
                                contD++;
                            }
                        }
                    }
                }
                dh.push({ h: h, d: contD });
            }
            dh = orderDBresponseLR(Object.values(dh), "d", "number");
            for (let f = 0; f < fichas.length; f++) {
                if (fichas[f].leccion == leccionId && fichas[f].dia == null && fichas[f].hora == null && fichas[f].revisada == false) {
                    console.log('No Debe de entrar aqui :/');
                    for (let rd = f; rd < fichas[f].duracion; rd++) {
                        fichas[rd].revisada = true;
                    }
                    let cortardias = false;
                    let cardsOnHold = new Array();
                    let hmax = dh[dh.length - 1].h + parseInt(fichas[f].duracion) - 1;
                    let dayR = daysRandom([], horario.dias, 0);
                    for (let d = 0; d < dayR.length; d++) {
                        for (let du = dh[dh.length - 1].h; du <= hmax; du++) {
                            let nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == du && arrTl.d != false);
                            let ocupadosG;
                            let ocupadosP;
                            for (const grupo of lecGroups) {
                                ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == du && ocupados.grupo == grupo);
                                if (ocupadosG != -1) {
                                    break;
                                }
                            }
                            for (const profe of profes) {
                                ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == du && ocupados.profesor == profe.id_profesor);
                                if (ocupadosP != -1) {
                                    break;
                                }
                            }
                            if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1) {
                                cardsOnHold.push({
                                    dia: dayR[d],
                                    hora: du,
                                });
                            } else {
                                cardsOnHold = [];
                                break;
                            }
                            if (cardsOnHold.length == fichas[f].duracion) {
                                let hInicioFicha = parseInt(cardsOnHold[0].hora);
                                let hFinalFicha = parseInt(cardsOnHold[0].hora) + parseInt(fichas[f].duracion) - 1;
                                cortardias = true;
                                for (const [index, fEsp] of cardsOnHold.entries()) {
                                    fichas[f].dia = fEsp.dia;
                                    fichas[f].hora = fEsp.hora;
                                    for (const lecG of lecGroups) {
                                        for (const lecP of profes) {
                                            ocupados.push({
                                                dia: fEsp.dia,
                                                hora: fEsp.hora,
                                                hora_inicio: hInicioFicha,
                                                hora_final: hFinalFicha,
                                                duracion: parseInt(fichas[f].duracion),
                                                ficha: fichas[f].ficha,
                                                leccion: fichas[f].leccion,
                                                curso: fichas[f].curso,
                                                grupo: lecG,
                                                profesor: lecP.id_profesor,
                                                id_relacion: relaciones.id
                                            });
                                        }
                                    }
                                    if (index + 1 < cardsOnHold.length) {
                                        f++;
                                    }
                                }
                                break;
                            }
                        }
                        if (cortardias == true) {
                            break;
                        }
                    }
                }
            }
        }
    }

    function receso({ lecciones, fichas, ocupados, relaciones }) {
        let ocupadoR = new Array();
        table = {
            name: "blm_vista_horas_recesos",
            fields: ["*"],
            conditions: {
                id_horario: ["=", horario.id],
            },
        };
        let hrr = [];
        let promesa = searchItemsDB(table).then((hrs) => {
            return orderDBresponse(hrs, "inicio");
        }).then((sortData) => {
            if (Object.keys(sortData).length > 0) {
                let arrayPromise2 = new Array();
                let hr = [];
                sortData.forEach((element) => {
                    hr.push({
                        id: element.id,
                        nombre: element.nombre,
                        tipo: element.tipo,
                        nombreTimbre: element.nombre_timbre,
                        id_timbre: element.id_timbre,
                    });
                });
                for (const [index, h] of hr.entries()) {
                    if (h.tipo == 1) {
                        let grupos = [];
                        table = {
                            name: "blm_vista_timbres_grupos",
                            fields: ["*"],
                            conditions: {
                                id_timbre: ["=", h.id_timbre],
                            },
                        };
                        let promesa2 = searchItemsDB(table).then((grupo) => {
                            for (const g in grupo) {
                                if (Object.hasOwnProperty.call(grupo, g)) {
                                    grupos.push(grupo[g].id_grupo);
                                }
                            }
                            return grupos;
                        }).then((grupos) => {
                            hrr.push({
                                hora_a: hr[index - 1].nombre,
                                receso: h.nombre,
                                id_timbre: h.id_timbre,
                                grupos: grupos,
                            });
                            let hora = hr[index - 1].nombre;
                            for (let gpp = 0; gpp < grupos.length; gpp++) {
                                ocupadoR.push({
                                    grupo: parseInt(`${grupos[gpp]}`),
                                    hora: parseInt(`${hora.charAt(hora.length - 1)}`),
                                });
                            }
                            return ocupadoR;
                        }).catch((e) => {
                            return ocupadoR;
                        });
                        arrayPromise2.push(promesa2);
                    }
                }
                return Promise.allSettled(arrayPromise2).then((res) => {
                    for (const leccionId of relaciones.leccionesId) {
                        let arrTl = lecciones[leccionId].tl;
                        let profes = lecciones[leccionId].profesores;
                        let lecGroups = lecciones[leccionId].grupos;
                        for (let f = 0; f < fichas.length; f++) {
                            if (fichas[f].leccion == leccionId && fichas[f].dia == null && fichas[f].hora == null) {
                                let cardsOnHold = new Array();
                                let dayR = daysRandom([], horario.dias, 0);
                                let auxDay = 0;
                                let agrego = [];
                                for (let d = auxDay; d < dayR.length; d++) {
                                    let lecR = leccRandom([], horario.lecciones, 0);
                                    for (let l = 0; l < lecR.length; l++) {
                                        for (let fd = 0; fd < fichas[f].duracion; fd++) {
                                            if (lecR[l] + parseInt(fichas[f].duracion) - 1 <= parseInt(horario.lecciones) || cardsOnHold.length > 0) {
                                                let nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == lecR[l] && arrTl.d != false);
                                                let receso;
                                                let ocupadosG;
                                                let ocupadosP;
                                                for (const grupo of lecGroups) {
                                                    receso = ocupadoR.findIndex((ocupadoR) => ocupadoR.hora == lecR[l] && ocupadoR.grupo == parseInt(grupo));
                                                    if (receso != -1) {
                                                        fd = parseInt(fichas[f].duracion);
                                                        break;
                                                    }
                                                }
                                                if (parseInt(fichas[f].duracion) == 1) {
                                                    receso = -1;
                                                }
                                                for (const grupo of lecGroups) {
                                                    ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] && ocupados.grupo == grupo);
                                                    if (ocupadosG != -1) {
                                                        break;
                                                    }
                                                }
                                                for (const profe of profes) {
                                                    ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecR[l] && ocupados.profesor == profe.id_profesor);
                                                    if (ocupadosP != -1) {
                                                        break;
                                                    }
                                                }
                                                if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1 && receso == -1) {
                                                    cardsOnHold.push({
                                                        dia: dayR[d],
                                                        hora: lecR[l],
                                                    });
                                                    lecR[l]++;
                                                } else {
                                                    cardsOnHold = [];
                                                    break;
                                                }
                                            }
                                            if (cardsOnHold.length == fichas[f].duracion) {
                                                let hInicioFicha = parseInt(cardsOnHold[0].hora);
                                                let hFinalFicha = parseInt(cardsOnHold[0].hora) + parseInt(fichas[f].duracion) - 1;
                                                for (const [index, fEsp] of cardsOnHold.entries()) {
                                                    fichas[f].dia = fEsp.dia;
                                                    fichas[f].hora = fEsp.hora;
                                                    for (const lecG of lecGroups) {
                                                        for (const lecP of profes) {
                                                            ocupados.push({
                                                                dia: fEsp.dia,
                                                                hora: fEsp.hora,
                                                                hora_inicio: hInicioFicha,
                                                                hora_final: hFinalFicha,
                                                                duracion: parseInt(fichas[f].duracion),
                                                                ficha: fichas[f].ficha,
                                                                leccion: fichas[f].leccion,
                                                                curso: fichas[f].curso,
                                                                grupo: lecG,
                                                                profesor: lecP.id_profesor,
                                                                id_relacion: relaciones.id
                                                            });
                                                        }
                                                    }
                                                    if (index + 1 < cardsOnHold.length) {
                                                        f++;
                                                    }
                                                }
                                                agrego.push(1);
                                                // auxDay++;
                                                break;
                                            }
                                        }
                                        if (agrego.length > 0) {
                                            break;
                                        }
                                    }
                                    if (agrego.length > 0) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    return fichas;
                }).catch((e) => {
                    reject(e);
                });
            }
        });
        return Promise.allSettled([promesa]).then((rr) => {
            return fichas;
        });
    }

    function constraintCardDistribution({ fichas, ocupados, lecciones, relaciones, relacionesTotales }) {
        //console.log(fichas);
        let dataDistribution = Object.values(relaciones.distribucion);
        let lecGroupsInc = relaciones.grupos;
        let leccionRelacion = relaciones.leccionesId;
        let maxLesson = parseInt(dataDistribution[0].leccion_hasta);
        let minLesson = parseInt(dataDistribution[0].leccion_desde);
        let maxDay = parseInt(dataDistribution[0].ficha_hasta);
        //let minDay = parseInt(dataDistribution[0].ficha_desde);
        let diaDistribution = parseInt(dataDistribution[0].distribucion_dias_lecciones); //Opcion donde se activan los selectOpction, activado (1).
        let lecDistribucion = parseInt(dataDistribution[0].lecciones_consecutivos); //Las lecciones deben de ser consecutivas, activado (1).
        let diasConsecutivos = parseInt(dataDistribution[0].dias_consecutivos); // Las dos primeras opciones, la primera (0) y la segunda (1).
        if (diaDistribution == 0) {
            maxLesson = parseInt(horario.lecciones);
            minLesson = 1;
        }
        for (const leccionId of relaciones.leccionesId) {
            console.log("\n")
            console.log("\n")
            console.warn(leccionId == "1" ? "///////////// Instituto Bocar //////////////" : "//////////////Fisica/////////////")
            console.group();
            let daysPerLesson = [];
            let arrTl = lecciones[leccionId].tl;
            let profes = lecciones[leccionId].profesores;
            let lecGroups = lecciones[leccionId].grupos;
            let dayR = daysRandom([], horario.dias, 0);
            for (let d = 0; d < dayR.length; d++) {
                console.log("dia de distribucion: " + dayR[d])
                let lessonsComplete = { day: dayR[d], horasTotales: 0, cardsLessons: [] };
                let ocupadosTemp = [];
                //let indexAuxCons = -1;
                let lecR = [];
                if (lecDistribucion == 1 && maxLesson >= (parseInt(horario.lecciones) / 2)) { // Consecutivas // Las horas no serán random (serán consecutivas 1,2,3,...)
                    for (let index = 1; index <= horario.lecciones; index++) {
                        lecR.push(index);
                    }
                } else { // Se generan las horas random (3,1,2,...)
                    lecR = leccRandom([], horario.lecciones, 0)
                }
                //console.log(lecR)
                let l = 0;
                let f = 0;
                let zero = false;

                labelCards:
                for (f; f < fichas.length; f++) {
                    //console.log("¿Se repite la f? : " , f)
                    if (zero == true) {
                        f = 0;
                        zero = false;
                    }
                    //console.log("Valor de f al inicio del for: " + f)
                    if (diaDistribution == 1 && maxDay != 0 && daysPerLesson.length == maxDay) {
                        break;
                    }
                    if (diasConsecutivos == 1 && (daysPerLesson.includes(dayR[d] - 1) || daysPerLesson.includes(dayR[d] + 1))) {
                        break;
                    }
                    let readF = []
                    let cardsOnHold = new Array();
                    if (fichas[f].leccion == leccionId && fichas[f].dia == null && fichas[f].hora == null && fichas[f].revisada == false) {
                        fichas[f].revisada = true;
                        let codeCard = `F${fichas[f].ficha}:L${fichas[f].leccion}`;
                        if (!readF.includes(codeCard)) {
                            readF.push(codeCard)
                            let findF = lessonsComplete['cardsLessons'].findIndex(lC => lC.ficha == fichas[f].ficha)
                            if (findF != -1) {
                                continue;
                            }
                            //console.log(fichas[f])
                            let verificado = false;
                            let mayConsecutive = false;

                            for (l; l < lecR.length; l++) {
                                let ll = l;
                                console.log("hora de distribucion : " + lecR[ll])
                                //Se puede hacer el filtro de las fichas que se encuentran en el dia y agregar la suma de lecciones en el valor inicial de LessonsComplete
                                if (lecDistribucion == 1) {
                                    let readDistribution = consecutiveCardDistribution(ocupadosTemp, ocupados, lecGroups, profes, fichas[f], arrTl, verificado, mayConsecutive, leccionRelacion, lecGroupsInc, dayR, d, lecR, ll, horario, relaciones);
                                    verificado = readDistribution[0];
                                    mayConsecutive = readDistribution[1];
                                    d = readDistribution[2];
                                    ll = readDistribution[3];
                                }
                                let lecL = lecR[ll];
                                let findL = lessonsComplete['cardsLessons'].findIndex(lC => lC.dia == dayR[d] && lC.hora == lecL);
                                //console.log(lessonsComplete['cardsLessons'], dayR[d], lecL, findL)
                                if (findL != -1) {
                                    if ((ll + 1) < lecR.length) {
                                        continue
                                    }

                                }
                                //console.log("lecR[l] (index , valor): ", l, lecL)
                                let cardsAdd = lessonsComplete.cardsLessons; //Grupo de fichas para insertarse
                                for (let fd = 0; fd < fichas[f].duracion; fd++) {
                                    if (lecL + parseInt(fichas[f].duracion) - 1 <= parseInt(horario.lecciones) || cardsOnHold.length > 0) {
                                        let nextAvailable;
                                        let ocupadosG;
                                        let ocupadosP;
                                        let ocupadosGTemp;
                                        let ocupadosPTemp;
                                        nextAvailable = arrTl.findIndex((arrTl) => arrTl.dia == dayR[d] && arrTl.hora == lecL && arrTl.d != false);
                                        for (const grupo of lecGroups) {
                                            ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecL && ocupados.grupo == grupo);
                                            ocupadosGTemp = ocupadosTemp.findIndex((ocupadosT) => ocupadosT.dia == dayR[d] && ocupadosT.hora == lecL && ocupadosT.grupo == grupo);
                                            if (ocupadosG != -1 || ocupadosGTemp != -1) {
                                                break;
                                            }
                                        }
                                        for (const profe of profes) {
                                            ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == dayR[d] && ocupados.hora == lecL && ocupados.profesor == profe.id_profesor);
                                            ocupadosPTemp = ocupadosTemp.findIndex((ocupadosT) => ocupadosT.dia == dayR[d] && ocupadosT.hora == lecL && ocupadosT.profesor == profe.id_profesor);
                                            if (ocupadosP != -1 || ocupadosPTemp != -1) {
                                                break;
                                            }
                                        }
                                        if (((lecDistribucion == 1 && mayConsecutive == true) || lecDistribucion == 0)) {
                                            if (nextAvailable != -1 && ocupadosG == -1 && ocupadosP == -1 && ocupadosGTemp == -1 && ocupadosPTemp == -1) {
                                                let check = cardsOnHold.length > 0 ? true : MismaMateriaMultipleRelacion({
                                                    fichas: fichas,
                                                    lecciones: lecciones,
                                                    relaciones: relacionesTotales,
                                                    relacion: relaciones,
                                                    dia: dayR[d],
                                                    hora: lecL,
                                                    horaFin: lecL + parseInt(fichas[f].duracion) - 1,
                                                    ficha: fichas[f],
                                                    ocupadosTemp: ocupadosTemp,
                                                });
                                                if (check) {
                                                    cardsOnHold.push({
                                                        dia: dayR[d],
                                                        hora: lecL,
                                                    });
                                                } else {
                                                    //cardsOnHold = [];
                                                    break;
                                                }
                                                lecL++;
                                            } else {
                                                cardsOnHold = [];
                                                break;
                                            }
                                        }
                                    }
                                }

                                //console.log("Valor de f antes de iteracion " + f)
                                if ((cardsOnHold.length == fichas[f].duracion) && ((lecDistribucion == 1 && mayConsecutive == true) || lecDistribucion == 0)) {
                                    let sumLessons = lessonsComplete.horasTotales + parseInt(fichas[f].duracion);
                                    let hInicioFicha = parseInt(cardsOnHold[0].hora);
                                    let hFinalFicha = parseInt(cardsOnHold[0].hora) + parseInt(fichas[f].duracion) - 1;
                                    if (sumLessons <= maxLesson) {
                                        lessonsComplete.horasTotales = sumLessons;
                                        //console.log("Guarda en cardsAss con leccion: " + leccionId, lessonsComplete, sumLessons, mayConsecutive)
                                        for (const [index, fEsp] of cardsOnHold.entries()) {
                                            cardsAdd.push({ ficha: fichas[f].ficha, leccion: fichas[f].leccion, dia: fEsp.dia, hora: fEsp.hora, hora_inicio: hInicioFicha, hora_final: hFinalFicha, duracion: fichas[f].duracion, curso: fichas[f].curso });
                                            for (const lecG of lecGroups) {
                                                for (const lecP of profes) {
                                                    ocupadosTemp.push({
                                                        dia: fEsp.dia,
                                                        hora: fEsp.hora,
                                                        hora_inicio: hInicioFicha,
                                                        hora_final: hFinalFicha,
                                                        duracion: parseInt(fichas[f].duracion),
                                                        ficha: fichas[f].ficha,
                                                        leccion: fichas[f].leccion,
                                                        curso: fichas[f].curso,
                                                        grupo: lecG,
                                                        profesor: lecP.id_profesor,
                                                        id_relacion: relaciones.id
                                                    });
                                                }
                                            }
                                            if (index + 1 < cardsOnHold.length) {
                                                f++;
                                            }
                                        }
                                    }
                                }
                                //console.log("Valor de f despues de iteracion " + f)
                                let compareSum = lessonsComplete.horasTotales;
                                let fichasB = fichas.findIndex(fichaB => fichaB.ficha > fichas[f].ficha && fichaB.leccion == fichas[f].leccion);
                                //Cuando existe algun error pero puede que no este completo y necesita validar si puede cuardar o necesita validar otro día.
                                //console.log(lecDistribucion, mayConsecutive, fichasB, l, lecR.length - 1, compareSum, maxLesson)
                                if ((lecDistribucion == 1 && mayConsecutive == false) || fichasB == -1 || l == (lecR.length - 1) || (compareSum == maxLesson)) {
                                    //console.log("Valida si es apto para guardarse")
                                    if (compareSum >= minLesson && compareSum <= maxLesson) {
                                        //console.log("Guarda en el arreglo de fichas con leccion:" + leccionId, lessonsComplete, compareSum, mayConsecutive)

                                        daysPerLesson.push(lessonsComplete.day);
                                        console.log(cardsAdd)
                                        for (const cards of cardsAdd) {
                                            let cardChange = fichas.findIndex(arr => arr.ficha == cards.ficha && arr.leccion == cards.leccion && arr.dia == null && arr.hora == null);
                                            //console.log(cards.ficha,cards.leccion, cardChange)
                                            console.log(`Se guarda la ficha en dia ${cards.dia}, hora ${cards.hora}`)
                                            fichas[cardChange].dia = cards.dia;
                                            fichas[cardChange].hora = cards.hora;
                                            for (const lecG of lecGroups) {
                                                for (const lecP of profes) {
                                                    ocupados.push({
                                                        dia: cards.dia,
                                                        hora: cards.hora,
                                                        hora_inicio: cards.hora_inicio,
                                                        hora_final: cards.hora_final,
                                                        duracion: parseInt(cards.duracion),
                                                        ficha: cards.ficha,
                                                        leccion: cards.leccion,
                                                        curso: cards.curso,
                                                        grupo: lecG,
                                                        profesor: lecP.id_profesor,
                                                        id_relacion: relaciones.id
                                                    });
                                                }
                                            }
                                        }
                                        break labelCards;
                                    }
                                }
                                if (lecDistribucion == 1 && mayConsecutive == false) {
                                    //console.log("Vuelte todo a los valores de inicio")
                                    lessonsComplete = { day: dayR[d], horasTotales: 0, cardsLessons: [] };
                                    ocupadosTemp = [];
                                    zero = true;
                                }
                                if (cardsOnHold.length == 0) {
                                    //console.log("Solamente fichas f a 0")
                                    zero = true;
                                }
                                //console.log("Este es el valor de l antes "+l, lecR.length)
                                l++;
                                //console.log("Este es el valor de l despues "+l, lecR.length)
                                continue labelCards;
                            }
                            if ((l == lecR.length) || ((lecDistribucion == 1) && (mayConsecutive == false))) {
                                break;
                            }
                        }
                    }
                }
            }
            console.groupEnd();
        }
    }
    /// Aqui esta Carlo
    function MismaMateriaMultipleRelacion({ fichas, lecciones, relaciones, relacion, dia, hora, horaFin, ficha, ocupadosTemp }) {
        console.log({ fichas, lecciones, relaciones, relacion, dia, hora, horaFin, ficha });
        let relacionesCheck = {
            1: checkDiffDay,
            2: checknoConsecutivas,
            3: checkconstraintCardDistribution,
            4: checkMismoDia,
            5: checkconsecutivasf,
            //6: checkreceso,
            10: checkconstraintSimultaneousSubjects,
            11: checkmismaLeccion,
            13: checkinicioFinal,
        };
        let check = new Array();
        let parametro = { dia: dia, hora: hora, horaFin: horaFin, cursoActual: ficha, fichas: fichas, ocupadosTemp: ocupadosTemp };
        //Relaciones diferentes a la actual
        let relacionesCheckFilter = relaciones.filter(relacionT => relacionT.leccionesId.includes(ficha.leccion) && relacionT.id != relacion.id);
        for (const relacionCF of relacionesCheckFilter) {
            parametro.relacion = relacionCF;
            parametro.lecciones = lecciones;
            let BanderasRelaciones = relacionesCheck[relacionCF.condicion](parametro);
            check.push(BanderasRelaciones);
        }
        console.log(check);
        return check.includes(false) ? false : true;
    }

    function checkDiffDay({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones }) {
        console.log({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones });
        let lecGroupsInc = relacion.grupos;
        let leccionRelacion = relacion.leccionesId;
        let lecGroups = lecciones[cursoActual.leccion].grupos;
        if (relacion.estado_grupo == "2") {
            for (const cardG of lecGroups) {
                let restrictDay = fichas.findIndex((fichaEval) => fichaEval.dia == dia && fichaEval.curso != cursoActual.curso && fichaEval.grupos.includes(cardG) && leccionRelacion.includes(fichaEval.leccion) && lecGroupsInc.includes(fichaEval.grupo));
                if (restrictDay != -1) {
                    return false;
                }
            }
        } else {
            for (const cardG of lecGroups) {
                let restrictDay = fichas.findIndex((fichaEval) => fichaEval.dia == dia && fichaEval.curso != cursoActual.curso && fichaEval.grupos.includes(cardG) && leccionRelacion.includes(fichaEval.leccion));
                if (restrictDay != -1) {
                    return false;
                }
            }
        }
        console.log('checkDiffDay');
        return true;
    }

    function checknoConsecutivas({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones }) {
        console.log('Check No Consecutivas', relacion);
        let flagGrupos = false;
        for (const id of cursoActual.grupos) {
            if (relacion.estado_grupo == 1) {
                let filtroNoConsecutivas = fichas.findIndex(fichas => !relacion.cursos.includes(fichas.curso) && fichas.hora >= hora - 1 && horaFin + 1 <= fichas.hora && fichas.grupos.includes(id));
                console.log(filtroNoConsecutivas);
                if (filtroNoConsecutivas == -1) {
                    flagGrupos = true;
                    break;
                }
            } else {
                if (relacion.grupos.includes(id)) {
                    let filtroNoConsecutivas = fichas.findIndex(fichas => !relacion.cursos.includes(fichas.curso) && fichas.hora >= hora - 1 && horaFin + 1 <= fichas.hora && fichas.grupos.includes(id));
                    if (filtroNoConsecutivas == -1) {
                        flagGrupos = true;
                        break;
                    }
                }
            }
        }
        return flagGrupos;
    }

    function checkconstraintCardDistribution({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones }) {
        // return data = { dia: dia, hora: hora, cursoActual: cursoActual, curso: curso };
    }

    function checkMismoDia({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones }) {
        console.log('checkMismoDia', dia);
        let flagGrupos = false;
        for (const id of cursoActual.grupos) {
            if (relacion.estado_grupo == 1) {
                let filtroMismoDia = fichas.findIndex(fichas => relacion.cursos.includes(fichas.curso) && dia == fichas.dia && fichas.grupos.includes(id));
                console.log(filtroMismoDia);
                if (filtroMismoDia == -1) {
                    flagGrupos = true;
                    break;
                }
            } else {
                if (relacion.grupos.includes(id)) {
                    let filtroMismoDia = fichas.findIndex(fichas => relacion.cursos.includes(fichas.curso) && dia == fichas.dia && fichas.grupos.includes(id));
                    if (filtroMismoDia == -1) {
                        flagGrupos = true;
                        break;
                    }
                }
            }
        }
        return flagGrupos;

    }

    function checkconsecutivasf({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones, ocupadosTemp }) {
        console.log('\n')
        console.log("&&&&&&&&&&&&&&&&&&&&&&& inicio de check consecutivas &&&&&&&&&&&&&&&&&&&&&&&&", ocupadosTemp)
        let lecGroupsInc = relacion.grupos;
        let leccionRelacion = relacion.leccionesId;
        let lecGroups = lecciones[cursoActual.leccion].grupos;
        let filterPosiblePairs = [];
        let verificar = true
        labelGrupos:
        for (let cardG of lecGroups) {
            let read = [];
            if (relacion.estado_grupo == "2") {
                filterPosiblePairs = fichas.filter((fichas) => fichas.leccion != cursoActual.leccion && fichas.dia != null && fichas.grupos.includes(cardG) && leccionRelacion.includes(fichas.leccion) && lecGroupsInc.includes(fichas.grupo));
                console.log(`Grupo( ${cardG} ) filtro de coincidencias posiblepairs`, filterPosiblePairs)
            } else {
                filterPosiblePairs = fichas.filter((fichas) => fichas.leccion != cursoActual.leccion && fichas.dia != null && fichas.grupos.includes(cardG) && leccionRelacion.includes(fichas.leccion));
                console.log(`Grupo( ${cardG} ) filtro de coincidencias posiblepairs`, filterPosiblePairs)
            }
            if (filterPosiblePairs.length == 0) {
                console.log("No hay fichas con para comparar, entonces es: ", true)
                verificar = true;
            } else {
                for (let filterP of filterPosiblePairs) {
                    if (!read.includes(`F${filterP.ficha}:L${filterP.leccion}`)) {
                        read.push(`F${filterP.ficha}:L${filterP.leccion}`);
                        if (filterP.dia == dia) {
                            console.log("Coincide el dia filter: ", filterP.dia, " con dia de entrada: ", dia)
                            let before = hora - 1;
                            console.log("Hora / Before ", hora, before)
                            let searchBefore = fichas.findIndex(fichas => fichas.hora == before && fichas.leccion == filterP.leccion && fichas.ficha == filterP.ficha);
                            let searchBeforeTemp = ocupadosTemp.findIndex(ocupadosT => ocupadosT.hora == before && ocupadosT.leccion == cursoActual.leccion /* && ocupadosT.ficha == cursoActual.ficha */);
                            if (searchBefore != -1 || searchBeforeTemp != -1) {
                                console.log("Valida si antes hay una ficha, y estoy en hora " + hora, fichas[searchBefore], ocupadosTemp[searchBeforeTemp])
                                verificar = true
                                break labelGrupos;
                            } else {
                                let after = parseInt(cursoActual.duracion) + hora;
                                console.log("Hora / After ", hora, after)
                                let searchAfter = fichas.findIndex(fichas => fichas.hora == after && fichas.leccion == filterP.leccion && fichas.ficha == filterP.ficha);
                                let searchAfterTemp = ocupadosTemp.findIndex(ocupadosT => ocupadosT.hora == after && ocupadosT.leccion == cursoActual.leccion /* && ocupadosT.ficha == cursoActual.ficha */);
                                if (searchAfter != -1 || searchAfterTemp != -1) {
                                    console.log("Valida si Despues hay una ficha, y estoy en hora " + hora, fichas[searchAfter], ocupadosTemp[searchAfterTemp])
                                    verificar = true
                                    break labelGrupos;
                                } else {
                                    console.log(searchBeforeTemp, before, searchAfterTemp, after, cursoActual.leccion, cursoActual.ficha)
                                    console.log("Desde la hora" + hora + "no encontro antes ni despues: ", false)
                                    verificar = false
                                }
                            }
                        } else {
                            console.log("No coincide el dia filter: ", filterP.dia, " con dia de entrada: ", dia, " - ", false)
                            verificar = false;
                        }
                    }
                }
            }
        }
        console.log("&&&&&&&&&&&&&&&&&&&&&&&&& fin de check consecutivas &&&&&&&&&&&&&&&&&&&&&&&&&");
        console.log('\n')
        return verificar == true ? true : false;
    }

    function checkconsecutivasf2({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones }) {
        let lecGroupsInc = relacion.grupos;
        let leccionRelacion = relacion.leccionesId;
        let lecGroups = lecciones[cursoActual.leccion].grupos;
        let arrTl = lecciones[leccionId].tl;
        let searchCourse = fichas.findIndex(fichas => fichas.dia == dia && fichas.leccion == cursoActual.leccion);
        let consecutive = false;
        if (searchCourse != -1) {
            let filterPosiblePairs = [];
            let read = [];
            labelVerificar:
            for (let cardG of lecGroups) {
                if (relaciones.estado_grupo == "2") {
                    filterPosiblePairs = fichas.filter((fichas) => fichas.dia == dayR[d] && fichas.grupo == cardG && leccionRelacion.includes(fichas.leccion) && lecGroupsInc.includes(ocupadosT.grupo));
                } else {
                    filterPosiblePairs = fichas.filter((fichas) => fichas.dia == dayR[d] && fichas.grupo == cardG && leccionRelacion.includes(fichas.leccion));
                }
                if (filterPosiblePairs.length > 0) {
                    for (let cons of filterPosiblePairs) {
                        if (!read.includes(`F${iFichas.ficha}:L${iFichas.leccion}`)) {
                            read.push(`F${iFichas.ficha}:L${iFichas.leccion}`);
                            let diaCons = cons.dia;
                            let horaMen = cons.hora_inicio;
                            let duracionF = parseInt(cursoActual.duracion)
                            let lBefore = lecR.indexOf(cons.hora_inicio - duracionF);
                            let lAfter = lecR.indexOf(cons.hora_inicio + parseInt(cons.duracion));
                            let tlBefore = arrTl.findIndex((arrTl) => arrTl.dia == diaCons && arrTl.hora >= lecR[lBefore] && arrTl.hora < horaMen && arrTl.d == false); //antes... true
                            let ocupadosG;
                            let ocupadosP;
                            let profesores = [];
                            for (const p of profes) {
                                profesores.push(p.id_profesor);
                            }
                            if (tlBefore == -1) { //Era != -1
                                ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lBefore] && ocupados.hora < horaMen && lecGroups.includes(ocupados.grupo));
                                ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lBefore] && ocupados.hora < horaMen && profesores.includes(ocupados.profesor));
                            }
                            if (lecR[lBefore] > 0 && lecR[lBefore] < horaMen && tlBefore == -1 && ocupadosP == -1 && ocupadosG == -1) { //tlBefore era... != -1
                                d = dayR.indexOf(cons.dia);
                                l = lBefore;
                                consecutive = true;
                                break labelVerificar;
                            } else {
                                let tlAfter = arrTl.findIndex((arrTl) => arrTl.dia == diaCons && arrTl.hora >= lecR[lAfter] && arrTl.hora <= (lecR[lAfter] + duracionF - 1) && arrTl.d == false);
                                let ocupadosGG;
                                let ocupadosPP;
                                if (tlAfter == -1) {
                                    ocupadosGG = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lAfter] && ocupados.hora <= (lecR[lAfter] + duracionF - 1) && lecGroups.includes(ocupados.grupo));
                                    ocupadosPP = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lAfter] && ocupados.hora <= (lecR[lAfter] + duracionF - 1) && profesores.includes(ocupados.profesor));
                                }
                                if (lecR[lAfter] + parseInt(cursoActual.duracion) - 1 <= parseInt(horario.lecciones) && tlAfter == -1 && ocupadosPP == -1 && ocupadosGG == -1) {
                                    d = dayR.indexOf(cons.dia);
                                    l = lAfter;
                                    consecutive = true;
                                    break labelVerificar;
                                } else {
                                    mayConsecutive = false;
                                    verificado = false;
                                }
                            }
                        }
                    }

                }
            }
        } else {
            consecutive = true;
        }
        return true;
    }

    function checkconstraintSimultaneousSubjects({ dia, hora, cursoActual, curso, CursosEvaluados }) {
        return data = { dia: dia, hora: hora, cursoActual: cursoActual, curso: curso };
    }

    function checkmismaLeccion({ dia, hora, cursoActual, cursos, lecciones, relacion, fichas }) {
        let cursosArr = new Array();
        console.log('entra a misma leccion');
        let flagGrupos = false;
        for (const id of cursoActual.grupos) {
            let horaPiloto = fichas.findIndex(fichas => fichas.curso == cursoActual.curso && fichas.grupos.includes(id) && fichas.hora != null);
            let horaP;
            if (horaPiloto != -1) {
                horaP = fichas[horaPiloto].hora;
            }
            console.log(horaP, cursoActual, dia, hora, relacion.grupo);
            if (relacion.estado_grupo == 1) {
                console.log('Todos los grupos');
                if (horaPiloto != -1) {
                    if (hora == horaP) {
                        flagGrupos = true;
                        break;
                    }
                } else {
                    flagGrupos = true;
                }
            } else {
                console.log('Grupos especificos');
                if (relacion.grupo.includes(id)) {
                    if (horaPiloto != -1) {
                        if (hora == horaP) {
                            flagGrupos = true;
                            break;
                        }
                    } else {
                        flagGrupos = true;
                    }
                }
            }
        }
        return flagGrupos;
        // return data = { dia: dia, hora: hora, cursoActual: cursoActual, curso: curso };
    }

    function checkinicioFinal({ dia, hora, horaFin, cursoActual, fichas, relacion, lecciones }) {
        console.log('checkInicioFinal', hora, horaFin);
        let flagGrupos = false;
        for (const id of cursoActual.grupos) {
            if (relacion.estado_grupo == 1) {
                if (hora == 1 || horaFin == horario.lecciones) {
                    flagGrupos = true;
                    break;
                }
            } else {
                if (relacion.grupos.includes(id)) {
                    let filtroMismoDia = fichas.findIndex(fichas => relacion.cursos.includes(fichas.curso) && dia == fichas.dia && fichas.grupos.includes(id));
                    if (hora == 1 || horaFin == horario.lecciones) {
                        flagGrupos = true;
                        break;
                    }
                }
            }
        }
        return flagGrupos;
    }

    muyGrandeGNuevHorario.addEventListener('change', e => {
        if (muyGrandeGNuevHorario.checked == true) {
            contenedorVueltasGNuevHorario.classList.remove('oculto');
        } else {
            contenedorVueltasGNuevHorario.classList.add('oculto');
        }
    });

    normalGNuevHorario.addEventListener('change', e => {
        if (normalGNuevHorario.checked == true) {
            contenedorVueltasGNuevHorario.classList.add('oculto');
        }
    });

    grandeGNuevHorario.addEventListener('change', e => {
        if (grandeGNuevHorario.checked == true) {
            contenedorVueltasGNuevHorario.classList.add('oculto');
        }
    });
});

function validateAvailability(ocupados, ocupadosTemp, day, firstHour, lastHour, groups, teachers) {
    let ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == day && ocupados.hora >= firstHour && ocupados.hora <= lastHour && groups.includes(ocupados.grupo));
    let ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == day && ocupados.hora >= firstHour && ocupados.hora <= lastHour && teachers.includes(ocupados.profesor));
    let ocupadosGTemp = ocupadosTemp.findIndex((ocupadosTemp) => ocupadosTemp.dia == day && ocupadosTemp.hora >= firstHour && ocupadosTemp.hora <= lastHour && groups.includes(ocupadosTemp.grupo));
    let ocupadosPTemp = ocupadosTemp.findIndex((ocupadosTemp) => ocupadosTemp.dia == day && ocupadosTemp.hora >= firstHour && ocupadosTemp.hora <= lastHour && teachers.includes(ocupadosTemp.profesor));
    if (ocupadosG == -1 && ocupadosP == -1 && ocupadosGTemp == -1 && ocupadosPTemp == -1) {
        return true
    } else {
        return false
    }
    //return [ocupadosG, ocupadosP, ocupadosGTemp, ocupadosPTemp];
}

function tempOcupados(ocupadosTemp, groupsInspect, teachersInspect, day, hour, duration) {
    let hora = hour;
    for (let index = 0; index < duration; index++) {
        for (const lecG of groupsInspect) {
            for (const lecP of teachersInspect) {
                ocupadosTemp.push({
                    dia: day,
                    hora: hora,
                    grupo: lecG,
                    profesor: lecP
                });
            }
        }
        hora++;
    }
    return ocupadosTemp
}

function consecutiveCardDistribution(ocupadosTemp, ocupados, lecGroups, profes, fichaF, arrTl, verificado, mayConsecutive, leccionRelacion, lecGroupsInc, dayR, d, lecR, l, horario, relaciones) {
    let consecutive = [];

    labelVerificar:
    for (let cardG of lecGroups) {
        if (relaciones.estado_grupo == "2") {
            consecutive = ocupadosTemp.filter((ocupadosT) => ocupadosT.curso == fichaF.curso && ocupadosT.dia == dayR[d] && ocupadosT.grupo == cardG && leccionRelacion.includes(ocupadosT.leccion) && lecGroupsInc.includes(ocupadosT.grupo));
        } else {
            consecutive = ocupadosTemp.filter((ocupadosT) => ocupadosT.curso == fichaF.curso && ocupadosT.dia == dayR[d] && ocupadosT.grupo == cardG && leccionRelacion.includes(ocupadosT.leccion));
        }
        //console.log(consecutive)
        if (consecutive.length != 0) {
            for (let cons of consecutive) {
                let diaCons = cons.dia;
                let horaMen = cons.hora_inicio;
                let duracionF = parseInt(fichaF.duracion)
                let lBefore = lecR.indexOf(cons.hora_inicio - duracionF);
                let lAfter = lecR.indexOf(cons.hora_inicio + parseInt(cons.duracion));
                let tlBefore = arrTl.findIndex((arrTl) => arrTl.dia == diaCons && arrTl.hora >= lecR[lBefore] && arrTl.hora < horaMen && arrTl.d == false); //antes... true
                let ocupadosG;
                let ocupadosP;
                let ocupadosGTemp;
                let ocupadosPTemp;
                let profesores = [];
                for (const p of profes) {
                    profesores.push(p.id_profesor);
                }
                if (tlBefore == -1) { //Era != -1
                    ocupadosG = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lBefore] && ocupados.hora < horaMen && lecGroups.includes(ocupados.grupo));
                    ocupadosP = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lBefore] && ocupados.hora < horaMen && profesores.includes(ocupados.profesor));
                    ocupadosGTemp = ocupadosTemp.findIndex((ocupadosT) => ocupadosT.dia == diaCons && ocupadosT.hora >= lecR[lBefore] && ocupadosT.hora < horaMen && lecGroups.includes(ocupadosT.grupo));
                    ocupadosPTemp = ocupadosTemp.findIndex((ocupadosT) => ocupadosT.dia == diaCons && ocupadosT.hora >= lecR[lBefore] && ocupadosT.hora < horaMen && profesores.includes(ocupadosT.profesor));
                }
                if (lecR[lBefore] > 0 && lecR[lBefore] < horaMen && tlBefore == -1 && ocupadosP == -1 && ocupadosG == -1 && ocupadosPTemp == -1 && ocupadosGTemp == -1) { //tlBefore era... != -1
                    // console.log(ocupados.slice(),lecR[lBefore], horaMen, tlBefore, ocupadosG, ocupadosP)
                    d = dayR.indexOf(cons.dia);
                    l = lBefore;
                    // console.log("before: dia - " + dayR[d] + "+  hora - " + lecR[l])
                    mayConsecutive = true;
                    verificado = true;
                    break labelVerificar;
                } else {
                    let tlAfter = arrTl.findIndex((arrTl) => arrTl.dia == diaCons && arrTl.hora >= lecR[lAfter] && arrTl.hora <= (lecR[lAfter] + duracionF - 1) && arrTl.d == false);
                    let ocupadosGG;
                    let ocupadosPP;
                    let ocupadosGGTemp;
                    let ocupadosPPTemp;
                    if (tlAfter == -1) {
                        ocupadosGG = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lAfter] && ocupados.hora <= (lecR[lAfter] + duracionF - 1) && lecGroups.includes(ocupados.grupo));
                        ocupadosPP = ocupados.findIndex((ocupados) => ocupados.dia == diaCons && ocupados.hora >= lecR[lAfter] && ocupados.hora <= (lecR[lAfter] + duracionF - 1) && profesores.includes(ocupados.profesor));
                        ocupadosGGTemp = ocupadosTemp.findIndex((ocupadosT) => ocupadosT.dia == diaCons && ocupadosT.hora >= lecR[lAfter] && ocupadosT.hora <= (lecR[lAfter] + duracionF - 1) && lecGroups.includes(ocupadosT.grupo));
                        ocupadosPPTemp = ocupadosTemp.findIndex((ocupadosT) => ocupadosT.dia == diaCons && ocupadosT.hora >= lecR[lAfter] && ocupadosT.hora <= (lecR[lAfter] + duracionF - 1) && profesores.includes(ocupadosT.profesor));
                    }
                    //console.log(tlAfter,ocupadosGG,ocupadosPP,ocupadosGGTemp,ocupadosPPTemp)
                    if (lecR[lAfter] + parseInt(fichaF.duracion) - 1 <= parseInt(horario.lecciones) && tlAfter == -1 && ocupadosPP == -1 && ocupadosGG == -1 && ocupadosPPTemp == -1 && ocupadosGGTemp == -1) {
                        d = dayR.indexOf(cons.dia);
                        l = lAfter;
                        mayConsecutive = true;
                        // console.log("after: dia - " + dayR[d] + "+  hora - " + lecR[l])
                        verificado = true;
                        break labelVerificar;
                    } else {
                        mayConsecutive = false;
                        verificado = false;
                        //break;
                    }
                }
            }
        } else {
            mayConsecutive = true;
            verificado = true;
            //console.log("Primera entrada en el dia" + mayConsecutive)
        }
    }
    return [verificado, mayConsecutive, d, l];
}

function fichasRestriction(fichasDB) {
    let fichasToEvaluate = new Object();
    let idsLessons = new Array();
    for (const ficha of fichasDB) {
        if (!idsLessons.includes(ficha.id_leccion)) {
            idsLessons.push(ficha.id_leccion);
        }

        if (fichasToEvaluate.hasOwnProperty(ficha.id_leccion)) {
            if (fichasToEvaluate[ficha.id_leccion].hasOwnProperty(ficha.id_dia)) {
                fichasToEvaluate[ficha.id_leccion][ficha.id_dia].push(ficha.id_hora);
            } else {
                fichasToEvaluate[ficha.id_leccion][ficha.id_dia] = [ficha.id_hora];
            }
        } else {
            fichasToEvaluate[ficha.id_leccion] = { [ficha.id_dia]: [ficha.id_hora] };
        }
    }
    return [fichasToEvaluate, idsLessons];
}

function throughMatriz({ dayBlock = false, blockCourse = false, matriz, daysRand, hoursRand, ocupados, fichas, relaciones }) {
    for (const idCurso in matriz) {
        let currentItem = matriz[idCurso];
        if (currentItem.id_curso == blockCourse) continue;
        let flag = false;
        for (const day of daysRand) {
            let dayUsed = dayBlock != false ? dayBlock : day;
            if (flag) break;
            for (const hour of hoursRand) {
                let hourUsed = hour;
                let free = currentItem.tl;
                let freeTimeItem;
                for (let i = 0; i < currentItem.leccion; i++) {
                    freeTimeItem = free.findIndex(
                        (free) => free.dia == dayUsed && free.hora == hourUsed && free.d
                    );
                    hourUsed++;
                    if (freeTimeItem == -1) break;
                }
                if (freeTimeItem != -1) {
                    let ocupadosTeachers,
                        ocupadosGroups,
                        flagTimes = false;
                    for (const teacher of currentItem.profesores) {
                        if (flagTimes) break;
                        hourUsed = hour;
                        for (let i = 0; i < currentItem.leccion; i++) {
                            ocupadosTeachers = ocupados.findIndex(
                                (ocupados) =>
                                    ocupados.dia == dayUsed &&
                                    ocupados.hora == hourUsed &&
                                    ocupados.profesor == teacher.id_profesor
                            );
                            hourUsed++;
                            if (ocupadosTeachers != -1) {
                                flagTimes = true;
                                break;
                            }
                        }
                    }
                    flagTimes = false;
                    for (const group of currentItem.grupos) {
                        if (flagTimes) break;
                        hourUsed = hour;
                        for (let i = 0; i < currentItem.leccion; i++) {
                            ocupadosGroups = ocupados.findIndex(
                                (ocupados) =>
                                    ocupados.dia == dayUsed &&
                                    ocupados.hora == hourUsed &&
                                    ocupados.grupo == group
                            );
                            hourUsed++;
                            if (ocupadosGroups != -1) {
                                flagTimes = true;
                                break;
                            }
                        }
                    }
                    if (ocupadosTeachers == -1 && ocupadosGroups == -1) {
                        hourUsed = hour;
                        let firstHour = hour;
                        for (let i = 0; i < currentItem.leccion; i++) {
                            let hInicioFicha = parseInt(firstHour);
                            let hFinalFicha = parseInt(firstHour) + parseInt(currentItem.fichas[i].duracion) - 1;
                            for (const profesor of currentItem.profesores) {
                                for (const grupo of currentItem.grupos) {
                                    data = {
                                        dia: dayUsed,
                                        hora: hourUsed,
                                        hora_inicio: hInicioFicha,
                                        hora_final: hFinalFicha,
                                        duracion: parseInt(currentItem.fichas[i].duracion),
                                        ficha: currentItem.fichas[i].ficha,
                                        leccion: currentItem.id,
                                        curso: currentItem.id_curso,
                                        profesor: profesor.id_profesor,
                                        grupo: grupo,
                                        id_relacion: relaciones.id,
                                    };
                                    ocupados.push(data);
                                }
                            }
                            let fichaToEvaluate = fichas.findIndex(
                                (fichas) => fichas === currentItem.fichas[i]
                            );
                            fichas[fichaToEvaluate].dia = dayUsed;
                            fichas[fichaToEvaluate].hora = hourUsed;
                            dayBlock = dayUsed;
                            hourUsed++;
                        }
                        flag = true;
                        break;
                    }
                }
            }
        }
    }
}

function forceCloseModal() {
    let containerLoader = document.querySelector(".containerLoader");
    containerLoader.style.display = "none";
}