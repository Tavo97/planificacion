window.addEventListener('load', (e) => {
    let langText = globalVariable.langText;
    let msGObj = globalVariable.msGObj;

    LFCDOTCHECKDcls.addEventListener('change', (e) => {
        LFCDOTCHECKDcls.checked = true;
        LFCNDOTCHECKDcls.checked = false;
    });

    LFCNDOTCHECKDcls.addEventListener('change', (e) => {
        LFCNDOTCHECKDcls.checked = true;
        LFCDOTCHECKDcls.checked = false;
    });

    configuracionRestriccionesClases.addEventListener('click', (e) => {
        frmDcls.reset();
        let groups = document.querySelectorAll(`[data-group]`);
        LFCNDOTCHECKDcls.checked = true;
        groups.forEach(element => {
            element.addEventListener('change', (e) => {
                checkGroup(e.target);
            });
        });
        $('#ModalDcls').modal({
            backdrop: 'static',
            keyboard: false
        });
        peticionFrmDcls.value = peticionFrmRestriccionesClases.value;
        let op = parseInt(peticionFrmDcls.value);
        if (op == 3) {
            table = {
                name: 'blm_distribucion_asignatura',
                fields: ['*'],
                conditions: { 'id_relacion': ['=', idFrmRestriccionesClases.value] }
            };
            searchItemsDB(table).then((resp) => {
                if (Object.keys(resp).length > 0) {
                    for (const item in resp) {
                        let DCCHEditar; // donde almacena la variable de 0o1
                        let elemento = resp[item].dias_consecutivos;
                        let lecCon = resp[item].lecciones_consecutivos;
                        let distDL = resp[item].distribucion_dias_lecciones;
                        if (elemento == 0) {
                            LFCDOTCHECKDcls.checked = true;
                            LFCNDOTCHECKDcls.checked = false;
                            DCCHEditar = elemento;
                        }
                        if (elemento == 1) {
                            LFCDOTCHECKDcls.checked = false;
                            LFCNDOTCHECKDcls.checked = true;
                            DCCHEditar = elemento;
                        }
                        if (lecCon == 1) {
                            SDCCHECKDcls.checked = true;
                        }
                        if (lecCon == 0) {
                            SDCCHECKDcls.checked = false;
                        }
                        if (distDL == 1) {
                            ENDYNLPDFCHECKDcls.checked = true;
                        }
                        if (distDL == 0) {
                            ENDYNLPDFCHECKDcls.checked = false;
                        }
                        /* SDCCHECKDcls.checked = resp[item].lecciones_consecutivos;
                        ENDYNLPDFCHECKDcls.checked = resp[item].distribucion_dias_lecciones; */
                        DesdeH1Dcls.value = resp[item].ficha_desde;
                        HastaH1Dcls.value = resp[item].ficha_hasta;
                        DesdeH2Dcls.value = resp[item].leccion_desde;
                        HastaH2Dcls.value = resp[item].leccion_hasta;
                        idFrmDcls.value = resp[item].id;
                        if (ENDYNLPDFCHECKDcls.checked == true) {
                            DesdeH1Dcls.disabled = false;
                            HastaH1Dcls.disabled = false;
                            DesdeH2Dcls.disabled = false;
                            HastaH2Dcls.disabled = false;
                        } else {
                            DesdeH1Dcls.disabled = true;
                            HastaH1Dcls.disabled = true;
                            DesdeH2Dcls.disabled = true;
                            HastaH2Dcls.disabled = true;
                        }
                        camposFrmDcls.value = JSON.stringify({
                            'id': 'idFrmDcls',
                            'dias_consecutivos': 'DCCHEditar',
                            'lecciones_consecutivos': 'SDCCHECKDcls',
                            'distribucion_dias_lecciones': ' ENDYNLPDFCHECKDcls',
                            'ficha_desde': 'DesdeH1Dcls',
                            'ficha_hasta': 'HastaH1Dcls',
                            'leccion_desde': 'DesdeH2Dcls',
                            'leccion_hasta': 'HastaH2Dcls',
                            'id_relacion': 'idFrmRestriccionesClases'
                        });
                    }
                } else { }
            }).catch((e) => {
                peticionFrmDcls.value = 2;
            });
        }
        if (ENDYNLPDFCHECKDcls.checked == true) {
            DesdeH1Dcls.disabled = false;
            HastaH1Dcls.disabled = false;
            DesdeH2Dcls.disabled = false;
            HastaH2Dcls.disabled = false;
        } else {
            DesdeH1Dcls.disabled = true;
            HastaH1Dcls.disabled = true;
            DesdeH2Dcls.disabled = true;
            HastaH2Dcls.disabled = true;
        }
    });

    labelBtnModalDclsAgregar.addEventListener('click', (e) => {
        /*  peticionFrmDcls.value = peticionFrmRestriccionesClases.value; */
        /*  let op = parseInt(peticionFrmDcls.value); */
        let DiasConsecutivosGR = document.querySelectorAll('[data-group="diasConsecutivosDcls"]');
        let DCCH;
        DiasConsecutivosGR.forEach(element => {
            if (element.checked == 1) {
                DCCH = element.value;
            }
        });
        if (peticionFrmRestriccionesClases.value == 2 && peticionFrmDcls.value == 2) {
            if (idFrmRestriccionesClases.value == "") {
                idItemHorarioRestriccionesClases.value = titleModulePlaneacion.dataset.id;
                FichasDivididasFrmRestriccionesClases.value = DCCH;
                SHMDCFrmRestriccionesClases.value = SDCCHECKDcls.checked == true ? 1 : 0;
                DLeccionesRestriccionesClases.value = ENDYNLPDFCHECKDcls.checked == true ? 1 : 0;
                if (DLeccionesRestriccionesClases.value == 1) {
                    dfndd1FrmRestriccionesClases.value = DesdeH1Dcls.value;
                    dfndh1FrmRestriccionesClases.value = HastaH1Dcls.value;
                    nldid2FrmRestriccionesClases.value = DesdeH2Dcls.value;
                    nldih2FrmRestriccionesClases.value = HastaH2Dcls.value;
                } else {
                    dfndd1FrmRestriccionesClases.value = 0;
                    dfndh1FrmRestriccionesClases.value = 0;
                    nldid2FrmRestriccionesClases.value = 0;
                    nldih2FrmRestriccionesClases.value = 0;
                }
                msGObj.loader.style.display = 'block';
                msGObj.labelLoader.style.color = '#28a745';
                msGObj.labelLoader.innerHTML = langText.msg_saved;
                console.log("2dentro")
            }
            console.log("2fuera");
        }
        if (peticionFrmRestriccionesClases.value == 3 && peticionFrmDcls.value == 3) {
            console.log("3dentro")
            let DiasConsecutivosEditado = document.querySelectorAll('[data-group="diasConsecutivosDcls"]');
            let DCCHEditado;
            let mismoDia;
            let distribuirF;
            let desdeFichas, hastaFichas, desdeIntervalo, hastaIntervalo;
            DiasConsecutivosEditado.forEach(element => {
                if (element.checked == 1) {
                    DCCHEditado = element.value;
                }
                console.log(element.checked);
                console.log(DCCHEditado);
            });
            if (SDCCHECKDcls.checked == true) {
                mismoDia = SDCCHECKDcls.value
            } else {
                mismoDia = 0;
            }
            if (ENDYNLPDFCHECKDcls.checked == false) {
                distribuirF = 0;
                desdeFichas = 0;
                hastaFichas = 0;
                desdeIntervalo = 0;
                hastaIntervalo = 0;
            } else {
                distribuirF = ENDYNLPDFCHECKDcls.value
                desdeFichas = DesdeH1Dcls.value;
                hastaFichas = HastaH1Dcls.value;
                desdeIntervalo = DesdeH2Dcls.value;
                hastaIntervalo = HastaH2Dcls.value;
            }
            let formData = new FormData();
            formData.append('idFrmDcls', idFrmDcls.value);
            formData.append('DCCHEditar', DCCHEditado);
            formData.append('SDCCHECKDcls', mismoDia);
            formData.append('ENDYNLPDFCHECKDcls', distribuirF);
            formData.append('DesdeH1Dcls', desdeFichas);
            formData.append('HastaH1Dcls', hastaFichas);
            formData.append('DesdeH2Dcls', desdeIntervalo);
            formData.append('HastaH2Dcls', hastaIntervalo);
            formData.append('idFrmRestriccionesClases', idFrmRestriccionesClases.value);
            saveItemsDB(langText, 3, formData, false, {
                name: 'blm_distribucion_asignatura',
                campos: {
                    'id': 'idFrmDcls',
                    'dias_consecutivos': 'DCCHEditar',
                    'lecciones_consecutivos': 'SDCCHECKDcls',
                    'distribucion_dias_lecciones': 'ENDYNLPDFCHECKDcls',
                    'ficha_desde': 'DesdeH1Dcls',
                    'ficha_hasta': 'HastaH1Dcls',
                    'leccion_desde': 'DesdeH2Dcls',
                    'leccion_hasta': 'HastaH2Dcls',
                    'id_relacion': 'idFrmRestriccionesClases'
                }
            }).then((res) => {
                msGObj.loader.style.display = 'block';
                msGObj.labelLoader.style.color = '#28a745';
                msGObj.labelLoader.innerHTML = langText.msg_saved;
            }).catch((e) => {
                console.log(e)
                msGObj.loader.style.display = 'block';
                msGObj.labelLoader.innerHTML = langText.msg_no_save;
                msGObj.labelLoader.style.color = 'tomato';
            });
        }
        if (peticionFrmRestriccionesClases.value == 3 && peticionFrmDcls.value == 2) {
            console.log("ultimo caso")
            try {
                let data = new Object();
                data.target = '../blocks/planificacion/peticionesSql.php';
                data.method = 'POST';
                data.send = true;
                data.form = [true, frmDcls];
                let flag = true;
                let frdata = new FormData();
                let lfc = 0;
                let sdc = 0;
                let edc = 0;
                if (LFCDOTCHECKDcls.checked == true) {
                    lfc = LFCDOTCHECKDcls.value;
                } else {
                    lfc = LFCNDOTCHECKDcls.value;
                }
                if (SDCCHECKDcls.checked == true) {
                    sdc = SDCCHECKDcls.value
                }
                if (ENDYNLPDFCHECKDcls.checked == true) {
                    edc = ENDYNLPDFCHECKDcls.value
                }
                frdata.append('tabla', 'blm_distribucion_asignatura');
                frdata.append('peticion', 2);
                frdata.append('campos', JSON.stringify({
                    'dias_consecutivos': 'LFCDOTCHECKDcls',
                    'lecciones_consecutivos': 'SDCCHECKDcls',
                    'distribucion_dias_lecciones': 'ENDYNLPDFCHECKDcls',
                    'ficha_desde': 'DesdeH1Dcls',
                    'ficha_hasta': 'HastaH1Dcls',
                    'leccion_desde': 'DesdeH2Dcls',
                    'leccion_hasta': 'HastaH2Dcls',
                    'id_relacion': 'idFrmRestriccionesClases'
                }));
                frdata.append('LFCDOTCHECKDcls', lfc);
                frdata.append('SDCCHECKDcls', sdc);
                frdata.append('ENDYNLPDFCHECKDcls', edc);
                frdata.append('DesdeH1Dcls', DesdeH1Dcls.value);
                frdata.append('HastaH1Dcls', HastaH1Dcls.value);
                frdata.append('DesdeH2Dcls', DesdeH2Dcls.value);
                frdata.append('HastaH2Dcls', HastaH2Dcls.value);
                frdata.append('idFrmRestriccionesClases', idFrmRestriccionesClases.value);
                data.form = [false, frdata];
                ajaxData(data).then((resp) => {
                    msGObj.labelLoader.style.color = '#28a745';
                    if (resp[0] != false) {
                        msGObj.labelLoader.innerHTML = langText.msg_relacion_fichas_divididas;
                    } else {
                        flag = false;
                    }
                });
                console.log(frdata)
            } catch {
                console.log("Error")
            }
        }
        closeModal(JSON.stringify({ status: 1, close: ['ModalDcls'] }));
        closeWorkLand(msGObj);
    });

    labelBtnModalDclsCancelar.addEventListener('click', (e) => {
        let langText = globalVariable.langText;
        data = {
            'dataSets': {
                action: 'Info',
                modal: JSON.stringify({
                    status: 1,
                    close: ['ModalMsg', 'ModalDcls']
                })
            },
            'msg': langText.msg_only_alert,
            'ico': 'info',
            'closeDataSet': {
                visible: 1,
                modal: JSON.stringify({
                    status: 1,
                    close: ['ModalMsg']
                })
            }
        };
        changeMsgModal(data);
        $('#ModalMsg').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    ENDYNLPDFCHECKDcls.addEventListener('click', (e) => {
        if (ENDYNLPDFCHECKDcls.checked == true) {
            DesdeH1Dcls.disabled = false;
            HastaH1Dcls.disabled = false;
            DesdeH2Dcls.disabled = false;
            HastaH2Dcls.disabled = false;
        } else {
            DesdeH1Dcls.disabled = true;
            HastaH1Dcls.disabled = true;
            DesdeH2Dcls.disabled = true;
            HastaH2Dcls.disabled = true;
        }
    });
});